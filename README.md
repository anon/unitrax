# UniTraX

UniTraX is a data analytics system that provides users with a personal
differentially private bound on privacy loss. UniTraX is able to allow more
queries than previous systems, without giving up on analytic accuracy.

For further information, please, read our paper
"UniTraX: Protecting Data Privacy with Discoverable Biases" at
https://doi.org/10.1007/978-3-319-89722-6_12
or the extended technical report (additionally includes the proofs, otherwise
identical) at https://www.mpi-sws.org/tr/2018-001.pdf

This repository contains the research prototype implementation of UniTraX. It is
an extended version beyond the one used for the paper.

See the separate https://gitlab.mpi-sws.org/anon/unitrax-data repository for the
data we used in our experiments.

## Installation

The following reflects the setup we used for our experiments.

Machine 1: The remote DB server
* Windows Server 2016
* MS SQL Server

Machine 2: UniTraX server/client machine
* Windows Server 2016
* Visual Studio (on Machine 2; this is the UniTraX server and client machine)
* C#
* .Net framework 4.7.2
* Git
* PINQ from https://www.microsoft.com/en-us/research/project/privacy-integrated-queries-pinq/
* Financial/Medical data from https://sorry.vse.cz/~berka/challenge/pkdd1999/chall.htm
* Mobility data from https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page
(we used an older version of January 2013 yellow cab data)

One must apply the patch from the source folder to PINQueryable.cs.

Visual studio should tell which NuGet packages must be installed.

Copies of the original as well as the transformed data files for our experiments
are located in the separate https://gitlab.mpi-sws.org/anon/unitrax-data
repository.

Otherwise, the record transformer changes the data to the format required by
UniTraX. It is implemented in Java and was only ever used on OSX.

We prepared the mobility data manually. Thus, there is no record transformer for
that dataset in the repository. Please have a look at the mobility dataset's
data access files to infer a working schema and produce corresponding csv files.

This repository contains a cleaned copy of the originally used code, without the
PINQ component (as we are not sure about their licensing). We renamed and moved
things around in the process.

Please keep in mind, this repository was never used in its current state. We
did not test all its content. One experiment compiled on a Windows 10 Pro
virtual machine after our cleaning, but we probably missed dependency and naming
issues with the other experiments. If something looks wrong, it probably is.

Please let us know if you find anything. We will try to fix it.

## Configuration

There are no configuration files. Veriables are hard-coded in each experiment's
main file. Accordingly, each experiment compiles into its own executable.

The only file that must be adapted is the DB server description file in the
hosts folder of the provisioning part of the infrastructure library.
Please provide your own file instead of ExampleDBServer.cs.

The Windows firewall on Machine 1 (the DB server) must be configured to allow
remote connections, including access to the management interface as well as
power shell.

We especially remember the following two problems we had with Windows on the DB
server machine:

1. Only the built-in Administrator account had all the permissions for our
commands, a manually added administrator account did not work
2. Turning off the firewall prevented management connections, the firewall had
to be turned on and the correct rules be active (we simply activated all
possible rule templates)

## Operation

In our setup Machine 1 has nothing but MS SQL Server installed and works as the
remote database server.

Machine 2 does everything else. This includes all experiment setup / teardown.

Experiments roughly follow the following outline:

1. Get started on Machine 2
2. Connect to Machine 1
   * Drop any databases
   * Stop MS SQL service
   * Remove RAM-disk
   * Create RAM-disk
   * Start MS SQL service
2. Load data from disk of Machine 2 into RAM of Machine 2
3. Connect to Machine 1
   * Create DB (with files on RAM-disk)
   * Create tables
   * Load data into tables (add budget information on the fly)
   * Create indexes
   * Make DB read-only
4. Setup one of Direct (no protection), PINQ, or UniTraX (utilizing PINQ)
5. Execute queries
6. Dump log
7. Query for remaining privacy budgets (this can take significant time)
8. Dump budget data
9. Repeat for different parameters

We used the log converter to read the logs and plot graphs with gnuplot. Again,
this utility is implemented in Java and was only ever used on OSX.

## Contact

Please feel free to contact me at munz@mpi-sws.org

However, I might be busy with other things and not be able to help you.
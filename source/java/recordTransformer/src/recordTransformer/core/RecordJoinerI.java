package recordTransformer.core;

public interface RecordJoinerI {
	
	public Record join(Record recordA, Record[] recordsB);
}
package recordTransformer.core;

import java.text.ParseException;
import java.util.HashMap;

public class RecordFieldParserEnumerableDouble extends RecordFieldParserEnumerable {

	private double low;
	private double high;
	
	private int count = 0;
	private int numberFormatException = 0;
	private int outOfBoundsException = 0;
	
	public RecordFieldParserEnumerableDouble(double low, double high, HashMap<Double, String[]> dictionary) {
		super(dictionary);
		this.low = low;
		this.high = high;
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		count++;
		Double value = get(field);
		if (value == null) {
			try {
				value = Double.parseDouble(field);
			} catch (NumberFormatException e) {
				numberFormatException++;
				throw new ParseException("Caught NumberFormatException: " + e.getMessage(), 0);
			}
			if (value < low || high < value) {
				outOfBoundsException++;
				throw new ParseException("value " + value + " outside allowed bounds (" + low + ", " + high + ")", 0);
			}
		}
		recordBuilder.add(value);
	}
	
	@Override
	public String report() {
		return "RecordFieldParserEnumerableDouble: " + (numberFormatException + outOfBoundsException) + " fails out of " + count + " (nfe " + numberFormatException + ", oobe " + outOfBoundsException + ") -- " + super.report();
	}
}
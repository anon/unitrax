package recordTransformer.core;

import java.text.ParseException;
import java.util.HashMap;

public class RecordFieldParserDummy implements RecordFieldParserI {

	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		recordBuilder.add(field);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		return new HashMap[] {};
	}
	
	@Override
	public String report() {
		return "RecordFieldParserDummy: no report data available";
	}
}
package recordTransformer.core;

import java.text.ParseException;
import java.util.HashMap;

public interface RecordFieldParserI {
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException;
	public HashMap<Double, String[]>[] getDictionaries();
	public String report();
}
package recordTransformer.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class Table {
	
	public static Table join(String label, String[] columnLabels, int idIndex, int numDoubleColumns, Table tableA, Table tableB,
			int matchColumnIndexA, int matchColumnIndexB, RecordJoinerI recordJoiner, DictionaryJoinerI dictionaryJoiner) {
		if (label == null) throw new NullPointerException("label is null");
		if (tableA == null) throw new NullPointerException("tableA is null");
		if (tableB == null) throw new NullPointerException("tableB is null");
		if (columnLabels == null) throw new NullPointerException("columnLabels is null");
		if (recordJoiner == null) throw new NullPointerException("recordJoiner is null");
		if (dictionaryJoiner == null) throw new NullPointerException("dictionaryJoiner is null");
		TableBuilder tableBuilder = new TableBuilder(label, columnLabels, idIndex, numDoubleColumns);
		HashSet<Integer> matchedRecordIndexesB = new HashSet<>();
		for (int i = 0; i < tableA.numRecords(); i++) {
			LinkedList<Integer> matchRecordIndexesB = new LinkedList<>();
			for (int j = 0; j < tableB.numRecords(); j++) {
				if (tableA.doubleAt(i, matchColumnIndexA) == tableB.doubleAt(j, matchColumnIndexB)) {
					matchRecordIndexesB.add(j);
					matchedRecordIndexesB.add(j);
				}
			}
			if (matchRecordIndexesB.isEmpty()) tableBuilder.addRecord(recordJoiner.join(tableA.recordAt(i), null));
			else {
				Record[] matches = new Record[matchRecordIndexesB.size()];
				for (int j = 0; j < matchRecordIndexesB.size(); j++) matches[j] = tableB.recordAt(matchRecordIndexesB.get(j));
				tableBuilder.addRecord(recordJoiner.join(tableA.recordAt(i), matches));
			}
		}
		for (int i = 0; i < tableB.numRecords(); i++) {
			if (matchedRecordIndexesB.contains(i)) continue;
			tableBuilder.addRecord(recordJoiner.join(null, new Record[] { tableB.recordAt(i) }));
		}
		dictionaryJoiner.joinDictionaries(tableA, tableB, tableBuilder);
		return tableBuilder.toTable();
	}
	
	public static Table readFrom(BufferedReader reader, String delimiter, String label, String[] columnLabels, RecordFieldParserI[] fieldParsers, RecordFinalizerI recordFinalizer, int idIndex, int numDoubleColumns, LinePreparerI linePreparer) throws IOException {
		if (reader == null) throw new NullPointerException("reader is null");
		if (delimiter == null) throw new NullPointerException("delimiter is null");
		if (label == null) throw new NullPointerException("label is null");
		if (columnLabels == null) throw new NullPointerException("columnLabels is null");
		if (fieldParsers == null) throw new NullPointerException("fieldParsers is null");
		if (linePreparer == null) throw new NullPointerException("linePreparer is null");
		TableBuilder tableBuilder = new TableBuilder(label, columnLabels, idIndex, numDoubleColumns);
		LinkedList<String> unparseableLines = new LinkedList<>();
		int linesParsed = 0;
		for (String line = reader.readLine(); line != null; line = reader.readLine()) {
			line = linePreparer.prepare(line);
			Record record = Record.parseRecord(line, delimiter, fieldParsers, recordFinalizer);
			if (record != null) {
				tableBuilder.addRecord(record);
				linesParsed++;
			}
			else unparseableLines.add(line);
		}
		for (RecordFieldParserI fieldParser : fieldParsers) {
			if (fieldParser.getDictionaries() == null) throw new NullPointerException("got null array");
			else for (HashMap<Double, String[]> dictionary : fieldParser.getDictionaries()) tableBuilder.addDictionary(dictionary);
		}
		if (recordFinalizer != null) {
			if (recordFinalizer.getDictionaries() == null) throw new NullPointerException("got null array");
			else for (HashMap<Double, String[]> dictionary : recordFinalizer.getDictionaries()) tableBuilder.addDictionary(dictionary);
		}
		System.out.flush();
		System.out.print("Table: Successfully parsed " + linesParsed + " of " + (linesParsed + unparseableLines.size()) + " lines.");
		if (unparseableLines.size() > 0) System.out.println(" The following " + unparseableLines.size() + " lines failed to parse:");
		else System.out.println();
		for (String line : unparseableLines) System.out.println(line);
		int index = 0;
		for (RecordFieldParserI fieldParser : fieldParsers) { 
			System.out.println(columnLabels[index++]);
			System.out.println(fieldParser.report());
		}
		return tableBuilder.toTable();
	}
	
	public static Table readFrom(String filename, String delimiter, String label, String[] columnLabels, RecordFieldParserI[] fieldParsers, RecordFinalizerI recordFinalizer, int idIndex, int numDoubleColumns, LinePreparerI linePreparer) {
		if (filename == null) throw new NullPointerException("filename is null");
		if (delimiter == null) throw new NullPointerException("delimiter is null");
		if (label == null) throw new NullPointerException("label is null");
		if (columnLabels == null) throw new NullPointerException("columnLabels is null");
		if (fieldParsers == null) throw new NullPointerException("fieldParsers is null");
		if (linePreparer == null) throw new NullPointerException("linePreparer is null");
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			br.readLine();
			return Table.readFrom(br, delimiter, label, columnLabels, fieldParsers, recordFinalizer, idIndex, numDoubleColumns, linePreparer);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("files should be in the provided path");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("files should have content");
		}
	}

	private String label;
	private String[] columnLabels;
	private Record[] records;
	private HashMap<Double, String[]>[] dictionaries;
	
	public Table(String label, String[] columnLabels, Record[] records, HashMap<Double, String[]>[] dictionaries) {
		if (label == null) throw new NullPointerException("label is null");
		if (columnLabels == null) throw new NullPointerException("columnLabels is null");
		if (records == null) throw new NullPointerException("records is null");
		if (dictionaries == null) throw new NullPointerException("dictionaries is null");
		if (records.length <= 0) throw new IllegalArgumentException("records is empty");
		if (columnLabels.length != records[0].numDoubles() + records[0].numStrings()) throw new IllegalArgumentException("not the required amount of columnlabels");
		if (dictionaries.length != records[0].numDoubles()) throw new IllegalArgumentException("not the required amount of dictionaries");
		for (Record record : records) if (record.numDoubles() != records[0].numDoubles() || record.numStrings() != records[0].numStrings()) throw new IllegalArgumentException("records have different number of fields");
		this.label = label;
		this.columnLabels = columnLabels;
		this.records = records;
		this.dictionaries = dictionaries;
	}
	
	public String getLabel() {
		return label;
	}
	
	public int numColumns() {
		return columnLabels.length;
	}
	
	public int numRecords() {
		return records.length;
	}
	
	public String columnLabelAt(int index) {
		if (index < 0 || index > columnLabels.length - 1) throw new IndexOutOfBoundsException();
		return columnLabels[index];
	}
	
	public double doubleAt(int recordIndex, int doubleColumnIndex) {
		if (recordIndex < 0 || recordIndex > records.length - 1) throw new IndexOutOfBoundsException();
		if (doubleColumnIndex < 0 || doubleColumnIndex > columnLabels.length - 1 || doubleColumnIndex > records[0].numDoubles() - 1) throw new IndexOutOfBoundsException();
		return records[recordIndex].doubleAt(doubleColumnIndex);
	}
	
	public String stringAt(int recordIndex, int stringColumnIndex) {
		if (recordIndex < 0 || recordIndex > records.length - 1) throw new IndexOutOfBoundsException();
		if (stringColumnIndex < 0 || stringColumnIndex > columnLabels.length - 1 || stringColumnIndex > records[0].numStrings() - 1) throw new IndexOutOfBoundsException();
		return records[recordIndex].stringAt(stringColumnIndex);
	}
	
	public Record recordAt(int index) {
		if (index < 0 || index > records.length - 1) throw new IndexOutOfBoundsException();
		return records[index];
	}
	
	public HashMap<Double, String[]> dictionaryAt(int index) {
		if (index < 0 || index > columnLabels.length - 1) throw new IndexOutOfBoundsException();
		if (dictionaries[index] == null) return null;
		return new HashMap<Double, String[]>(dictionaries[index]);
	}
		
	public void writeRecordsTo(String filename, String delimiter) throws IOException {
		if (filename == null) throw new NullPointerException("filename is null");
		if (delimiter == null) throw new NullPointerException("delimiter is null");
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
		for (int i = 0; i < columnLabels.length; i++) {
			pw.print(columnLabels[i]);
			if (i < columnLabels.length - 1) pw.print(delimiter);
		}
		pw.println();
		DecimalFormat format = new DecimalFormat("0.0");
		for (Record record : records) {
			for (int i = 0; i < record.numDoubles(); i++) {
				pw.print(format.format(record.doubleAt(i)));
				if (i < record.numDoubles() + record.numStrings() - 1) pw.print(delimiter);
			}
			for (int i = 0; i < record.numStrings(); i++) {
				pw.print("\"" + record.stringAt(i) + "\"");
				if (i < record.numStrings() - 1) pw.print(delimiter);
			}
			pw.println();
		}
		pw.close();
	}
	
	public void writeDictionariesTo(String filename, String delimiter) throws IOException {
		if (filename == null) throw new NullPointerException("filename is null");
		if (delimiter == null) throw new NullPointerException("delimiter is null");
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
		for (int i = 0; i < records[0].numDoubles(); i++) {
			pw.println(columnLabels[i]);
			if (dictionaries[i] == null) continue;
			LinkedList<Double> labels = new LinkedList<>(dictionaries[i].keySet());
			Collections.sort(labels);
			for (Double label : labels) pw.println(label + delimiter + Arrays.toString(dictionaries[i].get(label)));
		}		
		pw.close();
	}
	
	public void writeTo(String path) throws IOException {
		if (path == null) throw new NullPointerException("path is null");
		writeRecordsTo(path + File.separator + getLabel() + "_records.csv", ";");
		writeDictionariesTo(path + File.separator + getLabel() + "_dictionaries.csv", ";");
	}
}
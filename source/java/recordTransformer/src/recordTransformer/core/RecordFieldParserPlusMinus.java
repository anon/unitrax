package recordTransformer.core;

import java.text.ParseException;
import java.util.HashMap;

public class RecordFieldParserPlusMinus extends RecordFieldParserEnumerable {
	
	private static HashMap<Double, String[]> plusMinusMap() {
		HashMap<Double, String[]> plusMinusMap = new HashMap<>();
		plusMinusMap.put(0.0, new String[] { "" });
		plusMinusMap.put(1.0, new String[] { "-" });
		plusMinusMap.put(2.0, new String[] { "+-" });
		plusMinusMap.put(3.0, new String[] { "+" });
		plusMinusMap.put(4.0, new String[] { "2+" });
		return plusMinusMap;
	}
	
	public RecordFieldParserPlusMinus() {
		super(plusMinusMap());
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		Double value = get(field);
		if (value == null) {
			value = get(field.substring(0, 1));
			if (value == null) throw new ParseException("Cannot parse plus/minus from " + field, 0);
		}
		recordBuilder.add(value);
	}
}
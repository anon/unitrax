package recordTransformer.core;

import java.text.ParseException;

public class Record {

	public static Record parseRecord(String s, String delimiter, RecordFieldParserI[] fieldParsers, RecordFinalizerI recordFinalizer) {
		if (s == null) throw new NullPointerException("s is null");
		if (delimiter == null) throw new NullPointerException("delimiter is null");
		if (fieldParsers == null) throw new NullPointerException("fieldParsers is null");
		String[] words = s.split(delimiter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
		if (words.length != fieldParsers.length) throw new IllegalArgumentException("not the correct number of field parsers, required " + words.length + ", was " + fieldParsers.length + " for input " + s);
		RecordBuilder recordBuilder = new RecordBuilder();
		for (int i = 0; i < words.length; i++) {
			try {
				fieldParsers[i].parse(words[i], recordBuilder);
			} catch (ParseException e) {
				System.out.println("FAILED to parse! FieldParser #" + i + " failed to parse word " + words[i] + " of record " + s);
				return null;
			}
		}
		if (recordFinalizer != null) recordFinalizer.finalize(recordBuilder);
		return recordBuilder.toRecord();
	}
	
	private double[] doubles;
	private String[] strings;
	
	public Record(double[] doubles, String[] strings) {
		if (doubles == null) throw new NullPointerException("doubles is null");
		if (strings == null) throw new NullPointerException("strings is null");
		this.doubles = doubles;
		this.strings = strings;
	};
	
	public double doubleAt(int index) {
		return doubles[index];
	}
	
	public String stringAt(int index) {
		return strings[index];
	}
	
	public int numDoubles() {
		return doubles.length;
	}
	
	public int numStrings() {
		return strings.length;
	}
	
	@Override
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("Record: ");
		for (int i = 0; i < doubles.length; i++) b.append(doubleAt(i) + ",");
		for (int i = 0; i < strings.length; i++) b.append(stringAt(i) + ",");
		return b.toString();
	}
}
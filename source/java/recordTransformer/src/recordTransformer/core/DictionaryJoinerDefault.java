package recordTransformer.core;

public class DictionaryJoinerDefault implements DictionaryJoinerI {

	private int[] includeIndexesA;
	private int[] includeIndexesB;
	
	public DictionaryJoinerDefault(int[] includeIndexesA, int[] includeIndexesB) {
		if (includeIndexesA == null) throw new NullPointerException("includeIndexesA is null");
		if (includeIndexesB == null) throw new NullPointerException("includeIndexesB is null");
		this.includeIndexesA = includeIndexesA;
		this.includeIndexesB = includeIndexesB;
	}
	
	@Override
	public void joinDictionaries(Table tableA, Table tableB, TableBuilder tableBuilder) {
		if (tableA == null) throw new NullPointerException("tableA is null");
		if (tableB == null) throw new NullPointerException("tableB is null");
		if (tableBuilder == null) throw new NullPointerException("tableBuilder is null");
		for (int i = 0; i < includeIndexesA.length; i++) tableBuilder.addDictionary(tableA.dictionaryAt(includeIndexesA[i]));
		for (int i = 0; i < includeIndexesB.length; i++) tableBuilder.addDictionary(tableB.dictionaryAt(includeIndexesB[i]));
	}
}
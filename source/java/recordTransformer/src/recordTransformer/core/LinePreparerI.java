package recordTransformer.core;

public interface LinePreparerI {

	public String prepare(String line);
}
package recordTransformer.core;

public interface DictionaryJoinerI {

	public void joinDictionaries(Table tableA, Table tableB, TableBuilder tableBuilder);
}
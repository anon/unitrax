package recordTransformer.core;

import java.util.HashMap;

public class RecordFinalizerBudget implements RecordFinalizerI {

	private double budget;
	
	public RecordFinalizerBudget(double budget) {
		this.budget = budget;
	}
	
	@Override
	public void finalize(RecordBuilder recordBuilder) {
		recordBuilder.add(budget);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		return new HashMap[] { null };
	}
}
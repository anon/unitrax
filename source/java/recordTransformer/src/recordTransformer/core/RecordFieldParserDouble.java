package recordTransformer.core;

import java.text.ParseException;
import java.util.HashMap;

public class RecordFieldParserDouble implements RecordFieldParserI {
	
	private int numberFormatExceptions = 0;

	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		Double value = null;
		try {
			value = Double.parseDouble(field);
		} catch(NumberFormatException e) {
			numberFormatExceptions++;
			throw new ParseException("Caught NumberFormatException", 0);
		}
		recordBuilder.add(value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		return new HashMap[] { null };
	}

	@Override
	public String report() {
		return "RecordFieldParserDouble: had " + numberFormatExceptions + " NumberFormatExceptions";
	}
}
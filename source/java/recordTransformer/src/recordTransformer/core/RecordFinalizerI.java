package recordTransformer.core;

import java.util.HashMap;

public interface RecordFinalizerI {
	public void finalize(RecordBuilder recordBuilder);
	public HashMap<Double, String[]>[] getDictionaries();
}
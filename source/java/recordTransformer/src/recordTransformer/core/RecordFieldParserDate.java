package recordTransformer.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class RecordFieldParserDate implements RecordFieldParserI {

	private String pattern;
	private Date startDate;
	
	public RecordFieldParserDate(String pattern, Date startDate) {
		if (pattern == null) throw new NullPointerException("pattern is null");
		this.pattern = pattern;
		this.startDate = startDate;
	}
	
	public RecordFieldParserDate(String pattern) {
		this(pattern, null);
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
		if (startDate != null) sdf.set2DigitYearStart(startDate);
		GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.ENGLISH);
		cal.clear();
		cal.setTime(sdf.parse(field));
		double value = new Long(cal.getTimeInMillis() / 1000l).doubleValue();
		recordBuilder.add(value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		return new HashMap[] { null };
	}

	@Override
	public String report() {
		return "RecordFieldParserDate: no report data available";
	}
}
package recordTransformer.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class RecordFieldParserEnumerableDate extends RecordFieldParserEnumerable {
	
	private String pattern;
	private double low;
	private double high;
	private Date startDate;

	public RecordFieldParserEnumerableDate(String pattern, Date startDate, double low, double high, HashMap<Double, String[]> dictionary) {
		super(dictionary);
		if (pattern == null) throw new NullPointerException("pattern is null");
		this.pattern = pattern;
		this.startDate = startDate;
		this.low = low;
		this.high = high;
	}
	
	public RecordFieldParserEnumerableDate(String pattern, double low, double high, HashMap<Double, String[]> dictionary) {
		this(pattern, null, low, high, dictionary);
	}

	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		Double value = get(field);
		if (value == null) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
			if (startDate != null) sdf.set2DigitYearStart(startDate);
			GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.ENGLISH);
			cal.clear();
			cal.setTime(sdf.parse(field));
			value = new Long(cal.getTimeInMillis() / 1000l).doubleValue();
			if (value < low || high < value) throw new ParseException("value " + value + " outside allowed bounds (" + low + ", " + high + ")", 0);
		}
		recordBuilder.add(value);
	}
}
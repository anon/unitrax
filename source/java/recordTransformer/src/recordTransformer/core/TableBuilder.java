package recordTransformer.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public class TableBuilder {

	private String label;
	private String[] columnLabels;
	private LinkedList<Record> records;
	private LinkedList<Record> skippedRecords;
	private LinkedList<HashMap<Double, String[]>> dictionaries;
	private int idIndex;
	private HashSet<Double> ids;
	private int numDoubleColumns;

	public TableBuilder(String label, String[] columnLabels, int idIndex, int numDoubleColumns) {
		if (label == null) throw new NullPointerException("label is null");
		if (columnLabels == null) throw new NullPointerException("columnLabels is null");
		this.label = label;
		this.columnLabels = columnLabels;
		records = new LinkedList<>();
		skippedRecords = new LinkedList<>();
		dictionaries = new LinkedList<>();
		this.idIndex = idIndex;
		ids = new HashSet<>();
		this.numDoubleColumns = numDoubleColumns;
	}
	
	public void addRecord(Record record) {
		if (record == null) throw new NullPointerException("record is null");
		if (record.numDoubles() + record.numStrings() != columnLabels.length) throw new UnsupportedOperationException("record (" + record.numDoubles() + " + " + record.numStrings() + ") and columnLabels (" + columnLabels.length + ") have different length; " + record.toString() + " labels: " + Arrays.toString(columnLabels));
		if (ids.contains(record.doubleAt(idIndex))) {
			skippedRecords.add(record);
			return;
		}
		ids.add(record.doubleAt(idIndex));
		records.add(record);
	}
	
	public void addDictionary(HashMap<Double, String[]> dictionary) {
		dictionaries.add(dictionary);
	}
	
	public Table toTable() {
		if (records.isEmpty()) throw new UnsupportedOperationException("no records added");
		if (dictionaries.size() != numDoubleColumns) throw new UnsupportedOperationException("number of dictionaries (" + dictionaries.size() + ") do not match numDoubleColumns (" + numDoubleColumns + ")");
		Record[] r = new Record[records.size()];
		Iterator<Record> recordIterator = records.iterator();
		int index = 0;
		while (recordIterator.hasNext()) r[index++] = recordIterator.next();
		@SuppressWarnings("unchecked")
		HashMap<Double, String[]>[] d = new HashMap[dictionaries.size()];
		Iterator<HashMap<Double, String[]>> dictionaryIterator = dictionaries.iterator();
		index = 0;
		while (dictionaryIterator.hasNext()) d[index++] = dictionaryIterator.next();
		Table table = new Table(label, columnLabels, r, d);
		System.out.print("TableBuilder: Successfully built table.");
		if (skippedRecords.size() > 0) System.out.println(" The following records were skipped due to already existing IDs:");
		else System.out.println();
		for (Record record : skippedRecords) System.out.println(record.toString());
		return table;
	}
} 
package recordTransformer.core;

import java.util.LinkedList;

public class RecordBuilder {

	private LinkedList<Double> doubleFields;
	private LinkedList<String> stringFields;
	
	public RecordBuilder() {
		doubleFields = new LinkedList<>();
		stringFields = new LinkedList<>();
	}
	
	public void add(double value) {
		doubleFields.add(value);
	}
	
	public void add(String value) {
		stringFields.add(value);
	}
	
	public Record toRecord() {
		double[] doubleFields = new double[this.doubleFields.size()];
		for (int i = 0; i < doubleFields.length; i++) {
			doubleFields[i] = this.doubleFields.get(i);
		}
		String[] stringFields = new String[this.stringFields.size()];
		for (int i = 0; i < stringFields.length; i++) {
			stringFields[i] = this.stringFields.get(i);
		}
		return new Record(doubleFields, stringFields);
	}
}
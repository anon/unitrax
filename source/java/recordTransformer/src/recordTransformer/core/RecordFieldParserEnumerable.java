package recordTransformer.core;

import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

public class RecordFieldParserEnumerable implements RecordFieldParserI {

	private HashMap<String, Double> dictionary;
	private int addedWords;
	
	public RecordFieldParserEnumerable() {
		dictionary = new HashMap<>();
		dictionary.put("", 0.0);
		addedWords = 0;
	}
	
	public RecordFieldParserEnumerable(HashMap<Double, String[]> dictionary) {
		this();
		if (dictionary == null) throw new NullPointerException("dictionary is null");
		for (HashMap.Entry<Double, String[]> entry : dictionary.entrySet()) for (String value : entry.getValue()) this.dictionary.put(value, entry.getKey());
	}

	public Double get(String key) {
		if (key == null) throw new NullPointerException("key is null");
		return dictionary.get(key);
	}
	
	public void put(String key, Double value) {
		if (key == null) throw new NullPointerException("key is null");
		if (value == null) throw new NullPointerException("value is null");
		dictionary.put(key, value);
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		Double value = get(field);
		if (value == null) {
			put(field, Collections.max(dictionary.values()) + 1.0);
			addedWords++;
			value = get(field);
		}
		recordBuilder.add(value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		HashMap<Double, LinkedList<String>> tmpDictionary = new HashMap<>();
		for(HashMap.Entry<String, Double> entry : dictionary.entrySet()){
			LinkedList<String> list = tmpDictionary.get(entry.getValue());
			if (list == null) {
				tmpDictionary.put(entry.getValue(), new LinkedList<>());
				list = tmpDictionary.get(entry.getValue());
			}
		    list.add(entry.getKey());
		}
		HashMap<Double, String[]> retDictionary = new HashMap<>();
		for (HashMap.Entry<Double, LinkedList<String>> entry : tmpDictionary.entrySet()) {
			String[] arr = new String[entry.getValue().size()];
			retDictionary.put(entry.getKey(), entry.getValue().toArray(arr));
		}
		return new HashMap[] { retDictionary };
	}
	
	@Override
	public String report() {
		return "RecordFieldParserEnumerable: added " + addedWords + " to the dictionary";
	}
}
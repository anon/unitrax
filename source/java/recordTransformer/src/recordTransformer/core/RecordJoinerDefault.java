package recordTransformer.core;

public class RecordJoinerDefault implements RecordJoinerI {

	private int[] includeDoublesA;
	private int[] includeStringsA;
	private int[] includeDoublesB;
	private int[] includeStringsB;
	
	public RecordJoinerDefault(int[] includeDoublesA, int[] includeStringsA, int[] includeDoublesB, int[] includeStringsB) {
		if (includeDoublesA == null) throw new NullPointerException("includeDoublesA is null");
		if (includeStringsA == null) throw new NullPointerException("includeStringsA is null");
		if (includeDoublesB == null) throw new NullPointerException("includeDoublesB is null");
		if (includeStringsB == null) throw new NullPointerException("includeStringsB is null");
		this.includeDoublesA = includeDoublesA;
		this.includeStringsA = includeStringsA;
		this.includeDoublesB = includeDoublesB;
		this.includeStringsB = includeStringsB;
	}
	
	@Override
	public Record join(Record recordA, Record[] recordsB) {
		if (recordA == null) throw new NullPointerException("recordA is null");
		if (recordsB != null && recordsB.length > 1) throw new IllegalArgumentException("recordsB cannot have more than one record");
		RecordBuilder recordBuilder = new RecordBuilder();
		for (int i = 0; i < includeDoublesA.length; i++) recordBuilder.add(recordA.doubleAt(includeDoublesA[i]));
		for (int i = 0; i < includeStringsA.length; i++) recordBuilder.add(recordA.stringAt(includeStringsA[i]));
		for (int i = 0; i < includeDoublesB.length; i++) {
			if (recordsB == null) recordBuilder.add(0.0);
			else recordBuilder.add(recordsB[0].doubleAt(includeDoublesB[i]));
		}
		for (int i = 0; i < includeStringsB.length; i++) {
			if (recordsB == null) recordBuilder.add("\"\"");
			else recordBuilder.add(recordsB[0].stringAt(includeStringsB[i]));
		}
		return recordBuilder.toRecord();
	}
}
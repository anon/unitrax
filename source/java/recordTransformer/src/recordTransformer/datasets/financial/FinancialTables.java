package recordTransformer.datasets.financial;

import java.io.File;
import java.io.IOException;

import recordTransformer.core.DummyLinePreparer;
import recordTransformer.core.RecordFieldParserI;
import recordTransformer.core.Table;

public class FinancialTables {
	
	private Table loan = null;
	private Table order = null;
	private Table trans = null;
	private Table district = null;
	private Table account = null;
	private Table card = null;
	private Table disp = null;
	private Table client = null;
	
	public FinancialTables(String path) {
		String[] loanColumnLabels = new String[] {"loan_id", "account_id", "date", "amount", "duration", "payments", "status"};
		loan = Table.readFrom(path + File.separator + "loan.asc", "loan", ";", loanColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDate(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserEnumerate()},
				null, 0, 7, new DummyLinePreparer());
		String[] orderColumnLabels = new String[] {"order_id", "account_id", "bank_to", "account_to", "amount", "k_symbol"};
		FinancialRecordFieldParserEnumerate parser_bank_to = new FinancialRecordFieldParserEnumerate();
		FinancialRecordFieldParserEnumerate parser_k_symbol = new FinancialRecordFieldParserEnumerate();
		order = Table.readFrom(path + File.separator + "order.asc", "order", ";", orderColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				parser_bank_to,
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				parser_k_symbol},
				null, 0, 6, new DummyLinePreparer());
		String[] transColumnLabels = new String[] {"trans_id", "account_id", "date", "type", "operation", "amount", "balance", "k_symbol", "bank", "account"};
		trans = Table.readFrom(path + File.separator + "trans.asc", "trans", ";", transColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDate(),
				new FinancialRecordFieldParserEnumerate(),
				new FinancialRecordFieldParserEnumerate(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserEnumerate(parser_k_symbol.getDictionaries()[0]),
				new FinancialRecordFieldParserEnumerate(parser_bank_to.getDictionaries()[0]),
				new FinancialRecordFieldParserDoubleWithNull()},
				null, 0, 10, new DummyLinePreparer());
		String[] districtColumnLabels = new String[] {"district_id", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "A13", "A14", "A15", "A16"};
		district = Table.readFrom(path + File.separator + "district.asc", "district", ";", districtColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserEnumerate(),
				new FinancialRecordFieldParserEnumerate(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDoubleWithUnknown(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDoubleWithUnknown(),
				new FinancialRecordFieldParserDouble()},
				null, 0, 16, new DummyLinePreparer());
		String[] accountColumnLabels = new String[] {"account_id", "district_id", "frequency", "date"};
		account = Table.readFrom(path + File.separator + "account.asc", "account", ";", accountColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserEnumerate(),
				new FinancialRecordFieldParserDate()},
				null, 0, 4, new DummyLinePreparer());
		String[] cardColumnLabels = new String[] {"card_id", "disp_id", "type", "issued"};
		card = Table.readFrom(path + File.separator + "card.asc", "card", ";", cardColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserEnumerate(),
				new FinancialRecordFieldParserDateTime()},
				null, 0, 4, new DummyLinePreparer());
		String[] dispColumnLabels = new String[] {"disp_id", "client_id", "account_id", "type"};
		disp = Table.readFrom(path + File.separator + "disp.asc", "disp", ";", dispColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserEnumerate()},
				null, 0, 4, new DummyLinePreparer());
		String[] clientColumnLabels = new String[] {"client_id", "birthdate", "sex", "district_id"};
		client = Table.readFrom(path + File.separator + "client.asc", "client", ";", clientColumnLabels, new RecordFieldParserI[] {
				new FinancialRecordFieldParserDouble(),
				new FinancialRecordFieldParserBirthNumber(),
				new FinancialRecordFieldParserDouble()},
				null, 0, 4, new DummyLinePreparer());
	}
	
	public Table loan() {
		return loan;
	}
	
	public Table order() {
		return order;
	}
	
	public Table trans() {
		return trans;
	}
	
	public Table district() {
		return district;
	}
	
	public Table account() {
		return account;
	}
	
	public Table card() {
		return card;
	}
	
	public Table disp() {
		return disp;
	}
	
	public Table client() {
		return client;
	}
		
	public void writeAllTablesTo(String path) throws IOException {
		if (path == null) throw new NullPointerException("path is null");
		loan.writeTo(path);
		order.writeTo(path);
		trans.writeTo(path);
		district.writeTo(path);
		account.writeTo(path);
		card.writeTo(path);
		disp.writeTo(path);
		client.writeTo(path);
	}
}
package recordTransformer.datasets.financial;

import java.util.HashMap;

import recordTransformer.core.Record;
import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordJoinerI;

public class FinancialRecordJoinerAccountDisp implements RecordJoinerI {
	
	private static final int[] INCLUDEINDEXESA = new int[] {0, 1, 2, 3, 4};
	private static final int[] INCLUDEINDEXESB = new int[] {2, 3, 4, 5, 6};
	
	private HashMap<Double, String[]> dictionary;
	
	public FinancialRecordJoinerAccountDisp(HashMap<Double, String[]> dictionary) {
		if (dictionary == null) throw new NullPointerException("dictionary is null");
		this.dictionary = dictionary;
	}

	@Override
	public Record join(Record recordA, Record[] recordsB) {
		if (recordA == null) throw new NullPointerException("recordA is null");
		if (recordsB == null) throw new NullPointerException("recordsB is null");
		if (recordsB.length <= 0 || recordsB.length > 2) throw new IllegalArgumentException("recordsB must have 1-2 records");
		Record owner = recordsB[0];
		Record user = null;
		if (recordsB.length > 1) {
			user = recordsB[1];
			if (dictionary.get(user.doubleAt(1))[0].equals("OWNER")) {
				Record tmp = user;
				user = owner;
				owner = tmp;
			}
		}
		RecordBuilder recordBuilder = new RecordBuilder();
		for (int i = 0; i < INCLUDEINDEXESA.length; i++) recordBuilder.add(recordA.doubleAt(INCLUDEINDEXESA[i]));
		for (int i = 0; i < INCLUDEINDEXESB.length; i++) recordBuilder.add(recordsB[0].doubleAt(INCLUDEINDEXESB[i]));
		for (int i = 0; i < INCLUDEINDEXESB.length; i++) {
			if (recordsB.length > 1) recordBuilder.add(recordsB[1].doubleAt(INCLUDEINDEXESB[i]));
			else recordBuilder.add(0.0);
		}
		return recordBuilder.toRecord();
	}
}
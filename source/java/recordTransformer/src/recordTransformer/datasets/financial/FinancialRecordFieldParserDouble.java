package recordTransformer.datasets.financial;

import java.text.ParseException;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserDouble;

public class FinancialRecordFieldParserDouble extends RecordFieldParserDouble {

	@Override
	public void parse(String field, RecordBuilder record) throws ParseException {
		super.parse(field.replaceAll("^\"|\"$", ""), record);
	}
}
package recordTransformer.datasets.financial;

import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.TimeZone;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserI;

public class FinancialRecordFieldParserBirthNumber implements RecordFieldParserI {
	
	private HashMap<String, Double> dictionary;
	private int femaleCount = 0;
	private int maleCount = 0;
	
	public FinancialRecordFieldParserBirthNumber() {
		dictionary = new HashMap<>();
		dictionary.put("", 0.0);
		dictionary.put("male", 1.0);
		dictionary.put("female", 2.0);
	}

	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		field = field.replaceAll("^\"|\"$", "");
		String sex = "male";
		int year = Integer.parseInt(field.substring(0, 2));
		int month = Integer.parseInt(field.substring(2, 4));
		int date = Integer.parseInt(field.substring(4, 6));
		maleCount++;
		if (month > 50) {
			sex = "female";
			month -= 50;
			maleCount--;
			femaleCount++;
		}
		GregorianCalendar cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.ENGLISH);
		cal.set(1900 + year, month - 1, date, 0, 0, 0);
		long time = cal.getTimeInMillis() / 1000l;
		double birthdate = new Long(time).doubleValue();
		recordBuilder.add(birthdate);
		recordBuilder.add(dictionary.get(sex));
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		HashMap<Double, LinkedList<String>> tmpDictionary = new HashMap<>();
		for(HashMap.Entry<String, Double> entry : dictionary.entrySet()){
			LinkedList<String> list = tmpDictionary.get(entry.getValue());
			if (list == null) {
				tmpDictionary.put(entry.getValue(), new LinkedList<>());
				list = tmpDictionary.get(entry.getValue());
			}
		    list.add(entry.getKey());
		}
		HashMap<Double, String[]> retDictionary = new HashMap<>();
		for (HashMap.Entry<Double, LinkedList<String>> entry : tmpDictionary.entrySet()) {
			String[] arr = new String[entry.getValue().size()];
			retDictionary.put(entry.getKey(), entry.getValue().toArray(arr));
		}
		return new HashMap[] { null, retDictionary };
	}

	@Override
	public String report() {
		return "FinancialRecordFieldParserBirthNumber: females " + femaleCount + " males " + maleCount;
	}
}
package recordTransformer.datasets.financial;

import java.io.IOException;

import recordTransformer.core.DictionaryJoinerDefault;
import recordTransformer.core.RecordJoinerDefault;
import recordTransformer.core.Table;

public class FinancialRecordTransformer {
	
	public static void main(String[] args) {
		FinancialTables tables = new FinancialTables("/home/user/unitrax/data/financial/original");
		Table dispClient = Table.join("dispClient", new String[] {"disp_id", "account_id", "type", "birthdate", "sex", "district_id"}, 0, 6,
				tables.disp(), tables.client(), 1, 0,
				new RecordJoinerDefault(new int[] {0, 2, 3}, new int[] {}, new int[] {1, 2, 3}, new int[] {}),
				new DictionaryJoinerDefault(new int[] {0, 2, 3}, new int[] {1, 2, 3}));
		Table dispClientCard = Table.join("dispClientCard", new String[] {"account_id", "type", "birthdate", "sex", "district_id", "card_type", "card_issued"}, 0, 7,
				dispClient, tables.card(), 0, 1, 
				new RecordJoinerDefault(new int[] {1, 2, 3, 4, 5}, new int[] {}, new int[] {2, 3}, new int[] {}),
				new DictionaryJoinerDefault(new int[] {1, 2, 3, 4, 5}, new int[] {2, 3}));
		Table accountTrans = Table.join("account", new String[] {"account_id", "account_district_id", "account_frequency", "account_date", "account_balance"}, 0, 5,
				tables.account(), tables.trans(), 0, 1,
				new FinancialRecordJoinerAccountTrans(),
				new FinancialDictionaryJoinerAccountTrans());
		Table accountOwnerUser = Table.join("accountOwnerUser", new String[] {"account_id", "account_district_id", "account_frequency", "account_date", "account_balance",
				"owner_birthdate", "owner_sex", "owner_district_id", "owner_card_type", "owner_card_issued",
				"user_birthdate", "user_sex", "user_district_id", "user_card_type", "user_card_issued"}, 0, 15,
				accountTrans, dispClientCard, 0, 0,
				new FinancialRecordJoinerAccountDisp(tables.disp().dictionaryAt(3)),
				new FinancialDictionaryJoinerAccountDisp());
		Table account = Table.join("account", new String[] {"account_id", "account_district_id", "account_frequency", "account_date", "account_balance",
				"owner_birthdate", "owner_sex", "owner_district_id", "owner_card_type", "owner_card_issued",
				"user_birthdate", "user_sex", "user_district_id", "user_card_type", "user_card_issued",
				"loan_date", "loan_amount", "loan_duration", "loan_payments", "loan_status"}, 0, 20,
				accountOwnerUser, tables.loan(), 0, 1,
				new RecordJoinerDefault(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}, new int[] {}, new int[] {2, 3, 4, 5, 6}, new int[] {}),
				new DictionaryJoinerDefault(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}, new int[] {2, 3, 4, 5, 6}));
		try {
			account.writeTo("/home/user/unitrax/data/financial/transformed");
			tables.district().writeTo("/home/user/unitrax/data/financial/transformed");	
			tables.order().writeTo("/home/user/unitrax/data/financial/transformed");		
			tables.trans().writeTo("/home/user/unitrax/data/financial/transformed");	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
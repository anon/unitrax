package recordTransformer.datasets.financial;

import java.text.ParseException;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserDouble;

public class FinancialRecordFieldParserDoubleWithNull extends RecordFieldParserDouble {

	@Override
	public void parse(String field, RecordBuilder record) throws ParseException {
		String f = field.replaceAll("^\"|\"$", "");
		if (f.equals("")) f = "0.0";
		super.parse(f, record);
	}
}
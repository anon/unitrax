package recordTransformer.datasets.financial;

import recordTransformer.core.DictionaryJoinerI;
import recordTransformer.core.Table;
import recordTransformer.core.TableBuilder;

public class FinancialDictionaryJoinerAccountDisp implements DictionaryJoinerI {
	
	private int[] includeIndexesA;
	private int[] includeIndexesB;
	
	public FinancialDictionaryJoinerAccountDisp() {
		includeIndexesA = new int[] {0, 1, 2, 3, 4};
		includeIndexesB = new int[] {2, 3, 4, 5, 6};
	}
	@Override
	public void joinDictionaries(Table tableA, Table tableB, TableBuilder tableBuilder) {
		if (tableA == null) throw new NullPointerException("tableA is null");
		if (tableB == null) throw new NullPointerException("tableB is null");
		if (tableBuilder == null) throw new NullPointerException("tableBuilder is null");
		for (int i = 0; i < includeIndexesA.length; i++) tableBuilder.addDictionary(tableA.dictionaryAt(includeIndexesA[i]));
		for (int i = 0; i < includeIndexesB.length; i++) tableBuilder.addDictionary(tableB.dictionaryAt(includeIndexesB[i]));
		for (int i = 0; i < includeIndexesB.length; i++) tableBuilder.addDictionary(tableB.dictionaryAt(includeIndexesB[i]));
	}
}
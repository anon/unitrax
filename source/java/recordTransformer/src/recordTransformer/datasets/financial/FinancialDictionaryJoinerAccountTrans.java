package recordTransformer.datasets.financial;

import recordTransformer.core.DictionaryJoinerI;
import recordTransformer.core.Table;
import recordTransformer.core.TableBuilder;

public class FinancialDictionaryJoinerAccountTrans implements DictionaryJoinerI {
	
	private int[] includeIndexesA;
	
	public FinancialDictionaryJoinerAccountTrans() {
		includeIndexesA = new int[] {0, 1, 2, 3};
	}
	@Override
	public void joinDictionaries(Table tableA, Table tableB, TableBuilder tableBuilder) {
		if (tableA == null) throw new NullPointerException("tableA is null");
		if (tableB == null) throw new NullPointerException("tableB is null");
		if (tableBuilder == null) throw new NullPointerException("tableBuilder is null");
		for (int i = 0; i < includeIndexesA.length; i++) tableBuilder.addDictionary(tableA.dictionaryAt(includeIndexesA[i]));
		tableBuilder.addDictionary(null);
	}
}
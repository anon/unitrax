package recordTransformer.datasets.financial;

import recordTransformer.core.Record;
import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordJoinerI;

public class FinancialRecordJoinerAccountTrans implements RecordJoinerI {
	
	private int[] includeIndexesA;
	
	public FinancialRecordJoinerAccountTrans() {
		includeIndexesA = new int[] {0, 1, 2, 3};
	}

	@Override
	public Record join(Record recordA, Record[] recordsB) {
		if (recordA == null) throw new NullPointerException("recordA is null");
		if (recordsB == null) throw new NullPointerException("recordsB is null");
		Record last = null;
		if (recordsB.length > 0) last = recordsB[0];
		if (recordsB.length > 1) {
			for (Record record : recordsB) {
				if (record.doubleAt(2) > last.doubleAt(2)) last = record;
			}
		}
		RecordBuilder recordBuilder = new RecordBuilder();
		for (int i = 0; i < includeIndexesA.length; i++) recordBuilder.add(recordA.doubleAt(includeIndexesA[i]));
		if (recordsB.length > 0) recordBuilder.add(last.doubleAt(6));
		else recordBuilder.add(0.0);
		return recordBuilder.toRecord();
	}
}
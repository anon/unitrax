package recordTransformer.datasets.financial;

import java.text.ParseException;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserDouble;

public class FinancialRecordFieldParserDoubleWithUnknown extends RecordFieldParserDouble {

	@Override
	public void parse(String field, RecordBuilder record) throws ParseException {
		String f = field.replaceAll("^\"|\"$", "");
		if (f.equals("?")) f = "-1.0";
		super.parse(f, record);
	}
}
package recordTransformer.datasets.financial;

import java.text.ParseException;
import java.util.HashMap;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserEnumerable;

public class FinancialRecordFieldParserEnumerate extends RecordFieldParserEnumerable {

	public FinancialRecordFieldParserEnumerate() {
		super();
	}
	
	public FinancialRecordFieldParserEnumerate(HashMap<Double, String[]> dictionary) {
		super(dictionary);
	}
	
	@Override
	public void parse(String field, RecordBuilder record) throws ParseException {
		super.parse(field.replaceAll("^\"|\"$", ""), record);
	}
}
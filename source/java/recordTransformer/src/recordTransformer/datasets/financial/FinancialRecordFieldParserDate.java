package recordTransformer.datasets.financial;

import java.text.ParseException;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserDate;

public class FinancialRecordFieldParserDate extends RecordFieldParserDate {

	public FinancialRecordFieldParserDate() {
		super("yyMMdd");
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		super.parse(field.replaceAll("^\"|\"$", ""), recordBuilder);
	}
}
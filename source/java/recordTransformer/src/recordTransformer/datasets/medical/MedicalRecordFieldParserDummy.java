package recordTransformer.datasets.medical;

import java.text.ParseException;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserDummy;

public class MedicalRecordFieldParserDummy extends RecordFieldParserDummy{

	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		super.parse(field.replaceAll("^\"|\"$", ""), recordBuilder);
	}
}
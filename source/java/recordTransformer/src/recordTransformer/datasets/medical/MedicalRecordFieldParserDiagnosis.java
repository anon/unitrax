package recordTransformer.datasets.medical;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserI;

public class MedicalRecordFieldParserDiagnosis implements RecordFieldParserI {
	
	// SJS 359, SLE 310, BEHCET 103, PSS 94, RA 79, MCTD 63, APS 53, PM 51, DM 39, PN 23, FUO 22, AORTITIS 16, (IP 13), MRA 12, COLLAGEN 12, RAYNAUD 10
	public static final String[] DIAGNOSISSTRINGS = new String[] {"sjs", "sle", "behcet", "pss", "ra", "mctd", "aps", "pm", "dm", "pn", "fuo", "aortitis", "ip", "mra", "collagen", "raynaud", "other"};
	public static final String[] DIAGNOSISCOLUMNNAMES = new String[] {"diagnosis_sjs", "diagnosis_sle", "diagnosis_behcet", "diagnosis_pss", "diagnosis_ra", "diagnosis_mctd", "diagnosis_aps", "diagnosis_pm", "diagnosis_dm", "diagnosis_pn", "diagnosis_fuo", "diagnosis_aortitis", "diagnosis_ip", "diagnosis_mra", "diagnosis_collagen", "diagnosis_raynaud", "diagnosis_other"};
	
	private static final String GROUPSEPARATOR = Character.toString((char) 29);
	
	private int count = 0;
	private int otherCount = 0;
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		count++;
		field = field.replaceAll("^\"|\"$", "");
		Double[] diagnosisValues = new Double[DIAGNOSISSTRINGS.length];
		Arrays.fill(diagnosisValues, 0.0);
		String[] words = field.split("[(), \\+\\-" + GROUPSEPARATOR + "]+");
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			while (word != null) word = detectAndRemove(word, diagnosisValues);
		}
		for (int i = 0; i < diagnosisValues.length; i++) recordBuilder.add(diagnosisValues[i]);
		if (diagnosisValues[diagnosisValues.length - 1] > 0.0) otherCount++;
		recordBuilder.add(field);
	}
	
	private String detectAndRemove(String word, Double[] diagnosisValues) {
		if (word == null) throw new NullPointerException("word is null");
		if (diagnosisValues == null) throw new NullPointerException("diagnosisValues is null");
		if (word.isEmpty()) return null;
		int index = -1;
		int length = -1;
		for (int i = 0; i < DIAGNOSISSTRINGS.length; i++) {
			if (word.toLowerCase().startsWith(DIAGNOSISSTRINGS[i]) && DIAGNOSISSTRINGS[i].length() > length) {
				index = i;
				length = DIAGNOSISSTRINGS[i].length();
			}
		}
		if (index > -1) {
			diagnosisValues[index] = 1.0;
			return word.toLowerCase().replaceAll("^" + DIAGNOSISSTRINGS[index], "");
		}
		if (!word.isEmpty()) diagnosisValues[diagnosisValues.length - 1] = 1.0;
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		HashMap<Double, String[]>[] dictionaries = new HashMap[DIAGNOSISSTRINGS.length];
		Arrays.fill(dictionaries, null);
		return dictionaries;
	}
	
	@Override
	public String report() {
		return "MedicalRecordFieldParserDiagnosis: " + otherCount + " out of " + count + " fields had other diagnosis";
	}
}
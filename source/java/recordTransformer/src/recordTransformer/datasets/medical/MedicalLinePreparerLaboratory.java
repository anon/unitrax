package recordTransformer.datasets.medical;

import recordTransformer.core.LinePreparerI;

public class MedicalLinePreparerLaboratory implements LinePreparerI {

	private String delimiter;
	private int numFields;
	private double index;
	
	public MedicalLinePreparerLaboratory(String delimiter, int numFields) {
		if (delimiter == null) throw new NullPointerException("delimiter is null");
		this.delimiter = delimiter;
		this.numFields = numFields;
		index = 0.0;
	}
	
	@Override
	public String prepare(String line) {
		String[] words = line.split("," + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
		StringBuffer b = new StringBuffer();
		b.append(index++);
		b.append(delimiter);
		for (int i = 0; i < numFields - 1; i++) {
			if (words.length > i) b.append(words[i]);
			if (i < numFields - 2) b.append(delimiter);
		}
		return b.toString();
	}
}
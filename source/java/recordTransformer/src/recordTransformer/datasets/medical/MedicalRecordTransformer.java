package recordTransformer.datasets.medical;

import java.io.IOException;

import recordTransformer.core.Table;

public class MedicalRecordTransformer {

	public static void main(String[] args) {
		MedicalTables tables = new MedicalTables("/home/user/unitrax/data/medical/original");		
		Table patient = Table.join("patient", new String[] {
				"id", "sex", "date_of_birth", "date_of_initial_data_collection", "date_of_first_visit",
				"date_of_examination",
				"admission",
				"diagnosis_sjs", "diagnosis_sle", "diagnosis_behcet", "diagnosis_pss", "diagnosis_ra", "diagnosis_mctd", "diagnosis_aps",
				"diagnosis_pm", "diagnosis_dm", "diagnosis_pn", "diagnosis_fuo", "diagnosis_aortitis", "diagnosis_ip", "diagnosis_mra",
				"diagnosis_collagen", "diagnosis_raynaud", "diagnosis_other",
				"acl_iga", "acl_igg", "acl_igm", "ana", "ana_pattern_d", "ana_pattern_n", "ana_pattern_p", "ana_pattern_s", "kct", "rvvt", "lac", "thrombosis",
				"budget",
				"diagnosis_general",
				"diagnosis_special", "symptoms"},
				0, 37, tables.patient(), tables.specialLab(), 0, 0, new MedicalRecordJoinerPatientSpecialLab(
						0.0, -2208988800.0, 631152000.0, 0.0, 0.0, 0.0,
						-1.0, -1.0, -1.0, -1.0, 0.0, 0.0, 0.0, 0.0,
						0.0, 0.0, 0.0, -1.0, 1.0, ""),
				new MedicalDictionaryJoinerPatientSpecialLab());		
		try {
			patient.writeTo("/home/user/unitrax/data/medical/transformed");
			tables.laboratory().writeTo("/home/user/unitrax/data/medical/transformed");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
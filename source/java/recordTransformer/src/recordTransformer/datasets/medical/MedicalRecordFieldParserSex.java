package recordTransformer.datasets.medical;

import java.text.ParseException;
import java.util.HashMap;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserEnumerable;

public class MedicalRecordFieldParserSex extends RecordFieldParserEnumerable {
	
	private static HashMap<Double, String[]> sexMap() {
		HashMap<Double, String[]> sexMap = new HashMap<>();
		sexMap.put(0.0, new String[] { "" });
		sexMap.put(1.0, new String[] { "F" });
		sexMap.put(2.0, new String[] { "M" });
		return sexMap;
	}
	
	private int count = 0;
	private int exceptionCount = 0;
	
	public MedicalRecordFieldParserSex() {
		super(sexMap());
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		count++;
		Double value = get(field);
		if (value == null) {
			exceptionCount++;
			throw new ParseException("Cannot parse sex from " + field, 0);
		}
		recordBuilder.add(value);
	}
	
	@Override
	public String report() {
		return "MedicalRecordFieldParserSex: parse exceptions " + exceptionCount + " out of " + count + " -- " + super.report();
	}
}
package recordTransformer.datasets.medical;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserEnumerableDate;
import recordTransformer.core.RecordFieldParserI;

public class MedicalRecordFieldParserDateYearFirst implements RecordFieldParserI {
	
	private double low;
	private double high;
	private double nullValue;
	private HashMap<Double, String[]> dictionary;
	
	private int count = 0;
	private int e1Count = 0;
	private int e2Count = 0;
	private int e3Count = 0;
	
	public MedicalRecordFieldParserDateYearFirst(double nullValue, double low, double high) {
		if (low <= nullValue && nullValue <= high) throw new IllegalArgumentException("nullValue " + nullValue + " between low " + low + " and high " + high);
		this.low = low;
		this.high = high;
		this.nullValue = nullValue;
		dictionary = new HashMap<>();
		dictionary.put(nullValue, new String[] { "" });
	}
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		count++;
		Date startDate = new Date(-2208988800000l);
		RecordFieldParserEnumerableDate dateParser;
		try {
			dateParser = new RecordFieldParserEnumerableDate("yyyy/M/d", startDate, low, high, dictionary);
			dateParser.parse(field, recordBuilder);
		} catch (ParseException pe1) {
			e1Count++;
			try {
				dateParser = new RecordFieldParserEnumerableDate("yy.M.d", startDate, low, high, dictionary);
				dateParser.parse(field, recordBuilder);
			} catch (ParseException pe2) {
				e2Count++;
				try {
					dateParser = new RecordFieldParserEnumerableDate("yyMMdd", startDate, low, high, dictionary);
					dateParser.parse(field, recordBuilder);
				} catch (ParseException pe3) {
					e3Count++;
					recordBuilder.add(nullValue);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		return new HashMap[] { dictionary };
	}
	
	@Override
	public String report() {
		return "MedicalRecordFieldParserDateYearFirst: " + e3Count + " fails out of " + count + " (e1 " + e1Count + " e2 " + e2Count + " e3 " + e3Count + ")";
	}
}
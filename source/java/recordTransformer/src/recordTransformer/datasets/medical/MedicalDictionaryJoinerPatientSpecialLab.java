package recordTransformer.datasets.medical;

import recordTransformer.core.DictionaryJoinerI;
import recordTransformer.core.Table;
import recordTransformer.core.TableBuilder;

public class MedicalDictionaryJoinerPatientSpecialLab implements DictionaryJoinerI {

	private static final int[] DIAGNOSISINDEXESA = new int[] {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
	
	@Override
	public void joinDictionaries(Table tableA, Table tableB, TableBuilder tableBuilder) {
		if (tableA == null) throw new NullPointerException("tableA is null");
		if (tableB == null) throw new NullPointerException("tableB is null");
		if (tableBuilder == null) throw new NullPointerException("tableBuilder is null");

		// id, sex, date_of_birth, date_of_initial_data_collection, date_of_first_visit
		for (int i : new int[] { 0, 1, 2, 3, 4 }) tableBuilder.addDictionary(tableA.dictionaryAt(i));
		// date_of_examination
		tableBuilder.addDictionary(tableB.dictionaryAt(1));
		// admission
		tableBuilder.addDictionary(tableA.dictionaryAt(5));
		// diagnosis_sjs, diagnosis_sle, diagnosis_behcet, diagnosis_pss, diagnosis_ra, diagnosis_mctd, diagnosis_aps, diagnosis_pm, diagnosis_dm, diagnosis_pn, diagnosis_fuo, diagnosis_aortitis, diagnosis_ip, diagnosis_mra, diagnosis_collagen, diagnosis_raynaud, diagnosis_other
		for (int i = 0; i < DIAGNOSISINDEXESA.length; i++) {
			int indexA = DIAGNOSISINDEXESA[i];
			tableBuilder.addDictionary(tableA.dictionaryAt(indexA));
		}
		// acl_iga, acl_igg, acl_igm, ana, ana_pattern_d, ana_pattern_n, ana_pattern_p, ana_pattern_s, kct, rvvt, lac, thrombosis
		for (int i : new int[] { 9, 2, 3, 4, 5, 6, 7, 8, 27, 28, 29, 30 }) tableBuilder.addDictionary(tableB.dictionaryAt(i));
		// budget
		tableBuilder.addDictionary(tableA.dictionaryAt(23));
	}
}
package recordTransformer.datasets.medical;

import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserEnumerableDouble;

public class MedicalRecordFieldParserEnumerableDouble extends RecordFieldParserEnumerableDouble{

	private static HashMap<Double, String[]> doubleDictionary(double nullValue) {
		HashMap<Double, String[]> doubleDictionary = new HashMap<>();
		doubleDictionary.put(nullValue, new String[] { "" });
		return doubleDictionary;
	}
	
	private double nullValue;
	private int count = 0;
	private int exceptionCount = 0;
	LinkedList<String> exceptionMessages = new LinkedList<>();
	
	public MedicalRecordFieldParserEnumerableDouble(double low, double high, double nullValue) {
		this(low, high, nullValue, doubleDictionary(nullValue));
	}
	
	public MedicalRecordFieldParserEnumerableDouble(double low, double high, double nullValue, HashMap<Double, String[]> dictionary) {
		super(low, high, dictionary);
		this.nullValue = nullValue;
	}


	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		count++;
		try {
			super.parse(field, recordBuilder);
		} catch (ParseException e) {
			exceptionMessages.add(e.getMessage());
			exceptionCount++;
			recordBuilder.add(nullValue);
		}
	}
	
	@Override
	public String report() {
		StringBuffer b = new StringBuffer();
		b.append("MedicalRecordFieldParserEnumerableDouble: parse exceptions " + exceptionCount + " out of " + count + " -- " + super.report());
		for (String e : exceptionMessages) b.append("\n" + e);
		return b.toString();
	}
}
package recordTransformer.datasets.medical;

import java.text.ParseException;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserPlusMinus;

public class MedicalRecordFieldParserPlusMinus extends RecordFieldParserPlusMinus {

	private int count = 0;
	private int exceptionCount = 0;
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		count++;
		try {
			super.parse(field, recordBuilder);
		} catch (ParseException e) {
			exceptionCount++;
			recordBuilder.add(0.0);
		}
	}
	
	@Override
	public String report() {
		return "MedicalRecordFieldParserPlusMinus: parse exceptions " + exceptionCount + " out of " + count + " -- " + super.report();
	}
}
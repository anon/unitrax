package recordTransformer.datasets.medical;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import recordTransformer.core.DummyLinePreparer;
import recordTransformer.core.RecordFieldParserDouble;
import recordTransformer.core.RecordFieldParserEnumerableDate;
import recordTransformer.core.RecordFieldParserEnumerableDouble;
import recordTransformer.core.RecordFieldParserI;
import recordTransformer.core.RecordFinalizerBudget;
import recordTransformer.core.Table;

public class MedicalTables {

	private Table patient = null;
	private Table specialLab = null;
	private Table laboratory = null;
	
	public MedicalTables(String path) {
		String[] patientPreLabels = new String[] {"id", "sex", "date_of_birth", "date_of_initial_data_collection", "date_of_first_visit", "admission"};
		String[] patientPostLabels = new String[] {"budget", "diagnosis"};
		String[] patientColumnLabels = new String[patientPreLabels.length + MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length + patientPostLabels.length];
		for (int i = 0; i < patientPreLabels.length; i++) patientColumnLabels[i] = patientPreLabels[i];
		for (int i = 0; i < MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length; i++) patientColumnLabels[patientPreLabels.length + i] = MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES[i];
		for (int i = 0; i < patientPostLabels.length; i++) patientColumnLabels[patientPreLabels.length + MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length + i] = patientPostLabels[i];	
		patient = Table.readFrom(path + File.separator + "TSUM_A.CSV", ",", "patient", patientColumnLabels, new RecordFieldParserI[] {
				new RecordFieldParserDouble(),
				new MedicalRecordFieldParserSex(),
				// -2208988800.0 = GMT: Monday, January 1, 1900 12:00:00 AM
				// -2208988799.0 = GMT: Monday, January 1, 1900 12:00:01 AM
				// 631152000.0   = GMT: Monday, January 1, 1990 12:00:00 AM
				new MedicalRecordFieldParserDateYearFirst(-2208988800.0, -2208988799.0, 631152000.0),
				// 631152000.0   = GMT: Monday, January 1, 1990 12:00:00 AM
				// 631152001.0   = GMT: Monday, January 1, 1990 12:00:01 AM
				// 915148800.0   = GMT: Friday, January 1, 1999 12:00:00 AM
				new MedicalRecordFieldParserDateYearFirst(631152000.0, 631152001.0, 915148800.0),
				// 0.0           = GMT: Thursday, January 1, 1970 12:00:00 AM
				// 1.0           = GMT: Thursday, January 1, 1970 12:00:01 AM
				// 915148800.0   = GMT: Friday, January 1, 1999 12:00:00 AM
				new MedicalRecordFieldParserDateYearFirst(0.0, 1.0, 915148800.0),
				new MedicalRecordFieldParserPlusMinus(),
				new MedicalRecordFieldParserDiagnosis()},
				new RecordFinalizerBudget(1.0), 0, patientPreLabels.length + MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length + 1, new DummyLinePreparer());
		
		String[] specialLabPreLabels = new String[] {"id", "date_of_examination", "acl_igg", "acl_igm", "ana"};
		String[] specialLabMidLabels = new String[] {"acl_iga"};
		String[] specialLabPostLabels = new String[] {"kct", "rvvt", "lac", "thrombosis", "diagnosis", "symptoms"};
		String[] specialLabColumnLabels = new String[specialLabPreLabels.length + MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES.length + specialLabMidLabels.length + MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length + specialLabPostLabels.length];
		for (int i = 0; i < specialLabPreLabels.length; i++) specialLabColumnLabels[i] = specialLabPreLabels[i];
		for (int i = 0; i < MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES.length; i++) specialLabColumnLabels[specialLabPreLabels.length + i] = MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES[i];
		for (int i = 0; i < specialLabMidLabels.length; i++) specialLabColumnLabels[specialLabPreLabels.length + MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES.length + i] = specialLabMidLabels[i];
		for (int i = 0; i < MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length; i++) specialLabColumnLabels[specialLabPreLabels.length + MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES.length + specialLabMidLabels.length + i] = MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES[i];
		for (int i = 0; i < specialLabPostLabels.length; i++) specialLabColumnLabels[specialLabPreLabels.length + MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES.length + specialLabMidLabels.length + MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length + i] = specialLabPostLabels[i];
		HashMap<Double, String[]> anaMap = new HashMap<>();
		anaMap.put(4097.0, new String[] { ">4096" });
		anaMap.put(-1.0, new String[] { "" });
		HashMap<Double, String[]> doubleNullMap = new HashMap<>();
		doubleNullMap.put(-1.0, new String[] { "" });
		specialLab = Table.readFrom(path + File.separator + "TSUM_B.CSV", ",", "specialLab", specialLabColumnLabels, new RecordFieldParserI[] {
				new RecordFieldParserDouble(),
				// 0.0           = GMT: Thursday, January 1, 1970 12:00:00 AM
				// 1.0           = GMT: Thursday, January 1, 1970 12:00:01 AM
				// 915148800.0   = GMT: Friday, January 1, 1999 12:00:00 AM
				new MedicalRecordFieldParserDateYearFirst(0.0, 1.0, 915148800.0),
				new RecordFieldParserEnumerableDouble(0.0, 1000.0, doubleNullMap),
				new RecordFieldParserEnumerableDouble(0.0, 200000.0, doubleNullMap),
				new RecordFieldParserEnumerableDouble(0.0, 4096.0, anaMap),
				new MedicalRecordFieldParserAnaPattern(),
				new RecordFieldParserEnumerableDouble(0.0, 50000.0, doubleNullMap),
				new MedicalRecordFieldParserDiagnosis(),
				new MedicalRecordFieldParserPlusMinus(),
				new MedicalRecordFieldParserPlusMinus(),
				new MedicalRecordFieldParserPlusMinus(),
				new MedicalRecordFieldParserDummy(),
				new RecordFieldParserEnumerableDouble(0.0, 3.0, doubleNullMap)},
				null, 0, specialLabPreLabels.length + MedicalRecordFieldParserAnaPattern.ANACOLUMNNAMES.length + specialLabMidLabels.length + MedicalRecordFieldParserDiagnosis.DIAGNOSISCOLUMNNAMES.length + 4, new DummyLinePreparer());

		HashMap<Double, String[]> dateMap = new HashMap<>();
		dateMap.put(0.0, new String[] { "" });
		HashMap<Double, String[]> ldhMap = new HashMap<>();
		ldhMap.put(-1.0, new String[] { "" });
		ldhMap.put(10000.0, new String[] { ">5000" });
		HashMap<Double, String[]> cpkMap = new HashMap<>();
		cpkMap.put(-1.0, new String[] { "" });
		cpkMap.put(10.0, new String[] { "<37", "<33", "<34" });
		cpkMap.put(10000.0, new String[] { "5000<" });
		HashMap<Double, String[]> wbcMap = new HashMap<>();
		wbcMap.put(-1.0, new String[] { "" });
		wbcMap.put(1.0, new String[] { "<13.9" });
		HashMap<Double, String[]> pltMap = new HashMap<>();
		pltMap.put(-1.0, new String[] { "" });
		pltMap.put(15.0, new String[] { ">12" });
		pltMap.put(25.0, new String[] { ">22PT" });
		pltMap.put(60.0, new String[] { ">50" });
		pltMap.put(65.0, new String[] { "60-70" });
		pltMap.put(80.0, new String[] { ">77" });
		pltMap.put(90.0, new String[] { ">86" });
		pltMap.put(110.0, new String[] { ">109", ">108", ">107", ">100"  });
		pltMap.put(120.0, new String[] { ">116" });
		pltMap.put(140.0, new String[] { ">133" });
		pltMap.put(160.0, new String[] { ">155", ">150" });
		pltMap.put(170.0, new String[] { ">166", ">165", ">163PT", ">163" });
		pltMap.put(180.0, new String[] { ">174PT", ">171", ">170" });
		pltMap.put(200.0, new String[] { ">195" });
		pltMap.put(220.0, new String[] { ">217", ">216" });
		pltMap.put(230.0, new String[] { ">221" });
		pltMap.put(240.0, new String[] { ">230" });
		pltMap.put(260.0, new String[] { ">253", ">250" });
		pltMap.put(270.0, new String[] { ">267", ">264", ">260" });
		pltMap.put(280.0, new String[] { ">271PT" });
		pltMap.put(290.0, new String[] { ">280PT" });
		pltMap.put(300.0, new String[] { ">297" });
		pltMap.put(310.0, new String[] { ">305" });
		pltMap.put(320.0, new String[] { ">313" });
		pltMap.put(340.0, new String[] { ">330" });
		pltMap.put(380.0, new String[] { "370<" });
		pltMap.put(390.0, new String[] { ">382" });
		pltMap.put(400.0, new String[] { ">393" });
		HashMap<Double, String[]> apttMap = new HashMap<>();
		apttMap.put(-1.0, new String[] { "" });
		apttMap.put(181.0, new String[] { ">180" });
		HashMap<Double, String[]> at3Map = new HashMap<>();
		at3Map.put(202.0, new String[] { "202H" });
		at3Map.put(188.0, new String[] { "188H" });
		at3Map.put(172.0, new String[] { "172H" });
		at3Map.put(170.0, new String[] { "170H" });
		at3Map.put(168.0, new String[] { "168H" });
		at3Map.put(166.0, new String[] { "166H" });
		at3Map.put(160.0, new String[] { "160H" });
		at3Map.put(159.0, new String[] { "159H" });
		at3Map.put(158.0, new String[] { "158H" });
		at3Map.put(157.0, new String[] { "157H" });
		at3Map.put(154.0, new String[] { "154H" });
		at3Map.put(153.0, new String[] { "153H" });
		at3Map.put(152.0, new String[] { "152H", "152.0H" });
		at3Map.put(150.0, new String[] { "150H", "150.0H" });
		at3Map.put(149.0, new String[] { "149H" });
		at3Map.put(148.0, new String[] { "148H" });
		at3Map.put(147.0, new String[] { "147H" });
		at3Map.put(146.0, new String[] { "146H" });
		at3Map.put(145.0, new String[] { "145H" });
		at3Map.put(144.0, new String[] { "144H" });
		at3Map.put(143.0, new String[] { "143H" });
		at3Map.put(142.0, new String[] { "142H" });
		at3Map.put(141.0, new String[] { "141H", "141.0H" });
		at3Map.put(140.0, new String[] { "140H", "140.0H" });
		at3Map.put(139.0, new String[] { "139H" });
		at3Map.put(138.0, new String[] { "138H", "138.0H" });
		at3Map.put(137.0, new String[] { "137H" });
		at3Map.put(136.0, new String[] { "136H", "136.0H" });
		at3Map.put(135.0, new String[] { "135H" });
		at3Map.put(134.0, new String[] { "134H", "134.0H" });
		at3Map.put(133.0, new String[] { "133H", "133.0H" });
		at3Map.put(-1.0, new String[] { "" });
		HashMap<Double, String[]> a2piMap = new HashMap<>();
		a2piMap.put(172.0, new String[] { "172H" });
		a2piMap.put(166.0, new String[] { "166H" });
		a2piMap.put(154.0, new String[] { "154H" });
		a2piMap.put(150.0, new String[] { "150H" });
		a2piMap.put(149.0, new String[] { "149H" });
		a2piMap.put(148.0, new String[] { "148H" });
		a2piMap.put(147.0, new String[] { "147H" });
		a2piMap.put(146.0, new String[] { "146H" });
		a2piMap.put(144.0, new String[] { "144H" });
		a2piMap.put(143.0, new String[] { "143H" });
		a2piMap.put(142.0, new String[] { "142H" });
		a2piMap.put(141.0, new String[] { "141H" });
		a2piMap.put(140.0, new String[] { "140H", "140.0H" });
		a2piMap.put(139.0, new String[] { "139H" });
		a2piMap.put(138.0, new String[] { "138H" });
		a2piMap.put(137.0, new String[] { "137.0H" });
		a2piMap.put(136.0, new String[] { "136H", "136.0H" });
		a2piMap.put(135.0, new String[] { "135H", "135.0H" });
		a2piMap.put(134.0, new String[] { "134H", "134.0H" });
		a2piMap.put(133.0, new String[] { "133H", "132H", "131H", "131.0H" });
		a2piMap.put(-1.0, new String[] { "" });
		HashMap<Double, String[]> uproMap = new HashMap<>();
		uproMap.put(-16.0, new String[] { "" });
		uproMap.put(-17.0, new String[] { "TR" });
		uproMap.put(-18.0, new String[] { "ERROR" });
		uproMap.put(-19.0, new String[] { "%%" });
		uproMap.put(-20.0, new String[] { "-" });
		uproMap.put(30.0, new String[] { "+1(30)" });
		uproMap.put(100.0, new String[] { "+2(100)" });
		uproMap.put(500.0, new String[] { "+3(500)" });
		uproMap.put(350.0, new String[] { ">=300" });
		uproMap.put(1100.0, new String[] { ">=1000" });
		HashMap<Double, String[]> iggMap = new HashMap<>();
		iggMap.put(-1.0, new String[] { "" });
		iggMap.put(1.0, new String[] { "<2.00" });
		HashMap<Double, String[]> igaMap = new HashMap<>();
		igaMap.put(-1.0, new String[] { "" });
		igaMap.put(16.0, new String[] { "<33" });
		igaMap.put(15.0, new String[] { "<31", "<30" });
		igaMap.put(1.0, new String[] { "<29" });
		igaMap.put(0.2, new String[] { "<0.4" });
		igaMap.put(0.15, new String[] { "<0.30" });
		igaMap.put(0.0, new String[] { "<0" });
		HashMap<Double, String[]> igmMap = new HashMap<>();
		igmMap.put(-1.0, new String[] { "" });
		igmMap.put(15.0, new String[] { "<30" });
		igmMap.put(14.0, new String[] { "<29", "<28" });
		igmMap.put(13.0, new String[] { "<27" });
		igmMap.put(12.0, new String[] { "<25" });
		igmMap.put(10.0, new String[] { "<20" });
		igmMap.put(0.8, new String[] { "<1.6" });
		igmMap.put(0.5, new String[] { "<1" });
		igmMap.put(0.4, new String[] { "<0.9" });
		igmMap.put(0.3, new String[] { "<0.7", "<0.6" });
		igmMap.put(0.1, new String[] { "<0.30", "<0.3" });
		HashMap<Double, String[]> crpMap = new HashMap<>();
		crpMap.put(-9.0, new String[] { "" });
		crpMap.put(-8.0, new String[] { "-" });
		crpMap.put(-7.0, new String[] { "6+<" });
		crpMap.put(-6.0, new String[] { "6+" });
		crpMap.put(-5.0, new String[] { "5+" });
		crpMap.put(-4.0, new String[] { "4+" });
		crpMap.put(-3.0, new String[] { "3+" });
		crpMap.put(-2.0, new String[] { "2+" });
		crpMap.put(-1.0, new String[] { "+" });
		crpMap.put(80.0, new String[] { ">79.5" });
		crpMap.put(14.0, new String[] { ">7.0" });
		crpMap.put(12.0, new String[] { ">6" });
		crpMap.put(0.15, new String[] { "<0.3" });
		crpMap.put(0.1, new String[] { "<0.2" });
		crpMap.put(0.05, new String[] { "<0.1" });
		crpMap.put(0.001, new String[] { "<0.002" });
		HashMap<Double, String[]> rfMap = new HashMap<>();
		rfMap.put(-1.0, new String[] { "" });
		rfMap.put(2561.0, new String[] { ">2560" });
		rfMap.put(39.0, new String[] { "40>", "<40" });
		rfMap.put(24.9, new String[] { "<25.0" });
		rfMap.put(23.5, new String[] { "<23.6" });
		rfMap.put(23.2, new String[] { "<23.3" });
		rfMap.put(22.9, new String[] { "<23.0" });
		rfMap.put(22.2, new String[] { "<22.3" });
		rfMap.put(21.9, new String[] { "<22.0" });
		rfMap.put(21.2, new String[] { "<21.3" });
		rfMap.put(20.9, new String[] { "<21.0", "<21" });
		rfMap.put(20.7, new String[] { "<20.8" });
		rfMap.put(20.4, new String[] { "<20.5" });
		rfMap.put(19.9, new String[] { "<20.0" });
		rfMap.put(19.7, new String[] { "<19.8" });
		rfMap.put(19.4, new String[] { "<19.5" });
		rfMap.put(19.2, new String[] { "<19.3" });
		rfMap.put(18.9, new String[] { "<19.0", "<19" });
		rfMap.put(18.7, new String[] { "<18.8" });
		rfMap.put(18.4, new String[] { "<18.5" });
		rfMap.put(18.2, new String[] { "<18.3" });
		rfMap.put(17.7, new String[] { "<17.8" });
		rfMap.put(17.4, new String[] { "<17.5" });
		rfMap.put(16.9, new String[] { "<17.0" });
		rfMap.put(16.4, new String[] { "<16.5" });
		rfMap.put(16.2, new String[] { "<16.3" });
		rfMap.put(15.9, new String[] { "<16" });
		rfMap.put(15.7, new String[] { "<15.8" });
		rfMap.put(15.2, new String[] { "<15.3" });
		rfMap.put(11.9, new String[] { "<12" });
		rfMap.put(11.0, new String[] { "<11.1" });
		rfMap.put(10.9, new String[] { "<11" });
		rfMap.put(10.0, new String[] { "<10.6" });
		rfMap.put(9.0, new String[] { "<10" });
		HashMap<Double, String[]> c3Map = new HashMap<>();
		c3Map.put(-1.0, new String[] { "" });
		c3Map.put(2.0, new String[] { "<2.7" });
		c3Map.put(1.0, new String[] { "<2" });
		HashMap<Double, String[]> c4Map = new HashMap<>();
		c4Map.put(-1.0, new String[] { "" });
		c4Map.put(11.0, new String[] { "<12" });
		c4Map.put(6.0, new String[] { "<7", "<6.6", "<6.5" });
		c4Map.put(5.0, new String[] { "<6" });
		c4Map.put(4.0, new String[] { "<5" });
		c4Map.put(3.0, new String[] { "<4" });
		c4Map.put(2.0, new String[] { "<3" });
		c4Map.put(1.0, new String[] { "<2", "<1.4" });
		c4Map.put(0.5, new String[] { "<1" });
		HashMap<Double, String[]> rnpSmSc170SsaSsbCentromeaMap = new HashMap<>();
		rnpSmSc170SsaSsbCentromeaMap.put(-2.0, new String[] { "" });
		rnpSmSc170SsaSsbCentromeaMap.put(-1.0, new String[] { "negative" });
		laboratory = Table.readFrom(path + File.separator + "TSUM_C.CSV", ",", "laboratory", new String[] {
				"id", "patient_id", "date", "got", "gpt", "ldh", "alp", "tp", "alb", "ua", "un", "cre", "t-bil", "t-cho", "tg", "cpk", "glu", "wbc", "rbc", "hgb", "hct", "plt", "pt", "pt_note", "aptt",
				"fg", "at3", "a2pi", "u-pro", "igg", "iga", "igm", "crp", "ra", "rf", "c3", "c4", "rnp", "sm", "sc170", "ssa", "ssb", "centromea", "dna"},
				new RecordFieldParserI[] {
					new RecordFieldParserDouble(), // id
					new RecordFieldParserDouble(), // patient_id
					// 0.0           = GMT: Thursday, January 1, 1970 12:00:00 AM
					// 1.0           = GMT: Thursday, January 1, 1970 12:00:01 AM
					// 946684800.0   = GMT: Saturday, January 1, 2000 12:00:00 AM
					new RecordFieldParserEnumerableDate("yyMMdd", new Date(-2208988800000l), 1.0, 946684800.0, dateMap), // date
					new MedicalRecordFieldParserEnumerableDouble(0.0, 25000.0, -1.0), // got
					new MedicalRecordFieldParserEnumerableDouble(0.0, 5000.0, -1.0), // gpt
					new MedicalRecordFieldParserEnumerableDouble(0.0, 70000.0, -1.0, ldhMap), // ldh
					new MedicalRecordFieldParserEnumerableDouble(0.0, 3200.0, -1.0), // alp
					new MedicalRecordFieldParserEnumerableDouble(0.0, 12.0, -1.0), // tp
					new MedicalRecordFieldParserEnumerableDouble(0.0, 6.0, -1.0), // alb
					new MedicalRecordFieldParserEnumerableDouble(0.0, 33.0, -1.0), // ua
					new MedicalRecordFieldParserEnumerableDouble(0.0, 160.0, -1.0), // un
					new MedicalRecordFieldParserEnumerableDouble(0.0, 18.0, -1.0), // cre
					new MedicalRecordFieldParserEnumerableDouble(0.0, 27.0, -1.0), // t-bil
					new MedicalRecordFieldParserEnumerableDouble(0.0, 1000.0, -1.0), // t-cho
					new MedicalRecordFieldParserEnumerableDouble(0.0, 1500.0, -1.0), // tg
					new MedicalRecordFieldParserEnumerableDouble(0.0, 25000.0, -1.0, cpkMap), // cpk
					new MedicalRecordFieldParserEnumerableDouble(0.0, 1000.0, -1.0), // glu
					new MedicalRecordFieldParserEnumerableDouble(0.0, 120.0, -1.0, wbcMap), // wbc
					new MedicalRecordFieldParserEnumerableDouble(0.0, 10.0, -1.0), // rbc
					new MedicalRecordFieldParserEnumerableDouble(0.0, 20.0, -1.0), // hgb
					new MedicalRecordFieldParserEnumerableDouble(0.0, 60.0, -1.0), // hct
					new MedicalRecordFieldParserEnumerableDouble(0.0, 9000.0, -1.0, pltMap), // plt
					new MedicalRecordFieldParserEnumerableDouble(0.0, 75.0, -1.0), // pt
					new MedicalRecordFieldParserEnumerableDouble(0.0, 185.0, -1.0), // pt_note
					new MedicalRecordFieldParserEnumerableDouble(0.0, 180.0, -1.0, apttMap), // aptt
					new MedicalRecordFieldParserEnumerableDouble(0.0, 1000.0, -1.0), // fg
					new MedicalRecordFieldParserEnumerableDouble(0.0, 200.0, -1.0, at3Map), // at3
					new MedicalRecordFieldParserEnumerableDouble(0.0, 200.0, -1.0, a2piMap), // a2pi
					new MedicalRecordFieldParserEnumerableDouble(-15.0, 1500.0, -16.0, uproMap), // u-pro
					new MedicalRecordFieldParserEnumerableDouble(0.0, 8000.0, -1.0, iggMap), // igg
					new MedicalRecordFieldParserEnumerableDouble(0.0, 2000.0, -1.0, igaMap), // iga
					new MedicalRecordFieldParserEnumerableDouble(0.0, 3000.0, -1.0, igmMap), // igm
					new MedicalRecordFieldParserEnumerableDouble(0.0, 50.0, -9.0, crpMap), // crp
					new MedicalRecordFieldParserPlusMinus(), // ra
					new MedicalRecordFieldParserEnumerableDouble(0.0, 7000.0, -1.0, rfMap), // rf
					new MedicalRecordFieldParserEnumerableDouble(0.0, 230.0, -1.0, c3Map), // c3
					new MedicalRecordFieldParserEnumerableDouble(0.0, 1000.0, -1.0, c4Map), // c4
					new MedicalRecordFieldParserEnumerableDouble(0.0, 256.0, -2.0, rnpSmSc170SsaSsbCentromeaMap), // rnp
					new MedicalRecordFieldParserEnumerableDouble(0.0, 32.0, -2.0, rnpSmSc170SsaSsbCentromeaMap), // sm
					new MedicalRecordFieldParserEnumerableDouble(0.0, 16.0, -2.0, rnpSmSc170SsaSsbCentromeaMap), // sc170
					new MedicalRecordFieldParserEnumerableDouble(0.0, 256.0, -2.0, rnpSmSc170SsaSsbCentromeaMap), // ssa
					new MedicalRecordFieldParserEnumerableDouble(0.0, 128.0, -2.0, rnpSmSc170SsaSsbCentromeaMap), // ssb
					new MedicalRecordFieldParserEnumerableDouble(0.0, 5500.0, -2.0, rnpSmSc170SsaSsbCentromeaMap), // hgb
					new MedicalRecordFieldParserEnumerableDouble(0.0, 200.0, -1.0), // dna
				},
				null, 0, 44, new MedicalLinePreparerLaboratory(",", 44));
	}
	
	public Table patient() {
		return patient;
	}

	public Table specialLab() {
		return specialLab;
	}
	
	public Table laboratory() {
		return laboratory;
	}
	
	public void writeAllTablesTo(String path) throws IOException {
		if (path == null) throw new NullPointerException("path is null");
		patient.writeTo(path);
		specialLab.writeTo(path);
		laboratory.writeTo(path);
	}
}
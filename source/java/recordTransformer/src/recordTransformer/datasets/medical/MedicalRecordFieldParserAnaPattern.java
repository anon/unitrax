package recordTransformer.datasets.medical;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;

import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordFieldParserI;

public class MedicalRecordFieldParserAnaPattern implements RecordFieldParserI {
	
	public static final String[] ANAPATTERNSTRINGS = new String[] {"D", "N", "P", "S"};
	public static final String[] ANACOLUMNNAMES = new String[] {"ana_pattern_d", "ana_pattern_n", "ana_pattern_p", "ana_pattern_s"};
	
	@Override
	public void parse(String field, RecordBuilder recordBuilder) throws ParseException {
		if (field == null) throw new NullPointerException("field is null");
		if (recordBuilder == null) throw new NullPointerException("recordBuilder is null");
		field = field.replaceAll("^\"|\"$", "");
		Double[] anaPatternValues = new Double[ANAPATTERNSTRINGS.length];
		Arrays.fill(anaPatternValues, 0.0);
		for (int i = 0; i < ANAPATTERNSTRINGS.length; i++) {
			if (field.contains(ANAPATTERNSTRINGS[i])) anaPatternValues[i] = 1.0;
		}
		for (int i = 0; i < anaPatternValues.length; i++) recordBuilder.add(anaPatternValues[i]);
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<Double, String[]>[] getDictionaries() {
		HashMap<Double, String[]>[] dictionaries = new HashMap[ANAPATTERNSTRINGS.length];
		Arrays.fill(dictionaries, null);
		return dictionaries;
	}

	@Override
	public String report() {
		return "MedicalRecordFieldParserAnaPattern: no report data available";
	}
}
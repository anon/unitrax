package recordTransformer.datasets.medical;

import recordTransformer.core.Record;
import recordTransformer.core.RecordBuilder;
import recordTransformer.core.RecordJoinerI;

public class MedicalRecordJoinerPatientSpecialLab implements RecordJoinerI {
	
	private static final int[] DIAGNOSISINDEXESA = new int[] {6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
	private static final int[] DIAGNOSISINDEXESB = new int[] {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};
	
	private double sexNull;
	private double dateOfBirthNull;
	private double dateOfInitialDataCollectionNull;
	private double dateOfFirstVisitNull;
	private double dateOfExaminationNull;
	private double admissionNull;
	private double aclIgaNull;
	private double aclIggNull;
	private double aclIgmNull;
	private double anaNull;
	private double anaPatternDNull;
	private double anaPatternNNull;
	private double anaPatternPNull;
	private double anaPatternSNull;
	private double kctNull;
	private double rvvtNull;
	private double lacNull;
	private double thrombosisNull;
	private double budgetNull;
	private String stringNull;
	
	public MedicalRecordJoinerPatientSpecialLab(double sexNull, double dateOfBirthNull, double dateOfInitialDataCollectionNull, double dateOfFirstVisitNull, double dateOfExaminationNull, double admissionNull,
			double aclIgaNull, double aclIggNull, double aclIgmNull, double anaNull, double anaPatternDNull, double anaPatternNNull, double anaPatternPNull, double anaPatternSNull, double kctNull, double rvvtNull, double lacNull, double thrombosisNull, double budgetNull, String stringNull) {
		if (stringNull == null) throw new NullPointerException("stringNull is null");
		this.sexNull = sexNull;
		this.dateOfBirthNull = dateOfBirthNull;
		this.dateOfInitialDataCollectionNull = dateOfInitialDataCollectionNull;
		this.dateOfFirstVisitNull = dateOfFirstVisitNull;
		this.dateOfExaminationNull = dateOfExaminationNull;
		this.admissionNull = admissionNull;
		this.aclIgaNull = aclIgaNull;
		this.aclIggNull = aclIggNull;
		this.aclIgmNull = aclIgmNull;
		this.anaNull = anaNull;
		this.anaPatternDNull = anaPatternDNull;
		this.anaPatternNNull = anaPatternNNull;
		this.anaPatternPNull = anaPatternPNull;
		this.anaPatternSNull = anaPatternSNull;
		this.kctNull = kctNull;
		this.rvvtNull = rvvtNull;
		this.lacNull = lacNull;
		this.thrombosisNull = thrombosisNull;
		this.budgetNull = budgetNull;
		this.stringNull = stringNull;
	}

	@Override
	public Record join(Record recordA, Record[] recordsB) {
		if (recordA == null && recordsB == null) throw new NullPointerException("both inputs are null");
		if (recordA == null && (recordsB.length <= 0 || recordsB.length > 1)) throw new IllegalArgumentException("recordsB must have 1 record");
		if (recordA != null && recordsB != null && (recordsB.length <= 0 || recordsB.length > 1)) throw new IllegalArgumentException("recordsB must have 1 record");
		RecordBuilder recordBuilder = new RecordBuilder();
		
		// id, sex, date_of_birth, date_of_initial_data_collection, date_of_first_visit
		if (recordA != null) for (int i : new int[] { 0, 1, 2, 3, 4 }) recordBuilder.add(recordA.doubleAt(i));
		else {
			recordBuilder.add(recordsB[0].doubleAt(0));
			recordBuilder.add(sexNull);
			recordBuilder.add(dateOfBirthNull);
			recordBuilder.add(dateOfInitialDataCollectionNull);
			recordBuilder.add(dateOfFirstVisitNull);
		}
		// date_of_examination
		if (recordsB != null) recordBuilder.add(recordsB[0].doubleAt(1));
		else recordBuilder.add(dateOfExaminationNull);
		// admission
		if (recordA != null) recordBuilder.add(recordA.doubleAt(5));
		else recordBuilder.add(admissionNull);
		// diagnosis_sjs, diagnosis_sle, diagnosis_behcet, diagnosis_pss, diagnosis_ra, diagnosis_mctd, diagnosis_aps, diagnosis_pm, diagnosis_dm, diagnosis_pn, diagnosis_fuo, diagnosis_aortitis, diagnosis_ip, diagnosis_mra, diagnosis_collagen, diagnosis_raynaud, diagnosis_other
		for (int i = 0; i < DIAGNOSISINDEXESA.length; i++) {
			int indexA = DIAGNOSISINDEXESA[i];
			int indexB = DIAGNOSISINDEXESB[i];
			if (recordA == null) recordBuilder.add(recordsB[0].doubleAt(indexB));
			else if (recordsB == null) recordBuilder.add(recordA.doubleAt(indexA));
			else recordBuilder.add(recordA.doubleAt(indexA) > 0.0 || recordsB[0].doubleAt(indexB) > 0.0 ? 1.0 : 0.0);
		}
		// acl_iga, acl_igg, acl_igm, ana, ana_pattern_d, ana_pattern_n, ana_pattern_p, ana_pattern_s, kct, rvvt, lac, thrombosis
		if (recordsB != null) for (int i : new int[] { 9, 2, 3, 4, 5, 6, 7, 8, 27, 28, 29, 30 }) recordBuilder.add(recordsB[0].doubleAt(i));
		else {
			recordBuilder.add(aclIgaNull);
			recordBuilder.add(aclIggNull);
			recordBuilder.add(aclIgmNull);
			recordBuilder.add(anaNull);
			recordBuilder.add(anaPatternDNull);
			recordBuilder.add(anaPatternNNull);
			recordBuilder.add(anaPatternPNull);
			recordBuilder.add(anaPatternSNull);
			recordBuilder.add(kctNull);
			recordBuilder.add(rvvtNull);
			recordBuilder.add(lacNull);
			recordBuilder.add(thrombosisNull);
		}
		// budget
		if (recordA != null) recordBuilder.add(recordA.doubleAt(23));
		else recordBuilder.add(budgetNull);
		// diagnosis_general
		if (recordA != null) recordBuilder.add(recordA.stringAt(0));
		else recordBuilder.add(stringNull);
		// diagnosis_special, symptoms
		if (recordsB != null) for (int i : new int[] { 0, 1 }) recordBuilder.add(recordsB[0].stringAt(i));
		else {
			recordBuilder.add(stringNull);
			recordBuilder.add(stringNull);
		}
		return recordBuilder.toRecord();
	}
}
package logConverter;

public class LogEntryNumberPartitions extends LogEntry {

	private int setupNumberPartitions;
	private int executeNumberPartitions;
	private int teardownNumberPartitions;

	public LogEntryNumberPartitions(int setupNumberPartitions, int executeNumberPartitions, int teardownNumberPartitions) {
		super("NumberPartitions");
		if (setupNumberPartitions < 0) throw new IllegalArgumentException("setupNumberPartitions cannot be less than 0");
		if (executeNumberPartitions < 0) throw new IllegalArgumentException("executeNumberPartitions cannot be less than 0");
		if (teardownNumberPartitions < 0) throw new IllegalArgumentException("teardownNumberPartitions cannot be less than 0");
		this.setupNumberPartitions = setupNumberPartitions;
		this.executeNumberPartitions = executeNumberPartitions;
		this.teardownNumberPartitions = teardownNumberPartitions;
	}
	
	public int getSetupNumberPartitions() {
		return setupNumberPartitions;
	}

	public int getExecuteNumberPartitions() {
		return executeNumberPartitions;
	}

	public int getTeardownNumberPartitions() {
		return teardownNumberPartitions;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof LogEntryNumberPartitions)) return false;
		if (!super.equals(object)) return false;
		LogEntryNumberPartitions other = (LogEntryNumberPartitions) object;
		if (setupNumberPartitions != other.setupNumberPartitions) return false;
		if (executeNumberPartitions != other.executeNumberPartitions) return false;
		if (teardownNumberPartitions != other.teardownNumberPartitions) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + ";setupNumberPartitions:" + setupNumberPartitions + ";executeNumberPartitions:" + executeNumberPartitions + ";teardownNumberPartitions:" + teardownNumberPartitions;
	}
}
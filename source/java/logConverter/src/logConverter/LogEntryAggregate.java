package logConverter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class LogEntryAggregate extends LogEntry {

	public static LogEntryAggregate average(List<LogEntryAggregate> entries) {
		if (entries == null) throw new NullPointerException("entries is null");
		if (entries.size() < 1) throw new IllegalArgumentException("need at least one entry for average");
		SummaryStatistics valueStats = new SummaryStatistics();
		SummaryStatistics epsilonStats = new SummaryStatistics();
		SummaryStatistics setupTimeStats = new SummaryStatistics();
		SummaryStatistics executeTimeStats = new SummaryStatistics();
		SummaryStatistics bkExecuteTimeStats = new SummaryStatistics();
		SummaryStatistics teardownTimeStats = new SummaryStatistics();
		SummaryStatistics totalTimeStats = new SummaryStatistics();
		String name = entries.get(0).getName();
		String message = entries.get(0).getMessage();
		LogEntryNumberPartitions numberPartitions = entries.get(0).numberPartitions;
		for (LogEntryAggregate entry : entries) {
			if (!name.equals(entry.getName())) throw new IllegalArgumentException("names must match");
			if (!message.equals(entry.getMessage())) throw new IllegalArgumentException("messages must match");
			if (!numberPartitions.equals(entry.numberPartitions)) {
				throw new IllegalArgumentException("numberPartitions must match");
			}
			epsilonStats.addValue(entry.epsilon.getValue());
			valueStats.addValue(entry.value.getValue());
			setupTimeStats.addValue(entry.setupTime.getValue());
			executeTimeStats.addValue(entry.executeTime.getValue());
			bkExecuteTimeStats.addValue(entry.bkExecuteTime.getValue());
			teardownTimeStats.addValue(entry.teardownTime.getValue());
			totalTimeStats.addValue(entry.totalTime.getValue());
		}
		Value epsilon = new Value(epsilonStats.getMean(), epsilonStats.getStandardDeviation());
		Value value = new Value(valueStats.getMean(), valueStats.getStandardDeviation());
		Value setupTime = new Value(setupTimeStats.getMean(), setupTimeStats.getStandardDeviation());
		Value executeTime = new Value(executeTimeStats.getMean(), executeTimeStats.getStandardDeviation());
		Value bkExecuteTime = new Value(bkExecuteTimeStats.getMean(), bkExecuteTimeStats.getStandardDeviation());
		Value teardownTime = new Value(teardownTimeStats.getMean(), teardownTimeStats.getStandardDeviation());
		Value totalTime = new Value(totalTimeStats.getMean(), totalTimeStats.getStandardDeviation());
		return new LogEntryAggregate(name, message, epsilon, value, setupTime, executeTime, bkExecuteTime, teardownTime, totalTime, null, numberPartitions);
	}

	public static LogEntryAggregate averageSkipExtremes(List<LogEntryAggregate> entries, int skip) {
		if (entries == null) throw new NullPointerException("entries is null");
		if (entries.size() < 1 + 2 * skip) throw new IllegalArgumentException("need at least three entries");
		LinkedList<Double> epsilonList = new LinkedList<>();
		LinkedList<Double> valueList = new LinkedList<>();
		LinkedList<Double> setupTimeList = new LinkedList<>();
		LinkedList<Double> executeTimeList = new LinkedList<>();
		LinkedList<Double> bkExecuteTimeList = new LinkedList<>();
		LinkedList<Double> teardownTimeList = new LinkedList<>();
		LinkedList<Double> totalTimeList = new LinkedList<>();
		String name = entries.get(0).getName();
		String message = entries.get(0).getMessage();
		LogEntryNumberPartitions numberPartitions = entries.get(0).numberPartitions;
		for (LogEntryAggregate entry : entries) {
			if (!name.equals(entry.getName())) throw new IllegalArgumentException("names must match");
			if (!message.equals(entry.getMessage())) throw new IllegalArgumentException("messages must match");
			if (!numberPartitions.equals(entry.numberPartitions)) {
				throw new IllegalArgumentException("numberPartitions must match");
			}
			epsilonList.add(entry.epsilon.getValue());
			valueList.add(entry.value.getValue());
			setupTimeList.add(entry.setupTime.getValue());
			executeTimeList.add(entry.executeTime.getValue());
			bkExecuteTimeList.add(entry.bkExecuteTime.getValue());
			teardownTimeList.add(entry.teardownTime.getValue());
			totalTimeList.add(entry.totalTime.getValue());
		}
		Collections.sort(epsilonList);
		Collections.sort(valueList);
		Collections.sort(setupTimeList);
		Collections.sort(executeTimeList);
		Collections.sort(bkExecuteTimeList);
		Collections.sort(teardownTimeList);
		Collections.sort(totalTimeList);
		for (int i = 0; i < skip; i++) epsilonList.removeFirst();
		for (int i = 0; i < skip; i++) valueList.removeFirst();
		for (int i = 0; i < skip; i++) setupTimeList.removeFirst();
		for (int i = 0; i < skip; i++) executeTimeList.removeFirst();
		for (int i = 0; i < skip; i++) bkExecuteTimeList.removeFirst();
		for (int i = 0; i < skip; i++) teardownTimeList.removeFirst();
		for (int i = 0; i < skip; i++) totalTimeList.removeFirst();
		for (int i = 0; i < skip; i++) epsilonList.removeLast();
		for (int i = 0; i < skip; i++) valueList.removeLast();
		for (int i = 0; i < skip; i++) setupTimeList.removeLast();
		for (int i = 0; i < skip; i++) executeTimeList.removeLast();
		for (int i = 0; i < skip; i++) bkExecuteTimeList.removeLast();
		for (int i = 0; i < skip; i++) teardownTimeList.removeLast();
		for (int i = 0; i < skip; i++) totalTimeList.removeLast();
		SummaryStatistics epsilonStats = new SummaryStatistics();
		SummaryStatistics valueStats = new SummaryStatistics();
		SummaryStatistics setupTimeStats = new SummaryStatistics();
		SummaryStatistics executeTimeStats = new SummaryStatistics();
		SummaryStatistics bkExecuteTimeStats = new SummaryStatistics();
		SummaryStatistics teardownTimeStats = new SummaryStatistics();
		SummaryStatistics totalTimeStats = new SummaryStatistics();
		for (Double value : epsilonList) epsilonStats.addValue(value);
		for (Double value : valueList) valueStats.addValue(value);
		for (Double value : setupTimeList) setupTimeStats.addValue(value);
		for (Double value : executeTimeList) executeTimeStats.addValue(value);
		for (Double value : bkExecuteTimeList) bkExecuteTimeStats.addValue(value);
		for (Double value : teardownTimeList) teardownTimeStats.addValue(value);
		for (Double value : totalTimeList) totalTimeStats.addValue(value);
		Value epsilon = new Value(epsilonStats.getMean(), epsilonStats.getStandardDeviation());
		Value value = new Value(valueStats.getMean(), valueStats.getStandardDeviation());
		Value setupTime = new Value(setupTimeStats.getMean(), setupTimeStats.getStandardDeviation());
		Value executeTime = new Value(executeTimeStats.getMean(), executeTimeStats.getStandardDeviation());
		Value bkExecuteTime = new Value(bkExecuteTimeStats.getMean(), bkExecuteTimeStats.getStandardDeviation());
		Value teardownTime = new Value(teardownTimeStats.getMean(), teardownTimeStats.getStandardDeviation());
		Value totalTime = new Value(totalTimeStats.getMean(), totalTimeStats.getStandardDeviation());
		return new LogEntryAggregate(name, message, epsilon, value, setupTime, executeTime, bkExecuteTime, teardownTime, totalTime, null, numberPartitions);
	}
	
	public static Double valueDifference(LogEntryAggregate a, LogEntryAggregate b) {
		if (a == null) throw new NullPointerException("a is null");
		if (b == null) throw new NullPointerException("b is null");
		if (!a.getName().equals(b.getName())) throw new IllegalArgumentException("names must match: " + a.getName() + " " + b.getName());
		return a.value.getValue() - b.value.getValue();
	}

	public static Double executeTimeDifferencePercent(LogEntryAggregate a, LogEntryAggregate b) {
		if (a == null) throw new NullPointerException("a is null");
		if (b == null) throw new NullPointerException("b is null");
		if (!a.getName().equals(b.getName())) throw new IllegalArgumentException("names must match: " + a.getName() + " " + b.getName());
		double aVal = a.executeTime.getValue();
		double bVal = b.executeTime.getValue();
		if (aVal == 0.0 || bVal == 0.0) return Double.NaN;
		return ((aVal - bVal) / aVal) * 100.0;
	}
	
	private String message;
	private Value epsilon;
	private Value value;
	private Value totalTime;
	private Value setupTime;
	private Value executeTime;
	private Value bkExecuteTime;
	private Value teardownTime;
	private List<LogEntryRange> ranges;
	private LogEntryNumberPartitions numberPartitions;
	
	public LogEntryAggregate(String name, String message, Value epsilon, Value value, Value setupTime, Value executeTime, Value bkExecuteTime, Value teardownTime,
			Value totalTime, List<LogEntryRange> ranges, LogEntryNumberPartitions numberPartitions) {
		super(name);
		if (message == null) throw new NullPointerException("message is null");
		if (epsilon == null) throw new NullPointerException("epsilon is null");
		if (value == null) throw new NullPointerException("value is null");
		if (setupTime == null) throw new NullPointerException("setupTime is null");
		if (executeTime == null) throw new NullPointerException("executeTime is null");
		if (bkExecuteTime == null) throw new NullPointerException("bkExecuteTime is null");
		if (teardownTime == null) throw new NullPointerException("teardownTime is null");
		if (totalTime == null) throw new NullPointerException("totalTime is null");
		if (numberPartitions == null) throw new NullPointerException("numberPartitions is null");
		this.message = message;
		this.epsilon = epsilon;
 		this.value = value;
 		this.totalTime = totalTime;
		this.setupTime = setupTime;
		this.executeTime = executeTime;
		this.bkExecuteTime = bkExecuteTime;
		this.teardownTime = teardownTime;
		this.ranges = ranges;
		this.numberPartitions = numberPartitions;
	}
	
	public String getMessage() {
		return message;
	}

	public Value getEpsilon() {
		return epsilon;
	}
	
	public Value getValue() {
		return value;
	}

	public Value getTotalTime() {
		return totalTime;
	}
	
	public Value getSetupTime() {
		return setupTime;
	}
	
	public Value getExecuteTime() {
		return executeTime;
	}

	public Value getBkExecuteTime() {
		return bkExecuteTime;
	}

	public Value getTeardownTime() {
		return teardownTime;
	}
	
	public double getSetupNumberPartitions() {
		return numberPartitions.getSetupNumberPartitions();
	}

	public double getExecuteNumberPartitions() {
		return numberPartitions.getExecuteNumberPartitions();
	}

	public double getTeardownNumberPartitions() {
		return numberPartitions.getTeardownNumberPartitions();
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof LogEntryAggregate)) return false;
		if (!super.equals(object)) return false;
		LogEntryAggregate other = (LogEntryAggregate) object;
		if (!message.equals(other.message)) return false;
		if (!epsilon.equals(other.epsilon) )return false;
		if (!value.equals(other.value) )return false;
		if (!setupTime.equals(other.setupTime)) return false;
		if (!executeTime.equals(other.executeTime)) return false;
		if (!bkExecuteTime.equals(other.bkExecuteTime)) return false;
		if (!teardownTime.equals(other.teardownTime)) return false;
		if ((ranges == null && other.ranges != null) || (ranges != null && other.ranges == null)) return false;
		if (ranges != null && ranges.size() != other.ranges.size()) return false;
		if (ranges != null) for (int i = 0; i < ranges.size(); i++) if (!ranges.get(i).equals(other.ranges.get(i))) return false;
		if (!numberPartitions.equals(other.numberPartitions)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(super.toString() + ";");
		buf.append("message:" + message + ";");
		buf.append("epsilon:" + epsilon.toString() + ";");
		buf.append("value:" + value.toString() + ";");
		buf.append("setupTime:" + setupTime.toString() + ";");
		buf.append("executeTime:" + executeTime.toString() + ";");
		buf.append("bkExecuteTime:" + bkExecuteTime.toString() + ";");
		buf.append("teardownTime:" + teardownTime.toString() + ";");
		buf.append("numberPartitions:" + numberPartitions.toString() + ";");
		if (ranges != null) for (LogEntry range : ranges) buf.append("\n\t\t" + range.toString());
		return buf.toString();
	}
}
package logConverter;

public class LogEntry {

	private String name;
	
	public LogEntry(String name) {
		if (name == null) throw new NullPointerException("name is null");
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof LogEntry)) return false;
		LogEntry other = (LogEntry) object;
		if (!name.equals(other.name)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "LogEntry: " + name;
	}
}
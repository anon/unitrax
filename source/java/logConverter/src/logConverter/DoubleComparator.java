package logConverter;

import java.util.Comparator;

public class DoubleComparator implements Comparator<Double> {
	
	private double tolerance;
	
	public DoubleComparator(double tolerance) {
		if (tolerance < 0.0) throw new IllegalArgumentException("tolerance is below zero");
		this.tolerance = tolerance;
	}
	
	@Override
	public int compare(Double o1, Double o2) {
		if (o1 - o2 > tolerance) return 1;
		if (o2 - o1 > tolerance) return -1;
		return 0;
	}
}
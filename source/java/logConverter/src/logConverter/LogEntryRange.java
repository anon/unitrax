package logConverter;

public class LogEntryRange extends LogEntry {

	private String fieldName;
	private long low;
	private long high;
	
	public LogEntryRange(String fieldName, long low, long high) {
		super("Range");
		if (fieldName == null) throw new NullPointerException("fieldName is null");
		this.fieldName = fieldName;
		this.low = low;
		this.high = high;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof LogEntryRange)) return false;
		if (!super.equals(object)) return false;
		LogEntryRange other = (LogEntryRange) object;
		if (!fieldName.equals(other.fieldName)) return false;
		if (low != other.low) return false;
		if (high != other.high) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + ";fieldName:" + fieldName;
	}
}
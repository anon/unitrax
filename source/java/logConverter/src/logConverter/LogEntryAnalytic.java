package logConverter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class LogEntryAnalytic extends LogEntry {

	public static LogEntryAnalytic average(List<LogEntryAnalytic> entries) {
		if (entries == null) throw new NullPointerException("entries is null");
		if (entries.size() < 2) throw new IllegalArgumentException("need at least two entries for average");
		long id = entries.get(0).id;
		String message = entries.get(0).message;
		SummaryStatistics timeStats = new SummaryStatistics();
		for (LogEntryAnalytic entry : entries) {
			if (entry.id != id) throw new IllegalArgumentException("ids of entries must match");
			if (!entry.message.equals(message)) throw new IllegalArgumentException("messages of entries must match");
			timeStats.addValue(entry.time.getValue());
		}
		Value time = new Value(timeStats.getMean(), timeStats.getStandardDeviation());
		int size = entries.get(0).aggregates.size();
		for (LogEntryAnalytic entry : entries) {
			if (entry.aggregates.size() != size) {
				throw new IllegalArgumentException("queries must have same size");
			}
		}
		List<LogEntryAggregate> resultQueries = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			List<LogEntryAggregate> currentQueries = new LinkedList<>();
			for (LogEntryAnalytic entry : entries) currentQueries.add(entry.aggregates.get(i));
			resultQueries.add(LogEntryAggregate.average(currentQueries));
		}
		return new LogEntryAnalytic(id, time, message, resultQueries);
	}
	
	public static LogEntryAnalytic averageSkipExtremes(List<LogEntryAnalytic> entries, int skip) {
		if (entries == null) throw new NullPointerException("entries is null");
		if (entries.size() < 1 + 2 * skip) throw new IllegalArgumentException("need at least three entries");
		long id = entries.get(0).id;
		String message = entries.get(0).message;
		LinkedList<Double> values = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) {
			if (entry.id != id) throw new IllegalArgumentException("ids of entries must match");
			if (!entry.message.equals(message)) throw new IllegalArgumentException("messages of entries must match");
			values.add(entry.time.getValue());
		}
		Collections.sort(values);
		for (int i = 0; i < skip; i++) values.removeFirst();
		for (int i = 0; i < skip; i++) values.removeLast();
		SummaryStatistics timeStats = new SummaryStatistics();
		for (Double value : values) timeStats.addValue(value);
		Value time = new Value(timeStats.getMean(), timeStats.getStandardDeviation());
		int size = entries.get(0).aggregates.size();
		for (LogEntryAnalytic entry : entries) {
			if (entry.aggregates.size() != size) {
				throw new IllegalArgumentException("queries must have same size");
			}
		}
		List<LogEntryAggregate> resultQueries = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			List<LogEntryAggregate> currentQueries = new LinkedList<>();
			for (LogEntryAnalytic entry : entries) currentQueries.add(entry.aggregates.get(i));
			resultQueries.add(LogEntryAggregate.averageSkipExtremes(currentQueries, skip));
		}
		return new LogEntryAnalytic(id, time, message, resultQueries);
	}
	
	public static List<Double> valueDifference(LogEntryAnalytic a, LogEntryAnalytic b) {
		if (a == null) throw new NullPointerException("a is null");
		if (b == null) throw new NullPointerException("b is null");
		if (a.aggregates.size() != b.aggregates.size()) throw new IllegalArgumentException("queries must have same size; was " + a.aggregates.size() + " vs. " + b.aggregates.size());
		List<Double> resultList = new LinkedList<>();
		for (int i = 0; i < a.aggregates.size(); i++) resultList.add(LogEntryAggregate.valueDifference(a.aggregates.get(i), b.aggregates.get(i)));
		return resultList;
	}

	public static List<Double> executeTimeDifferencePercent(LogEntryAnalytic a, LogEntryAnalytic b) {
		if (a == null) throw new NullPointerException("a is null");
		if (b == null) throw new NullPointerException("b is null");
		if (a.aggregates.size() != b.aggregates.size()) throw new IllegalArgumentException("queries must have same size; was " + a.aggregates.size() + " vs. " + b.aggregates.size());
		List<Double> resultList = new LinkedList<>();
		for (int i = 0; i < a.aggregates.size(); i++) resultList.add(LogEntryAggregate.executeTimeDifferencePercent(a.aggregates.get(i), b.aggregates.get(i)));
		return resultList;
	}
	
	private long id;
	private Value time;
	private String message;
	private List<LogEntryAggregate> aggregates;
	
	public LogEntryAnalytic(long id, Value time, String message, List<LogEntryAggregate> aggregates) {
		super("Analytic");
		if (time == null) throw new NullPointerException("time is null");
		if (message == null) throw new NullPointerException("message is null");
		if (aggregates == null) throw new NullPointerException("aggregates is null");
		this.id = id;
		this.time = time;
		this.message = message;
		this.aggregates = aggregates;
	}
	
	public List<Value> getEpsilons() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getEpsilon());
		return list;
	}

	public List<Value> getValues() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getValue());
		return list;
	}
	
	public List<Value> getTotalTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getTotalTime());
		return list;
	}
	
	public List<Value> getSetupTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getSetupTime());
		return list;
	}
	
	public List<Value> getExecuteTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getExecuteTime());
		return list;
	}

	public List<Value> getBkExecuteTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getBkExecuteTime());
		return list;
	}
	
	public List<Value> getTeardownTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getTeardownTime());
		return list;
	}

	public List<Double> getSetupNumberPartitions() {
		List<Double> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getSetupNumberPartitions());
		return list;
	}

	public List<Double> getExecuteNumberPartitions() {
		List<Double> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getExecuteNumberPartitions());
		return list;
	}

	public List<Double> getTeardownNumberPartitions() {
		List<Double> list = new LinkedList<>();
		for (LogEntryAggregate query : aggregates) list.add(query.getTeardownNumberPartitions());
		return list;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof LogEntryAnalytic)) return false;
		if (!super.equals(object)) return false;
		LogEntryAnalytic other = (LogEntryAnalytic) object;
		if (id != other.id) return false;
		if (!time.equals(other.time)) return false;
		if (!message.equals(other.message)) return false;
		if (aggregates.size() != other.aggregates.size()) return false;
		for (int i = 0; i < aggregates.size(); i++) if (!aggregates.get(i).equals(other.aggregates.get(i))) return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(super.toString() + ";");
		buf.append("id:" + id + ";");
		buf.append("time:" + time.toString() + ";");
		buf.append("message:" + message + ";");
		buf.append("\nqueries:");
		for (LogEntryAggregate query : aggregates) buf.append("\n\t" + query.toString());
		return buf.toString();
	}
}
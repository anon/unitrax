package logConverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Parser {

	public static Log[][] generateLogs(String[] databases, String[] systems, String kind, String instancePath, int runs, boolean filterDummies, int skip) throws NumberFormatException, IOException {
		String[] systemPaths = systemPaths(databases, systems, kind, instancePath);
		String[][] logFileNames = logFileNames(systemPaths, runs);
		
		int numLogs = runs;
		if (numLogs == 1) numLogs = 2;
		Log[][] logs = new Log[numLogs][logFileNames.length];
		for (int i = 0; i < logFileNames.length; i++) {
			List<Log> list = new ArrayList<>(numLogs - 1);
			for (int j = 1; j < numLogs; j++) {
				if (logFileNames[i] == null) logs[j][i] = null;
				else {
					logs[j][i] = Parser.parseLogFile(logFileNames[i][j - 1], filterDummies);
					list.add(logs[j][i]);
				}
			}
			if (list.size() == 0) logs[0][i] = null;
			else if (list.size() == 1) logs[0][i] = list.get(0);
			else if (list.size() < 1 + 2 * skip) logs[0][i] = Log.average(list);
			else logs[0][i] = Log.averageSkipExtremes(list, skip);
		}
		return logs;
	}
	
	private static String[] systemPaths(String[] databases, String[] systems, String kind, String instancePath) {
		String[] ret = new String[databases.length * systems.length];
		for (int i = 0; i < databases.length; i++) for (int j = 0; j < systems.length; j++) {
			if (databases[i] != null && systems[j] != null) ret[i * systems.length + j] = instancePath + File.separator + databases[i] + "-" + systems[j] + "-" + kind;
			else ret[i * systems.length + j] = null;
		}
		return ret;
	}
	
	private static String[][] logFileNames(String[] systemPaths, int runs) {
		String[][] ret = new String[systemPaths.length][];
		for (int i = 0; i < systemPaths.length; i++) {
			if (systemPaths[i] == null) ret[i] = null;
			else if (runs == 1) ret[i] = new String[] { systemPaths[i] + File.separator + "run0_results.csv" };
			else {
				ret[i] = new String[runs - 1];
				for (int j = 1; j < runs; j++) ret[i][j - 1] = systemPaths[i] + File.separator + "run" + j + "_results.csv";
			}
		}
		return ret;
	}
	
	private static Log parseLogFile(String fileName, boolean filterDummies) throws IOException, NumberFormatException {
		if (fileName == null) throw new NullPointerException("fileName is null");
		FileReader fileReader = new FileReader(fileName);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		boolean aggregateHappened = false;
		List<LogEntryAnalytic> lastAnalytic = new LinkedList<>();
		List<LogEntryAggregate> lastAggregate = new LinkedList<>();
		List<LogEntryRange> lastRange = new LinkedList<>();
		LogEntryNumberPartitions lastNumberPartitions = null;
		LogEntryNumberPartitions currentLastNumberPartitions = null;
		while ((line = bufferedReader.readLine()) != null) {
			String[] words = line.split(";");
			switch (words[0]) {
			case "Range":
				if (aggregateHappened) {
					lastRange = new LinkedList<>();
					aggregateHappened = false;
				}
				lastRange.add(new LogEntryRange(words[1], Long.parseLong(words[2]), Long.parseLong(words[3])));
				break;
			case "NumberPartitions":
				if (lastNumberPartitions != null) throw new IllegalStateException("cannot have new number of partitions without the old one being used by a primitive query");
				lastNumberPartitions = new LogEntryNumberPartitions(Integer.parseInt(words[1]), Integer.parseInt(words[2]), Integer.parseInt(words[3]));
				break;
			case "Aggregate":
				currentLastNumberPartitions = new LogEntryNumberPartitions(1, 1, 1);
				if (lastNumberPartitions != null) currentLastNumberPartitions = lastNumberPartitions;
				double epsilon = (new Long(Long.parseLong(words[3]))).doubleValue();
				double value = Double.parseDouble(words[4]);
				double setupTime = Double.parseDouble(words[5]);
				double executeTime = Double.parseDouble(words[6]);
				double bkExecuteTime = Double.parseDouble(words[7]);
				double teardownTime = Double.parseDouble(words[8]);
				LogEntryAggregate entry = new LogEntryAggregate(words[1], words[2], new Value(epsilon, 0.0), new Value(value, 0.0), new Value(setupTime, 0.0), new Value(executeTime, 0.0),
						new Value(bkExecuteTime, 0.0), new Value(teardownTime, 0.0), new Value(setupTime + executeTime + teardownTime, 0.0), lastRange, currentLastNumberPartitions);
				if (!(filterDummies && words[1].equals("Dummy"))) lastAggregate.add(entry);
				lastNumberPartitions = null;
				aggregateHappened = true;
				break;
			case "Analytic":
				if (lastNumberPartitions != null) throw new IllegalStateException("cannot have number of partitions for analytic");
				lastAnalytic.add(new LogEntryAnalytic(Long.parseLong(words[1]), new Value(Double.parseDouble(words[2]), 0.0), words[3], lastAggregate));
				lastAggregate = new LinkedList<>();
				lastRange = new LinkedList<>();
				aggregateHappened = false;
				break;
			default:
				throw new IllegalStateException("Unrecognized line: " + line);
			}
		}
		fileReader.close();
		if (lastNumberPartitions != null) throw new IllegalStateException("cannot have number of partitions at end");
		if (lastRange.size() > 0) throw new IllegalStateException("cannot have ranges at end");
		if (lastAggregate.size() > 0) throw new IllegalStateException("cannot have aggregates at end");
		return new Log(fileName, lastAnalytic);
	}

	public static List<List<double[]>> generateBudgetUsedLists(String[] databases, String[] systems, String kind, String instancePath) throws NumberFormatException, IOException {
		String[] systemPaths = systemPaths(databases, systems, kind, instancePath);
		String[] budgetUsedFileNames = budgetUsedFileNames(systemPaths);
		List<List<double[]>> data = new ArrayList<List<double[]>>(budgetUsedFileNames.length);
		for (int i = 0; i < budgetUsedFileNames.length; i++) {
			if (budgetUsedFileNames[i] == null) data.add(null);
			else data.add(parseBudgetUsedFile(budgetUsedFileNames[i]));
		}
		return data;
	}
	
	private static String[] budgetUsedFileNames(String[] systemPaths) {
		String[] ret = new String[systemPaths.length];
		for (int i = 0; i < systemPaths.length; i++) {
			if (systemPaths[i] == null) ret[i] = null;
			else ret[i] = systemPaths[i] + File.separator + "run0_budgetUsed.csv";
		}
		return ret;
	}
	
	private static List<double[]> parseBudgetUsedFile(String fileName) throws IOException, NumberFormatException {
		if (fileName == null) throw new NullPointerException("fileName is null");
		FileReader fileReader = new FileReader(fileName);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		List<double[]> counts = new LinkedList<>();
		while ((line = bufferedReader.readLine()) != null) {
			String[] words = line.split(",");
			double budget = Double.parseDouble(words[0]);
			double number = new Integer(Integer.parseInt(words[1])).doubleValue();
			counts.add(new double[] { budget, number });
		}
		fileReader.close();
		return counts;
	}
}
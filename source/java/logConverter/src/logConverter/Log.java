package logConverter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Log {
	
	public static List<double[]> rowsTotalTimes(Log[] logs, boolean ste) {
		List<List<Value>> lists = new ArrayList<List<Value>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getTotalTimes());
		}
		return Data.generateRowsValue(lists, ste);
	}

	public static List<double[]> rowsSetupTimes(Log[] logs, boolean ste) {
		List<List<Value>> lists = new ArrayList<List<Value>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getSetupTimes());
		}
		return Data.generateRowsValue(lists, ste);
	}

	public static List<double[]> rowsExecuteTimes(Log[] logs, boolean ste) {
		List<List<Value>> lists = new ArrayList<List<Value>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getExecuteTimes());
		}
		return Data.generateRowsValue(lists, ste);
	}

	public static List<double[]> rowsTeardownTimes(Log[] logs, boolean ste) {
		List<List<Value>> lists = new ArrayList<List<Value>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getTeardownTimes());
		}
		return Data.generateRowsValue(lists, ste);
	}
	
	public static List<double[]> rowsSetupPartitions(Log[] logs) {
		List<List<Double>> lists = new ArrayList<List<Double>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getSetupNumberPartitions());
		}
		return Data.generateRowsDouble(lists);
	}
	
	public static List<double[]> rowsExecutePartitions(Log[] logs) {
		List<List<Double>> lists = new ArrayList<List<Double>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getExecuteNumberPartitions());
		}
		return Data.generateRowsDouble(lists);
	}
	
	public static List<double[]> rowsTeardownPartitions(Log[] logs) {
		List<List<Double>> lists = new ArrayList<List<Double>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getTeardownNumberPartitions());
		}
		return Data.generateRowsDouble(lists);
	}
	
	public static List<double[]> rowsValues(Log[] logs, boolean ste) {
		List<List<Value>> lists = new ArrayList<List<Value>>(logs.length);
		for (int i = 0; i < logs.length; i++) {
			if (logs[i] == null) lists.add(null);
			else lists.add(logs[i].getValues());
		}
		return Data.generateRowsValue(lists, ste);
	}
	
	public static Log average(List<Log> logs) {
		if (logs == null) throw new NullPointerException("logs is null");
		if (logs.size() < 2) throw new IllegalArgumentException("need at least two logs for average");
		int size = logs.get(0).entries.size();
		for (Log log : logs) if (log.entries.size() != size) throw new IllegalArgumentException("logs must have same size");
		List<LogEntryAnalytic> resultEntries = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			List<LogEntryAnalytic> currentEntries = new LinkedList<>();
			for (Log log : logs) currentEntries.add(log.entries.get(i));
			resultEntries.add(LogEntryAnalytic.average(currentEntries));
		}
		StringBuffer buf = new StringBuffer();
		buf.append("Average:");
		for (Log log : logs) buf.append(log.name + ";");
		return new Log(buf.toString(), resultEntries);
	}
	
	public static Log averageSkipExtremes(List<Log> logs, int skip) {
		if (logs == null) throw new NullPointerException("logs is null");
		if (logs.size() < 3) throw new IllegalArgumentException("need at least three logs");
		int size = logs.get(0).entries.size();
		for (Log log : logs) if (log.entries.size() != size) throw new IllegalArgumentException("logs must have same size");
		List<LogEntryAnalytic> resultEntries = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			List<LogEntryAnalytic> currentEntries = new LinkedList<>();
			for (Log log : logs) currentEntries.add(log.entries.get(i));
			resultEntries.add(LogEntryAnalytic.averageSkipExtremes(currentEntries, skip));
		}
		StringBuffer buf = new StringBuffer();
		buf.append("Average:");
		for (Log log : logs) buf.append(log.name + ";");
		return new Log(buf.toString(), resultEntries);
	}
	
	public static List<Double> valueDifferences(Log a, Log b) {
		if (a == null) throw new NullPointerException("a is null");
		if (b == null) throw new NullPointerException("b is null");
		if (a.entries.size() != b.entries.size()) throw new IllegalArgumentException("logs must have same size");
		List<Double> list = new LinkedList<>();
		for (int i = 0; i < a.entries.size(); i++) list.addAll(LogEntryAnalytic.valueDifference(a.entries.get(i), b.entries.get(i)));
		return list;
	}

	public static List<Double> executeTimeDifferencesPercent(Log a, Log b) {
		if (a == null) throw new NullPointerException("a is null");
		if (b == null) throw new NullPointerException("b is null");
		if (a.entries.size() != b.entries.size()) throw new IllegalArgumentException("logs must have same size");
		List<Double> list = new LinkedList<>();
		for (int i = 0; i < a.entries.size(); i++) list.addAll(LogEntryAnalytic.executeTimeDifferencePercent(a.entries.get(i), b.entries.get(i)));
		return list;
	}
	
	private String name;
	private List<LogEntryAnalytic> entries;
	
	public Log(String name, List<LogEntryAnalytic> entries) {
		if (name == null) throw new NullPointerException("name is null");
		if (entries == null) throw new NullPointerException("entries is null");
		this.name = name;
		this.entries = entries;
	}
	
	public List<Value> getEpsilons() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getEpsilons());
		return list;
	}
	
	public List<Value> getValues() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getValues());
		return list;
	}
	
	public List<Value> getTotalTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getTotalTimes());
		return list;
	}
	
	public List<Value> getSetupTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getSetupTimes());
		return list;
	}
	
	public List<Value> getExecuteTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getExecuteTimes());
		return list;
	}

	public List<Value> getBkExecuteTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getBkExecuteTimes());
		return list;
	}
	
	public List<Value> getTeardownTimes() {
		List<Value> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getTeardownTimes());
		return list;
	}
	
	public List<Double> getSetupNumberPartitions() {
		List<Double> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getSetupNumberPartitions());
		return list;
	}
	
	public List<Double> getExecuteNumberPartitions() {
		List<Double> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getExecuteNumberPartitions());
		return list;
	}
	
	public List<Double> getTeardownNumberPartitions() {
		List<Double> list = new LinkedList<>();
		for (LogEntryAnalytic entry : entries) list.addAll(entry.getTeardownNumberPartitions());
		return list;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Log)) return false;
		Log other = (Log) object;
		if (!name.equals(other.name)) return false;
		for (LogEntryAnalytic entry : entries) if (!other.entries.contains(entry)) return false;
		return true;
	}
	
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append("Log: name:" + name + ";");
		for (LogEntryAnalytic entry : entries) buf.append("\n" + entry.toString());
		return buf.toString();
	}
}
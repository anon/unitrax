package logConverter;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ExternalCommand {
	
	public static void execute(List<String> command) throws IOException, InterruptedException, ExecutionException {
		ProcessBuilder builder = new ProcessBuilder(command);
		builder.inheritIO();
		Process process = builder.start();
		int exitCode = process.waitFor();
		assert exitCode == 0;
		process.destroy();
	}
}
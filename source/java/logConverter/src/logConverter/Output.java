package logConverter;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Output {

	private static String[] databases = new String[] { "Disk", "Mem" };
	private static String[] systems = new String[] { "Direct", "PINQ", "Always-BK", "Always-BK-Even", "Threshold-BK", "Threshold-BK-Even" };
	private static String[] kinds = new String[] { "Runtime", "Budget" };
	
	private static String output = "evaluation01";
	private static int runs = 8;
	private static int skip = 1;
	private static double percentRate = 0.05;
	private static String instanceName = "2200_2019-01-01_00-00-00-000";
	private static String instancePath = File.separator + "home" + File.separator + "user" + File.separator + "unitrax" + File.separator +
			"experiments" + File.separator + "financial" + File.separator + "experiment" + File.separator + instanceName;
	private static String scriptPath = File.separator + "home" + File.separator + "user" + File.separator + "unitrax" + File.separator + 
			"gitlab" + File.separator + "unitrax" + File.separator + "scripts" + File.separator + "plot" + File.separator + "java";
	private static String gnuplot = File.separator + "usr" + File.separator + "local" + File.separator + "bin" + File.separator + "gnuplot";
	
	
	private static String outputPath = instancePath + File.separator + "output" + File.separator + output;
	private static String plotPath = instancePath + File.separator + "plot" + File.separator + output;
	private static boolean filterDummies = true;

	private static String fileNameSortedAll = "sortedAll";
	private static String fileNameSortedPercent = "sortedPercent";
	private static String fileNameUnsortedAll = "unsortedAll";
	private static String fileNameUnsortedPercent = "unsortedPercent";
	private static String fileNameBudgets = "budgets";
	
	private static String[] generateHeader(String[] additionalFirst, String[] databases, String[] systems, boolean ste, int skipSte) {
		int additionalIndex = 0;
		if (additionalFirst != null) additionalIndex = additionalFirst.length;
		int numCols = databases.length * systems.length + additionalIndex;
		String[] ret = new String[numCols];
		for (int i = 0; i < databases.length; i++) {
			for (int j = 0; j < systems.length; j++) {
				if (databases[i] != null && systems[j] != null) ret[i * systems.length + j + additionalIndex] = databases[i] + "-" + systems[j];
				else ret[i * systems.length + j + additionalIndex] = "missing";
			}
		}
		if (additionalFirst != null) for (int i = 0; i < additionalFirst.length; i++) ret[i] = additionalFirst[i];

		if (ste) {
			numCols += databases.length * systems.length + additionalIndex - skipSte;
			String[] result = new String[numCols];
			int index = 0;
			for (int i = 0; i < ret.length; i++) {
				result[index] = ret[i];
				index++;
				if (skipSte <= i) {
					result[index] = "StE " + ret[i];
					index++;
				}
			}
			return result;
		}
		return ret;
	}
	
	private static String[] generateRelativeHeader(String[] additionalFirst, String[] databases, String[] systems, int relativeIndex, int skip) {
		String[] headers = generateHeader(additionalFirst, databases, systems, false, 0);
		String relativeHeader = headers[relativeIndex];
		for (int i = skip; i < headers.length; i++) {
			if (headers[i].equals("missing")) continue;
			headers[i] = headers[i] + "/" + relativeHeader;
		}
		return headers;
	}
	
	private static void plot(String scriptName, String inputFileName, String outputFileName) throws IOException, InterruptedException, ExecutionException {
		ExternalCommand.execute(Arrays.asList(gnuplot, "-c", scriptPath + File.separator + scriptName,
				outputPath + File.separator + inputFileName, 
				plotPath + File.separator + outputFileName));
	}
	
	private static void outputBudgetUsedCDF(String kind) {
		try {
			List<List<double[]>> budgetUsedLists = Parser.generateBudgetUsedLists(databases, systems, kind, instancePath);
			List<List<double[]>> budgetUsedListsRelative = Data.relativeBudgetNumbers(budgetUsedLists, 1);
			List<double[]> cdf = Data.countedValuesToCdf(budgetUsedListsRelative);
			Result result = new Result(generateRelativeHeader(new String[] { "cdf" }, databases, systems, 1 + 1, 1), cdf);
			result.writeTo(outputPath + File.separator + kind + "-" + fileNameBudgets + "-cdf.csv");
			plot("budgets.gp", kind + "-" + fileNameBudgets + "-cdf.csv", kind + "-" + fileNameBudgets + "-cdf.eps");
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private static void outputValueDifferences(int[] percentIndexes, Log[][] logsForKind, String kind) {
		try {
			for (int i = 0; i < logsForKind.length; i++) {
				String run = "run" + i;
				if (i == 0) run = "average";
				Log[] logsRun = logsForKind[i];
			
				List<double[]> unsortedRows = Log.rowsValues(logsRun, false);
				Result unsortedResultValues = new Result(generateHeader(null, databases, systems, false, 0), unsortedRows);
				unsortedResultValues.writeTo(outputPath + File.separator + kind + "-" + run + "-Values-" + fileNameUnsortedAll + ".csv");
				unsortedResultValues.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-Values-" + fileNameUnsortedAll + ".csv");
				plot("values.gp", kind + "-" + run + "-Values-" + fileNameUnsortedAll + ".csv", kind + "-" + run + "-Values-" + fileNameUnsortedAll + ".eps");	
				
				List<double[]> unsortedDifferenceRows = Data.differenceNumbersAbs(unsortedRows, 0);
				Result unsortedResult = new Result(generateHeader(null, databases, systems, false, 0), unsortedDifferenceRows);
				unsortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedAll + ".csv");
				unsortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedAll + ".csv");
				plot("values.gp", kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedAll + ".csv", kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedAll + ".eps");				

				List<double[]> unsortedDifferenceRowsPercent = Data.filterBy(unsortedDifferenceRows, percentIndexes);
				Result unsortedPercentResult = new Result(generateHeader(null, databases, systems, false, 0), unsortedDifferenceRowsPercent);
				unsortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedPercent + ".csv");
				unsortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedPercent + ".csv");
				plot("values.gp", kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedPercent + ".csv", kind + "-" + run + "-ValueDifferences-" + fileNameUnsortedPercent + ".eps");		
				
				List<double[]> unsortedCdfRows = Data.cdf(unsortedDifferenceRows, 0.0, 0.1, 10000000, true, 100);
				Result unsortedCdfResult = new Result(generateRelativeHeader(new String[] { "cdf" }, databases, systems, 0 + 1, 1), unsortedCdfRows);
				unsortedCdfResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ValueDifferences-cdf.csv");
				plot("valuesCdf.gp", kind + "-" + run + "-ValueDifferences-cdf.csv", kind + "-" + run + "-ValueDifferences-cdf.eps");
				
				List<double[]> unsortedRowsExecute = Log.rowsExecuteTimes(logsRun, true);
				int[] sortFilter = Data.getSortFilter(unsortedRowsExecute, 0);
				List<double[]> sortedDifferenceRows = Data.filterBy(unsortedDifferenceRows, sortFilter);
				Result sortedResult = new Result(generateHeader(null, databases, systems, false, 0), sortedDifferenceRows);
				sortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ValueDifferences-" + fileNameSortedAll + ".csv");
				sortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ValueDifferences-" + fileNameSortedAll + ".csv");
				plot("values.gp", kind + "-" + run + "-ValueDifferences-" + fileNameSortedAll + ".csv", kind + "-" + run + "-ValueDifferences-" + fileNameSortedAll + ".eps");
				
				List<double[]> unsortedRowsPercentExecute = Data.filterBy(unsortedRowsExecute, percentIndexes);
				int[] sortFilterPercent = Data.getSortFilter(unsortedRowsPercentExecute, 0);
				List<double[]> sortedDifferenceRowsPercent = Data.filterBy(unsortedDifferenceRowsPercent, sortFilterPercent);
				Result sortedPercentResult = new Result(generateHeader(null, databases, systems, false, 0), sortedDifferenceRowsPercent);				
				sortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ValueDifferences-" + fileNameSortedPercent + ".csv");
				sortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ValueDifferences-" + fileNameSortedPercent + ".csv");
				plot("values.gp", kind + "-" + run + "-ValueDifferences-" + fileNameSortedPercent + ".csv", kind + "-" + run + "-ValueDifferences-" + fileNameSortedPercent + ".eps");
			}
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private static void outputExecuteTime(int[] percentIndexes, Log[][] logsForKind, String kind) {
		try {
			for (int i = 0; i < logsForKind.length; i++) {
				String run = "run" + i;
				if (i == 0) run = "average";
				Log[] logsRun = logsForKind[i];
			
				List<double[]> unsortedRows = Log.rowsExecuteTimes(logsRun, true);
				Result unsortedResult = new Result(generateHeader(null, databases, systems, true, 0), unsortedRows);
				unsortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedAll +".csv");
				unsortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedAll +".csv");
				plot("execute.gp", kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedAll +".csv", kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedAll +".eps");
			
				List<double[]> unsortedRowsPercent = Data.filterBy(unsortedRows, percentIndexes);
				Result unsortedPercentResult = new Result(generateHeader(null, databases, systems, true, 0), unsortedRowsPercent);				
				unsortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedPercent + ".csv");
				unsortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedPercent + ".csv");
				plot("execute.gp", kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedPercent + ".csv", kind + "-" + run + "-ExecuteTimes-" + fileNameUnsortedPercent + ".eps");
				
				List<double[]> unsortedRowsNoSte = Log.rowsExecuteTimes(logsRun, false);
				List<double[]> unsortedRelativeRowsDirect = Data.relativeNumbers(unsortedRowsNoSte, 0);
				Result unsortedRelativeResultDirect = new Result(generateHeader(null, databases, systems, false, 0), unsortedRelativeRowsDirect);
				unsortedRelativeResultDirect.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-direct-relative-unsorted.csv");
				unsortedRelativeResultDirect.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ExecuteTimes-direct-relative-unsorted.csv");
				plot("relative.gp", kind + "-" + run + "-ExecuteTimes-direct-relative-unsorted.csv", kind + "-" + run + "-ExecuteTimes-direct-relative-unsorted.eps");
				List<double[]> unsortedCdfRowsDirect = Data.cdf(unsortedRelativeRowsDirect, 0.0, 1000.0, 100000, true, 1000);
				Result unsortedCdfResultDirect = new Result(generateRelativeHeader(new String[] { "cdf" }, databases, systems, 0 + 1, 1), unsortedCdfRowsDirect);
				unsortedCdfResultDirect.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-direct-cdf.csv");
				plot("executeRelative.gp", kind + "-" + run + "-ExecuteTimes-direct-cdf.csv", kind + "-" + run + "-ExecuteTimes-direct-cdf.eps");

				List<double[]> unsortedRelativeRowsPINQ = Data.relativeNumbers(unsortedRowsNoSte, 1);
				Result unsortedRelativeResultPINQ = new Result(generateHeader(null, databases, systems, false, 0), unsortedRelativeRowsPINQ);
				unsortedRelativeResultPINQ.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-pinq-relative-unsorted.csv");
				unsortedRelativeResultPINQ.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ExecuteTimes-pinq-relative-unsorted.csv");
				plot("relative.gp", kind + "-" + run + "-ExecuteTimes-pinq-relative-unsorted.csv", kind + "-" + run + "-ExecuteTimes-pinq-relative-unsorted.eps");
				List<double[]> unsortedCdfRowsPINQ = Data.cdf(unsortedRelativeRowsPINQ, 0.0, 1000.0, 100000, true, 1000);
				Result unsortedCdfResultPINQ = new Result(generateRelativeHeader(new String[] { "cdf" }, databases, systems, 1 + 1, 1), unsortedCdfRowsPINQ);
				unsortedCdfResultPINQ.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-pinq-cdf.csv");
				plot("executeRelative.gp", kind + "-" + run + "-ExecuteTimes-pinq-cdf.csv", kind + "-" + run + "-ExecuteTimes-pinq-cdf.eps");
				
				int[] sortFilter = Data.getSortFilter(unsortedRows, 0);
				List<double[]> sortedRows = Data.filterBy(unsortedRows, sortFilter);
				Result sortedResult = new Result(generateHeader(null, databases, systems, true, 0), sortedRows);
				sortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-" + fileNameSortedAll +".csv");
				sortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ExecuteTimes-" + fileNameSortedAll + ".csv");
				plot("execute.gp", kind + "-" + run + "-ExecuteTimes-" + fileNameSortedAll + ".csv", kind + "-" + run + "-ExecuteTimes-" + fileNameSortedAll + ".eps");

				int[] sortFilterPercent = Data.getSortFilter(unsortedRowsPercent, 0);
				List<double[]> sortedRowsPercent = Data.filterBy(unsortedRowsPercent, sortFilterPercent);
				Result sortedPercentResult = new Result(generateHeader(null, databases, systems, true, 0), sortedRowsPercent);				
				sortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-ExecuteTimes-" + fileNameSortedPercent + ".csv");
				sortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-ExecuteTimes-" + fileNameSortedPercent + ".csv");
				plot("execute.gp", kind + "-" + run + "-ExecuteTimes-" + fileNameSortedPercent + ".csv", kind + "-" + run + "-ExecuteTimes-" + fileNameSortedPercent + ".eps");
			}
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private static void outputTotalTime(int[] percentIndexes, Log[][] logsForKind, String kind) {
		try {
			for (int i = 0; i < logsForKind.length; i++) {
				String run = "run" + i;
				if (i == 0) run = "average";
				Log[] logsRun = logsForKind[i];
			
				List<double[]> unsortedRows = Log.rowsTotalTimes(logsRun, true);
				Result unsortedResult = new Result(generateHeader(null, databases, systems, true, 0), unsortedRows);
				unsortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-" + fileNameUnsortedAll + ".csv");
				unsortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TotalTimes-" + fileNameUnsortedAll + ".csv");
				plot("execute.gp", kind + "-" + run + "-TotalTimes-" + fileNameUnsortedAll + ".csv", kind + "-" + run + "-TotalTimes-" + fileNameUnsortedAll + ".eps");

				List<double[]> unsortedRowsPercent = Data.filterBy(unsortedRows, percentIndexes);
				Result unsortedPercentResult = new Result(generateHeader(null, databases, systems, true, 0), unsortedRowsPercent);				
				unsortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-" + fileNameUnsortedPercent + ".csv");
				unsortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TotalTimes-" + fileNameUnsortedPercent + ".csv");
				plot("execute.gp", kind + "-" + run + "-TotalTimes-" + fileNameUnsortedPercent + ".csv", kind + "-" + run + "-TotalTimes-" + fileNameUnsortedPercent + ".eps");
				
				List<double[]> unsortedRowsNoSte = Log.rowsTotalTimes(logsRun, false);
				List<double[]> unsortedRelativeRowsDirect = Data.relativeNumbers(unsortedRowsNoSte, 0);
				Result unsortedRelativeResultDirect = new Result(generateHeader(null, databases, systems, false, 0), unsortedRelativeRowsDirect);
				unsortedRelativeResultDirect.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-direct-relative-unsorted.csv");
				unsortedRelativeResultDirect.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TotalTimes-direct-relative-unsorted.csv");
				plot("relative.gp", kind + "-" + run + "-TotalTimes-direct-relative-unsorted.csv", kind + "-" + run + "-TotalTimes-direct-relative-unsorted.eps");
				List<double[]> unsortedCdfRowsDirect = Data.cdf(unsortedRelativeRowsDirect, 0.0, 1000.0, 100000, true, 1000);
				Result unsortedCdfResultDirect = new Result(generateRelativeHeader(new String[] { "cdf" }, databases, systems, 0 + 1, 1), unsortedCdfRowsDirect);
				unsortedCdfResultDirect.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-direct-cdf.csv");
				plot("executeRelative.gp", kind + "-" + run + "-TotalTimes-direct-cdf.csv", kind + "-" + run + "-TotalTimes-direct-cdf.eps");

				List<double[]> unsortedRelativeRowsPINQ = Data.relativeNumbers(unsortedRowsNoSte, 1);
				Result unsortedRelativeResultPINQ = new Result(generateHeader(null, databases, systems, false, 0), unsortedRelativeRowsPINQ);
				unsortedRelativeResultPINQ.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-pinq-relative-unsorted.csv");
				unsortedRelativeResultPINQ.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TotalTimes-pinq-relative-unsorted.csv");
				plot("relative.gp", kind + "-" + run + "-TotalTimes-pinq-relative-unsorted.csv", kind + "-" + run + "-TotalTimes-pinq-relative-unsorted.eps");
				List<double[]> unsortedCdfRowsPINQ = Data.cdf(unsortedRelativeRowsPINQ, 0.0, 1000.0, 100000, true, 1000);
				Result unsortedCdfResultPINQ = new Result(generateRelativeHeader(new String[] { "cdf" }, databases, systems, 1 + 1, 1), unsortedCdfRowsPINQ);
				unsortedCdfResultPINQ.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-pinq-cdf.csv");
				plot("executeRelative.gp", kind + "-" + run + "-TotalTimes-pinq-cdf.csv", kind + "-" + run + "-TotalTimes-pinq-cdf.eps");
				
				int[] sortFilter = Data.getSortFilter(unsortedRows, 0);
				List<double[]> sortedRows = Data.filterBy(unsortedRows, sortFilter);
				Result sortedResult = new Result(generateHeader(null, databases, systems, true, 0), sortedRows);
				sortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-" + fileNameSortedAll + ".csv");
				sortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TotalTimes-" + fileNameSortedAll + ".csv");
				plot("execute.gp", kind + "-" + run + "-TotalTimes-" + fileNameSortedAll + ".csv", kind + "-" + run + "-TotalTimes-" + fileNameSortedAll + ".eps");
				
				int[] sortFilterPercent = Data.getSortFilter(unsortedRowsPercent, 0);
				List<double[]> sortedRowsPercent = Data.filterBy(unsortedRowsPercent, sortFilterPercent);
				Result sortedPercentResult = new Result(generateHeader(null, databases, systems, true, 0), sortedRowsPercent);				
				sortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TotalTimes-" + fileNameSortedPercent + ".csv");
				sortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TotalTimes-" + fileNameSortedPercent + ".csv");
				plot("execute.gp", kind + "-" + run + "-TotalTimes-" + fileNameSortedPercent + ".csv", kind + "-" + run + "-TotalTimes-" + fileNameSortedPercent + ".eps");
			}
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private static void outputPartitions(int[] percentIndexes, Log[][] logsForKind, String kind) {
		try {
			for (int i = 0; i < logsForKind.length; i++) {
				String run = "run" + i;
				if (i == 0) run = "average";
				Log[] logsRun = logsForKind[i];
			
				List<double[]> unsortedRows = Log.rowsTeardownPartitions(logsRun);
				Result unsortedResult = new Result(generateHeader(null, databases, systems, false, 0), unsortedRows);
				unsortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedAll + ".csv");
				unsortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedAll + ".csv");
				plot("partitions.gp", kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedAll + ".csv", kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedAll + ".eps");

				List<double[]> unsortedRowsPercent = Data.filterBy(unsortedRows, percentIndexes);
				Result unsortedPercentResult = new Result(generateHeader(null, databases, systems, false, 0), unsortedRowsPercent);				
				unsortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedPercent + ".csv");
				unsortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedPercent + ".csv");
				plot("partitions.gp", kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedPercent + ".csv", kind + "-" + run + "-TeardownPartitions-" + fileNameUnsortedPercent + ".eps");
				
				List<double[]> unsortedRowsExecute = Log.rowsExecuteTimes(logsRun, true);
				int[] sortFilter = Data.getSortFilter(unsortedRowsExecute, 0);
				List<double[]> sortedRows = Data.filterBy(unsortedRows, sortFilter);
				Result sortedResult = new Result(generateHeader(null, databases, systems, false, 0), sortedRows);
				sortedResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TeardownPartitions-" + fileNameSortedAll + ".csv");
				sortedResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TeardownPartitions-" + fileNameSortedAll + ".csv");
				plot("partitions.gp", kind + "-" + run + "-TeardownPartitions-" + fileNameSortedAll + ".csv", kind + "-" + run + "-TeardownPartitions-" + fileNameSortedAll + ".eps");

				List<double[]> unsortedRowsPercentExecute = Data.filterBy(unsortedRowsExecute, percentIndexes);
				int[] sortFilterPercent = Data.getSortFilter(unsortedRowsPercentExecute, 0);
				List<double[]> sortedRowsPercent = Data.filterBy(unsortedRowsPercent, sortFilterPercent);
				Result sortedPercentResult = new Result(generateHeader(null, databases, systems, false, 0), sortedRowsPercent);				
				sortedPercentResult.writeTo(outputPath + File.separator + kind + "-" + run + "-TeardownPartitions-" + fileNameSortedPercent + ".csv");
				sortedPercentResult.writeStatisticsTo(outputPath + File.separator + "statistics-" + kind + "-" + run + "-TeardownPartitions-" + fileNameSortedPercent + ".csv");
				plot("partitions.gp", kind + "-" + run + "-TeardownPartitions-" + fileNameSortedPercent + ".csv", kind + "-" + run + "-TeardownPartitions-" + fileNameSortedPercent + ".eps");
			}
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			System.out.println("Output STARTED");
			(new File(outputPath)).mkdirs();
			(new File(plotPath)).mkdirs();
			int[] percentIndexes = Data.generatePercent(5000, percentRate);
			for (String kind : kinds) {
				if (kind == null) continue;
				Log[][] logs = Parser.generateLogs(databases, systems, kind, instancePath, runs, filterDummies, skip);
				
				Thread budgetUsedThread = new Thread(() -> outputBudgetUsedCDF(kind));
				Thread executeTimeThread = new Thread(() -> outputExecuteTime(percentIndexes, logs, kind));
				Thread totalTimeThread = new Thread(() -> outputTotalTime(percentIndexes, logs, kind));
				Thread partitionsThread = new Thread(() -> outputPartitions(percentIndexes, logs, kind));
				Thread valueDifferencesThread = new Thread(() -> outputValueDifferences(percentIndexes, logs, kind));
				
				budgetUsedThread.start();
				executeTimeThread.start();
				totalTimeThread.start();
				partitionsThread.start();
				valueDifferencesThread.start();
				
				budgetUsedThread.join();
				executeTimeThread.join();
				totalTimeThread.join();
				partitionsThread.join();
				valueDifferencesThread.join();
			}
			System.out.println("Output FINISHED");
		} catch (NumberFormatException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			System.out.println("Something went wrong: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
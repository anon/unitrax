package logConverter;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.util.Pair;

public class Data {
	
	public static List<double[]> generateRowsDouble(List<List<Double>> columns) {
		if (columns == null) throw new NullPointerException("columns is null");
		Integer size = null;
		for (List<Double> column : columns) {
			if (column == null) continue;
			if (column.isEmpty()) throw new IllegalArgumentException("column is empty");
			if (size != null && !size.equals(column.size())) throw new IllegalArgumentException("columns are not same size");
			size = column.size();
		}
		List<double[]> rows = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			double[] row = new double[columns.size()];
			for (int j = 0; j < columns.size(); j++) {
				List<Double> list = columns.get(j);
				if (list == null) row[j] = Double.NaN;
				else row[j] = list.get(i);
			}
			rows.add(row);
		}
		return rows;
	}
	
	public static List<double[]> generateRowsValue(List<List<Value>> columns, boolean ste) {
		if (columns == null) throw new NullPointerException("columns is null");
		Integer size = null;
		for (int i = 0; i < columns.size(); i++) {
			List<Value> column = columns.get(i);
			if (column == null) continue;
			if (column.isEmpty()) throw new IllegalArgumentException("column is empty");
			if (size != null && !size.equals(column.size())) {
				throw new IllegalArgumentException("columns are not same size, was " + column.size() + " should be " + size);
			}
			size = column.size();
		}
		int rowSize = columns.size();
		if (ste) rowSize *= 2;
		List<double[]> rows = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			double[] row = new double[rowSize];
			for (int j = 0; j < columns.size(); j++) {
				List<Value> list = columns.get(j);
				if (ste) {
					if (list == null) {
						row[j * 2] = Double.NaN;
						row[j * 2 + 1] = Double.NaN;
					} else {
						row[j * 2] = list.get(i).getValue();
						row[j * 2 + 1] = list.get(i).getStE();
					}
				} else {
					if (list == null) row[j] = Double.NaN;
					else row[j] = list.get(i).getValue();
				}
			}
			rows.add(row);
		}
		return rows;
	}

	public static int[] getSortFilter(List<double[]> input, int sortIndex) {
		List<Pair<Double, Integer>> list = new ArrayList<Pair<Double, Integer>>(input.size());
		for (int i = 0; i < input.size(); i++) list.add(new Pair<Double, Integer>(input.get(i)[sortIndex], i));
		Collections.sort(list, new Comparator<Pair<Double, Integer>>() {
			@Override
			public int compare(Pair<Double, Integer> o1, Pair<Double, Integer> o2) {
				return Double.compare(o1.getFirst(), o2.getFirst());
			}
		});
		int[] result = new int[list.size()];
		for (int i = 0; i < list.size(); i++) result[i] = list.get(i).getSecond();
		return result;
	}
	
	public static int[] generatePercent(int totalNumber, double rate) {
		Random r = new Random();
		List<Integer> list = new LinkedList<Integer>();
		for (int i = 0; i < totalNumber; i++) if (r.nextDouble() < rate) list.add(i);
		int[] result = new int[list.size()];
		for (int i = 0; i < list.size(); i++) result[i] = list.get(i);
		return result;
	}
	
	public static List<double[]> filterBy(List<double[]> input, int[] filterBy) {
		List<double[]> result = new ArrayList<double[]>(filterBy.length);
		for (int i = 0; i < filterBy.length; i++) {
			if (filterBy[i] > input.size() - 1) break;
			result.add(input.get(filterBy[i]));
		}
		return result;
	}

	public static List<double[]> valueDifferences(Log compareTo, Log[] logs) {
		List<List<Double>> list = new ArrayList<List<Double>>(logs.length);
		for (int i = 0; i < logs.length; i++) list.add(Log.valueDifferences(compareTo, logs[i]));
		return generateRowsDouble(list);
	}
	
	public static List<double[]> executeTimeDifferencesPercent(Log compareTo, Log[] logs) {
		List<List<Double>> list = new ArrayList<List<Double>>(logs.length);
		for (int i = 0; i < logs.length; i++) list.add(Log.executeTimeDifferencesPercent(compareTo, logs[i]));
		return generateRowsDouble(list);
	}
	
	public static List<double[]> cdf(List<double[]> input, double start, double end, int numIntervals, boolean log, int logSteps) {
		int numSystems = input.get(0).length;
		double interval = (end - start) / new Double(numIntervals);
		double totalCount = 0.0;
		double[][] counts = new double[numSystems][];
		for (int i = 0; i < input.size(); i++) {
			totalCount++;
			for (int j = 0; j < numSystems; j++) {
				int index = -1;
				double value = input.get(i)[j];
				if (Double.isNaN(value)) continue;
				if (value < start) index = 0;
				else if (value >= end) index = numIntervals + 1;
				else index = new Double((value - start) / interval).intValue() + 1;
				if (counts[j] == null) counts[j] = new double[numIntervals + 2];
				counts[j][index]++;
			}
		}
		
		double[][][] valuedCounts = new double[numSystems][][];
		for (int i = 0; i < numSystems; i++) {
			if (counts[i] == null) continue;
			valuedCounts[i] = new double[counts[i].length][];
			for (int j = 0; j < counts[i].length; j++) {
				valuedCounts[i][j] = new double[2];
				if (j == 0) valuedCounts[i][j][0] = start;
				else if (j == counts[i].length - 1) valuedCounts[i][j][0] = end;
				else valuedCounts[i][j][0] = start + (new Double(j + 1) - 0.5) * interval;
				valuedCounts[i][j][1] = counts[i][j];
			}
		}
		
		int count = numIntervals + 2;
		if (log) {
			count = 0;
			int mult = 1;
			int intervals = numIntervals;
			while (intervals > 0) {
				for (int i = 0; i < logSteps; i++) {
					if (intervals <= 0) break;
					count++;
					intervals -= mult;
				}
				mult *= 1000;
			}
			count += 2;
			double[][][] origCounts = valuedCounts;
			valuedCounts = new double[origCounts.length][][];
			for (int i = 0; i < origCounts.length; i++) {
				if (origCounts[i] == null) continue;
				valuedCounts[i] = new double[count][];
				valuedCounts[i][0] = origCounts[i][0];
				valuedCounts[i][valuedCounts[i].length - 1] = origCounts[i][origCounts[i].length - 1];
				intervals = numIntervals;
				mult = 1;
				int indexNew = 1;
				int indexOld = 1;
				while (intervals > 0) {
					for (int j = 0; j < logSteps; j++) {
						if (intervals <= 0) break;
						double value = 0.0;
						int c = 0;
						double num = 0.0;
						for (int k = 0; k < mult; k++) {
							if (intervals <= 0) break;
							value += origCounts[i][indexOld][0];
							c += origCounts[i][indexOld][1];
							num++;
							indexOld++;
							intervals--;
						}
						valuedCounts[i][indexNew] = new double[2];
						valuedCounts[i][indexNew][0] = value / num;
						valuedCounts[i][indexNew][1] = c;
						indexNew++;
					}
					mult *= 1000;
				}
			}
		}
		
		List<double[]> result = new ArrayList<double[]>(count);
		double[] runningCounts = new double[numSystems];
		for (int i = 0; i < count; i++) {
			double value = Double.NaN;
			for (int j = 0; j < numSystems; j++) {
				if (valuedCounts[j] == null) {
					runningCounts[j] = Double.NaN;
					continue;
				}
				if (Double.isNaN(value)) value = valuedCounts[j][i][0];
				else if (value != valuedCounts[j][i][0]) {
					throw new InvalidParameterException("values should match " + value + " " + valuedCounts[j][i][0]);
				}
				runningCounts[j] += valuedCounts[j][i][1];
			}
			double[] res = new double[numSystems + 1];
			for (int j = 0; j < numSystems; j++) {
				if (Double.isNaN(runningCounts[j])) res[j + 1] = Double.NaN;
				else res[j + 1] = runningCounts[j] / totalCount;
			}
			res[0] = value;
			result.add(res);
		}	
		return result;
	}
	
	public static List<double[]> relativeNumbers(List<double[]> input, int relativeIndex) {
		List<double[]> result = new ArrayList<double[]>(input.size());
		Iterator<double[]> iterator = input.iterator();
		while (iterator.hasNext()) {
			double[] row = iterator.next();
			if (Double.isNaN(row[relativeIndex])) throw new InvalidParameterException("relative values cannot be NaN");
			double[] res = new double[row.length];
			for (int i = 0; i < row.length; i++) {
				if (Double.isNaN(row[i])) res[i] = Double.NaN;
				else res[i] = row[i] / row[relativeIndex];
			}
			result.add(res);
		}
		return result;
	}
	
	public static List<double[]> differenceNumbersAbs(List<double[]> input, int differenceIndex) {
		List<double[]> result = new ArrayList<double[]>(input.size());
		Iterator<double[]> iterator = input.iterator();
		while (iterator.hasNext()) {
			double[] row = iterator.next();
			if (Double.isNaN(row[differenceIndex])) throw new InvalidParameterException("difference values cannot be NaN");
			double[] res = new double[row.length];
			for (int i = 0; i < row.length; i++) {
				if (Double.isNaN(row[i])) res[i] = Double.NaN;
				else res[i] = Math.abs(row[i] - row[differenceIndex]);
			}
			result.add(res);
		}
		return result;
	}

	public static List<double[]> countedValuesToCdf(List<List<double[]>> countedValues) {
		double[] countsTotal = new double[countedValues.size()];
		LinkedList<double[]> list = new LinkedList<double[]>();
		for (int i = 0; i < countedValues.size(); i++) {
			if (countedValues.get(i) == null) continue;
			int index = list.size() / 2;
			for (double[] kv : countedValues.get(i)) {
				if (kv.length != 2) throw new IllegalArgumentException("counted values must be arrays length 2");
				while (index > 0 && kv[0] < list.get(index)[0]) index--;
				while (index < list.size() && kv[0] > list.get(index)[0]) index++;
				if (index < list.size() - 1 && kv[0] == list.get(index)[0]) list.get(index)[i + 1] += kv[1];
				else {
					double[] insert = new double[countedValues.size() + 1];
					insert[0] = kv[0];
					insert[i + 1] = kv[1];					
					list.add(index, insert);
				}
				countsTotal[i] += kv[1];
			}
		}
		double refCount = 0.0;
		for (int i = 0; i < countsTotal.length; i++) {
			if (countsTotal[i] == 0.0) continue;
			if (refCount == 0.0) {
				refCount = countsTotal[i];
				continue;
			}
			if (refCount != countsTotal[i]) throw new InvalidParameterException("total counts must be the same");
		}
		double[] countsCurrent = new double[countedValues.size()];
		List<double[]> result = new ArrayList<double[]>(list.size() * 2);
		result.add(new double[countedValues.size() + 1]);
		Iterator<double[]> iterator = list.iterator();
		double lastValue = 0.0;
		while(iterator.hasNext()) {
			double[] current = iterator.next();
			double[] result1 = new double[countedValues.size() + 1];
			result1[0] = current[0];
			lastValue = current[0];
			double[] result2 = new double[countedValues.size() + 1];
			result2[0] = current[0];
			for (int i = 1; i < current.length; i++) {
				if (countsTotal[i - 1] == 0.0) result1[i] = Double.NaN;
				else result1[i] = countsCurrent[i - 1] / countsTotal[i - 1];
				countsCurrent[i - 1] += current[i];
				if (countsTotal[i - 1] == 0.0) result2[i] = Double.NaN;
				else result2[i] = countsCurrent[i - 1] / countsTotal[i - 1];
			}
			result.add(result1);
			result.add(result2);
		}
		double[] resultFinal = new double[countedValues.size() + 1];
		resultFinal[0] = lastValue;
		for (int i = 1; i < resultFinal.length; i++) {
			if (countsTotal[i - 1] == 0.0) resultFinal[i] = Double.NaN;
			else resultFinal[i] = countsCurrent[i - 1] / countsTotal[i - 1];
		}
		result.add(resultFinal);
		return result;
	}

	public static List<List<double[]>> relativeBudgetNumbers(List<List<double[]>> input, int relativeIndex) {
		if (input.get(relativeIndex) == null) throw new NullPointerException("relative input is null");
		if (input.get(relativeIndex).size() != 1) throw new NullPointerException("relative input must have exactly one value");
		double relativeValue = input.get(relativeIndex).get(0)[0];
		List<List<double[]>> result = new ArrayList<>(input.size());
		for (int i = 0; i < input.size(); i++) {
			if (input.get(i) == null) {
				result.add(null);
				continue;
			}
			List<double[]> res = new ArrayList<>(input.get(i).size());
			for (int j = 0; j < input.get(i).size(); j++) res.add(new double[] { input.get(i).get(j)[0] / relativeValue, input.get(i).get(j)[1] });
			result.add(res);
		}
		return result;
	}	

}
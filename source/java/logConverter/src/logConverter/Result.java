package logConverter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class Result {
	
	private String[] headerRow;
	private List<double[]> rows;
	private DescriptiveStatistics[] statistics;
	private int[] NPos;
	private int[] NNeg;
	private int[] NNaN;
	
	public Result(String[] headerRow, List<double[]> rows) {
		if (headerRow == null) throw new NullPointerException("headerRow is null");
		if (rows == null) throw new NullPointerException("rows is null");
		if (rows.isEmpty()) throw new IllegalArgumentException("rows is empty");
		for (double[] row : rows) {
			if (row == null) throw new NullPointerException("row is null");
			if (row.length != headerRow.length) {
				throw new IllegalArgumentException("all rows must have the same number of columns");
			}
		}
		for (String header : headerRow) if (header == null) throw new NullPointerException("header is null");
		this.headerRow = headerRow;
		this.rows = rows;
		GenerateStatistics();
 	}
	
	public String[] getHeaderRow() {
		return headerRow;
	}
	
	public List<double[]> getRows() {
		return rows;
	}
	
	public void GenerateStatistics() {
		statistics = new DescriptiveStatistics[headerRow.length];
		NPos = new int[headerRow.length];
		NNeg = new int[headerRow.length];
		NNaN = new int[headerRow.length];
		for (int i = 0; i < headerRow.length; i++) statistics[i] = new DescriptiveStatistics();
		for (double[] row : rows) {
			for (int i = 0; i < headerRow.length; i++) {
				if (Double.isNaN(row[i])) {
					NNaN[i]++;
					continue;
				}
				statistics[i].addValue(row[i]);
				if (row[i] < 0.0) NNeg[i]++;
				else NPos[i]++;
			}
		}
	}
	
	public void writeTo(String fileName) throws IOException {
		if (fileName == null) throw new NullPointerException("fileName is null");
		PrintWriter pw = new PrintWriter(new FileWriter(fileName));
		for (int i = 0; i < headerRow.length; i++) pw.print(headerRow[i] + (i < headerRow.length - 1 ? "," : ""));
		pw.println();
		for (double[] row : rows) {
			for (int i = 0; i < headerRow.length; i++) pw.print(row[i] + (i < headerRow.length - 1 ? "," : ""));
			pw.println();
		}
		pw.close();		
	}

	public void writeStatisticsTo(String fileName) throws IOException {
		if (fileName == null) throw new NullPointerException("fileName is null");
		PrintWriter pw = new PrintWriter(new FileWriter(fileName));
		for (int i = 0; i < headerRow.length; i++) pw.print(headerRow[i] + ",");
		pw.println("statistic");
		for (int i = 0; i < headerRow.length; i++) pw.print(NNeg[i] + ",");
		pw.println("countNeg");
		for (int i = 0; i < headerRow.length; i++) pw.print(NPos[i] + ",");
		pw.println("countPos");
		for (int i = 0; i < headerRow.length; i++) pw.print(NNaN[i] + ",");
		pw.println("countNaN");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getN() + ",");
		pw.println("count");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getMin() + ",");
		pw.println("minimum");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getMax() + ",");
		pw.println("maximum");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getMean() + ",");
		pw.println("mean");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getPercentile(50.0) + ",");
		pw.println("median");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getPercentile(90.0) + ",");
		pw.println("90th");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getPercentile(95.0) + ",");
		pw.println("95th");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getPercentile(99.0) + ",");
		pw.println("99th");
		for (int i = 0; i < headerRow.length; i++) pw.print(statistics[i].getStandardDeviation() + ",");
		pw.println("std");
		pw.close();		
	}
}
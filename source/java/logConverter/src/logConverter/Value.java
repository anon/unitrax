package logConverter;

public class Value {

	private double value;
	private double StE;
	
	public Value(double value, double StE) {
		this.value = value;
		this.StE = StE;
	}
	
	public double getValue() {
		return value;
	}
	
	public double getStE() {
		return StE;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Value)) return false;
		Value other = (Value) object;
		if (value != other.value) return false;
		if (StE != other.StE) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "value:" + value + ",StE:" + StE;
	}
}
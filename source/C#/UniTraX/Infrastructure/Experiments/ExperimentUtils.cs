﻿namespace UniTraX.Infrastructure.ExperimentUtils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using UniTraX.Core.Utils;
    using UniTraX.Infrastructure.Provisioning;
    using UniTraX.Queries.Core;
    using UniTraX.Queries.Recording;

    public class ExperimentUtils<R>
    {
        public static void SetupExperimentDirect(String instanceDirName, String experimentName)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
#endif
            Directory.CreateDirectory(Path.Combine(instanceDirName, experimentName));
        }

        public static double[][] RunExperimentDirect(String instanceDirName, String experimentName, IExperimentServer server, IQueryable<R> queryable, List<IQuery<R>> queries, int run, string databaseName)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
#endif
            var experimentDirName = Path.Combine(instanceDirName, experimentName);
            var results = new double[queries.Count()][];
            System.Threading.Thread.Sleep(1000);
            Recorder.Reset();
            Recorder.StartGlobalTimer();
#if RECORD
            var timer = Recorder.StartTimer();
#endif
            for (int i = 0; i < queries.Count(); i++) results[i] = queries[i].ApplyTo(queryable);
#if RECORD
            var time = Recorder.StopTimer(timer);
            Console.WriteLine("Took time ms: " + time);
#endif
            Recorder.StopGlobalTimer();
            Recorder.ToFile(Path.Combine(experimentDirName, "run" + run + "_results.csv"));
            FileUtils.WriteLineToFile("0," + queryable.Count(), Path.Combine(experimentDirName, "run" + run + "_budgetUsed.csv"));
            return results;
        }

        public static double[][][] RepeatExperimentDirect(String instanceDirName, String experimentName, IExperimentServer server, IQueryable<R> queryable, List<IQuery<R>> queries, string databaseName, int repeats)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
            if (repeats < 1) throw new ArgumentException("repeats is invalid");
#endif
            var results = new double[repeats][][];
            for (int i = 0; i < repeats; i++)
            {
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": STARTED");
                results[i] = RunExperimentDirect(instanceDirName, experimentName, server, queryable, queries, i, databaseName);
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": FINISHED");
            }
            return results;
        }

        public static void SetupExperimentPINQ(String instanceDirName, String experimentName)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
#endif
            Directory.CreateDirectory(Path.Combine(instanceDirName, experimentName));
        }

        public static double[][] RunExperimentPINQ(String instanceDirName, String experimentName, IExperimentServer server, PINQueryableFactory<R> queryableFactoryPINQ, List<IQuery<R>> queries, double defaultTotalBudget, int run, string databaseName)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (queryableFactoryPINQ == null) throw new ArgumentNullException(nameof(queryableFactoryPINQ));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
#endif
            var experimentDirName = Path.Combine(instanceDirName, experimentName);
            var queryables = queryableFactoryPINQ.GeneratePINQueryable();
            var queryable = queryables.Item1;
            var queryablePINQ = queryables.Item2;
            var results = new double[queries.Count()][];
            System.Threading.Thread.Sleep(1000);
            Recorder.Reset();
            Recorder.StartGlobalTimer();
#if RECORD
            var timer = Recorder.StartTimer();
#endif
            for (int i = 0; i < queries.Count(); i++) results[i] = queries[i].ApplyTo(queryablePINQ);
#if RECORD
            var time = Recorder.StopTimer(timer);
            Console.WriteLine("Took time ms: " + time);
#endif
            Recorder.StopGlobalTimer();
            Recorder.ToFile(Path.Combine(experimentDirName, "run" + run + "_results.csv"));
            FileUtils.WriteLineToFile((defaultTotalBudget - queryablePINQ.remaining()) + "," + queryable.Count(), Path.Combine(experimentDirName, "run" + run + "_budgetUsed.csv"));
            return results;
        }

        public static double[][][] RepeatExperimentPINQ(String instanceDirName, String experimentName, IExperimentServer server, PINQueryableFactory<R> queryableFactoryPINQ, List<IQuery<R>> queries, double defaultTotalBudget, string databaseName, int repeats)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (queryableFactoryPINQ == null) throw new ArgumentNullException(nameof(queryableFactoryPINQ));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
            if (repeats < 1) throw new ArgumentException("repeats is invalid");
#endif
            var results = new double[repeats][][];
            for (int i = 0; i < repeats; i++)
            {
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": STARTED");
                results[i] = RunExperimentPINQ(instanceDirName, experimentName, server, queryableFactoryPINQ, queries, defaultTotalBudget, i, databaseName);
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": FINISHED");
            }
            return results;
        }

        public static void SetupExperimentBK(String instanceDirName, String experimentName)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
#endif
            Directory.CreateDirectory(Path.Combine(instanceDirName, experimentName));
        }

        public static double[][] RunExperimentBK(String instanceDirName, String experimentName, IExperimentServer server, BKQueryableFactory<R> queryableFactory, List<IQuery<R>> queries, int run, string databaseName)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (queryableFactory == null) throw new ArgumentNullException(nameof(queryableFactory));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
#endif
            var experimentDirName = Path.Combine(instanceDirName, experimentName);
            var queryable = queryableFactory.GenerateBKQueryable();
            var results = new double[queries.Count()][];
            System.Threading.Thread.Sleep(1000);
            Recorder.Reset();
            Recorder.StartGlobalTimer();
#if RECORD
            var timer = Recorder.StartTimer();
#endif
            for (int i = 0; i < queries.Count(); i++) results[i] = queries[i].ApplyTo(queryable);
#if RECORD
            var time = Recorder.StopTimer(timer);
            Console.WriteLine("Took time ms: " + time);
#endif
            Recorder.StopGlobalTimer();
            Recorder.ToFile(Path.Combine(experimentDirName, "run" + run + "_results.csv"));
            queryable.CleanHistory();
            queryable.WriteDetailedBudgetUseToFile(Path.Combine(experimentDirName, "run" + run + "_budgetUsed.csv"));
            return results;
        }

        public static double[][][] RepeatExperimentBK(String instanceDirName, String experimentName, IExperimentServer server, BKQueryableFactory<R> queryableFactory, List<IQuery<R>> queries, string databaseName, int repeats)
        {
#if DEBUG
            if (instanceDirName == null) throw new ArgumentNullException(nameof(instanceDirName));
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (queryableFactory == null) throw new ArgumentNullException(nameof(queryableFactory));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
            if (repeats < 1) throw new ArgumentException("repeats is invalid");
#endif
            var results = new double[repeats][][];
            for (int i = 0; i < repeats; i++)
            {
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": STARTED");
                results[i] = RunExperimentBK(instanceDirName, experimentName, server, queryableFactory, queries, i, databaseName);
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": FINISHED");
            }
            return results;
        }
    }
}
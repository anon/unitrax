﻿namespace UniTraX.Infrastructure.ExperimentUtils
{
    using System;
    using System.Collections.Generic;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.BoundedData;
    using UniTraX.Core.History;
    using UniTraX.Core.Partition;

    public class BKQueryableFactory<RB>
    {
        private readonly BoundedDataAccess<RB> BoundedDataAccess;

        public BKQueryableFactory(BoundedDataAccess<RB> boundedDataAccess)
        {
#if DEBUG
            if (boundedDataAccess == null) throw new ArgumentNullException(nameof(boundedDataAccess));
#endif
            BoundedDataAccess = boundedDataAccess;
        }

        public BKQueryable<RB> GenerateBKQueryable()
        {
            var boundary = new Partition(new HashSet<ContiguousPartition>() { BoundedDataAccess.Record.Boundary });
            return new BKQueryable<RB>(BoundedDataAccess.GetQueryable(), new Bookkeeper<RB>(boundary, BoundedDataAccess,
                new InMemoryHistory(BoundedDataAccess.Record.GetBudgetIndex())), boundary);
        }
    }
}
﻿namespace UniTraX.Infrastructure.ExperimentUtils
{
    using PINQ;
    using System;
    using System.Linq;

    public class PINQueryableFactory<R>
    {
        private readonly IQueryable<R> Queryable;
        private readonly double TotalBudget;

        public PINQueryableFactory(IQueryable<R> queryable, double totalBudget)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Queryable = queryable;
            TotalBudget = totalBudget;
        }

        public Tuple<IQueryable<R>, PINQueryable<R>> GeneratePINQueryable()
        {
            return new Tuple<IQueryable<R>, PINQueryable<R>>(Queryable, new PINQueryable<R>(Queryable, new PINQAgentBudget(TotalBudget)));
        }
    }
}
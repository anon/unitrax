﻿namespace UniTraX.Infrastructure.DataConversion
{
    using System;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;

    public class DoubleTypeConverter : ITypeConverter<double>, IFieldType
    {
        public static DoubleTypeConverter WholeNumber = new DoubleTypeConverter(DoubleType.WholeNumber);
        public static DoubleTypeConverter Integer = new DoubleTypeConverter(DoubleType.Integer);
        public static DoubleTypeConverter TwoDecimal = new DoubleTypeConverter(DoubleType.TwoDecimal);
        public static DoubleTypeConverter LongLat = new DoubleTypeConverter(DoubleType.LongLat);

        private static long ToSystem(double value, double center, double step)
        {
            var system = (value - center) / step;
            if (system >= 0.0) system += 0.0000001;
            else system -= 0.0000001;
            return (long)system;
        }

        private static double ToType(long value, double center, double step, bool larger)
        {
            var tolerance = 0.0000000000001;
#if DEBUG
            if (step / 1000.0 <= tolerance) throw new Exception("step is too small");
#endif
            var result = center + value * step;
            if (larger) return result + tolerance;
            return result - tolerance;
        }

        public IType<double> Type { get; private set; }
        public Interval System { get; private set; }

        public DoubleTypeConverter(IType<double> type)
        {
#if DEBUG
            if (type == null) throw new ArgumentNullException(nameof(type));
#endif
            Type = type;
            System = new Interval(ToSystem(Type.Min, Type.Center, Type.Step), ToSystem(Type.Max, Type.Center, Type.Step));
        }

        public long ToSystem(double value)
        {
#if DEBUG
            if (!Type.IsValid(value)) throw new ArgumentException("value not valid in type");
#endif
            var system = ToSystemUnchecked(value);
#if DEBUG
            if (!System.Encloses(system)) throw new Exception("should not happen");
#endif
            return system;
        }

        public double ToType(long value, bool larger)
        {
#if DEBUG
            if (!System.Encloses(value)) throw new ArgumentException("value not valid in system");
#endif
            var type = ToTypeUnchecked(value, larger);
#if DEBUG
            if (!Type.IsValid(type)) throw new Exception("should not happen");
#endif
            return type;
        }

        public long ToSystemUnchecked(double value)
        {
            return ToSystem(value, Type.Center, Type.Step);
        }

        public double ToTypeUnchecked(long value, bool larger)
        {
            return ToType(value, Type.Center, Type.Step, larger);
        }

        public override string ToString()
        {
            return "[DTC:" + Type.ToString() + ";" + System.ToString() + "]";
        }

        public Interval Bounds => System;
        public Func<long, bool, double> Convert => ToType;
        public Func<long, bool, double> ConvertUnchecked => ToTypeUnchecked;
    }
}
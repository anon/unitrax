﻿namespace UniTraX.Infrastructure.DataConversion
{
    using System;

    public interface IType<T> where T : IComparable<T>
    {
        T Min { get; }
        T Center { get; }
        T Max { get; }
        T Step { get; }
        T Tolerance { get; }
        bool IsValid(T value);
        string ToString();
    }
}
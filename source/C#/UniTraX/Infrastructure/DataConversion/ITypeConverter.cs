﻿namespace UniTraX.Infrastructure.DataConversion
{
    using System;
    using UniTraX.Core.Partition;

    public interface ITypeConverter<T> where T : IComparable<T>
    {
        IType<T> Type { get; }
        Interval System { get; }
        long ToSystem(T value);
        T ToType(long value, bool larger);
        string ToString();
    }
}
﻿namespace UniTraX.Infrastructure.DataConversion
{
    using System;

    public class DoubleType : IType<double>
    {
        public static DoubleType WholeNumber = new DoubleType(0.0, 100000000000.0, 1.0);
        public static DoubleType Integer = new DoubleType(-100000000000.0, 100000000000.0, 1.0);
        public static DoubleType TwoDecimal = new DoubleType(-100000000000.0, 100000000000.0, 0.01);
        public static DoubleType LongLat = new DoubleType(-5000.0, 5000.0, 0.000001);

        public static bool IsValid(double value, double origin, double step, double tolerance)
        {
#if DEBUG
            if (step <= 0.0) throw new ArgumentException("step must be greater zero");
            if (tolerance < 0.0) throw new ArgumentException("tolerance cannot be negative");
#endif
            var difference = Math.Abs(value - origin);
            var remainder = difference % step;
            if (0 - tolerance < remainder && remainder < tolerance) return true;
            remainder = step - remainder;
            if (0 - tolerance < remainder && remainder < tolerance) return true;
            return false;
        }

        public double Min { get; private set; }
        public double Center { get; private set; }
        public double Max { get; private set; }
        public double Step { get; private set; }
        public double Tolerance { get; private set; }

        public DoubleType(double min, double max, double step)
        {
            var tolerance = step / 1000.0;
#if DEBUG
            if (step <= 0.0) throw new ArgumentException("step must be above zero");
            if (min - max > tolerance) throw new ArgumentException("min is greater than max");
            if (!IsValid(max, min, step, tolerance)) throw new ArgumentException("center 0.0 is not a multiple of step from min away");
            if (!IsValid(0.0, min, step, tolerance)) throw new ArgumentException("center 0.0 is not a multiple of step from min away");
#endif
            Min = min;
            Center = 0.0;
            Max = max;
            Step = step;
            Tolerance = tolerance;
        }

        public bool IsValid(double value)
        {
            if (value + Tolerance < Min) return false;
            if (value - Tolerance > Max) return false;
            if (IsValid(value, Center, Step, Tolerance)) return true;
            if (IsValid(value, Min, Step, Tolerance)) return true;
            if (IsValid(value, Max, Step, Tolerance)) return true;
            return false;
        }

        public override string ToString()
        {
            return "[DT:" + Min + ";" + Center + ";" + Max + ";" + Step + ";" + Tolerance + "]";
        }
    }
}
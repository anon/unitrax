﻿namespace UniTraX.Infrastructure.Provisioning
{
    using System;
    using System.Data.SqlClient;

    public class DatabaseServerInterface
    {
        private readonly string Datasource;
        private readonly string InitialCatalog;
        private readonly string Username;
        private readonly string Password;
        private SqlConnection Connection;

        public DatabaseServerInterface(string datasource, string initialCatalog, string username, string password)
        {
#if DEBUG
            if (datasource == null) throw new ArgumentNullException(nameof(datasource));
            if (initialCatalog == null) throw new ArgumentNullException(nameof(initialCatalog));
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
#endif
            Datasource = datasource;
            InitialCatalog = initialCatalog;
            Username = username;
            Password = password;
            RenewConnection();
        }

        private void RenewConnection()
        {
            if (Connection != null)
            {
                try
                {
                    Connection.Close();
                    Connection.Dispose();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Database error:");
                    Console.WriteLine(ex.Message);
                }
            }
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder["Data Source"] = Datasource;
            builder["Initial Catalog"] = InitialCatalog;
            builder["User ID"] = Username;
            builder["Password"] = Password;
            Connection = new SqlConnection(builder.ToString());
        }

        public void Execute(string command)
        {
            try
            {
                Connection.Open();
                var sqlCommand = new SqlCommand(command, Connection);
                var dataReader = sqlCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    var values = new Object[dataReader.FieldCount];
                    var num = dataReader.GetValues(values);
                    for (var i = 0; i < num; i++) Console.Write("Value " + values[i] + ";");
                    Console.WriteLine();
                }
                dataReader.Close();
                sqlCommand.Dispose();
                Connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Database error:");
                Console.WriteLine(ex.Message);
                RenewConnection();
                throw new ProvisioningException("DatabaseServerInterface caught exception.");
            }
        }
    }
}
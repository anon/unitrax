﻿namespace UniTraX.Infrastructure.Provisioning
{
    using Microsoft.SqlServer.Management.Smo;
    using System.Data.SqlClient;

    public interface IExperimentServer
    {
        void CreateDatabaseOnDisk(string name);
        void CreateDatabaseInMem(string name);
        void MakeDatabaseReadonly(string name);
        void Wipe();
        void Setup();
        Database GetDatabase(string name);
        string GetDatabaseConnectionString(string databaseName);
        SqlConnection GetSqlConnection(string databaseName);
    }
}
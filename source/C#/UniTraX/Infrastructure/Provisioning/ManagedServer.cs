﻿namespace UniTraX.Infrastructure.Provisioning
{
    using Microsoft.SqlServer.Management.Smo;
    using Microsoft.SqlServer.Management.Smo.Wmi;
    using System;

    public class ManagedServer
    {
        private ManagedComputer Host;

        public ManagedServer (string host, string username, string password)
        {
#if DEBUG
            if (host == null) throw new ArgumentNullException(nameof(host));
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
#endif
            Host = new ManagedComputer(host, username, password);
            Host.ConnectionSettings.ProviderArchitecture = ProviderArchitecture.Use64bit;
        }

        public void ListServices()
        {
            try
            {
                Console.WriteLine("Listing Services");
                foreach (Service s in Host.Services) Console.WriteLine("Service: DisplayName " + s.DisplayName + " Name " + s.Name + " Description " + s.Description);
                Console.WriteLine("Listed Services");
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void StartService(string name)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            try
            {
                Console.WriteLine("Starting Service " + name);
                var service = Host.Services[name];
                if (service.ServiceState != ServiceState.Running) service.Start();
                while (service.ServiceState != ServiceState.Running)
                {
                    service.Refresh();
                }
                Console.WriteLine("Started Service " + name);
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void StopService(string name)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            try
            {
                Console.WriteLine("Stopping Service " + name);
                var service = Host.Services[name];
                if (service.ServiceState != ServiceState.Stopped) service.Stop();
                while (service.ServiceState != ServiceState.Stopped) service.Refresh();
                Console.WriteLine("Stopped Service " + name);
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public bool ServiceIsRunning(string name)
        {
            try
            {
                Console.WriteLine("Reading State of Service " + name);
                if (Host.Services[name].ServiceState != ServiceState.Running) return false;
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return true;
        }

        public bool ServiceIsStopped(string name)
        {
            try
            {
                Console.WriteLine("Reading State of Service " + name);
                if (Host.Services[name].ServiceState != ServiceState.Stopped) return false;
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return true;
        }

        private void HandleSMOException(SmoException smoex)
        {
#if DEBUG
            if (smoex == null) throw new ArgumentNullException(nameof(smoex));
#endif
            Console.WriteLine("This is an SMO Exception");
            Console.WriteLine(smoex.Message);
            Console.WriteLine(smoex.StackTrace);
            Exception ex;
            ex = smoex;
            while (!object.ReferenceEquals(ex.InnerException, (null)))
            {
                Console.WriteLine(ex.InnerException.Message);
                Console.WriteLine(ex.InnerException.StackTrace);
                ex = ex.InnerException;
            }
            throw new ProvisioningException("ManagedServer caught SMO exception.");
        }

        private void HandleException(Exception ex)
        {
#if DEBUG
            if (ex == null) throw new ArgumentNullException(nameof(ex));
#endif
            Console.WriteLine("This is not an SMO exception.");
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
            throw new ProvisioningException("ManagedServer caught non-SMO exception.");
        }
    }
}
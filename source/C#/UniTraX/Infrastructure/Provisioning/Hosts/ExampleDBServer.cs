﻿namespace UniTraX.Infrastructure.Provisioning
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    public class ExampleDBServer : IExperimentServer
    {
        private readonly string Host;
        private readonly string Username;
        private readonly string Password;
        private readonly string DatabaseUsername;
        private readonly string DatabasePassword;
        private readonly ManagedServer Server;
        private readonly DatabaseServer DatabaseServer;
        private readonly string Hostname;
        private readonly string RamDiskSize;
        private readonly List<string> PotentialDatabases;

        public ExampleDBServer()
        {
            Host = @"server0815.example.com";
            Username = @"SERVER0815\Administrator";
            Password = @"password";
            DatabaseUsername = @"sa";
            DatabasePassword = @"password";
            Server = new ManagedServer(Host, Username, Password);
            DatabaseServer = new DatabaseServer(Host, DatabaseUsername, DatabasePassword);
            Hostname = "server0815";
            RamDiskSize = "131072MB";
            PotentialDatabases = new List<string>();
            PotentialDatabases.Add("mobility-disk");
            PotentialDatabases.Add("mobility-mem");
            PotentialDatabases.Add("financial-disk");
            PotentialDatabases.Add("financial-mem");
            PotentialDatabases.Add("medical-disk");
            PotentialDatabases.Add("medical-mem");
        }

        public void CreateRamDisk()
        {
            RamDisk.Create(Host, Username, Password, Hostname, RamDiskSize);
        }

        public void RemoveRamDisk()
        {
            RamDisk.Remove(Host, Username, Password, Hostname);
        }

        public Database GetDatabase(string name)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            return DatabaseServer.GetDatabase(name);
        }

        public string GetDatabaseConnectionString(string databaseName)
        {
#if DEBUG
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
#endif
            return @"Data Source=" + Host + @";Initial Catalog=" + databaseName + @";User ID=" + DatabaseUsername + @";Password=" + DatabasePassword;
        }

        public SqlConnection GetSqlConnection(string databaseName)
        {
#if DEBUG
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
#endif
            return new SqlConnection(GetDatabaseConnectionString(databaseName));
        }

        public void StartDBService()
        {
            Server.StartService(@"MSSQL$MSSQLSERVER2017");
        }

        public void StopDBService()
        {
            Server.StopService(@"MSSQL$MSSQLSERVER2017");
        }

        public bool DBServiceIsRunning()
        {
            return Server.ServiceIsRunning(@"MSSQL$MSSQLSERVER2017");
        }

        public bool DBServiceIsStopped()
        {
            return Server.ServiceIsStopped(@"MSSQL$MSSQLSERVER2017");
        }

        public void CreateDatabaseOnDisk(string name)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            DatabaseServer.CreateOnDisk(name, @"E:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\Data\" + name + @".mdf", @"E:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\Data\" + name + @".ldf");
        }

        public void CreateDatabaseInMem(string name)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            DatabaseServer.CreateInMem(name, @"E:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\Data\" + name + @"_sysdata.mdf", @"E:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\Data\" + name + @"_log.ldf", @"E:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\Data\" + name + @"_memopt");
        }

        public void MakeDatabaseReadonly(string name)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            DatabaseServer.MakeDatabaseReadonly(name);
        }

        public void Wipe()
        {
            if (DBServiceIsRunning())
            {
                foreach (var db in PotentialDatabases)
                {
                    try
                    {
                        DatabaseServer.Drop(db);
                    }
                    catch (ProvisioningException ex)
                    {
                        Console.WriteLine("Could not drop database " + db + ", caught exception: " + ex.Message);
                    }
                }
                StopDBService();
            }
            RemoveRamDisk();
        }

        public void Setup()
        {
            if (!DBServiceIsStopped()) throw new ProvisioningException("Cannot Setup() server0815.example.com: DB Service is already running.");
            CreateRamDisk();
            PowerShellInterface shell = new PowerShellInterface();
            shell.ExecuteRemote(@"New-Item -path 'E:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\Data' -type directory", Host, Username, Password, true);
            StartDBService();
        }
    }
}
﻿namespace UniTraX.Infrastructure.Provisioning
{
    using System;
    using System.Collections.ObjectModel;
    using System.Management.Automation;

    public class PowerShellInterface
    {
        private PowerShell Shell;

        public PowerShellInterface() { }

        public void Execute(string script, bool stopAtNonTerminatingErrors)
        {
#if DEBUG
            if (script == null) throw new ArgumentNullException(nameof(script));
#endif
            Console.WriteLine("Running PowerShell script: " + script);
            try
            {
                Shell = PowerShell.Create();
                Shell.AddScript(script);
                Collection<PSObject> results = Shell.Invoke();
                Console.WriteLine("Output:");
                foreach (var psObject in results) Console.WriteLine(psObject);
                var hasErrors = Shell.Streams.Error.Count > 0;
                if (hasErrors) Console.WriteLine("Non-terminating errors:");
                foreach (ErrorRecord err in Shell.Streams.Error) Console.WriteLine(err.ToString());
                if (hasErrors && stopAtNonTerminatingErrors) throw new ProvisioningException("PowerShellInterface caught non-terminating errors.");
                Console.WriteLine("Ran PowerShell script");
                Shell.Dispose();
            }
            catch (RuntimeException ex)
            {
                Console.WriteLine("Terminating error:");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                if (Shell != null) Shell.Dispose();
                throw new ProvisioningException("PowerShellInterface caught RuntimeException.");
            }
        }

        public void ExecuteRemote(string script, string host, string username, string password, bool stopAtNonTerminatingErrors)
        {
#if DEBUG
            if (script == null) throw new ArgumentNullException(nameof(script));
            if (host == null) throw new ArgumentNullException(nameof(host));
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
#endif
            string s = @"$Pass = ConvertTo-SecureString -AsPlainText " + password + 
                " -Force; $Cred = New-Object System.Management.Automation.PSCredential -ArgumentList " + username + 
                ",$Pass; Invoke-Command -Credential $Cred -ComputerName " + host + 
                " -ScriptBlock {" + script + "} | Out-Null";
            Execute(s, stopAtNonTerminatingErrors);
        }
    }
}
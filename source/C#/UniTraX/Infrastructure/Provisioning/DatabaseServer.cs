﻿namespace UniTraX.Infrastructure.Provisioning
{
    using Microsoft.SqlServer.Management.Common;
    using Microsoft.SqlServer.Management.Smo;
    using System;

    public class DatabaseServer
    {
        private ServerConnection Connection;
        private Server Server;

        public DatabaseServer(string host, string username, string password)
        {
#if DEBUG
            if (host == null) throw new ArgumentNullException(nameof(host));
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
#endif
            Connection = new ServerConnection(host);
            Connection.LoginSecure = false;
            Connection.Login = username;
            Connection.Password = password;
            Server = new Server(Connection);
        }

        public void Drop(string databaseName)
        {
#if DEBUG
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
#endif
            Console.WriteLine("Dropping Database " + databaseName);
            try
            {
                Server.KillAllProcesses(databaseName);
                Server.KillDatabase(databaseName);
                Console.WriteLine("Dropped Database " + databaseName);
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex, true);
            }
            catch (Exception ex)
            {
                HandleException(ex, true);
            }
        }

        public void CreateOnDisk(string databaseName, string sysdataFileName, string logFileName)
        {
#if DEBUG
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
            if (sysdataFileName == null) throw new ArgumentNullException(nameof(sysdataFileName));
            if (logFileName == null) throw new ArgumentNullException(nameof(logFileName));
#endif
            Console.WriteLine("Creating On-Disk Database " + databaseName);
            try
            {
                var database = new Database(Server, databaseName)
                {
                    ContainmentType = ContainmentType.None,
                    Collation = "Latin1_General_100_CS_AS_KS_WS_SC",
                    CompatibilityLevel = CompatibilityLevel.Version140,
                    AnsiNullDefault = false,
                    AnsiWarningsEnabled = false,
                    ArithmeticAbortEnabled = false,
                    AutoClose = false,
                    AutoShrink = false,
                    AutoUpdateStatisticsEnabled = true,
                    CloseCursorsOnCommitEnabled = false,
                    LocalCursorsDefault = false,
                    ConcatenateNullYieldsNull = false,
                    NumericRoundAbortEnabled = false,
                    QuotedIdentifiersEnabled = false,
                    RecursiveTriggersEnabled = false,
                    BrokerEnabled = false,
                    AutoUpdateStatisticsAsync = false,
                    DateCorrelationOptimization = false,
                    Trustworthy = false,
                    IsParameterizationForced = false,
                    IsReadCommittedSnapshotOn = false,
                    HonorBrokerPriority = false,
                    RecoveryModel = RecoveryModel.Simple,
                    UserAccess = DatabaseUserAccess.Multiple,
                    PageVerify = PageVerify.None,
                    DatabaseOwnershipChaining = false,
                    FilestreamNonTransactedAccess = FilestreamNonTransactedAccessType.Off,
                    TargetRecoveryTime = 60,
                    DelayedDurability = DelayedDurability.Forced,
                    IsVarDecimalStorageFormatEnabled = true,
                    LegacyCardinalityEstimation = DatabaseScopedConfigurationOnOff.Off,
                    LegacyCardinalityEstimationForSecondary = DatabaseScopedConfigurationOnOff.Primary,
                    MaxDop = 1,
                    MaxDopForSecondary = 1,
                    ParameterSniffing = DatabaseScopedConfigurationOnOff.Off,
                    ParameterSniffingForSecondary = DatabaseScopedConfigurationOnOff.Primary,
                    QueryOptimizerHotfixes = DatabaseScopedConfigurationOnOff.Off,
                    QueryOptimizerHotfixesForSecondary = DatabaseScopedConfigurationOnOff.Primary
                };
                var primaryFileGroup = new FileGroup(database, "PRIMARY", FileGroupType.RowsFileGroup);
                var sysdataFile = new DataFile(primaryFileGroup, databaseName + "_SysData", sysdataFileName)
                {
                    Size = 8192,
                    Growth = 65536,
                    GrowthType = FileGrowthType.KB,
                    IsPrimaryFile = true
                };
                primaryFileGroup.Files.Add(sysdataFile);
                database.FileGroups.Add(primaryFileGroup);
                var logFile = new LogFile(database, databaseName + "_Log", logFileName)
                {
                    Size = 8192,
                    Growth = 65536,
                    GrowthType = FileGrowthType.KB
                };
                database.LogFiles.Add(logFile);
                database.Create();
                database.SetSnapshotIsolation(true);
                database.QueryStoreOptions.DesiredState = QueryStoreOperationMode.Off;
                database.SetOwner("sa");
                database.Alter();
                Console.WriteLine("Created On-Disk Database " + databaseName);
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex, true);
            }
            catch (Exception ex)
            {
                HandleException(ex, true);
            }
        }

        public void CreateInMem(string databaseName, string sysdataFileName, string logFileName, string memoptDirName)
        {
#if DEBUG
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
            if (sysdataFileName == null) throw new ArgumentNullException(nameof(sysdataFileName));
            if (logFileName == null) throw new ArgumentNullException(nameof(logFileName));
            if (memoptDirName == null) throw new ArgumentNullException(nameof(memoptDirName));
#endif
            Console.WriteLine("Creating In-Mem Database " + databaseName);
            try
            {
                var database = new Database(Server, databaseName)
                {
                    ContainmentType = ContainmentType.None,
                    Collation = "Latin1_General_100_CS_AS_KS_WS_SC",
                    CompatibilityLevel = CompatibilityLevel.Version140,
                    AnsiNullDefault = false,
                    AnsiWarningsEnabled = false,
                    ArithmeticAbortEnabled = false,
                    AutoClose = false,
                    AutoShrink = false,
                    AutoUpdateStatisticsEnabled = true,
                    CloseCursorsOnCommitEnabled = false,
                    LocalCursorsDefault = false,
                    ConcatenateNullYieldsNull = false,
                    NumericRoundAbortEnabled = false,
                    QuotedIdentifiersEnabled = false,
                    RecursiveTriggersEnabled = false,
                    BrokerEnabled = false,
                    AutoUpdateStatisticsAsync = false,
                    DateCorrelationOptimization = false,
                    Trustworthy = false,
                    IsParameterizationForced = false,
                    IsReadCommittedSnapshotOn = false,
                    HonorBrokerPriority = false,
                    RecoveryModel = RecoveryModel.Simple,
                    UserAccess = DatabaseUserAccess.Multiple,
                    PageVerify = PageVerify.None,
                    DatabaseOwnershipChaining = false,
                    FilestreamNonTransactedAccess = FilestreamNonTransactedAccessType.Off,
                    TargetRecoveryTime = 60,
                    DelayedDurability = DelayedDurability.Forced,
                    IsVarDecimalStorageFormatEnabled = true,
                    LegacyCardinalityEstimation = DatabaseScopedConfigurationOnOff.Off,
                    LegacyCardinalityEstimationForSecondary = DatabaseScopedConfigurationOnOff.Primary,
                    MaxDop = 1,
                    MaxDopForSecondary = 1,
                    ParameterSniffing = DatabaseScopedConfigurationOnOff.Off,
                    ParameterSniffingForSecondary = DatabaseScopedConfigurationOnOff.Primary,
                    QueryOptimizerHotfixes = DatabaseScopedConfigurationOnOff.Off,
                    QueryOptimizerHotfixesForSecondary = DatabaseScopedConfigurationOnOff.Primary
                };
                var primaryFileGroup = new FileGroup(database, "PRIMARY", FileGroupType.RowsFileGroup);
                var sysdataFile = new DataFile(primaryFileGroup, databaseName + "_SysData", sysdataFileName)
                {
                    Size = 8192,
                    Growth = 65536,
                    GrowthType = FileGrowthType.KB,
                    IsPrimaryFile = true
                };
                primaryFileGroup.Files.Add(sysdataFile);
                database.FileGroups.Add(primaryFileGroup);
                var logFile = new LogFile(database, databaseName + "_Log", logFileName)
                {
                    Size = 8192,
                    Growth = 65536,
                    GrowthType = FileGrowthType.KB
                };
                database.LogFiles.Add(logFile);
                var memoptFileGroup = new FileGroup(database, "MEMOPT", FileGroupType.MemoryOptimizedDataFileGroup);
                var memoptDir = new DataFile(memoptFileGroup, databaseName + "_MemOpt", memoptDirName);
                memoptFileGroup.Files.Add(memoptDir);
                database.FileGroups.Add(memoptFileGroup);
                database.Create();
                database.SetSnapshotIsolation(true);
                database.QueryStoreOptions.DesiredState = QueryStoreOperationMode.Off;
                database.SetOwner("sa");
                database.Alter();
                Console.WriteLine("Created In-Mem Database " + databaseName);
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex, true);
            }
            catch (Exception ex)
            {
                HandleException(ex, true);
            }
        }

        public Database GetDatabase(string name)
        {
            try
            {
                var db = Server.Databases[name];
                if (db == null) throw new ProvisioningException("Could not retrieve database " + name);
                return db;
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex, true);
            }
            catch (Exception ex)
            {
                HandleException(ex, true);
            }
            return null;
        }

        public void MakeDatabaseReadonly(string name)
        {
            Console.WriteLine("Making Database " + name + " read-only.");
            try
            {
                var db = Server.Databases[name];
                if (db == null) throw new ProvisioningException("Could not retrieve database " + name);
                db.DatabaseOptions.ReadOnly = true;
                db.Alter(TerminationClause.RollbackTransactionsImmediately);
            }
            catch (SmoException smoex)
            {
                HandleSMOException(smoex, false);
            }
            catch (Exception ex)
            {
                HandleException(ex, true);
            }
            Console.WriteLine("Made Database " + name + " read-only.");
        }

        private void HandleSMOException(SmoException smoex, bool throwProvisioningException)
        {
#if DEBUG
            if (smoex == null) throw new ArgumentNullException(nameof(smoex));
#endif
            Console.WriteLine("This is an SMO Exception");
            Console.WriteLine(smoex.Message);
            Console.WriteLine(smoex.StackTrace);
            Exception ex;
            ex = smoex;
            while (!object.ReferenceEquals(ex.InnerException, (null)))
            {
                Console.WriteLine(ex.InnerException.Message);
                Console.WriteLine(ex.InnerException.StackTrace);
                ex = ex.InnerException;
            }
            if (throwProvisioningException) throw new ProvisioningException("DatabaseServer caught SMO exception.");
        }

        private void HandleException(Exception ex, bool throwProvisioningException)
        {
#if DEBUG
            if (ex == null) throw new ArgumentNullException(nameof(ex));
#endif
            Console.WriteLine("This is not an SMO exception.");
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
            if (throwProvisioningException) throw new ProvisioningException("DatabaseServer caught non-SMO exception.");
        }
    }
}
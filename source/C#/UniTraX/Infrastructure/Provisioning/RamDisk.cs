﻿namespace UniTraX.Infrastructure.Provisioning
{
    using System;

    public class RamDisk
    {
        public static void Create(string host, string username, string password, string hostname, string size)
        {
#if DEBUG
            if (host == null) throw new ArgumentNullException(nameof(host));
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (hostname == null) throw new ArgumentNullException(nameof(hostname));
            if (size == null) throw new ArgumentNullException(nameof(size));
#endif
            Console.WriteLine("Creating RamDisk on " + host + " with username " + username + " password " + password + " hostname " + hostname + " and size " + size);
            PowerShellInterface shell = new PowerShellInterface();
            shell.ExecuteRemote(@"New-IscsiVirtualDisk -Path 'ramdisk:dbdisk.vhdx' -Size " + size, host, username, password, true);
            shell.ExecuteRemote(@"New-IscsiServerTarget -TargetName 'dbdisk' -InitiatorIds 'IQN:iqn.1991-05.com.microsoft:" + host + @"'", host, username, password, true);
            shell.ExecuteRemote(@"Add-IscsiVirtualDiskTargetMapping -TargetName 'dbdisk' -Path 'ramdisk:dbdisk.vhdx'", host, username, password, true);
            shell.ExecuteRemote(@"New-IscsiTargetPortal -TargetPortalAddress '" + host + @"' -TargetPortalPortNumber 3260", host, username, password, true);
            shell.ExecuteRemote(@"$address = (Get-InitiatorPort); $address | Select NodeAddress; Set-InitiatorPort -NodeAddress $address.NodeAddress -NewNodeAddress 'iqn.1991-05.com.microsoft:" + host + "'", host, username, password, true);
            shell.ExecuteRemote(@"Get-IscsiTarget", host, username, password, true);
            shell.ExecuteRemote(@"Connect-IscsiTarget -NodeAddress 'iqn.1991-05.com.microsoft:" + hostname + @"-dbdisk-target' -IsPersistent $False", host, username, password, true);
            shell.ExecuteRemote(@"Initialize-Disk -Number 1", host, username, password, true);
            shell.ExecuteRemote(@"New-Partition -DiskNumber 1 -UseMaximumSize -DriveLetter E -Alignment 1048576", host, username, password, true);
            shell.ExecuteRemote(@"Format-Volume -DriveLetter E -AllocationUnitSize 65536 -FileSystem 'NTFS'", host, username, password, true);
            shell.ExecuteRemote(@"Remove-NTFSAccess -Path 'E:\' -Account 'BUILTIN\Users' -AccessRights AppendData", host, username, password, true);
            shell.ExecuteRemote(@"Remove-NTFSAccess -Path 'E:\' -Account 'BUILTIN\Users' -AccessRights CreateFiles", host, username, password, true);
            shell.ExecuteRemote(@"Remove-NTFSAccess -Path 'E:\' -Account 'BUILTIN\Users' -AccessRights ReadAndExecute", host, username, password, true);
            shell.ExecuteRemote(@"Add-NTFSAccess -Path 'E:\' -Account 'BUILTIN\Users' -AccessRights FullControl -AppliesTo ThisFolderSubfoldersAndFiles", host, username, password, true);
            Console.WriteLine("Created RamDisk on " + host);
        }

        public static void Remove(string host, string username, string password, string hostname)
        {
#if DEBUG
            if (host == null) throw new ArgumentNullException(nameof(host));
            if (username == null) throw new ArgumentNullException(nameof(username));
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (hostname == null) throw new ArgumentNullException(nameof(hostname));
#endif
            Console.WriteLine("Removing RamDisk on " + host + " with username " + username + " password " + password + " and hostname " + hostname);
            PowerShellInterface shell = new PowerShellInterface();
            shell.ExecuteRemote(@"Get-IscsiTarget", host, username, password, false);
            shell.ExecuteRemote(@"Disconnect-IscsiTarget -NodeAddress 'iqn.1991-05.com.microsoft:" + hostname + @"-dbdisk-target' -Confirm:$false", host, username, password, false);
            shell.ExecuteRemote(@"Remove-IscsiTargetPortal -TargetPortalAddress 'localhost' -Confirm:$false", host, username, password, false);
            shell.ExecuteRemote(@"Remove-IscsiTargetPortal -TargetPortalAddress '" + hostname + @"' -Confirm:$false", host, username, password, false);
            shell.ExecuteRemote(@"Remove-IscsiVirtualDiskTargetMapping -TargetName 'dbdisk' -Path 'ramdisk:dbdisk.vhdx'", host, username, password, false);
            shell.ExecuteRemote(@"Remove-IscsiServerTarget -TargetName 'dbdisk'", host, username, password, false);
            shell.ExecuteRemote(@"Remove-IscsiVirtualDisk -Path 'ramdisk:dbdisk.vhdx'", host, username, password, false);
            Console.WriteLine("Removed RamDisk on " + host);
        }
    }
}
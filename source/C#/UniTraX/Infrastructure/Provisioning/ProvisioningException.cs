﻿namespace UniTraX.Infrastructure.Provisioning
{
    using System;

    public class ProvisioningException : Exception
    {
        public ProvisioningException(string message) : base(message) { }
    }
}
﻿namespace UniTraX.Infrastructure.DataLoading
{
    using System;
    using System.Data;
    using System.IO;
    using UniTraX.Infrastructure.BudgetProvider;

    public class DataTableProvider
    {
        private readonly string DataFilename;
        private IBudgetProvider BudgetProvider;
        private readonly string[] ColumnNames;
        private readonly string BudgetColumnName;
        private readonly string[] StringColumnNames;

        public DataTableProvider(string dataFilename, IBudgetProvider budgetProvider, string[] columnNames, string budgetColumnName, string[] stringColumnNames)
        {
#if DEBUG
            if (dataFilename == null) throw new ArgumentNullException(nameof(dataFilename));
            if (columnNames == null) throw new ArgumentNullException(nameof(columnNames));
            if (columnNames.Length == 0) throw new ArgumentException("column names is empty");
            if (budgetColumnName == null) throw new ArgumentNullException(nameof(budgetColumnName));
#endif
            DataFilename = dataFilename;
            BudgetProvider = budgetProvider;
            ColumnNames = columnNames;
            BudgetColumnName = budgetColumnName;
            StringColumnNames = stringColumnNames;
        }

        public DataTable GetDataTable()
        {
            Console.WriteLine("Loading data into memory.");
            var dataTable = new DataTable();
            int rowCount = 0;
            var topPos = 0;
            Console.Write("Loaded rows: " + rowCount);
            foreach (var c in ColumnNames)
            {
                DataColumn column;
                if (c.Equals(BudgetColumnName) && BudgetProvider == null) continue;
                else if (StringColumnNames != null && Array.IndexOf(StringColumnNames, c) > -1)
                {
                    column = new DataColumn
                    {
                        DataType = System.Type.GetType("System.String"),
                        ColumnName = c
                    };
                }
                else
                {
                    column = new DataColumn
                    {
                        DataType = System.Type.GetType("System.Double"),
                        ColumnName = c
                    };
                }
                dataTable.Columns.Add(column);
            }
            using (var reader = new StreamReader(DataFilename))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    try
                    {
                        var row = dataTable.NewRow();
                        for (var i = 0; i < ColumnNames.Length; i++)
                        {
                            var c = ColumnNames[i];
                            var v = values[i];
                            v = v.Replace("\"", "");
                            {

                            }
                            if (c.Equals(BudgetColumnName))
                            {
                                if (BudgetProvider == null) continue;
                                else row[c] = BudgetProvider.NextBudget();
                            }
                            else if (StringColumnNames != null && Array.IndexOf(StringColumnNames, c) > -1)
                            {
                                row[c] = v;
                            }
                            else
                            {
                                row[c] = Double.Parse(v);
                            }
                        }
                        dataTable.Rows.Add(row);
                    }
                    catch (FormatException ex)
                    {
                        topPos = Console.CursorTop;
                        Console.SetCursorPosition(13, topPos);
                        Console.Write(rowCount);
                        Console.WriteLine();
                        Console.WriteLine("DataTableProvider caught FormatException: " + ex.Message + " In line: " + line);
                        Console.Write("Loaded rows: " + rowCount);
                        continue;
                    }
                    rowCount++;
                    if (rowCount % 10000 == 0)
                    {
                        topPos = Console.CursorTop;
                        Console.SetCursorPosition(13, topPos);
                        Console.Write(rowCount);
                    }
                }
                topPos = Console.CursorTop;
                Console.SetCursorPosition(13, topPos);
                Console.Write(rowCount);
            }
            Console.WriteLine();
            Console.WriteLine("Loaded data into memory.");
            return dataTable;
        }
    }
}
﻿namespace UniTraX.Infrastructure.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public class TableMem
    {
        public static Index CreateDefaultIndex(Table table, string name)
        {
#if DEBUG
            if (table == null) throw new ArgumentNullException(nameof(table));
            if (name == null) throw new ArgumentNullException(nameof(name));
#endif
            var index = new Index(table, name)
            {
                IndexType = IndexType.NonClusteredIndex,
                IndexKeyType = IndexKeyType.None
            };
            return index;
        }

        public static Table CreateTableInit(Database database, bool withBudget, string databaseName, string tableName, string[] columnNames, string primaryKeyName, string budgetColumnName, Dictionary<string, DataType> specialColumns)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
            if (databaseName == null) throw new ArgumentNullException(nameof(databaseName));
            if (tableName == null) throw new ArgumentNullException(nameof(tableName));
            if (columnNames == null) throw new ArgumentNullException(nameof(columnNames));
            if (columnNames.Length == 0) throw new ArgumentException("column names is empty");
            if (primaryKeyName == null) throw new ArgumentNullException(nameof(primaryKeyName));
            if (budgetColumnName == null) throw new ArgumentNullException(nameof(budgetColumnName));
#endif
            Console.WriteLine(databaseName + ": Creating table " + tableName + " " + (withBudget ? "with" : "without") + " budget.");
            var table = new Table(database, tableName)
            {
                IsMemoryOptimized = true,
                Durability = DurabilityType.SchemaOnly
            };
            foreach (var c in columnNames)
            {
                if (c.Equals(budgetColumnName) && !withBudget) continue;
                else if (specialColumns != null && specialColumns.ContainsKey(c))
                {
                    table.Columns.Add(new Column(table, c, specialColumns[c])
                    {
                        Nullable = false
                    });
                }
                else
                {
                    table.Columns.Add(new Column(table, c, DataType.Float)
                    {
                        Nullable = false
                    });
                }
            }
            var indexPk = new Index(table, "PK_" + databaseName + "_" + tableName)
            {
                IndexType = IndexType.NonClusteredIndex,
                IndexKeyType = IndexKeyType.DriPrimaryKey
            };
            indexPk.IndexedColumns.Add(new IndexedColumn(indexPk, primaryKeyName, false));
            table.Indexes.Add(indexPk);
            return table;
        }

        public static void CreateTableFinish(Table table, string tableName)
        {
#if DEBUG
            if (table == null) throw new ArgumentNullException(nameof(table));
            if (tableName == null) throw new ArgumentNullException(nameof(tableName));
#endif
            table.Create();
            Console.WriteLine("TableMem: Created table " + tableName);
        }

        public static void FillTable(DataTable dataTable, SqlConnection sqlConnection, string tableName)
        {
#if DEBUG
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
            if (sqlConnection == null) throw new ArgumentNullException(nameof(sqlConnection));
            if (tableName == null) throw new ArgumentNullException(nameof(tableName));
#endif
            SqlBulkCopy sqlbc = new SqlBulkCopy(sqlConnection, SqlBulkCopyOptions.KeepIdentity | SqlBulkCopyOptions.CheckConstraints, null)
            {
                DestinationTableName = "[" + tableName + "]",
                BulkCopyTimeout = 600
            };
            sqlbc.WriteToServer(dataTable);
        }
    }
}
﻿namespace UniTraX.Infrastructure.BudgetProvider
{
    public interface IBudgetProvider
    {
        double NextBudget();
    }
}
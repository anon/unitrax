﻿namespace UniTraX.Infrastructure.BudgetProvider
{
    public class ConstantBudgetProvider :IBudgetProvider
    {
        private readonly double Budget;

        public ConstantBudgetProvider(double budget)
        {
            Budget = budget;
        }

        public double NextBudget()
        {
            return Budget;
        }
    }
}
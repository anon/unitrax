﻿namespace UniTraX.Datasets.Medical.Queries
{
	using PINQ;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Queries.Core;

    public class WhereSusp<R> : IPINQCondition<R>
    {
        private readonly Expression<Func<R, bool>> SuspExpression;

        public WhereSusp(Expression<Func<R, bool>> suspExpression)
        {
#if DEBUG
            if (suspExpression == null) throw new ArgumentNullException(nameof(suspExpression));
#endif
            SuspExpression = suspExpression;
        }

        public IQueryable<R>[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return new IQueryable<R>[]
            {
                queryable.Where(SuspExpression)
            };
        }

        public PINQueryable<R>[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return new PINQueryable<R>[]
            {
                queryable.Where(SuspExpression)
            };
        }

        public IPINQCondition<R> Clone()
        {
            return new WhereSusp<R>(SuspExpression);
        }
    }
}
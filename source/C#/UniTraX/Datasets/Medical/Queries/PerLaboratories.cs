﻿namespace UniTraX.Datasets.Medical.Queries
{
	using PINQ;
    using System.Linq;
    using System.Linq.Expressions;
    using System;
    using UniTraX.Queries.Core;

    public class PerLaboratoriesCount<R> : IPINQCondition<R>
    {
        private class KeyExpression<T>
        {
            public static Expression<Func<T, int>> Generate(Expression<Func<T, double>> countExpression)
            {
#if DEBUG
                if (countExpression == null) throw new ArgumentNullException(nameof(countExpression));
#endif
                Expression expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(100.0)), Expression.Constant(100), Expression.Constant(101));
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(50.0)), Expression.Constant(50), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(20.0)), Expression.Constant(20), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(10.0)), Expression.Constant(10), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(5.0)), Expression.Constant(5), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(4.0)), Expression.Constant(4), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(3.0)), Expression.Constant(3), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(2.0)), Expression.Constant(2), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(1.0)), Expression.Constant(1), expression);
                expression = Expression.Condition(Expression.LessThanOrEqual(countExpression.Body, Expression.Constant(0.0)), Expression.Constant(0), expression);
                return Expression.Lambda<Func<T, int>>(expression, countExpression.Parameters);
            }
        }

        private static readonly int[] Keys = new int[] { 0, 1, 2, 3, 4, 5, 10, 20, 50, 100, 101 };

        private readonly Expression<Func<R, double>> CountExpression;

        public PerLaboratoriesCount(Expression<Func<R, double>> countExpression)
        {
#if DEBUG
            if (countExpression == null) throw new ArgumentNullException(nameof(countExpression));
#endif
            CountExpression = countExpression;
        }

        public IQueryable<R>[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            IQueryable<R>[] queryables = new IQueryable<R>[Keys.Length];
            foreach (var group in queryable.GroupBy(KeyExpression<R>.Generate(CountExpression))) queryables[Array.IndexOf(Keys, group.Key)] = group.AsQueryable<R>();
            for (int i = 0; i < queryables.Length; i++) if (queryables[i] == null) queryables[i] = Enumerable.Empty<R>().AsQueryable();
            return queryables;
        }

        public PINQueryable<R>[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var queryableMap = queryable.Partition(Keys, KeyExpression<R>.Generate(CountExpression));
            var queryables = new PINQueryable<R>[Keys.Length];
            for (int i = 0; i < Keys.Length; i++) queryables[i] = queryableMap[Keys[i]];
            return queryables;
        }

        public IPINQCondition<R> Clone()
        {
            return new PerLaboratoriesCount<R>(CountExpression);
        }
    }
}
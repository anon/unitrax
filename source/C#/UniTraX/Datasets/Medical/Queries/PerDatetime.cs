﻿namespace UniTraX.Datasets.Medical.Queries
{
    using System.Collections.Generic;
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Medical.DataAccess;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Queries.Core;

    public class PerYear1970to2000<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeDate1970to2000 = new DoubleTypeConverter(new DoubleType(0.0, 946684799.0, 1.0));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(31)
            {
                new Part(TypeDate1970to2000.ToSystem(0.0), TypeDate1970to2000.ToSystem(0.0), 99999),
                new Part(TypeDate1970to2000.ToSystem(1.0), TypeDate1970to2000.ToSystem(31536000.0 - 1.0), 1970),
                new Part(TypeDate1970to2000.ToSystem(31536000.0), TypeDate1970to2000.ToSystem(63072000.0 - 1.0), 1971),
                new Part(TypeDate1970to2000.ToSystem(63072000.0), TypeDate1970to2000.ToSystem(94694400.0 - 1.0), 1972),
                new Part(TypeDate1970to2000.ToSystem(94694400.0), TypeDate1970to2000.ToSystem(126230400.0 - 1.0), 1973),
                new Part(TypeDate1970to2000.ToSystem(126230400.0), TypeDate1970to2000.ToSystem(157766400.0 - 1.0), 1974),
                new Part(TypeDate1970to2000.ToSystem(157766400.0), TypeDate1970to2000.ToSystem(189302400.0 - 1.0), 1975),
                new Part(TypeDate1970to2000.ToSystem(189302400.0), TypeDate1970to2000.ToSystem(220924800.0 - 1.0), 1976),
                new Part(TypeDate1970to2000.ToSystem(220924800.0), TypeDate1970to2000.ToSystem(252460800.0 - 1.0), 1977),
                new Part(TypeDate1970to2000.ToSystem(252460800.0), TypeDate1970to2000.ToSystem(283996800.0 - 1.0), 1978),
                new Part(TypeDate1970to2000.ToSystem(283996800.0), TypeDate1970to2000.ToSystem(315532800.0 - 1.0), 1979),
                new Part(TypeDate1970to2000.ToSystem(315532800.0), TypeDate1970to2000.ToSystem(347155200.0 - 1.0), 1980),
                new Part(TypeDate1970to2000.ToSystem(347155200.0), TypeDate1970to2000.ToSystem(378691200.0 - 1.0), 1981),
                new Part(TypeDate1970to2000.ToSystem(378691200.0), TypeDate1970to2000.ToSystem(410227200.0 - 1.0), 1982),
                new Part(TypeDate1970to2000.ToSystem(410227200.0), TypeDate1970to2000.ToSystem(441763200.0 - 1.0), 1983),
                new Part(TypeDate1970to2000.ToSystem(441763200.0), TypeDate1970to2000.ToSystem(473385600.0 - 1.0), 1984),
                new Part(TypeDate1970to2000.ToSystem(473385600.0), TypeDate1970to2000.ToSystem(504921600.0 - 1.0), 1985),
                new Part(TypeDate1970to2000.ToSystem(504921600.0), TypeDate1970to2000.ToSystem(536457600.0 - 1.0), 1986),
                new Part(TypeDate1970to2000.ToSystem(536457600.0), TypeDate1970to2000.ToSystem(567993600.0 - 1.0), 1987),
                new Part(TypeDate1970to2000.ToSystem(567993600.0), TypeDate1970to2000.ToSystem(599616000.0 - 1.0), 1988),
                new Part(TypeDate1970to2000.ToSystem(599616000.0), TypeDate1970to2000.ToSystem(631152000.0 - 1.0), 1989),
                new Part(TypeDate1970to2000.ToSystem(631152000.0), TypeDate1970to2000.ToSystem(662688000.0 - 1.0), 1990),
                new Part(TypeDate1970to2000.ToSystem(662688000.0), TypeDate1970to2000.ToSystem(694224000.0 - 1.0), 1991),
                new Part(TypeDate1970to2000.ToSystem(694224000.0), TypeDate1970to2000.ToSystem(725846400.0 - 1.0), 1992),
                new Part(TypeDate1970to2000.ToSystem(725846400.0), TypeDate1970to2000.ToSystem(757382400.0 - 1.0), 1993),
                new Part(TypeDate1970to2000.ToSystem(757382400.0), TypeDate1970to2000.ToSystem(788918400.0 - 1.0), 1994),
                new Part(TypeDate1970to2000.ToSystem(788918400.0), TypeDate1970to2000.ToSystem(820454400.0 - 1.0), 1995),
                new Part(TypeDate1970to2000.ToSystem(820454400.0), TypeDate1970to2000.ToSystem(852076800.0 - 1.0), 1996),
                new Part(TypeDate1970to2000.ToSystem(852076800.0), TypeDate1970to2000.ToSystem(883612800.0 - 1.0), 1997),
                new Part(TypeDate1970to2000.ToSystem(883612800.0), TypeDate1970to2000.ToSystem(915148800.0 - 1.0), 1998),
                new Part(TypeDate1970to2000.ToSystem(915148800.0), TypeDate1970to2000.ToSystem(946684800.0 - 1.0), 1999)
            };
        }

        public PerYear1970to2000(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerYear1990to2000<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeDate1990to2000 = new DoubleTypeConverter(new DoubleType(631152000.0, 946684799.0, 1.0));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(11)
            {
                new Part(TypeDate1990to2000.ToSystem(631152000.0), TypeDate1990to2000.ToSystem(631152000.0), 99999),
                new Part(TypeDate1990to2000.ToSystem(631152001.0), TypeDate1990to2000.ToSystem(662688000.0 - 1.0), 1990),
                new Part(TypeDate1990to2000.ToSystem(662688000.0), TypeDate1990to2000.ToSystem(694224000.0 - 1.0), 1991),
                new Part(TypeDate1990to2000.ToSystem(694224000.0), TypeDate1990to2000.ToSystem(725846400.0 - 1.0), 1992),
                new Part(TypeDate1990to2000.ToSystem(725846400.0), TypeDate1990to2000.ToSystem(757382400.0 - 1.0), 1993),
                new Part(TypeDate1990to2000.ToSystem(757382400.0), TypeDate1990to2000.ToSystem(788918400.0 - 1.0), 1994),
                new Part(TypeDate1990to2000.ToSystem(788918400.0), TypeDate1990to2000.ToSystem(820454400.0 - 1.0), 1995),
                new Part(TypeDate1990to2000.ToSystem(820454400.0), TypeDate1990to2000.ToSystem(852076800.0 - 1.0), 1996),
                new Part(TypeDate1990to2000.ToSystem(852076800.0), TypeDate1990to2000.ToSystem(883612800.0 - 1.0), 1997),
                new Part(TypeDate1990to2000.ToSystem(883612800.0), TypeDate1990to2000.ToSystem(915148800.0 - 1.0), 1998),
                new Part(TypeDate1990to2000.ToSystem(915148800.0), TypeDate1990to2000.ToSystem(946684800.0 - 1.0), 1999)
            };
        }

        public PerYear1990to2000(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerDecade1900to1990<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeDate1900to1990 = new DoubleTypeConverter(new DoubleType(-2208988800.0, 631151999.0, 1.0));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(10)
            {
                new Part(TypeDate1900to1990.ToSystem(-2208988800.0), TypeDate1900to1990.ToSystem(-2208988800.0), 99999),
                new Part(TypeDate1900to1990.ToSystem(-2208988799.0), TypeDate1900to1990.ToSystem(-1893456000.0 - 1.0), 1900),
                new Part(TypeDate1900to1990.ToSystem(-1893456000.0), TypeDate1900to1990.ToSystem(-1577923200.0 - 1.0), 1910),
                new Part(TypeDate1900to1990.ToSystem(-1577923200.0), TypeDate1900to1990.ToSystem(-1262304000.0 - 1.0), 1920),
                new Part(TypeDate1900to1990.ToSystem(-1262304000.0), TypeDate1900to1990.ToSystem(-946771200.0 - 1.0), 1930),
                new Part(TypeDate1900to1990.ToSystem(-946771200.0), TypeDate1900to1990.ToSystem(-631152000.0 - 1.0), 1940),
                new Part(TypeDate1900to1990.ToSystem(-631152000.0), TypeDate1900to1990.ToSystem(-315619200.0 - 1.0), 1950),
                new Part(TypeDate1900to1990.ToSystem(-315619200.0), TypeDate1900to1990.ToSystem(0.0 - 1.0), 1960),
                new Part(TypeDate1900to1990.ToSystem(0.0), TypeDate1900to1990.ToSystem(315532800.0 - 1.0), 1970),
                new Part(TypeDate1900to1990.ToSystem(315532800.0), TypeDate1900to1990.ToSystem(631152000.0 - 1.0), 1980)
            };
        }

        public PerDecade1900to1990(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerDecade1970to2000<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeDate1970to2000 = new DoubleTypeConverter(new DoubleType(0.0, 946684799.0, 1.0));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(4)
            {
                new Part(TypeDate1970to2000.ToSystem(0.0), TypeDate1970to2000.ToSystem(0.0), 99999),
                new Part(TypeDate1970to2000.ToSystem(1.0), TypeDate1970to2000.ToSystem(315532800.0 - 1.0), 1970),
                new Part(TypeDate1970to2000.ToSystem(315532800.0), TypeDate1970to2000.ToSystem(631152000.0 - 1.0), 1980),
                new Part(TypeDate1970to2000.ToSystem(631152000.0), TypeDate1970to2000.ToSystem(946684800.0 - 1.0), 1990)
            };
        }

        public PerDecade1970to2000(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }
}
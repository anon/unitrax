﻿namespace UniTraX.Datasets.Medical.Queries
{
    using System.Collections.Generic;
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Medical.DataAccess;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Queries.Core;

    public class PerAclIga<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeMedicalAclIga = new DoubleTypeConverter(new DoubleType(-1.0, 49999.9, 0.1));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(16)
            {
                new Part(TypeMedicalAclIga.ToSystem(-1.0), TypeMedicalAclIga.ToSystem(-0.1), 99999),
                new Part(TypeMedicalAclIga.ToSystem(0.0), TypeMedicalAclIga.ToSystem(0.0), 0),
                new Part(TypeMedicalAclIga.ToSystem(0.1), TypeMedicalAclIga.ToSystem(1.0), 1),
                new Part(TypeMedicalAclIga.ToSystem(1.1), TypeMedicalAclIga.ToSystem(2.0), 2),
                new Part(TypeMedicalAclIga.ToSystem(2.1), TypeMedicalAclIga.ToSystem(3.0), 3),
                new Part(TypeMedicalAclIga.ToSystem(3.1), TypeMedicalAclIga.ToSystem(4.0), 4),
                new Part(TypeMedicalAclIga.ToSystem(4.1), TypeMedicalAclIga.ToSystem(5.0), 5),
                new Part(TypeMedicalAclIga.ToSystem(5.1), TypeMedicalAclIga.ToSystem(6.0), 6),
                new Part(TypeMedicalAclIga.ToSystem(6.1), TypeMedicalAclIga.ToSystem(7.0), 7),
                new Part(TypeMedicalAclIga.ToSystem(7.1), TypeMedicalAclIga.ToSystem(8.0), 8),
                new Part(TypeMedicalAclIga.ToSystem(8.1), TypeMedicalAclIga.ToSystem(9.0), 9),
                new Part(TypeMedicalAclIga.ToSystem(9.1), TypeMedicalAclIga.ToSystem(10.0), 10),
                new Part(TypeMedicalAclIga.ToSystem(10.1), TypeMedicalAclIga.ToSystem(15.0), 15),
                new Part(TypeMedicalAclIga.ToSystem(15.1), TypeMedicalAclIga.ToSystem(20.0), 20),
                new Part(TypeMedicalAclIga.ToSystem(20.1), TypeMedicalAclIga.ToSystem(30.0), 30),
                new Part(TypeMedicalAclIga.ToSystem(30.1), TypeMedicalAclIga.ToSystem(50000.0 - 0.1), 50000)
            };
        }

        public PerAclIga(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerAclIgg<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeMedicalAclIgg = new DoubleTypeConverter(new DoubleType(-1.0, 999.9, 0.1));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(23)
            {
                new Part(TypeMedicalAclIgg.ToSystem(-1.0), TypeMedicalAclIgg.ToSystem(-0.1), 99999),
                new Part(TypeMedicalAclIgg.ToSystem(0.0), TypeMedicalAclIgg.ToSystem(0.0), 0),
                new Part(TypeMedicalAclIgg.ToSystem(0.1), TypeMedicalAclIgg.ToSystem(0.1), 1),
                new Part(TypeMedicalAclIgg.ToSystem(0.2), TypeMedicalAclIgg.ToSystem(0.2), 2),
                new Part(TypeMedicalAclIgg.ToSystem(0.3), TypeMedicalAclIgg.ToSystem(0.3), 3),
                new Part(TypeMedicalAclIgg.ToSystem(0.4), TypeMedicalAclIgg.ToSystem(0.4), 4),
                new Part(TypeMedicalAclIgg.ToSystem(0.5), TypeMedicalAclIgg.ToSystem(0.5), 5),
                new Part(TypeMedicalAclIgg.ToSystem(0.6), TypeMedicalAclIgg.ToSystem(0.6), 6),
                new Part(TypeMedicalAclIgg.ToSystem(0.7), TypeMedicalAclIgg.ToSystem(0.7), 7),
                new Part(TypeMedicalAclIgg.ToSystem(0.8), TypeMedicalAclIgg.ToSystem(0.8), 8),
                new Part(TypeMedicalAclIgg.ToSystem(0.9), TypeMedicalAclIgg.ToSystem(0.9), 9),
                new Part(TypeMedicalAclIgg.ToSystem(1.0), TypeMedicalAclIgg.ToSystem(1.0), 10),
                new Part(TypeMedicalAclIgg.ToSystem(1.1), TypeMedicalAclIgg.ToSystem(1.1), 11),
                new Part(TypeMedicalAclIgg.ToSystem(1.2), TypeMedicalAclIgg.ToSystem(1.2), 12),
                new Part(TypeMedicalAclIgg.ToSystem(1.3), TypeMedicalAclIgg.ToSystem(1.3), 13),
                new Part(TypeMedicalAclIgg.ToSystem(1.4), TypeMedicalAclIgg.ToSystem(1.4), 14),
                new Part(TypeMedicalAclIgg.ToSystem(1.5), TypeMedicalAclIgg.ToSystem(1.5), 15),
                new Part(TypeMedicalAclIgg.ToSystem(1.6), TypeMedicalAclIgg.ToSystem(2.0), 20),
                new Part(TypeMedicalAclIgg.ToSystem(2.1), TypeMedicalAclIgg.ToSystem(3.0), 30),
                new Part(TypeMedicalAclIgg.ToSystem(3.1), TypeMedicalAclIgg.ToSystem(4.0), 40),
                new Part(TypeMedicalAclIgg.ToSystem(4.1), TypeMedicalAclIgg.ToSystem(5.0), 50),
                new Part(TypeMedicalAclIgg.ToSystem(5.1), TypeMedicalAclIgg.ToSystem(10.0), 100),
                new Part(TypeMedicalAclIgg.ToSystem(10.1), TypeMedicalAclIgg.ToSystem(1000.0 - 0.1), 10000)
            };
        }

        public PerAclIgg(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerAclIgm<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeMedicalAclIgm = new DoubleTypeConverter(new DoubleType(-1.0, 199999.9, 0.1));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(17)
            {
                new Part(TypeMedicalAclIgm.ToSystem(-1.0), TypeMedicalAclIgm.ToSystem(-0.1), 9999999),
                new Part(TypeMedicalAclIgm.ToSystem(0.0), TypeMedicalAclIgm.ToSystem(0.0), 0),
                new Part(TypeMedicalAclIgm.ToSystem(0.1), TypeMedicalAclIgm.ToSystem(1.0), 10),
                new Part(TypeMedicalAclIgm.ToSystem(1.1), TypeMedicalAclIgm.ToSystem(1.2), 12),
                new Part(TypeMedicalAclIgm.ToSystem(1.3), TypeMedicalAclIgm.ToSystem(1.4), 14),
                new Part(TypeMedicalAclIgm.ToSystem(1.5), TypeMedicalAclIgm.ToSystem(1.6), 16),
                new Part(TypeMedicalAclIgm.ToSystem(1.7), TypeMedicalAclIgm.ToSystem(1.8), 18),
                new Part(TypeMedicalAclIgm.ToSystem(1.9), TypeMedicalAclIgm.ToSystem(2.0), 20),
                new Part(TypeMedicalAclIgm.ToSystem(2.1), TypeMedicalAclIgm.ToSystem(2.2), 22),
                new Part(TypeMedicalAclIgm.ToSystem(2.3), TypeMedicalAclIgm.ToSystem(2.4), 24),
                new Part(TypeMedicalAclIgm.ToSystem(2.5), TypeMedicalAclIgm.ToSystem(2.6), 26),
                new Part(TypeMedicalAclIgm.ToSystem(2.7), TypeMedicalAclIgm.ToSystem(2.8), 28),
                new Part(TypeMedicalAclIgm.ToSystem(2.9), TypeMedicalAclIgm.ToSystem(3.0), 30),
                new Part(TypeMedicalAclIgm.ToSystem(3.1), TypeMedicalAclIgm.ToSystem(4.0), 40),
                new Part(TypeMedicalAclIgm.ToSystem(4.1), TypeMedicalAclIgm.ToSystem(5.0), 50),
                new Part(TypeMedicalAclIgm.ToSystem(5.1), TypeMedicalAclIgm.ToSystem(10.0), 100),
                new Part(TypeMedicalAclIgm.ToSystem(10.1), TypeMedicalAclIgm.ToSystem(200000.0 - 0.1), 2000000)
            };
        }

        public PerAclIgm(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerAna<R> : PerNumbers<R>
    {
        private static DoubleTypeConverter TypeMedicalAna = new DoubleTypeConverter(new DoubleType(-1.0, 4097.0, 1.0));

        public static List<Part> GenerateParts()
        {
            return new List<Part>(9)
            {
                new Part(TypeMedicalAna.ToSystem(-1.0), TypeMedicalAna.ToSystem(-1.0), 9999),
                new Part(TypeMedicalAna.ToSystem(0.0), TypeMedicalAna.ToSystem(0.0), 0),
                new Part(TypeMedicalAna.ToSystem(1.0), TypeMedicalAna.ToSystem(4.0), 4),
                new Part(TypeMedicalAna.ToSystem(5.0), TypeMedicalAna.ToSystem(16.0), 16),
                new Part(TypeMedicalAna.ToSystem(17.0), TypeMedicalAna.ToSystem(64.0), 64),
                new Part(TypeMedicalAna.ToSystem(65.0), TypeMedicalAna.ToSystem(256.0), 256),
                new Part(TypeMedicalAna.ToSystem(257.0), TypeMedicalAna.ToSystem(1024.0), 1024),
                new Part(TypeMedicalAna.ToSystem(1025.0), TypeMedicalAna.ToSystem(4096.0), 4096),
                new Part(TypeMedicalAna.ToSystem(4097.0), TypeMedicalAna.ToSystem(4097.0), 4097),
            };
        }

        public PerAna(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }
}
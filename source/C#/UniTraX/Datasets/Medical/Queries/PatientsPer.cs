﻿namespace UniTraX.Datasets.Medical.Queries
{
    using System.Linq;
    using UniTraX.Datasets.Medical.DataAccess;
    using UniTraX.Queries.Aggregation;
    using UniTraX.Queries.Core;

    public class PatientsPerThrombosis : AnalyticBaseFactory<Patient, PatientB>
    {
        public PatientsPerThrombosis(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerThrombosis", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new PerThrombosis<Patient>(Metadata.Record.Fields[35],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerThrombosis<PatientB>(Metadata.RecordB.Fields[35],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class SevereThrombosisPatientsPerSex : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerSex(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerSex", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerSex<Patient>(Metadata.Record.Fields[1],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerSex<PatientB>(Metadata.RecordB.Fields[1],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerDateOfBirth : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerDateOfBirth(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerDateOfBirth", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerDecade1900to1990<Patient>(Metadata.Record.Fields[2],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerDecade1900to1990<PatientB>(Metadata.RecordB.Fields[2],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerDateOfInitialDataCollection : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerDateOfInitialDataCollection(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerDateOfInitialDataCollection", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerYear1990to2000<Patient>(Metadata.Record.Fields[3],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerYear1990to2000<PatientB>(Metadata.RecordB.Fields[3],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerDateOfFirstVisit : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerDateOfFirstVisit(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerDateOfFirstVisit", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerDecade1970to2000<Patient>(Metadata.Record.Fields[4],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerDecade1970to2000<PatientB>(Metadata.RecordB.Fields[4],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerDateOfExamination : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerDateOfExamination(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerDateOfExamination", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerYear1970to2000<Patient>(Metadata.Record.Fields[5],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerYear1970to2000<PatientB>(Metadata.RecordB.Fields[5],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerAdmission : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerAdmission(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerAdmission", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerAdmission<Patient>(Metadata.Record.Fields[6],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerAdmission<PatientB>(Metadata.RecordB.Fields[6],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerAclIga : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerAclIga(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerAclIga", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerAclIga<Patient>(Metadata.Record.Fields[24],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerAclIga<PatientB>(Metadata.RecordB.Fields[24],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerAclIgg : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerAclIgg(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerAclIgg", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerAclIgg<Patient>(Metadata.Record.Fields[25],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerAclIgg<PatientB>(Metadata.RecordB.Fields[25],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerAclIgm : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerAclIgm(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerAclIgm", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerAclIgm<Patient>(Metadata.Record.Fields[26],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerAclIgm<PatientB>(Metadata.RecordB.Fields[26],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerAna : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerAna(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerAna", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerAna<Patient>(Metadata.Record.Fields[27],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerAna<PatientB>(Metadata.RecordB.Fields[27],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerKct : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerKct(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerKct", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerKct<Patient>(Metadata.Record.Fields[32],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerKct<PatientB>(Metadata.RecordB.Fields[32],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerRvvt : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerRvvt(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerRvvt", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerRvvt<Patient>(Metadata.Record.Fields[33],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerRvvt<PatientB>(Metadata.RecordB.Fields[33],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerLac : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerLac(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerLac", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new PerLac<Patient>(Metadata.Record.Fields[34],
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), CleanThreshold),
                new Dummy<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Patient>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerLac<PatientB>(Metadata.RecordB.Fields[34],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), CleanThreshold),
                new Dummy<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<PatientB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class SevereThrombosisPatientsPerSusp : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerSusp(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerSusp", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert,
                new WhereSusp<Patient>(patient => patient.DiagnosisSpecial.Contains("susp")), CleanThreshold));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert,
                new WhereSusp<PatientB>(patient => patient.DiagnosisSpecial.Contains("susp")), CleanThreshold)));
        }
    }

    public class SevereThrombosisPatientsPerLaboratories : AnalyticBaseFactory<Patient, PatientB>
    {
        public SevereThrombosisPatientsPerLaboratories(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "PatientsPerLaboratories", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Patient> GenerateQueryDirect()
        {
            return new WhereRange<Patient>(Metadata.Record.Fields[35], 2L, 3L,
                new Count<Patient>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert,
                new PerLaboratoriesCount<Patient>(patient => patient.Laboratories.Count()), CleanThreshold));
        }

        public override IQuery<Patient> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<PatientB> GenerateQueryBK()
        {
            return new WhereRange<PatientB>(Metadata.RecordB.Fields[35], 2L, 3L,
                new WhereBudget<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new Count<PatientB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert,
                new PerLaboratoriesCount<PatientB>(patient => patient.Laboratories.Count()), CleanThreshold)));
        }
    }
}
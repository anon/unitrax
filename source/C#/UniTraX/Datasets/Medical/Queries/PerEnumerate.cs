﻿namespace UniTraX.Datasets.Medical.Queries
{
    using UniTraX.Core.Specification;
    using UniTraX.Queries.Core;

    public class PerSex<R> : PerNumbers<R>
    {
        public PerSex(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, 0L, 0L, 3, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerAdmission<R> : PerNumbers<R>
    {
        public PerAdmission(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, 0L, 0L, 4, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerKct<R> : PerNumbers<R>
    {
        public PerKct(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, 0L, 0L, 4, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerRvvt<R> : PerNumbers<R>
    {
        public PerRvvt(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, 0L, 0L, 4, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerLac<R> : PerNumbers<R>
    {
        public PerLac(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, 0L, 0L, 4, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerThrombosis<R> : PerNumbers<R>
    {
        public PerThrombosis(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, -1L, 0L, 5, 0, forEach, forOthers, budgetOverRuntime) { }
    }
}
﻿namespace UniTraX.Datasets.Medical.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using System.Collections.Generic;
    using UniTraX.Infrastructure.DataLoading;

    public class MedicalDisk
    {
        public static string[] PatientColumnNames = new string[] {
            "id",
            "sex",
            "date_of_birth",
            "date_of_initial_data_collection",
            "date_of_first_visit",
            "date_of_examination",
            "admission",
            "diagnosis_sjs",
            "diagnosis_sle",
            "diagnosis_behcet",
            "diagnosis_pss",
            "diagnosis_ra",
            "diagnosis_mctd",
            "diagnosis_aps",
            "diagnosis_pm",
            "diagnosis_dm",
            "diagnosis_pn",
            "diagnosis_fuo",
            "diagnosis_aortitis",
            "diagnosis_ip",
            "diagnosis_mra",
            "diagnosis_collagen",
            "diagnosis_raynaud",
            "diagnosis_other",
            "acl_iga",
            "acl_igg",
            "acl_igm",
            "ana",
            "ana_pattern_d",
            "ana_pattern_n",
            "ana_pattern_p",
            "ana_pattern_s",
            "kct",
            "rvvt",
            "lac",
            "thrombosis",
            "budget",
            "diagnosis_general",
            "diagnosis_special",
            "symptoms"
        };
        public static string[] LaboratoryColumnNames = new string[] {
            "id",
            "patient_id",
            "date",
            "got",
            "gpt",
            "ldh",
            "alp",
            "tp",
            "alb",
            "ua",
            "un",
            "cre",
            "t-bil",
            "t-cho",
            "tg",
            "cpk",
            "glu",
            "wbc",
            "rbc",
            "hgb",
            "hct",
            "plt",
            "pt",
            "pt_note",
            "aptt",
            "fg",
            "at3",
            "a2pi",
            "u-pro",
            "igg",
            "iga",
            "igm",
            "crp",
            "ra",
            "rf",
            "c3",
            "c4",
            "rnp",
            "sm",
            "sc170",
            "ssa",
            "ssb",
            "centromea",
            "dna"
        };
        public static Dictionary<string, DataType> SpecialColumns()
        {
            return  new Dictionary<string, DataType>
            {
                { "diagnosis_general", DataType.NChar(41) },
                { "diagnosis_special", DataType.NChar(25) },
                { "symptoms", DataType.NChar(59) }
            };
        }

        public static void CreateTable(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Table patientTable = TableDisk.CreateTable(database, withBudget, "medical-disk", "patient", PatientColumnNames, "id", "budget", SpecialColumns());
            Table laboratoryTable = TableDisk.CreateTable(database, false, "medical-disk", "laboratory", LaboratoryColumnNames, "id", "budget", null);
            ForeignKey laboratoryPatientFK = new ForeignKey(laboratoryTable, "FK_laboratory_patient_id");
            ForeignKeyColumn laboratoryPatientFKC = new ForeignKeyColumn(laboratoryPatientFK, "patient_id", "id");
            laboratoryPatientFK.Columns.Add(laboratoryPatientFKC);
            laboratoryPatientFK.ReferencedTable = "patient";
            laboratoryPatientFK.Create();
            laboratoryTable.Alter();
        }

        public static void CreateIndexes(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Console.WriteLine("MedicalDisk: Creating indexes on tables patient and laboratory " + (withBudget ? "with" : "without") + " budget.");
            var patientTable = database.Tables["patient"];
            var patientIndexT = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis");
            patientIndexT.IndexedColumns.Add(new IndexedColumn(patientIndexT, "thrombosis", false));
            if (withBudget) patientIndexT.IndexedColumns.Add(new IndexedColumn(patientIndexT, "budget", false));
            patientTable.Indexes.Add(patientIndexT);
            patientIndexT.Create();
            patientIndexT.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexT.Alter();
            var patientIndexTS = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_sex");
            patientIndexTS.IndexedColumns.Add(new IndexedColumn(patientIndexTS, "thrombosis", false));
            patientIndexTS.IndexedColumns.Add(new IndexedColumn(patientIndexTS, "sex", false));
            if (withBudget) patientIndexTS.IndexedColumns.Add(new IndexedColumn(patientIndexTS, "budget", false));
            patientTable.Indexes.Add(patientIndexTS);
            patientIndexTS.Create();
            patientIndexTS.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTS.Alter();
            var patientIndexTDOB = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_date_of_birth");
            patientIndexTDOB.IndexedColumns.Add(new IndexedColumn(patientIndexTDOB, "thrombosis", false));
            patientIndexTDOB.IndexedColumns.Add(new IndexedColumn(patientIndexTDOB, "date_of_birth", false));
            if (withBudget) patientIndexTDOB.IndexedColumns.Add(new IndexedColumn(patientIndexTDOB, "budget", false));
            patientTable.Indexes.Add(patientIndexTDOB);
            patientIndexTDOB.Create();
            patientIndexTDOB.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTDOB.Alter();
            var patientIndexTDOIDC = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_date_of_initial_data_collection");
            patientIndexTDOIDC.IndexedColumns.Add(new IndexedColumn(patientIndexTDOIDC, "thrombosis", false));
            patientIndexTDOIDC.IndexedColumns.Add(new IndexedColumn(patientIndexTDOIDC, "date_of_initial_data_collection", false));
            if (withBudget) patientIndexTDOIDC.IndexedColumns.Add(new IndexedColumn(patientIndexTDOIDC, "budget", false));
            patientTable.Indexes.Add(patientIndexTDOIDC);
            patientIndexTDOIDC.Create();
            patientIndexTDOIDC.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTDOIDC.Alter();
            var patientIndexTDOFV = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_date_of_first_visit");
            patientIndexTDOFV.IndexedColumns.Add(new IndexedColumn(patientIndexTDOFV, "thrombosis", false));
            patientIndexTDOFV.IndexedColumns.Add(new IndexedColumn(patientIndexTDOFV, "date_of_first_visit", false));
            if (withBudget) patientIndexTDOFV.IndexedColumns.Add(new IndexedColumn(patientIndexTDOFV, "budget", false));
            patientTable.Indexes.Add(patientIndexTDOFV);
            patientIndexTDOFV.Create();
            patientIndexTDOFV.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTDOFV.Alter();
            var patientIndexTDOE = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_date_of_examination");
            patientIndexTDOE.IndexedColumns.Add(new IndexedColumn(patientIndexTDOE, "thrombosis", false));
            patientIndexTDOE.IndexedColumns.Add(new IndexedColumn(patientIndexTDOE, "date_of_examination", false));
            if (withBudget) patientIndexTDOE.IndexedColumns.Add(new IndexedColumn(patientIndexTDOE, "budget", false));
            patientTable.Indexes.Add(patientIndexTDOE);
            patientIndexTDOE.Create();
            patientIndexTDOE.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTDOE.Alter();
            var patientIndexTA = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_admission");
            patientIndexTA.IndexedColumns.Add(new IndexedColumn(patientIndexTA, "thrombosis", false));
            patientIndexTA.IndexedColumns.Add(new IndexedColumn(patientIndexTA, "admission", false));
            if (withBudget) patientIndexTA.IndexedColumns.Add(new IndexedColumn(patientIndexTA, "budget", false));
            patientTable.Indexes.Add(patientIndexTA);
            patientIndexTA.Create();
            patientIndexTA.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTA.Alter();
            var patientIndexTAIGA = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_acl_iga");
            patientIndexTAIGA.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGA, "thrombosis", false));
            patientIndexTAIGA.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGA, "acl_iga", false));
            if (withBudget) patientIndexTAIGA.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGA, "budget", false));
            patientTable.Indexes.Add(patientIndexTAIGA);
            patientIndexTAIGA.Create();
            patientIndexTAIGA.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTAIGA.Alter();
            var patientIndexTAIGG = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_acl_igg");
            patientIndexTAIGG.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGG, "thrombosis", false));
            patientIndexTAIGG.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGG, "acl_igg", false));
            if (withBudget) patientIndexTAIGG.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGG, "budget", false));
            patientTable.Indexes.Add(patientIndexTAIGG);
            patientIndexTAIGG.Create();
            patientIndexTAIGG.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTAIGG.Alter();
            var patientIndexTAIGM = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_acl_igm");
            patientIndexTAIGM.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGM, "thrombosis", false));
            patientIndexTAIGM.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGM, "acl_igm", false));
            if (withBudget) patientIndexTAIGM.IndexedColumns.Add(new IndexedColumn(patientIndexTAIGM, "budget", false));
            patientTable.Indexes.Add(patientIndexTAIGM);
            patientIndexTAIGM.Create();
            patientIndexTAIGM.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTAIGM.Alter();
            var patientIndexTANA = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_ana");
            patientIndexTANA.IndexedColumns.Add(new IndexedColumn(patientIndexTANA, "thrombosis", false));
            patientIndexTANA.IndexedColumns.Add(new IndexedColumn(patientIndexTANA, "ana", false));
            if (withBudget) patientIndexTANA.IndexedColumns.Add(new IndexedColumn(patientIndexTANA, "budget", false));
            patientTable.Indexes.Add(patientIndexTANA);
            patientIndexTANA.Create();
            patientIndexTANA.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTANA.Alter();
            var patientIndexTK = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_kct");
            patientIndexTK.IndexedColumns.Add(new IndexedColumn(patientIndexTK, "thrombosis", false));
            patientIndexTK.IndexedColumns.Add(new IndexedColumn(patientIndexTK, "kct", false));
            if (withBudget) patientIndexTK.IndexedColumns.Add(new IndexedColumn(patientIndexTK, "budget", false));
            patientTable.Indexes.Add(patientIndexTK);
            patientIndexTK.Create();
            patientIndexTK.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTK.Alter();
            var patientIndexTR = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_rvvt");
            patientIndexTR.IndexedColumns.Add(new IndexedColumn(patientIndexTR, "thrombosis", false));
            patientIndexTR.IndexedColumns.Add(new IndexedColumn(patientIndexTR, "rvvt", false));
            if (withBudget) patientIndexTR.IndexedColumns.Add(new IndexedColumn(patientIndexTR, "budget", false));
            patientTable.Indexes.Add(patientIndexTR);
            patientIndexTR.Create();
            patientIndexTR.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTR.Alter();
            var patientIndexTL = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_thrombosis_lac");
            patientIndexTL.IndexedColumns.Add(new IndexedColumn(patientIndexTL, "thrombosis", false));
            patientIndexTL.IndexedColumns.Add(new IndexedColumn(patientIndexTL, "lac", false));
            if (withBudget) patientIndexTL.IndexedColumns.Add(new IndexedColumn(patientIndexTL, "budget", false));
            patientTable.Indexes.Add(patientIndexTL);
            patientIndexTL.Create();
            patientIndexTL.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            patientIndexTL.Alter();
            if (withBudget)
            {
                var patientIndexB = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_budget");
                patientIndexB.IndexedColumns.Add(new IndexedColumn(patientIndexB, "budget", false));
                patientIndexB.IndexedColumns.Add(new IndexedColumn(patientIndexB, "diagnosis_special") { IsIncluded = true });
                patientTable.Indexes.Add(patientIndexB);
                patientIndexB.Create();
                patientIndexB.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
                patientIndexB.Alter();
            }
            patientTable.Alter();
            var laboratoryTable = database.Tables["laboratory"];
            var laboratoryIndexPID = TableDisk.CreateDefaultIndex(laboratoryTable, "index_nc_medical-disk_laboratory_patient_id_date");
            laboratoryIndexPID.IndexedColumns.Add(new IndexedColumn(laboratoryIndexPID, "patient_id", false));
            laboratoryIndexPID.IndexedColumns.Add(new IndexedColumn(laboratoryIndexPID, "date", false));
            laboratoryTable.Indexes.Add(laboratoryIndexPID);
            laboratoryIndexPID.Create();
            laboratoryIndexPID.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            laboratoryIndexPID.Alter();
            laboratoryTable.Alter();
            Console.WriteLine("MedicalDisk: Created indexes on tables patient and laboratory.");
        }
    }
}
﻿namespace UniTraX.Datasets.Medical.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using UniTraX.Infrastructure.DataLoading;

    public class MedicalMem
    {
        public static void CreateTable(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Table patientTable = TableMem.CreateTableInit(database, withBudget, "medical-mem", "patient", MedicalDisk.PatientColumnNames, "id", "budget", MedicalDisk.SpecialColumns());
            var patientIndexDOB = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_date_of_birth");
            patientIndexDOB.IndexedColumns.Add(new IndexedColumn(patientIndexDOB, "date_of_birth", false));
            if (withBudget) patientIndexDOB.IndexedColumns.Add(new IndexedColumn(patientIndexDOB, "budget", false));
            patientTable.Indexes.Add(patientIndexDOB);
            var patientIndexDOIDC = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_date_of_initial_data_collection");
            patientIndexDOIDC.IndexedColumns.Add(new IndexedColumn(patientIndexDOIDC, "date_of_initial_data_collection", false));
            if (withBudget) patientIndexDOIDC.IndexedColumns.Add(new IndexedColumn(patientIndexDOIDC, "budget", false));
            patientTable.Indexes.Add(patientIndexDOIDC);
            var patientIndexDOFV = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_date_of_first_visit");
            patientIndexDOFV.IndexedColumns.Add(new IndexedColumn(patientIndexDOFV, "date_of_first_visit", false));
            if (withBudget) patientIndexDOFV.IndexedColumns.Add(new IndexedColumn(patientIndexDOFV, "budget", false));
            patientTable.Indexes.Add(patientIndexDOFV);
            var patientIndexDOE = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_date_of_examination");
            patientIndexDOE.IndexedColumns.Add(new IndexedColumn(patientIndexDOE, "date_of_examination", false));
            if (withBudget) patientIndexDOE.IndexedColumns.Add(new IndexedColumn(patientIndexDOE, "budget", false));
            patientTable.Indexes.Add(patientIndexDOE);
            var patientIndexAIGA = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_acl_iga");
            patientIndexAIGA.IndexedColumns.Add(new IndexedColumn(patientIndexAIGA, "acl_iga", false));
            if (withBudget) patientIndexAIGA.IndexedColumns.Add(new IndexedColumn(patientIndexAIGA, "budget", false));
            patientTable.Indexes.Add(patientIndexAIGA);
            var patientIndexAIGG = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_acl_igg");
            patientIndexAIGG.IndexedColumns.Add(new IndexedColumn(patientIndexAIGG, "acl_igg", false));
            if (withBudget) patientIndexAIGG.IndexedColumns.Add(new IndexedColumn(patientIndexAIGG, "budget", false));
            patientTable.Indexes.Add(patientIndexAIGG);
            var patientIndexAIGM = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_acl_igm");
            patientIndexAIGM.IndexedColumns.Add(new IndexedColumn(patientIndexAIGM, "acl_igm", false));
            if (withBudget) patientIndexAIGM.IndexedColumns.Add(new IndexedColumn(patientIndexAIGM, "budget", false));
            patientTable.Indexes.Add(patientIndexAIGM);
            var patientIndexA = TableDisk.CreateDefaultIndex(patientTable, "index_nc_medical-disk_patient_ana");
            patientIndexA.IndexedColumns.Add(new IndexedColumn(patientIndexA, "ana", false));
            if (withBudget) patientIndexA.IndexedColumns.Add(new IndexedColumn(patientIndexA, "budget", false));
            patientTable.Indexes.Add(patientIndexA);
            TableMem.CreateTableFinish(patientTable, "patient");
            Table laboratoryTable = TableMem.CreateTableInit(database, withBudget, "medical-mem", "laboratory", MedicalDisk.LaboratoryColumnNames, "id", "budget", null);
            var laboratoryIndexPI = TableDisk.CreateDefaultIndex(laboratoryTable, "index_nc_medical-disk_laboratory_patient_id");
            laboratoryIndexPI.IndexedColumns.Add(new IndexedColumn(laboratoryIndexPI, "patient_id", false));
            laboratoryTable.Indexes.Add(laboratoryIndexPI);
            var laboratoryIndexD = TableDisk.CreateDefaultIndex(laboratoryTable, "index_nc_medical-disk_laboratory_date");
            laboratoryIndexD.IndexedColumns.Add(new IndexedColumn(laboratoryIndexD, "date", false));
            laboratoryTable.Indexes.Add(laboratoryIndexD);
            TableMem.CreateTableFinish(laboratoryTable, "laboratory");
            ForeignKey laboratoryPatientFK = new ForeignKey(laboratoryTable, "FK_laboratory_patient_id");
            ForeignKeyColumn laboratoryPatientFKC = new ForeignKeyColumn(laboratoryPatientFK, "patient_id", "id");
            laboratoryPatientFK.Columns.Add(laboratoryPatientFKC);
            laboratoryPatientFK.ReferencedTable = "patient";
            laboratoryPatientFK.Create();
            laboratoryTable.Alter();
        }
    }
}
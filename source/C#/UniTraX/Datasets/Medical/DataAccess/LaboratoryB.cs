﻿namespace UniTraX.Datasets.Medical.DataAccess
{
    using System.Collections.Generic;
    using System.Data.Linq;
    using System.Data.Linq.Mapping;

    [Table(Name = "laboratory")]
    public class LaboratoryB
    {
        [Column(Name = "id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Id;
        [Column(Name = "patient_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double PatientId;
        [Column(Name = "date", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Date;
        [Column(Name = "got", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Got;
        [Column(Name = "gpt", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Gpt;
        [Column(Name = "ldh", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Ldh;
        [Column(Name = "alp", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Alp;
        [Column(Name = "alb", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Alb;
        [Column(Name = "ua", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Ua;
        [Column(Name = "un", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Un;
        [Column(Name = "cre", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Cre;
        [Column(Name = "t-bil", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double TBil;
        [Column(Name = "t-cho", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double TCho;
        [Column(Name = "tg", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Tg;
        [Column(Name = "cpk", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Cpk;
        [Column(Name = "glu", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Glu;
        [Column(Name = "wbc", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Wbc;
        [Column(Name = "rbc", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Rbc;
        [Column(Name = "hgb", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Hgb;
        [Column(Name = "hct", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Hct;
        [Column(Name = "plt", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Plt;
        [Column(Name = "pt", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Pt;
        [Column(Name = "pt_note", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double PtNote;
        [Column(Name = "aptt", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Aptt;
        [Column(Name = "fg", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Fg;
        [Column(Name = "at3", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double At3;
        [Column(Name = "a2pi", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double A2pi;
        [Column(Name = "u-pro", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double UPro;
        [Column(Name = "igg", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Igg;
        [Column(Name = "iga", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Iga;
        [Column(Name = "igm", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Igm;
        [Column(Name = "crp", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Crp;
        [Column(Name = "ra", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Ra;
        [Column(Name = "rf", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Rf;
        [Column(Name = "c3", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double C3;
        [Column(Name = "c4", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double C4;
        [Column(Name = "rnp", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Rnp;
        [Column(Name = "sm", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Sm;
        [Column(Name = "sc170", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Sc170;
        [Column(Name = "ssa", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Ssa;
        [Column(Name = "ssb", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Ssb;
        [Column(Name = "centromea", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Centromea;
        [Column(Name = "dna", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Dna;
        private EntityRef<PatientB> _patient;
        [Association(Name = "FK_laboratory_patient_id", IsForeignKey = true, Storage = "_patient", OtherKey = "Id", ThisKey = "PatientId")]
        public PatientB Patient { get { return _patient.Entity; } set { _patient.Entity = value; } }
    }
}
﻿namespace UniTraX.Datasets.Medical.DataAccess
{
    using System.Data.Linq.Mapping;

    [Database(Name = "medical")]
    public class MedicalB : System.Data.Linq.DataContext
    {
        private static readonly MappingSource MappingSource = new AttributeMappingSource();

        public MedicalB(string connectionString) : base(connectionString, MappingSource)
        {
            ObjectTrackingEnabled = false;
            CommandTimeout = 300;
        }

        public System.Data.Linq.Table<PatientB> Patients
        {
            get { return GetTable<PatientB>(); }
        }

        public System.Data.Linq.Table<LaboratoryB> Laboratories
        {
            get { return GetTable<LaboratoryB>(); }
        }
    }
}
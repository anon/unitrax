﻿namespace UniTraX.Datasets.Medical.DataAccess
{
    using System.Data.Linq.Mapping;

    [Database(Name = "medical")]
    public class Medical : System.Data.Linq.DataContext
    {
        private static readonly MappingSource MappingSource = new AttributeMappingSource();

        public Medical(string connectionString) : base(connectionString, MappingSource)
        {
            ObjectTrackingEnabled = false;
            CommandTimeout = 300;
        }

        public System.Data.Linq.Table<Patient> Patients
        {
            get { return GetTable<Patient>(); }
        }

        public System.Data.Linq.Table<Laboratory> Laboratories
        {
            get { return GetTable<Laboratory>(); }
        }
    }
}
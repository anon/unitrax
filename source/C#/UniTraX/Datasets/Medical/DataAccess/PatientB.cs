﻿namespace UniTraX.Datasets.Medical.DataAccess
{
    using System.Collections.Generic;
    using System.Data.Linq;
    using System.Data.Linq.Mapping;

    [Table(Name = "patient")]
    public class PatientB
    {
        [Column(Name = "id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Id;
        [Column(Name = "sex", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Sex;
        [Column(Name = "date_of_birth", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DateOfBirth;
        [Column(Name = "date_of_initial_data_collection", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DateOfInitialDataCollection;
        [Column(Name = "date_of_first_visit", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DateOfFirstVisit;
        [Column(Name = "date_of_examination", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DateOfExamination;
        [Column(Name = "admission", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Admission;
        [Column(Name = "diagnosis_sjs", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisSjs;
        [Column(Name = "diagnosis_sle", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisSle;
        [Column(Name = "diagnosis_behcet", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisBehcet;
        [Column(Name = "diagnosis_pss", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisPss;
        [Column(Name = "diagnosis_ra", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisRa;
        [Column(Name = "diagnosis_mctd", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisMctd;
        [Column(Name = "diagnosis_aps", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisAps;
        [Column(Name = "diagnosis_pm", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisPm;
        [Column(Name = "diagnosis_dm", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisDm;
        [Column(Name = "diagnosis_pn", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisPn;
        [Column(Name = "diagnosis_fuo", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisFuo;
        [Column(Name = "diagnosis_aortitis", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisAortitis;
        [Column(Name = "diagnosis_ip", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisIp;
        [Column(Name = "diagnosis_mra", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisMra;
        [Column(Name = "diagnosis_collagen", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisCollagen;
        [Column(Name = "diagnosis_raynaud", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisRaynaud;
        [Column(Name = "diagnosis_other", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DiagnosisOther;
        [Column(Name = "acl_iga", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AclIga;
        [Column(Name = "acl_igg", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AclIgg;
        [Column(Name = "acl_igm", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AclIgm;
        [Column(Name = "ana", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Ana;
        [Column(Name = "ana_pattern_d", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AnaPatternD;
        [Column(Name = "ana_pattern_n", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AnaPatternN;
        [Column(Name = "ana_pattern_p", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AnaPatternP;
        [Column(Name = "ana_pattern_s", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AnaPatternS;
        [Column(Name = "kct", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Kct;
        [Column(Name = "rvvt", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Rvvt;
        [Column(Name = "lac", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Lac;
        [Column(Name = "thrombosis", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Thrombosis;
        [Column(Name = "budget", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Budget;
        [Column(Name = "diagnosis_general", DbType = "NChar(41) NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public string DiagnosisGeneral;
        [Column(Name = "diagnosis_special", DbType = "NChar(25) NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public string DiagnosisSpecial;
        [Column(Name = "symptoms", DbType = "NChar(59) NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public string Symptoms;
        private EntitySet<LaboratoryB> _laboratories = new EntitySet<LaboratoryB>();
        [Association(Name = "FK_laboratory_patient_id", Storage = "_laboratories", OtherKey = "PatientId", ThisKey = "Id")]
        public ICollection<LaboratoryB> Laboratories { get { return _laboratories; } set { _laboratories.Assign(value); } }
    }
}
﻿namespace UniTraX.Datasets.Medical.DataAccess
{
    using System;
    using System.Linq;
    using UniTraX.Core.BoundedData;
    using UniTraX.Core.Specification;
    using UniTraX.Infrastructure.DataConversion;

    public class Metadata : IQueryableFactory<PatientB>
    {
        private static DoubleTypeConverter TypeMedicalId =                          new DoubleTypeConverter(new DoubleType(2110.0, 9334041.0, 1.0));
        private static DoubleTypeConverter TypeMedicalSex =                         new DoubleTypeConverter(new DoubleType(0.0, 2.0, 1.0));
        private static DoubleTypeConverter TypeMedicalDateOfBirth =                 new DoubleTypeConverter(new DoubleType(-2208988800.0, 631151999.0, 1.0));
        private static DoubleTypeConverter TypeMedicalDateOfInitialDataCollection = new DoubleTypeConverter(new DoubleType(631152000.0, 946684799.0, 1.0));
        private static DoubleTypeConverter TypeMedicalDate =                        new DoubleTypeConverter(new DoubleType(0.0, 946684799.0, 1.0));
        private static DoubleTypeConverter TypeMedicalQuadrupel =                   new DoubleTypeConverter(new DoubleType(0.0, 3.0, 1.0));
        private static DoubleTypeConverter TypeMedicalBinary =                      new DoubleTypeConverter(new DoubleType(0.0, 1.0, 1.0));
        private static DoubleTypeConverter TypeMedicalAclIga =                      new DoubleTypeConverter(new DoubleType(-1.0, 49999.9, 0.1));
        private static DoubleTypeConverter TypeMedicalAclIgg =                      new DoubleTypeConverter(new DoubleType(-1.0, 999.9, 0.1));
        private static DoubleTypeConverter TypeMedicalAclIgm =                      new DoubleTypeConverter(new DoubleType(-1.0, 199999.9, 0.1));
        private static DoubleTypeConverter TypeMedicalAna =                         new DoubleTypeConverter(new DoubleType(-1.0, 4097.0, 1.0));
        private static DoubleTypeConverter TypeMedicalThrombosis =                  new DoubleTypeConverter(new DoubleType(-1.0, 3.0, 1.0));
        private static DoubleTypeConverter TypeMedicalBudget =                      new DoubleTypeConverter(new DoubleType(0.0, 10000000000.0, 0.01));

        public static Record<Patient> GenerateRecord()
        {
            var fields = new[]
            {
                new Field<Patient>(0,  "Id",                           TypeMedicalId,                           accessRecord => accessRecord.Id),
                new Field<Patient>(1,  "Sex",                          TypeMedicalSex,                          accessRecord => accessRecord.Sex),
                new Field<Patient>(2,  "DateOfBirth",                  TypeMedicalDateOfBirth,                  accessRecord => accessRecord.DateOfBirth),
                new Field<Patient>(3,  "DateOfInitialDataCollection",  TypeMedicalDateOfInitialDataCollection,  accessRecord => accessRecord.DateOfInitialDataCollection),
                new Field<Patient>(4,  "DateOfFirstVisit",             TypeMedicalDate,                         accessRecord => accessRecord.DateOfFirstVisit),
                new Field<Patient>(5,  "DateOfExamination",            TypeMedicalDate,                         accessRecord => accessRecord.DateOfExamination),
                new Field<Patient>(6,  "Admission",                    TypeMedicalQuadrupel,                    accessRecord => accessRecord.Admission),
                new Field<Patient>(7,  "DiagnosisSjs",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisSjs),
                new Field<Patient>(8,  "DiagnosisSle",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisSle),
                new Field<Patient>(9,  "DiagnosisBehcet",              TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisBehcet),
                new Field<Patient>(10, "DiagnosisPss",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisPss),
                new Field<Patient>(11, "DiagnosisRa",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisRa),
                new Field<Patient>(12, "DiagnosisMctd",                TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisMctd),
                new Field<Patient>(13, "DiagnosisAps",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisAps),
                new Field<Patient>(14, "DiagnosisPm",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisPm),
                new Field<Patient>(15, "DiagnosisDm",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisDm),
                new Field<Patient>(16, "DiagnosisPn",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisPn),
                new Field<Patient>(17, "DiagnosisFuo",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisFuo),
                new Field<Patient>(18, "DiagnosisAortitis",            TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisAortitis),
                new Field<Patient>(19, "DiagnosisIp",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisIp),
                new Field<Patient>(20, "DiagnosisMra",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisMra),
                new Field<Patient>(21, "DiagnosisCollagen",            TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisCollagen),
                new Field<Patient>(22, "DiagnosisRaynaud",             TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisRaynaud),
                new Field<Patient>(23, "DiagnosisOther",               TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisOther),
                new Field<Patient>(24, "AclIga",                       TypeMedicalAclIga,                       accessRecord => accessRecord.AclIga),
                new Field<Patient>(25, "AclIgg",                       TypeMedicalAclIgg,                       accessRecord => accessRecord.AclIgg),
                new Field<Patient>(26, "AclIgm",                       TypeMedicalAclIgm,                       accessRecord => accessRecord.AclIgm),
                new Field<Patient>(27, "Ana",                          TypeMedicalAna,                          accessRecord => accessRecord.Ana),
                new Field<Patient>(28, "AnaPatternD",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternD),
                new Field<Patient>(29, "AnaPatternN",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternN),
                new Field<Patient>(30, "AnaPatternP",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternP),
                new Field<Patient>(31, "AnaPatternS",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternS),
                new Field<Patient>(32, "Kct",                          TypeMedicalQuadrupel,                    accessRecord => accessRecord.Kct),
                new Field<Patient>(33, "Rvvt",                         TypeMedicalQuadrupel,                    accessRecord => accessRecord.Rvvt),
                new Field<Patient>(34, "Lac",                          TypeMedicalQuadrupel,                    accessRecord => accessRecord.Lac),
                new Field<Patient>(35, "Thrombosis",                   TypeMedicalThrombosis,                   accessRecord => accessRecord.Thrombosis)
            };
            return new Record<Patient>(fields);
        }

        public static Record<PatientB> GenerateRecordB()
        {
            var fields = new[]
            {
                new Field<PatientB>(0,  "Id",                           TypeMedicalId,                           accessRecord => accessRecord.Id),
                new Field<PatientB>(1,  "Sex",                          TypeMedicalSex,                          accessRecord => accessRecord.Sex),
                new Field<PatientB>(2,  "DateOfBirth",                  TypeMedicalDateOfBirth,                  accessRecord => accessRecord.DateOfBirth),
                new Field<PatientB>(3,  "DateOfInitialDataCollection",  TypeMedicalDateOfInitialDataCollection,  accessRecord => accessRecord.DateOfInitialDataCollection),
                new Field<PatientB>(4,  "DateOfFirstVisit",             TypeMedicalDate,                         accessRecord => accessRecord.DateOfFirstVisit),
                new Field<PatientB>(5,  "DateOfExamination",            TypeMedicalDate,                         accessRecord => accessRecord.DateOfExamination),
                new Field<PatientB>(6,  "Admission",                    TypeMedicalQuadrupel,                    accessRecord => accessRecord.Admission),
                new Field<PatientB>(7,  "DiagnosisSjs",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisSjs),
                new Field<PatientB>(8,  "DiagnosisSle",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisSle),
                new Field<PatientB>(9,  "DiagnosisBehcet",              TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisBehcet),
                new Field<PatientB>(10, "DiagnosisPss",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisPss),
                new Field<PatientB>(11, "DiagnosisRa",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisRa),
                new Field<PatientB>(12, "DiagnosisMctd",                TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisMctd),
                new Field<PatientB>(13, "DiagnosisAps",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisAps),
                new Field<PatientB>(14, "DiagnosisPm",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisPm),
                new Field<PatientB>(15, "DiagnosisDm",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisDm),
                new Field<PatientB>(16, "DiagnosisPn",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisPn),
                new Field<PatientB>(17, "DiagnosisFuo",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisFuo),
                new Field<PatientB>(18, "DiagnosisAortitis",            TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisAortitis),
                new Field<PatientB>(19, "DiagnosisIp",                  TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisIp),
                new Field<PatientB>(20, "DiagnosisMra",                 TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisMra),
                new Field<PatientB>(21, "DiagnosisCollagen",            TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisCollagen),
                new Field<PatientB>(22, "DiagnosisRaynaud",             TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisRaynaud),
                new Field<PatientB>(23, "DiagnosisOther",               TypeMedicalBinary,                       accessRecord => accessRecord.DiagnosisOther),
                new Field<PatientB>(24, "AclIga",                       TypeMedicalAclIga,                       accessRecord => accessRecord.AclIga),
                new Field<PatientB>(25, "AclIgg",                       TypeMedicalAclIgg,                       accessRecord => accessRecord.AclIgg),
                new Field<PatientB>(26, "AclIgm",                       TypeMedicalAclIgm,                       accessRecord => accessRecord.AclIgm),
                new Field<PatientB>(27, "Ana",                          TypeMedicalAna,                          accessRecord => accessRecord.Ana),
                new Field<PatientB>(28, "AnaPatternD",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternD),
                new Field<PatientB>(29, "AnaPatternN",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternN),
                new Field<PatientB>(30, "AnaPatternP",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternP),
                new Field<PatientB>(31, "AnaPatternS",                  TypeMedicalBinary,                       accessRecord => accessRecord.AnaPatternS),
                new Field<PatientB>(32, "Kct",                          TypeMedicalQuadrupel,                    accessRecord => accessRecord.Kct),
                new Field<PatientB>(33, "Rvvt",                         TypeMedicalQuadrupel,                    accessRecord => accessRecord.Rvvt),
                new Field<PatientB>(34, "Lac",                          TypeMedicalQuadrupel,                    accessRecord => accessRecord.Lac),
                new Field<PatientB>(35, "Thrombosis",                   TypeMedicalThrombosis,                   accessRecord => accessRecord.Thrombosis),
                new Field<PatientB>(36, "Budget",                       TypeMedicalBudget,                       accessRecord => accessRecord.Budget)
            };
            return new Record<PatientB>(36, fields);
        }

        public static readonly Record<Patient> Record = GenerateRecord();
        public static readonly Record<PatientB> RecordB = GenerateRecordB();

        public string ConnectionString { get; private set; }
        public BoundedDataAccess<PatientB> BoundedDataAccess { get; private set; }
        public IQueryable<PatientB> Patients;

        public Metadata(string connectionString)
        {
#if DEBUG
            if (connectionString == null) throw new ArgumentNullException(nameof(connectionString));
#endif
            ConnectionString = connectionString;
            BoundedDataAccess = new BoundedDataAccess<PatientB>(RecordB, this, new ExpressionProvider<PatientB>());
            Patients = new MedicalB(ConnectionString).Patients;
        }

        public IQueryable<PatientB> Create()
        {
            return Patients;
        }
    }
}
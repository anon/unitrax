﻿namespace UniTraX.Datasets.Mobility.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using UniTraX.Infrastructure.DataLoading;

    public class MobilityDisk
    {
        public static string[] ColumnNames = new string[] {
            "pk",
            "pickup_datetime",
            "dropoff_datetime",
            "passenger_count",
            "trip_time_in_secs",
            "trip_distance",
            "pickup_longitude",
            "pickup_latitude",
            "dropoff_longitude",
            "dropoff_latitude",
            "fare_amount",
            "surcharge",
            "mta_tax",
            "tip_amount",
            "tolls_amount",
            "total_amount",
            "budget"
        };

        public static void CreateTable(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            TableDisk.CreateTable(database, withBudget, "mobility-disk", "rides", ColumnNames, "pk", "budget", null);
        }

        public static void CreateIndexes(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Console.WriteLine("MobilityDisk: Creating indexes on table rides " + (withBudget ? "with" : "without") + " budget.");
            var table = database.Tables["rides"];
            var indexPD = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_pickup_datetime");
            indexPD.IndexedColumns.Add(new IndexedColumn(indexPD, "pickup_datetime", false));
            if (withBudget) indexPD.IndexedColumns.Add(new IndexedColumn(indexPD, "budget", false));
            table.Indexes.Add(indexPD);
            indexPD.Create();
            indexPD.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexPD.Alter();

            var indexToA = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_total_amount");
            indexToA.IndexedColumns.Add(new IndexedColumn(indexToA, "total_amount", false));
            if (withBudget) indexToA.IndexedColumns.Add(new IndexedColumn(indexToA, "budget", false));
            table.Indexes.Add(indexToA);
            indexToA.Create();
            indexToA.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexToA.Alter();

            var indexTiA = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_tip_amount");
            indexTiA.IndexedColumns.Add(new IndexedColumn(indexTiA, "tip_amount", false));
            if (withBudget) indexTiA.IndexedColumns.Add(new IndexedColumn(indexTiA, "budget", false));
            table.Indexes.Add(indexTiA);
            indexTiA.Create();
            indexTiA.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexTiA.Alter();

            var indexPC = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_passenger_count");
            indexPC.IndexedColumns.Add(new IndexedColumn(indexPC, "passenger_count", false));
            if (withBudget) indexPC.IndexedColumns.Add(new IndexedColumn(indexPC, "budget", false));
            table.Indexes.Add(indexPC);
            indexPC.Create();
            indexPC.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexPC.Alter();

            var indexTTIS = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_trip_time_in_secs");
            indexTTIS.IndexedColumns.Add(new IndexedColumn(indexTTIS, "trip_time_in_secs", false));
            if (withBudget) indexTTIS.IndexedColumns.Add(new IndexedColumn(indexTTIS, "budget", false));
            table.Indexes.Add(indexTTIS);
            indexTTIS.Create();
            indexTTIS.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexTTIS.Alter();

            var indexTD = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_trip_distance");
            indexTD.IndexedColumns.Add(new IndexedColumn(indexTD, "trip_distance", false));
            if (withBudget) indexTD.IndexedColumns.Add(new IndexedColumn(indexTD, "budget", false));
            table.Indexes.Add(indexTD);
            indexTD.Create();
            indexTD.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexTD.Alter();

            var indexPL = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_pickup_longitude_pickup_latitude");
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "pickup_longitude", false));
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "pickup_latitude", false));
            if (withBudget) indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "budget", false));
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "trip_distance") { IsIncluded = true });
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "tip_amount") { IsIncluded = true });
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "total_amount") { IsIncluded = true });
            table.Indexes.Add(indexPL);
            indexPL.Create();
            indexPL.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexPL.Alter();

            var indexStream = TableDisk.CreateDefaultIndex(table, "index_nc_mobility-disk_rides_stream");
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "dropoff_datetime", false));
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "pickup_longitude", false));
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "pickup_latitude", false));
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "dropoff_longitude", false));
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "dropoff_latitude", false));
            if (withBudget) indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "budget", false));
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "trip_time_in_secs") { IsIncluded = true });
            indexStream.IndexedColumns.Add(new IndexedColumn(indexStream, "trip_distance") { IsIncluded = true });
            table.Indexes.Add(indexStream);
            indexStream.Create();
            indexStream.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            indexStream.Alter();

            table.Alter();
            Console.WriteLine("MobilityDisk: Created indexes on table rides.");
        }
    }
}
﻿namespace UniTraX.Datasets.Mobility.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using UniTraX.Infrastructure.DataLoading;

    public class MobilityMem
    {
        public static void CreateTable(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Table table = TableMem.CreateTableInit(database, withBudget, "mobility-mem", "rides", MobilityDisk.ColumnNames, "pk", "budget", null);
            var indexPD = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_pickup_datetime");
            indexPD.IndexedColumns.Add(new IndexedColumn(indexPD, "pickup_datetime", false));
            if (withBudget) indexPD.IndexedColumns.Add(new IndexedColumn(indexPD, "budget", false));
            table.Indexes.Add(indexPD);

            var indexToA = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_total_amount");
            indexToA.IndexedColumns.Add(new IndexedColumn(indexToA, "total_amount", false));
            if (withBudget) indexToA.IndexedColumns.Add(new IndexedColumn(indexToA, "budget", false));
            table.Indexes.Add(indexToA);

            var indexTiA = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_tip_amount");
            indexTiA.IndexedColumns.Add(new IndexedColumn(indexTiA, "tip_amount", false));
            if (withBudget) indexTiA.IndexedColumns.Add(new IndexedColumn(indexTiA, "budget", false));
            table.Indexes.Add(indexTiA);

            var indexPC = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_passenger_count");
            indexPC.IndexedColumns.Add(new IndexedColumn(indexPC, "passenger_count", false));
            if (withBudget) indexPC.IndexedColumns.Add(new IndexedColumn(indexPC, "budget", false));
            table.Indexes.Add(indexPC);

            var indexTTIS = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_trip_time_in_secs");
            indexTTIS.IndexedColumns.Add(new IndexedColumn(indexTTIS, "trip_time_in_secs", false));
            if (withBudget) indexTTIS.IndexedColumns.Add(new IndexedColumn(indexTTIS, "budget", false));
            table.Indexes.Add(indexTTIS);

            var indexTD = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_trip_distance");
            indexTD.IndexedColumns.Add(new IndexedColumn(indexTD, "trip_distance", false));
            if (withBudget) indexTD.IndexedColumns.Add(new IndexedColumn(indexTD, "budget", false));
            table.Indexes.Add(indexTD);

            var indexPL = TableMem.CreateDefaultIndex(table, "index_nc_mobility-mem_rides_pickup_longitude_pickup_latitude");
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "pickup_longitude", false));
            indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "pickup_latitude", false));
            if (withBudget) indexPL.IndexedColumns.Add(new IndexedColumn(indexPL, "budget", false));
            table.Indexes.Add(indexPL);
            TableMem.CreateTableFinish(table, "rides");
        }
    }
}
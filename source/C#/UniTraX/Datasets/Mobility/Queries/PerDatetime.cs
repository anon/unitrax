﻿namespace UniTraX.Datasets.Mobility.Queries
{
    using UniTraX.Core.Specification;
    using UniTraX.Queries.Core;

    public class PerDay<R> : PerNumbers<R>
    {
        public PerDay(Field<R> field, long startSecond, int numberDays, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, startSecond, 86400L - 1L, numberDays, 1, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerHour<R> : PerNumbers<R>
    {
        public PerHour(Field<R> field, long startSecond, int numberHours, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, startSecond, 3600L - 1L, numberHours, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerDayJanuary2013<R> : PerDay<R>
    {
        public PerDayJanuary2013(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 1356994800L, 31, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerHourJanuary1st2013<R> : PerHour<R>
    {
        public PerHourJanuary1st2013(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 1356994800L, 24, forEach, forOthers, budgetOverRuntime) { }
    }
}
﻿namespace UniTraX.Datasets.Mobility.Queries
{
    using UniTraX.Core.Specification;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Queries.Core;

    public class Per50Cents<R> : PerNumbers<R>
    {
        public Per50Cents(Field<R> field, long start, int number50Cents, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, start, DoubleTypeConverter.TwoDecimal.ToSystem(0.5 - 0.01), number50Cents, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerDollar<R> : PerNumbers<R>
    {
        public PerDollar(Field<R> field, long start, int numberDollars, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, start, DoubleTypeConverter.TwoDecimal.ToSystem(1.0 - 0.01), numberDollars, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class Per2Dollars50<R> : PerNumbers<R>
    {
        public Per2Dollars50(Field<R> field, long start, int number2Dollars50, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, start, DoubleTypeConverter.TwoDecimal.ToSystem(2.5 - 0.01), number2Dollars50, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class Per50Cents0To10Dollar<R> : Per50Cents<R>
    {
        public Per50Cents0To10Dollar(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 0L, 20, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerDollar0To10<R> : PerDollar<R>
    {
        public PerDollar0To10(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 0L, 10, forEach, forOthers, budgetOverRuntime) { }
    }

    public class Per2Dollars50From0To50<R> : Per2Dollars50<R>
    {
        public Per2Dollars50From0To50(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 0L, 20, forEach, forOthers, budgetOverRuntime) { }
    }
}
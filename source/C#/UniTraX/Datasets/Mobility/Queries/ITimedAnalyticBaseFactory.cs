﻿namespace UniTraX.Datasets.Mobility.Queries
{
    using UniTraX.Core.Specification;
    using UniTraX.Queries.Core;

    public interface ITimedAnalyticBaseFactory<R, RB>
    {
        AnalyticBaseFactory<R, RB> GenerateQuery(long time, Field<R> field, Field<RB> fieldB);
    }
}
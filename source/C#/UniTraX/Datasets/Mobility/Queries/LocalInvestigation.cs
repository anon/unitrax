﻿namespace UniTraX.Datasets.Mobility.Queries
{
    using System.Collections.Generic;
    using UniTraX.Datasets.Mobility.DataAccess;
    using UniTraX.Queries.Aggregation;
    using UniTraX.Queries.Core;

    public class LocalInvestigation1 : AnalyticBaseFactory<Ride, RideB>
    {
        public LocalInvestigation1(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long[] epsilons) :
            base(id, "LocalInvestigation1", useEvenly, budgetOverRuntime, cleanThreshold, epsilons) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            var list = new List<IQuery<Ride>>(2)
            {
                new Average<Ride>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), Metadata.Record.Fields[13], 1000.0, CleanThreshold),
                new Average<Ride>(Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), Metadata.Record.Fields[15], 1000.0, CleanThreshold)
            };
            var multi = new Multi<Ride>(list);
            IQuery< Ride> forOthers = new Dummy<Ride>(Epsilons[1] + Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold);
            var conditional = new Conditional<Ride>(x => x[0] > 5000.0, new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold), multi, forOthers);
            forOthers = null;
            return new Partition2DGeo<Ride>(
                Metadata.Record.Fields[6], -74010000L, 2500L - 1L, 16,
                Metadata.Record.Fields[7], 40710000L, 2500L - 1L, 16,
                conditional, forOthers, BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            var list = new List<IQuery<RideB>>(2)
            {
                new Average<RideB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), Metadata.RecordB.Fields[13], 1000.0, CleanThreshold),
                new Average<RideB>(Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), Metadata.RecordB.Fields[15], 1000.0, CleanThreshold)
            };
            var multi = new Multi<RideB>(list);
            IQuery<RideB> forOthers = new Dummy<RideB>(Epsilons[1] + Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold);
            var conditional = new Conditional<RideB>(x => x[0] > 5000.0, new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold), multi, forOthers);
            forOthers = null;
            var budget = new WhereBudget<RideB>(Epsilons[0] + Epsilons[1] + Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()], conditional);
            return new Partition2DGeo<RideB>(
                Metadata.RecordB.Fields[6], -74010000L, 2500L - 1L, 16,
                Metadata.RecordB.Fields[7], 40710000L, 2500L - 1L, 16,
                budget, forOthers, BudgetOverRuntime);
        }
    }

    public class LocalInvestigation2 : AnalyticBaseFactory<Ride, RideB>
    {
        public LocalInvestigation2(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long[] epsilons) :
            base(id, "LocalInvestigation2", useEvenly, budgetOverRuntime, cleanThreshold, epsilons) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            var median = new Median<Ride>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), Metadata.Record.Fields[5], 0.0, 100.0, CleanThreshold);
            IQuery<Ride> forOthers = new Dummy<Ride>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold);
            var conditional = new Conditional<Ride>(x => x[0] > 1000.0, new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold), median, forOthers);
            forOthers = null;
            return new Partition2DGeo<Ride>(
                Metadata.Record.Fields[6], -74010000L, 2500L - 1L, 16,
                Metadata.Record.Fields[7], 40710000L, 2500L - 1L, 16,
                conditional, forOthers, BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            var median = new Median<RideB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(),
                Metadata.RecordB.Fields[5], 0.0, 100.0, CleanThreshold);
            IQuery<RideB> forOthers = new Dummy<RideB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold);
            var conditional = new Conditional<RideB>(x => x[0] > 1000.0, new Count<RideB>(Epsilons[0],
                Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold), median, forOthers);
            forOthers = null;
            var budget = new WhereBudget<RideB>(Epsilons[0] + Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()], conditional);
            return new Partition2DGeo<RideB>(
                Metadata.RecordB.Fields[6], -74010000L, 2500L - 1L, 16,
                Metadata.RecordB.Fields[7], 40710000L, 2500L - 1L, 16,
                budget, forOthers, BudgetOverRuntime);
        }
    }
}
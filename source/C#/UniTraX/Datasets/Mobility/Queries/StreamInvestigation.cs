﻿namespace UniTraX.Datasets.Mobility.Queries
{
    using System;
    using System.Collections.Generic;
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Mobility.DataAccess;
    using UniTraX.Queries.Aggregation;
    using UniTraX.Queries.Core;

    public class StreamInvestigationHourly : AnalyticBaseFactory<Ride, RideB>, ITimedAnalyticBaseFactory<Ride, RideB>
    {
        private Field<Ride> Field;
        private Field<RideB> FieldB;
        private long Time = long.MinValue;

        public StreamInvestigationHourly(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long[] epsilons) :
            base(id, "StreamInvestigationHourly", useEvenly, budgetOverRuntime, cleanThreshold, epsilons) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            var analysisList = new List<IQuery<Ride>>(2)
            {
                new Average<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), Metadata.Record.Fields[4], 10800.0, CleanThreshold),
                new Average<Ride>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), Metadata.Record.Fields[5], 100.0, CleanThreshold)
            };
            var analysis = new Multi<Ride>(analysisList);

            var numHours = 6L;
            while (Time - numHours * 3600L + 1L < Field.FieldType.Bounds.Low) numHours--;
            IQuery<Ride> conditional = null;
            for (long i = 0; i < numHours; i++)
            {
                conditional = new Conditional<Ride>(x => x[0] > 24.5,
                    new WhereRange<Ride>(Field, Time - (numHours - i) * 3600L + 1L, Time,
                    new Count<Ride>(Epsilons[2 + i], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold)),
                    new WhereRange<Ride>(Field, Time - (numHours - i) * 3600L + 1L, Time, analysis),
                    conditional);
            }
            IQuery<Ride> query = new WhereRange<Ride>(Metadata.Record.Fields[6], -74010000L, -73969999L, conditional);
            query = new WhereRange<Ride>(Metadata.Record.Fields[7], 40710000L, 40749999L, query);
            query = new WhereRange<Ride>(Metadata.Record.Fields[8], -73987500L, -73982499L, query);
            return new WhereRange<Ride>(Metadata.Record.Fields[9], 40757500L, 40759999L, query);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            var analysisList = new List<IQuery<RideB>>(2)
            {
                new Average<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), Metadata.RecordB.Fields[4], 10800.0, CleanThreshold),
                new Average<RideB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), Metadata.RecordB.Fields[5], 100.0, CleanThreshold)
            };
            var analysis = new Multi<RideB>(analysisList);

            var numHours = 6L;
            while (Time - numHours * 3600L + 1L < FieldB.FieldType.Bounds.Low) numHours--;
            IQuery<RideB> conditional = null;
            for (long i = 0; i < numHours; i++)
            {
                conditional = new Conditional<RideB>(x => x[0] > 24.5,
                    new WhereRange<RideB>(FieldB, Time - (numHours - i) * 3600L + 1L, Time,
                    new WhereBudget<RideB>(Epsilons[0] + Epsilons[1] + Epsilons[2 + i], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                    new Count<RideB>(Epsilons[2 + i], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold))),
                    new WhereRange<RideB>(FieldB, Time - (numHours - i) * 3600L + 1L, Time,
                    new WhereBudget<RideB>(Epsilons[0] + Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()], analysis)),
                    conditional);
            }
            IQuery<RideB> query = new WhereRange<RideB>(Metadata.RecordB.Fields[6], -74010000L, -73969999L, conditional);
            query = new WhereRange<RideB>(Metadata.RecordB.Fields[7], 40710000L, 40749999L, query);
            query = new WhereRange<RideB>(Metadata.RecordB.Fields[8], -73987500L, -73982499L, query);
            return new WhereRange<RideB>(Metadata.RecordB.Fields[9], 40757500L, 40759999L, query);
        }

        public AnalyticBaseFactory<Ride, RideB> GenerateQuery(long time, Field<Ride> field, Field<RideB> fieldB)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (fieldB == null) throw new ArgumentNullException(nameof(fieldB));
#endif
            Time = time;
            Field = field;
            FieldB = fieldB;

            return this;
        }
    }

    public class StreamInvestigationMonthly : AnalyticBaseFactory<Ride, RideB>, ITimedAnalyticBaseFactory<Ride, RideB>
    {
        private Field<Ride> Field;
        private Field<RideB> FieldB;
        private long Time = long.MinValue;

        public StreamInvestigationMonthly(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long[] epsilons) :
            base(id, "LocalInvestigationStreamMonthly", useEvenly, budgetOverRuntime, cleanThreshold, epsilons) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            var aggregate = new Average<Ride>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(),
                Metadata.Record.Fields[4], 10800.0, CleanThreshold);
            IQuery<Ride> forOthers = new Dummy<Ride>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(),
                UseEvenly, CleanThreshold);
            var conditional = new Conditional<Ride>(x => x[0] > 24.5, new Count<Ride>(Epsilons[0],
                Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold), aggregate, forOthers);
            var time = new WhereRange<Ride>(Field, Time - 2678400L + 1L, Time, conditional);
            IQuery<Ride> query = new WhereRange<Ride>(Metadata.Record.Fields[6], -73815000L, -73780000L - 1L, time);
            query = new WhereRange<Ride>(Metadata.Record.Fields[7], 40640000L, 40650000L - 1L, query);
            forOthers = null;
            return new Partition2DGeo<Ride>(
                Metadata.Record.Fields[8], -74010000L, 2500L - 1L, 16,
                Metadata.Record.Fields[9], 40710000L, 2500L - 1L, 16,
                query, forOthers, BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            var aggregate = new Average<RideB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(),
                Metadata.RecordB.Fields[4], 10800.0, CleanThreshold);
            IQuery<RideB> forOthers = new Dummy<RideB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(),
                UseEvenly, CleanThreshold);
            var conditional = new Conditional<RideB>(x => x[0] > 24.5, new Count<RideB>(Epsilons[0],
                Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold), aggregate, forOthers);
            var budget = new WhereBudget<RideB>(Epsilons[0] + Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()], conditional);
            var time = new WhereRange<RideB>(FieldB, Time - 2678400L + 1L, Time, budget);
            IQuery<RideB> query = new WhereRange<RideB>(Metadata.RecordB.Fields[6], -73815000L, -73780000L - 1L, time);
            query = new WhereRange<RideB>(Metadata.RecordB.Fields[7], 40640000L, 40650000L - 1L, query);
            forOthers = null;
            return new Partition2DGeo<RideB>(
                Metadata.RecordB.Fields[8], -74010000L, 2500L - 1L, 16,
                Metadata.RecordB.Fields[9], 40710000L, 2500L - 1L, 16,
                query, forOthers, BudgetOverRuntime);
        }

        public AnalyticBaseFactory<Ride, RideB> GenerateQuery(long time, Field<Ride> field, Field<RideB> fieldB)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (fieldB == null) throw new ArgumentNullException(nameof(fieldB));
#endif
            Time = time;
            Field = field;
            FieldB = fieldB;

            return this;
        }
    }
}
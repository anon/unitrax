﻿namespace UniTraX.Datasets.Mobility.Queries
{
    using UniTraX.Datasets.Mobility.DataAccess;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Queries.Aggregation;
    using UniTraX.Queries.Core;

    public class RidesPerPickupDateTime : AnalyticBaseFactory<Ride, RideB>
    {
        public RidesPerPickupDateTime(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "RidesPerPickupDateTime", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            return new PerDayJanuary2013<Ride>(Metadata.Record.Fields[1],
                new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold),
                new Dummy<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            return new WhereBudget<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerDayJanuary2013<RideB>(Metadata.RecordB.Fields[1],
                new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold),
                new Dummy<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class RidesPerTotalAmount : AnalyticBaseFactory<Ride, RideB>
    {
        public RidesPerTotalAmount(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "RidesPerTotalAmount", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            return new Per2Dollars50From0To50<Ride>(Metadata.Record.Fields[15],
                new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold),
                new Dummy<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            return new WhereBudget<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new Per2Dollars50From0To50<RideB>(Metadata.RecordB.Fields[15],
                new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold),
                new Dummy<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class RidesPerTipAmount : AnalyticBaseFactory<Ride, RideB>
    {
        public RidesPerTipAmount(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "RidesPerTipAmount", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            return new Per50Cents0To10Dollar<Ride>(Metadata.Record.Fields[13],
                new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold),
                new Dummy<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            return new WhereBudget<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new Per50Cents0To10Dollar<RideB>(Metadata.RecordB.Fields[13],
                new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold),
                new Dummy<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class RidesPerPassenegerCount : AnalyticBaseFactory<Ride, RideB>
    {
        public RidesPerPassenegerCount(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "RidesPerPassenegerCount", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            return new PerNumbers<Ride>(Metadata.Record.Fields[3], 0L, DoubleTypeConverter.WholeNumber.ToSystem(1.0 - 1.0), 11, 0,
                new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold),
                new Dummy<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            return new WhereBudget<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerNumbers<RideB>(Metadata.RecordB.Fields[3], 0L, DoubleTypeConverter.WholeNumber.ToSystem(1.0 - 1.0), 11, 0,
                new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold),
                new Dummy<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class RidesPerTripTimeInSecs : AnalyticBaseFactory<Ride, RideB>
    {
        public RidesPerTripTimeInSecs(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "RidesPerTripTimeInSecs", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            return new PerNumbers<Ride>(Metadata.Record.Fields[4], 0L, DoubleTypeConverter.WholeNumber.ToSystem(120.0 - 1.0), 15, 0,
                new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold),
                new Dummy<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            return new WhereBudget<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerNumbers<RideB>(Metadata.RecordB.Fields[4], 0L, DoubleTypeConverter.WholeNumber.ToSystem(120.0 - 1.0), 15, 0,
                new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold),
                new Dummy<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class RidesPerTripDistance : AnalyticBaseFactory<Ride, RideB>
    {
        public RidesPerTripDistance(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "RidesPerTripDistance", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Ride> GenerateQueryDirect()
        {
            return new PerNumbers<Ride>(Metadata.Record.Fields[5], 0L, DoubleTypeConverter.TwoDecimal.ToSystem(0.5 - 0.01), 20, 0,
                new Count<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), CleanThreshold),
                new Dummy<Ride>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Ride>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Ride> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<RideB> GenerateQueryBK()
        {
            return new WhereBudget<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerNumbers<RideB>(Metadata.RecordB.Fields[5], 0L, DoubleTypeConverter.TwoDecimal.ToSystem(0.5 - 0.01), 20, 0,
                new Count<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), CleanThreshold),
                new Dummy<RideB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<RideB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }
}
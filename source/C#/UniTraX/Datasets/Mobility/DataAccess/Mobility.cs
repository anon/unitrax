﻿namespace UniTraX.Datasets.Mobility.DataAccess
{
    using System.Data.Linq.Mapping;

    [Database(Name = "mobility")]
    public class Mobility : System.Data.Linq.DataContext
    {
        private static readonly MappingSource MappingSource = new AttributeMappingSource();

        public Mobility(string connectionString) : base(connectionString, MappingSource)
        {
            ObjectTrackingEnabled = false;
            CommandTimeout = 300;
        }

        public System.Data.Linq.Table<Ride> Rides
        {
            get { return GetTable<Ride>(); }
        }
    }
}
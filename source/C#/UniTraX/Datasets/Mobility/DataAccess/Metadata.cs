﻿namespace UniTraX.Datasets.Mobility.DataAccess
{
    using System;
    using System.Linq;
    using UniTraX.Core.BoundedData;
    using UniTraX.Core.Specification;
    using UniTraX.Infrastructure.DataConversion;

    public class Metadata : IQueryableFactory<RideB>
    {
        private static DoubleTypeConverter TypeMobilityPk =         new DoubleTypeConverter(new DoubleType(1.0, 15000000.0, 1.0));
        private static DoubleTypeConverter TypeMobilityPickup =     new DoubleTypeConverter(new DoubleType(1356994800.0, 1359673199.0, 1.0));
        private static DoubleTypeConverter TypeMobilityDropoff =    new DoubleTypeConverter(new DoubleType(1356994800.0, 1359716399.0, 1.0));
        private static DoubleTypeConverter TypeMobilityCount =      new DoubleTypeConverter(new DoubleType(0.0, 300.0, 1.0));
        private static DoubleTypeConverter TypeMobilityDuration =   new DoubleTypeConverter(new DoubleType(0.0, 10800.0, 1.0));
        private static DoubleTypeConverter TypeMobilityDistance =   new DoubleTypeConverter(new DoubleType(0.0, 100.0, 0.01));
        private static DoubleTypeConverter TypeMobilityGps =        new DoubleTypeConverter(new DoubleType(-5000.0, 5000.0, 0.000001));
        private static DoubleTypeConverter TypeMobilityDollars =    new DoubleTypeConverter(new DoubleType(0.0, 1000.0, 0.01));
        private static DoubleTypeConverter TypeMobilityBudget =     new DoubleTypeConverter(new DoubleType(0.0, 10000000000.0, 0.01));

        private static Record<Ride> GenerateRecord()
        {
            var fields = new[]
            {
                new Field<Ride>(0,  "Pk",               TypeMobilityPk,         accessRecord => accessRecord.Pk),
                new Field<Ride>(1,  "PickupDatetime",   TypeMobilityPickup,     accessRecord => accessRecord.PickupDatetime),
                new Field<Ride>(2,  "DropoffDatetime",  TypeMobilityDropoff,    accessRecord => accessRecord.DropoffDatetime),
                new Field<Ride>(3,  "PassengerCount",   TypeMobilityCount,      accessRecord => accessRecord.PassengerCount),
                new Field<Ride>(4,  "TripTimeInSecs",   TypeMobilityDuration,   accessRecord => accessRecord.TripTimeInSecs),
                new Field<Ride>(5,  "TripDistance",     TypeMobilityDistance,   accessRecord => accessRecord.TripDistance),
                new Field<Ride>(6,  "PickupLongitude",  TypeMobilityGps,        accessRecord => accessRecord.PickupLongitude),
                new Field<Ride>(7,  "PickupLatitude",   TypeMobilityGps,        accessRecord => accessRecord.PickupLatitude),
                new Field<Ride>(8,  "DropoffLongitude", TypeMobilityGps,        accessRecord => accessRecord.DropoffLongitude),
                new Field<Ride>(9,  "DropoffLatitude",  TypeMobilityGps,        accessRecord => accessRecord.DropoffLatitude),
                new Field<Ride>(10, "FareAmount",       TypeMobilityDollars,    accessRecord => accessRecord.FareAmount),
                new Field<Ride>(11, "Surcharge",        TypeMobilityDollars,    accessRecord => accessRecord.Surcharge),
                new Field<Ride>(12, "MtaTax",           TypeMobilityDollars,    accessRecord => accessRecord.MtaTax),
                new Field<Ride>(13, "TipAmount",        TypeMobilityDollars,    accessRecord => accessRecord.TipAmount),
                new Field<Ride>(14, "TollsAmount",      TypeMobilityDollars,    accessRecord => accessRecord.TollsAmount),
                new Field<Ride>(15, "TotalAmount",      TypeMobilityDollars,    accessRecord => accessRecord.TotalAmount)
            };
            return new Record<Ride>(fields);
        }

        private static Record<RideB> GenerateRecordB()
        {
            var fields = new[]
            {
                new Field<RideB>(0,  "Pk",               TypeMobilityPk,        accessRecord => accessRecord.Pk),
                new Field<RideB>(1,  "PickupDatetime",   TypeMobilityPickup,    accessRecord => accessRecord.PickupDatetime),
                new Field<RideB>(2,  "DropoffDatetime",  TypeMobilityDropoff,   accessRecord => accessRecord.DropoffDatetime),
                new Field<RideB>(3,  "PassengerCount",   TypeMobilityCount,     accessRecord => accessRecord.PassengerCount),
                new Field<RideB>(4,  "TripTimeInSecs",   TypeMobilityDuration,  accessRecord => accessRecord.TripTimeInSecs),
                new Field<RideB>(5,  "TripDistance",     TypeMobilityDistance,  accessRecord => accessRecord.TripDistance),
                new Field<RideB>(6,  "PickupLongitude",  TypeMobilityGps,       accessRecord => accessRecord.PickupLongitude),
                new Field<RideB>(7,  "PickupLatitude",   TypeMobilityGps,       accessRecord => accessRecord.PickupLatitude),
                new Field<RideB>(8,  "DropoffLongitude", TypeMobilityGps,       accessRecord => accessRecord.DropoffLongitude),
                new Field<RideB>(9,  "DropoffLatitude",  TypeMobilityGps,       accessRecord => accessRecord.DropoffLatitude),
                new Field<RideB>(10, "FareAmount",       TypeMobilityDollars,   accessRecord => accessRecord.FareAmount),
                new Field<RideB>(11, "Surcharge",        TypeMobilityDollars,   accessRecord => accessRecord.Surcharge),
                new Field<RideB>(12, "MtaTax",           TypeMobilityDollars,   accessRecord => accessRecord.MtaTax),
                new Field<RideB>(13, "TipAmount",        TypeMobilityDollars,   accessRecord => accessRecord.TipAmount),
                new Field<RideB>(14, "TollsAmount",      TypeMobilityDollars,   accessRecord => accessRecord.TollsAmount),
                new Field<RideB>(15, "TotalAmount",      TypeMobilityDollars,   accessRecord => accessRecord.TotalAmount),
                new Field<RideB>(16, "Budget",           TypeMobilityBudget,    accessRecord => accessRecord.Budget)
            };
            return new Record<RideB>(16, fields);
        }

        public static readonly Record<Ride> Record = GenerateRecord();
        public static readonly Record<RideB> RecordB = GenerateRecordB();

        public string ConnectionString { get; private set; }
        public BoundedDataAccess<RideB> BoundedDataAccess { get; private set; }
        public IQueryable<RideB> Rides;

        public Metadata(string connectionString)
        {
#if DEBUG
            if (connectionString == null) throw new ArgumentNullException(nameof(connectionString));
#endif
            ConnectionString = connectionString;
            BoundedDataAccess = new BoundedDataAccess<RideB>(RecordB, this, new ExpressionProvider<RideB>());
            Rides = new MobilityB(ConnectionString).Rides;
        }

        public IQueryable<RideB> Create()
        {
            return Rides;
        }
    }
}
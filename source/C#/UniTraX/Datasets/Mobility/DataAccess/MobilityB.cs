﻿namespace UniTraX.Datasets.Mobility.DataAccess
{
    using System.Data.Linq.Mapping;

    [Database(Name = "mobility")]
    public class MobilityB : System.Data.Linq.DataContext
    {
        private static readonly MappingSource MappingSource = new AttributeMappingSource();

        public MobilityB(string connectionString) : base(connectionString, MappingSource)
        {
            ObjectTrackingEnabled = false;
            CommandTimeout = 300;
        }

        public System.Data.Linq.Table<RideB> Rides
        {
            get { return GetTable<RideB>(); }
        }
    }
}
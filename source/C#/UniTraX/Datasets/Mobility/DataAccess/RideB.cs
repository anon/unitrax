﻿namespace UniTraX.Datasets.Mobility.DataAccess
{
    using System.Data.Linq.Mapping;

    [Table(Name = "rides")]
    public class RideB
    {
        [Column(Name = "pk", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Pk;
        [Column(Name = "pickup_datetime", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double PickupDatetime;
        [Column(Name = "dropoff_datetime", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DropoffDatetime;
        [Column(Name = "passenger_count", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double PassengerCount;
        [Column(Name = "trip_time_in_secs", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double TripTimeInSecs;
        [Column(Name = "trip_distance", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double TripDistance;
        [Column(Name = "pickup_longitude", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double PickupLongitude;
        [Column(Name = "pickup_latitude", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double PickupLatitude;
        [Column(Name = "dropoff_longitude", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DropoffLongitude;
        [Column(Name = "dropoff_latitude", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double DropoffLatitude;
        [Column(Name = "fare_amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double FareAmount;
        [Column(Name = "surcharge", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Surcharge;
        [Column(Name = "mta_tax", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double MtaTax;
        [Column(Name = "tip_amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double TipAmount;
        [Column(Name = "tolls_amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double TollsAmount;
        [Column(Name = "total_amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double TotalAmount;
        [Column(Name = "budget", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Budget;
    }
}
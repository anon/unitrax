﻿namespace UniTraX.Datasets.Financial.Queries
{
    using System.Collections.Generic;
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Financial.DataAccess;
    using UniTraX.Queries.Core;

    public class PerYear1990to2000<R> : PerNumbers<R>
    {
        public static List<Part> GenerateParts()
        {
            return new List<Part>(10)
            {
                new Part(631148400L, 662684400L - 1L, 1990),
                new Part(662684400L, 694220400L - 1L, 1991),
                new Part(694220400L, 725842800L - 1L, 1992),
                new Part(725842800L, 757378800L - 1L, 1993),
                new Part(757378800L, 788914800L - 1L, 1994),
                new Part(788914800L, 820450800L - 1L, 1995),
                new Part(820450800L, 852073200L - 1L, 1996),
                new Part(852073200L, 883609200L - 1L, 1997),
                new Part(883609200L, 915145200L - 1L, 1998),
                new Part(915145200L, 946681200L - 1L, 1999)
            };
        }

        public PerYear1990to2000(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerDecade1900to2000<R> : PerNumbers<R>
    {
        public static List<Part> GenerateParts()
        {
            return new List<Part>(10)
            {
                new Part(-2208992400L, -1893459600L - 1L,   1900),
                new Part(-1893459600L, -1577926800L - 1L,   1910),
                new Part(-1577926800L, -1262307600L - 1L,   1920),
                new Part(-1262307600L, -946774800L - 1L,    1930),
                new Part(-946774800L,  -631155600L - 1L,    1940),
                new Part(-631155600L,  -315622800L - 1L,    1950),
                new Part(-315622800L,  -3600L - 1L,         1960),
                new Part(-3600L,        315529200L - 1L,    1970),
                new Part( 315529200L,   631148400L - 1L,    1980),
                new Part( 631148400L,   946681200L - 1L,    1990)
            };
        }

        public PerDecade1900to2000(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }
}
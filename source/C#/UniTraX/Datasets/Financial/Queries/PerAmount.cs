﻿namespace UniTraX.Datasets.Financial.Queries
{
    using System.Collections.Generic;
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Financial.DataAccess;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Queries.Core;

    public class PerAccountBalance<R> : PerNumbers<R>
    {
        public static List<Part> GenerateParts()
        {
            return new List<Part>(18)
            {
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-30000.0), DoubleTypeConverter.TwoDecimal.ToSystem(-15000.0 - 0.01),   0),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-15000.0), DoubleTypeConverter.TwoDecimal.ToSystem(-6000.0 - 0.01),    1),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-6000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(-3000.0 - 0.01),    2),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-3000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(-1500.0 - 0.01),    3),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-1500.0),  DoubleTypeConverter.TwoDecimal.ToSystem(-600.0 - 0.01),     4),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-600.0),   DoubleTypeConverter.TwoDecimal.ToSystem(-300.0 - 0.01),     5),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-300.0),   DoubleTypeConverter.TwoDecimal.ToSystem(-150.0 - 0.01),     6),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(-150.0),   DoubleTypeConverter.TwoDecimal.ToSystem(0.0 - 0.01),        7),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(0.0),      DoubleTypeConverter.TwoDecimal.ToSystem(150.0 - 0.01),      8),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(150.0),    DoubleTypeConverter.TwoDecimal.ToSystem(300.0 - 0.01),      9),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(300.0),    DoubleTypeConverter.TwoDecimal.ToSystem(600.0 - 0.01),      10),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(600.0),    DoubleTypeConverter.TwoDecimal.ToSystem(1500.0 - 0.01),     11),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(1500.0),   DoubleTypeConverter.TwoDecimal.ToSystem(3000.0 - 0.01),     12),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(3000.0),   DoubleTypeConverter.TwoDecimal.ToSystem(6000.0 - 0.01),     13),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(6000.0),   DoubleTypeConverter.TwoDecimal.ToSystem(15000.0 - 0.01),    14),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(15000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(30000.0 - 0.01),    15),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(30000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(60000.0 - 0.01),    16),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(60000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(150000.0 - 0.01),   17)
            };
        }

        public PerAccountBalance(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerLoanAmount<R> : PerNumbers<R>
    {
        public static List<Part> GenerateParts()
        {
            return new List<Part>(12)
            {
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(0.0),      DoubleTypeConverter.TwoDecimal.ToSystem(150.0 - 0.01),      0),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(150.0),    DoubleTypeConverter.TwoDecimal.ToSystem(300.0 - 0.01),      1),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(300.0),    DoubleTypeConverter.TwoDecimal.ToSystem(600.0 - 0.01),      2),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(600.0),    DoubleTypeConverter.TwoDecimal.ToSystem(1500.0 - 0.01),     3),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(1500.0),   DoubleTypeConverter.TwoDecimal.ToSystem(3000.0 - 0.01),     4),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(3000.0),   DoubleTypeConverter.TwoDecimal.ToSystem(6000.0 - 0.01),     5),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(6000.0),   DoubleTypeConverter.TwoDecimal.ToSystem(15000.0 - 0.01),    6),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(15000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(30000.0 - 0.01),    7),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(30000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(60000.0 - 0.01),    8),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(60000.0),  DoubleTypeConverter.TwoDecimal.ToSystem(150000.0 - 0.01),   9),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(150000.0), DoubleTypeConverter.TwoDecimal.ToSystem(300000.0 - 0.01),   10),
                new Part(DoubleTypeConverter.TwoDecimal.ToSystem(300000.0), DoubleTypeConverter.TwoDecimal.ToSystem(600000.0 - 0.01),   11)
            };
        }

        public PerLoanAmount(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : base(field, GenerateParts(), forEach, forOthers, budgetOverRuntime) { }
    }
}
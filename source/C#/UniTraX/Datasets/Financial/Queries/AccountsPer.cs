﻿namespace UniTraX.Datasets.Financial.Queries
{
    using System.Linq;
    using UniTraX.Datasets.Financial.DataAccess;
    using UniTraX.Queries.Aggregation;
    using UniTraX.Queries.Core;

    public class AccountsPerOwnerSex : AnalyticBaseFactory<Account, AccountB>
    {
        public AccountsPerOwnerSex(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) : 
            base(id, "AccountsPerOwnerSex", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new PerSex<Account>(Metadata.Record.Fields[6],
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold),
                new Dummy<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold), BudgetOverRuntime);
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new PerSex<AccountB>(Metadata.RecordB.Fields[6],
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold),
                new Dummy<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }
    }

    public class FemaleAccountsPerAccountDistrict : AnalyticBaseFactory<Account, AccountB>
    {
        public FemaleAccountsPerAccountDistrict(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "FemaleAccountsPerAccountDistrict", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new PerDistrict<Account>(Metadata.Record.Fields[1],
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold),
                new Dummy<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new PerDistrict<AccountB>(Metadata.RecordB.Fields[1],
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold),
                new Dummy<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class FemaleAccountsPerAccountDate : AnalyticBaseFactory<Account, AccountB>
    {
        public FemaleAccountsPerAccountDate(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) : 
            base(id, "FemaleAccountsPerAccountDate", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new PerYear1990to2000<Account>(Metadata.Record.Fields[3],
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold),
                new Dummy<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new PerYear1990to2000<AccountB>(Metadata.RecordB.Fields[3],
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold),
                new Dummy<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class FemaleAccountsPerAccountBalance : AnalyticBaseFactory<Account, AccountB>
    {
        public FemaleAccountsPerAccountBalance(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "FemaleAccountsPerAccountBalance", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new PerAccountBalance<Account>(Metadata.Record.Fields[4],
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold),
                new Dummy<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new PerAccountBalance<AccountB>(Metadata.RecordB.Fields[4],
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold),
                new Dummy<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class FemaleAccountsPerOwnerBirthdate : AnalyticBaseFactory<Account, AccountB>
    {
        public FemaleAccountsPerOwnerBirthdate(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "FemaleAccountsPerOwnerBirthdate", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new PerDecade1900to2000<Account>(Metadata.Record.Fields[5],
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold),
                new Dummy<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new PerDecade1900to2000<AccountB>(Metadata.RecordB.Fields[5],
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold),
                new Dummy<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class FemaleAccountsPerLoanAmount : AnalyticBaseFactory<Account, AccountB>
    {
        public FemaleAccountsPerLoanAmount(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "FemaleAccountsPerLoanAmount", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new PerLoanAmount<Account>(Metadata.Record.Fields[14],
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold),
                new Dummy<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold), BudgetOverRuntime));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new PerLoanAmount<AccountB>(Metadata.RecordB.Fields[14],
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold),
                new Dummy<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold), BudgetOverRuntime)));
        }
    }

    public class FemaleAccountsPerTransactionsAmount : AnalyticBaseFactory<Account, AccountB>
    {
        public FemaleAccountsPerTransactionsAmount(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long epsilon) :
            base(id, "FemaleAccountsPerTransactionsAmount", useEvenly, budgetOverRuntime, cleanThreshold, new long[] { epsilon }) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert,
                new PerTransactionsExponentialCondition<Account>(account => account.Transactions.Sum(transaction => transaction.Amount), 10.0, 6), CleanThreshold));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            return new WhereBudget<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()],
                new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert,
                new PerTransactionsExponentialCondition<AccountB>(account => account.Transactions.Sum(transaction => transaction.Amount), 10.0, 6), CleanThreshold)));
        }
    }
}

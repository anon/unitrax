﻿namespace UniTraX.Datasets.Financial.Queries
{
	using PINQ;
    using System.Linq;
    using System.Linq.Expressions;
    using System;
    using UniTraX.Queries.Core;

    public class PerTransactionsExponentialCondition<R> : IPINQCondition<R>
    {
        private class KeyExpression<T>
        {
            public static Expression<Func<T, int>> Generate(Expression<Func<T, double>> valueExpression, double baseValue, int maxPower)
            {
#if DEBUG
                if (valueExpression == null) throw new ArgumentNullException(nameof(valueExpression));
                if (baseValue <= 0.0) throw new ArgumentException("baseValue is not valid");
                if (maxPower < 1) throw new ArgumentException("maxPower is not valid");
#endif
                int power = maxPower;
                double value = Math.Pow(baseValue, power);
                Expression expression = Expression.Condition(Expression.LessThanOrEqual(valueExpression.Body, Expression.Constant(value)), Expression.Constant(power), Expression.Constant(-1));
                for (power = power - 1; power > 0; power--)
                {
                    value = Math.Pow(baseValue, power);
                    expression = Expression.Condition(Expression.LessThanOrEqual(valueExpression.Body, Expression.Constant(value)), Expression.Constant(power), expression);
                }
                return Expression.Lambda<Func<T, int>>(expression, valueExpression.Parameters);
            }
        }

        private static int[] GenerateKeys(int maxPower)
        {
#if DEBUG
            if (maxPower < 1) throw new ArgumentException("maxPower is not valid");
#endif
            var keys = new int[maxPower + 1];
            keys[0] = -1;
            for (int i = 1; i <= maxPower; i++) keys[i] = i;
            return keys;
        }

        private readonly Expression<Func<R, double>> ValueExpression;
        private readonly double BaseValue;
        private readonly int MaxPower;

        public PerTransactionsExponentialCondition(Expression<Func<R, double>> valueExpression, double baseValue, int maxPower)
        {
#if DEBUG
            if (valueExpression == null) throw new ArgumentNullException(nameof(valueExpression));
            if (baseValue <= 0.0) throw new ArgumentException("baseValue is not valid");
            if (maxPower < 1) throw new ArgumentException("maxPower is not valid");
#endif
            ValueExpression = valueExpression;
            BaseValue = baseValue;
            MaxPower = maxPower;
        }

        public IQueryable<R>[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var queryables = new IQueryable<R>[MaxPower];
            foreach (var group in queryable.GroupBy(KeyExpression<R>.Generate(ValueExpression, BaseValue, MaxPower))) if(group.Key > 0) queryables[group.Key - 1] = group.AsQueryable<R>();
            for (int i = 0; i < queryables.Length; i++) if (queryables[i] == null) queryables[i] = Enumerable.Empty<R>().AsQueryable();
            return queryables;
        }

        public PINQueryable<R>[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var queryableMap = queryable.Partition(GenerateKeys(MaxPower), KeyExpression<R>.Generate(ValueExpression, BaseValue, MaxPower));
            var queryables = new PINQueryable<R>[MaxPower];
            for (int i = 1; i <= MaxPower; i++) queryables[i - 1] = queryableMap[i];
            return queryables;
        }

        public IPINQCondition<R> Clone()
        {
            return new PerTransactionsExponentialCondition<R>(ValueExpression, BaseValue, MaxPower);
        }
    }
}
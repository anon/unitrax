﻿namespace UniTraX.Datasets.Financial.Queries
{
    using System.Collections.Generic;
    using UniTraX.Datasets.Financial.DataAccess;
    using UniTraX.Queries.Aggregation;
    using UniTraX.Queries.Core;

    public class LocalInvestigation : AnalyticBaseFactory<Account, AccountB>
    {
        public LocalInvestigation(long id, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long[] epsilons) :
            base(id, "LocalInvestigation1", useEvenly, budgetOverRuntime, cleanThreshold, epsilons) { }

        public override IQuery<Account> GenerateQueryDirect()
        {
            var list = new List<IQuery<Account>>(5)
            {
                new Average<Account>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), Metadata.Record.Fields[5], 2208992400.0, CleanThreshold),
                new Median<Account>(Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), Metadata.Record.Fields[5], 2208992400.0, 3155673600.0, CleanThreshold),
                new Average<Account>(Epsilons[3], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), Metadata.Record.Fields[4], 150000.0, CleanThreshold),
                new Median<Account>(Epsilons[4], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), Metadata.Record.Fields[4], 30000.0, 180000.0, CleanThreshold),
                new Sum<Account>(Epsilons[5], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), Metadata.Record.Fields[4], 150000.0, CleanThreshold)
            };
            var multi = new Multi<Account>(list);

            IQuery<Account> forOthers = null;
            forOthers = new Dummy<Account>(Epsilons[1] + Epsilons[2] + Epsilons[3] + Epsilons[4] + Epsilons[5], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), UseEvenly, CleanThreshold);
            var conditional = new Conditional<Account>(x => x[0] > 19.5, new Count<Account>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<Account>(), CleanThreshold), multi, forOthers);
            forOthers = null;
            return new WhereRange<Account>(Metadata.Record.Fields[6], 2L, 2L,
                new PerDistrict<Account>(Metadata.Record.Fields[1], conditional, forOthers, BudgetOverRuntime));
        }

        public override IQuery<Account> GenerateQueryPINQ()
        {
            return GenerateQueryDirect();
        }

        public override IQuery<AccountB> GenerateQueryBK()
        {
            var list = new List<IQuery<AccountB>>(5)
            {
                new Average<AccountB>(Epsilons[1], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), Metadata.RecordB.Fields[5], 2208992400.0, CleanThreshold),
                new Median<AccountB>(Epsilons[2], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), Metadata.RecordB.Fields[5], 2208992400.0, 3155673600.0, CleanThreshold),
                new Average<AccountB>(Epsilons[3], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), Metadata.RecordB.Fields[4], 150000.0, CleanThreshold),
                new Median<AccountB>(Epsilons[4], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), Metadata.RecordB.Fields[4], 30000.0, 180000.0, CleanThreshold),
                new Sum<AccountB>(Epsilons[5], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), Metadata.RecordB.Fields[4], 150000.0, CleanThreshold)
            };
            var multi = new Multi<AccountB>(list);

            IQuery<AccountB> forOthers = null;
            forOthers = new Dummy<AccountB>(Epsilons[1] + Epsilons[2] + Epsilons[3] + Epsilons[4] + Epsilons[5], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), UseEvenly, CleanThreshold);
            var conditional = new Conditional<AccountB>(x => x[0] > 19.5, new Count<AccountB>(Epsilons[0], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()].FieldType.Convert, new PINQDummy<AccountB>(), CleanThreshold), multi, forOthers);
            forOthers = null;
            var budget = new WhereBudget<AccountB>(Epsilons[0] + Epsilons[1] + Epsilons[2] + Epsilons[3] + Epsilons[4] + Epsilons[5], Metadata.RecordB.Fields[Metadata.RecordB.GetBudgetIndex()], conditional);
            return new WhereRange<AccountB>(Metadata.RecordB.Fields[6], 2L, 2L,
                new PerDistrict<AccountB>(Metadata.RecordB.Fields[1], budget, forOthers, BudgetOverRuntime));
        }
    }
}
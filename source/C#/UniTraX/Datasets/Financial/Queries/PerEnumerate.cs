﻿namespace UniTraX.Datasets.Financial.Queries
{
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Financial.DataAccess;
    using UniTraX.Queries.Core;

    public class PerDistrict<R> : PerNumbers<R>
    {
        public PerDistrict(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 1L, 1L - 1L, 77, 1, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerCardType<R> : PerNumbers<R>
    {
        public PerCardType(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 0L, 1L - 1L, 4, 0, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerSex<R> : PerNumbers<R>
    {
        public PerSex(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 1L, 1L - 1L, 2, 1, forEach, forOthers, budgetOverRuntime) { }
    }

    public class PerLoanStatus<R> : PerNumbers<R>
    {
        public PerLoanStatus(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            base(field, 0L, 1L - 1L, 5, 0, forEach, forOthers, budgetOverRuntime) { }
    }
}
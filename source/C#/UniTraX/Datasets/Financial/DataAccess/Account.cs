﻿namespace UniTraX.Datasets.Financial.DataAccess
{
    using System.Collections.Generic;
    using System.Data.Linq;
    using System.Data.Linq.Mapping;

    [Table(Name = "account")]
    public class Account
    {
        [Column(Name = "account_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double AccountId;
        [Column(Name = "account_district_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountDistrictId;
        [Column(Name = "account_frequency", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountFrequency;
        [Column(Name = "account_date", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountDate;
        [Column(Name = "account_balance", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountBalance;
        [Column(Name = "owner_birthdate", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double OwnerBirthdate;
        [Column(Name = "owner_sex", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double OwnerSex;
        [Column(Name = "owner_district_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double OwnerDistrictId;
        [Column(Name = "owner_card_type", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double OwnerCardType;
        [Column(Name = "owner_card_issued", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double OwnerCardIssued;
        [Column(Name = "user_birthdate", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double UserBirthdate;
        [Column(Name = "user_sex", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double UserSex;
        [Column(Name = "user_district_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double UserDistrictId;
        [Column(Name = "loan_date", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double LoanDate;
        [Column(Name = "loan_amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double LoanAmount;
        [Column(Name = "loan_duration", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double LoanDuration;
        [Column(Name = "loan_payments", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double LoanPayments;
        [Column(Name = "loan_status", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double LoanStatus;
        private EntitySet<Order> _orders = new EntitySet<Order>();
        [Association(Name = "FK_order_account_id", Storage = "_orders", OtherKey = "AccountId", ThisKey = "AccountId")]
        public ICollection<Order> Orders { get { return _orders; } set { _orders.Assign(value); } }
        private EntitySet<Transaction> _transactions = new EntitySet<Transaction>();
        [Association(Name = "FK_transaction_account_id", Storage = "_transactions", OtherKey = "AccountId", ThisKey = "AccountId")]
        public ICollection<Transaction> Transactions { get { return _transactions; } set { _transactions.Assign(value); } }
        private EntityRef<District> _accountDistrict;
        [Association(Name = "FK_account_account_district_id", IsForeignKey = true, Storage = "_accountDistrict", ThisKey = "AccountDistrictId")]
        public District AccountDistrict { get { return _accountDistrict.Entity; } set { _accountDistrict.Entity = value; } }
        private EntityRef<District> _ownerDistrict;
        [Association(Name = "FK_account_owner_district_id", IsForeignKey = true, Storage = "_ownerDistrict", ThisKey = "OwnerDistrictId")]
        public District OwnerDistrict { get { return _ownerDistrict.Entity; } set { _ownerDistrict.Entity = value; } }
        private EntityRef<District> _userDistrict;
        [Association(Name = "FK_account_user_district_id", IsForeignKey = true, Storage = "_userDistrict", ThisKey = "UserDistrictId")]
        public District UserDistrict { get { return _userDistrict.Entity; } set { _userDistrict.Entity = value; } }
    }
}
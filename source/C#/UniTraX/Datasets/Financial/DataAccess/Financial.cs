﻿namespace UniTraX.Datasets.Financial.DataAccess
{
    using System.Data.Linq.Mapping;

    [Database(Name = "financial")]
    public class Financial : System.Data.Linq.DataContext
    {
        private static readonly MappingSource MappingSource = new AttributeMappingSource();

        public Financial(string connectionString) : base(connectionString, MappingSource)
        {
            ObjectTrackingEnabled = false;
            CommandTimeout = 300;
        }

        public System.Data.Linq.Table<Account> Accounts
        {
            get { return GetTable<Account>(); }
        }

        public System.Data.Linq.Table<District> Districts
        {
            get { return GetTable<District>(); }
        }

        public System.Data.Linq.Table<Order> Orders
        {
            get { return GetTable<Order>(); }
        }

        public System.Data.Linq.Table<Transaction> Transactions
        {
            get { return GetTable<Transaction>(); }
        }
    }
}
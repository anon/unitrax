﻿namespace UniTraX.Datasets.Financial.DataAccess
{
    using System.Collections.Generic;
    using System.Data.Linq;
    using System.Data.Linq.Mapping;

    [Table(Name = "district")]
    public class DistrictB
    {
        [Column(Name = "id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Id;
        [Column(Name = "name", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Name;
        [Column(Name = "region", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Region;
        [Column(Name = "no_inhabitants", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double NoInhabitants;
        [Column(Name = "no_municipalities_inhabitants_0", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double NoMunicipalitiesInhabitants0;
        [Column(Name = "no_municipalities_inhabitants_500", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double NoMunicipalitiesInhabitants500;
        [Column(Name = "no_municipalities_inhabitants_2000", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double NoMunicipalitiesInhabitants2000;
        [Column(Name = "no_municipalities_inhabitants_10000", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double NoMunicipalitiesInhabitants10000;
        [Column(Name = "no_cities", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double NoCities;
        [Column(Name = "urban_ratio", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double UrbanRatio;
        [Column(Name = "avg_salary", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AvgSalary;
        [Column(Name = "unemployment_95", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Unemployment95;
        [Column(Name = "unemployment_96", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Unemployment96;
        [Column(Name = "entrepreneurs_per_1000", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double EntrepreneursPer1000;
        [Column(Name = "crimes_95", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Crimes95;
        [Column(Name = "crimes_96", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Crimes96;
        private EntitySet<AccountB> _accountsAccountDistrict = new EntitySet<AccountB>();
        [Association(Name = "FK_District_Accounts_AD", Storage = "_accountsAccountDistrict", OtherKey = "AccountDistrictId", ThisKey = "Id")]
        public ICollection<AccountB> AccountsAccountDistrict { get { return _accountsAccountDistrict; } set { _accountsAccountDistrict.Assign(value); } }
        private EntitySet<AccountB> _accountsOwnerDistrict = new EntitySet<AccountB>();
        [Association(Name = "FK_District_Accounts_OD", Storage = "_accountsOwnerDistrict", OtherKey = "OwnerDistrictId", ThisKey = "Id")]
        public ICollection<AccountB> AccountsOwnerDistrict { get { return _accountsOwnerDistrict; } set { _accountsOwnerDistrict.Assign(value); } }
        private EntitySet<AccountB> _accountsUserDistrict = new EntitySet<AccountB>();
        [Association(Name = "FK_District_Accounts_UD", Storage = "_accountsUserDistrict", OtherKey = "UserDistrictId", ThisKey = "Id")]
        public ICollection<AccountB> AccountsUserDistrict { get { return _accountsUserDistrict; } set { _accountsUserDistrict.Assign(value); } }
    }
}
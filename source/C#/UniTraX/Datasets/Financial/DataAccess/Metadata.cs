﻿namespace UniTraX.Datasets.Financial.DataAccess
{
    using System;
    using System.Linq;
    using UniTraX.Core.BoundedData;
    using UniTraX.Core.Specification;
    using UniTraX.Infrastructure.DataConversion;

    public class Metadata : IQueryableFactory<AccountB>
    {
        private static DoubleTypeConverter TypeFinancialDistrictId =         new DoubleTypeConverter(new DoubleType(1.0, 77.0, 1.0));
        private static DoubleTypeConverter TypeFinancialDate =               new DoubleTypeConverter(new DoubleType(631148400.0, 946681199.0, 1.0));
        private static DoubleTypeConverter TypeFinancialBirthdate =          new DoubleTypeConverter(new DoubleType(-2208992400.0, 946681199.0, 1.0));
        private static DoubleTypeConverter TypeFinancialSex =                new DoubleTypeConverter(new DoubleType(1.0, 2.0, 1.0));
        private static DoubleTypeConverter TypeFinancialAccountId =          new DoubleTypeConverter(new DoubleType(1.0, 11382.0, 1.0));
        private static DoubleTypeConverter TypeFinancialAccountFrequency =   new DoubleTypeConverter(new DoubleType(1.0, 3.0, 1.0));
        private static DoubleTypeConverter TypeFinancialAccountBalance =     new DoubleTypeConverter(new DoubleType(-30000.0, 150000.0, 0.01));
        private static DoubleTypeConverter TypeFinancialOwnerCardType =      new DoubleTypeConverter(new DoubleType(0.0, 3.0, 1.0));
        private static DoubleTypeConverter TypeFinancialLoanAmount =         new DoubleTypeConverter(new DoubleType(0.0, 600000.0, 0.01));
        private static DoubleTypeConverter TypeFinancialLoanDuration =       new DoubleTypeConverter(new DoubleType(0.0, 60.0, 1.0));
        private static DoubleTypeConverter TypeFinancialLoanPayments =       new DoubleTypeConverter(new DoubleType(0.0, 10000.0, 0.01));
        private static DoubleTypeConverter TypeFinancialLoanStatus =         new DoubleTypeConverter(new DoubleType(0.0, 4.0, 1.0));
        private static DoubleTypeConverter TypeFinancialBudget =             new DoubleTypeConverter(new DoubleType(0.0, 10000000000.0, 0.01));

        private static Record<Account> GenerateRecord()
        {
            var fields = new[]
            {
                new Field<Account>(0,  "AccountId",         TypeFinancialAccountId,          accessRecord => accessRecord.AccountId),
                new Field<Account>(1,  "AccountDistrictId", TypeFinancialDistrictId,         accessRecord => accessRecord.AccountDistrictId),
                new Field<Account>(2,  "AccountFrequency",  TypeFinancialAccountFrequency,   accessRecord => accessRecord.AccountFrequency),
                new Field<Account>(3,  "AccountDate",       TypeFinancialDate,               accessRecord => accessRecord.AccountDate),
                new Field<Account>(4,  "AccountBalance",    TypeFinancialAccountBalance,     accessRecord => accessRecord.AccountBalance),
                new Field<Account>(5,  "OwnerBirthdate",    TypeFinancialBirthdate,          accessRecord => accessRecord.OwnerBirthdate),
                new Field<Account>(6,  "OwnerSex",          TypeFinancialSex,                accessRecord => accessRecord.OwnerSex),
                new Field<Account>(7,  "OwnerDistrictId",   TypeFinancialDistrictId,         accessRecord => accessRecord.OwnerDistrictId),
                new Field<Account>(8,  "OwnerCardType",     TypeFinancialOwnerCardType,      accessRecord => accessRecord.OwnerCardType),
                new Field<Account>(9,  "OwnerCardIssued",   TypeFinancialDate,               accessRecord => accessRecord.OwnerCardIssued),
                new Field<Account>(10, "UserBirthdate",     TypeFinancialBirthdate,          accessRecord => accessRecord.UserBirthdate),
                new Field<Account>(11, "UserSex",           TypeFinancialSex,                accessRecord => accessRecord.UserSex),
                new Field<Account>(12, "UserDistrictId",    TypeFinancialDistrictId,         accessRecord => accessRecord.UserDistrictId),
                new Field<Account>(13, "LoanDate",          TypeFinancialDate,               accessRecord => accessRecord.LoanDate),
                new Field<Account>(14, "LoanAmount",        TypeFinancialLoanAmount,         accessRecord => accessRecord.LoanAmount),
                new Field<Account>(15, "LoanDuration",      TypeFinancialLoanDuration,       accessRecord => accessRecord.LoanDuration),
                new Field<Account>(16, "LoanPayments",      TypeFinancialLoanPayments,       accessRecord => accessRecord.LoanPayments),
                new Field<Account>(17, "LoanStatus",        TypeFinancialLoanStatus,         accessRecord => accessRecord.LoanStatus)
            };
            return new Record<Account>(fields);
        }

        private static Record<AccountB> GenerateRecordB()
        {
            var fields = new[]
            {
                new Field<AccountB>(0,  "AccountId",         TypeFinancialAccountId,         accessRecord => accessRecord.AccountId),
                new Field<AccountB>(1,  "AccountDistrictId", TypeFinancialDistrictId,        accessRecord => accessRecord.AccountDistrictId),
                new Field<AccountB>(2,  "AccountFrequency",  TypeFinancialAccountFrequency,  accessRecord => accessRecord.AccountFrequency),
                new Field<AccountB>(3,  "AccountDate",       TypeFinancialDate,              accessRecord => accessRecord.AccountDate),
                new Field<AccountB>(4,  "AccountBalance",    TypeFinancialAccountBalance,    accessRecord => accessRecord.AccountBalance),
                new Field<AccountB>(5,  "OwnerBirthdate",    TypeFinancialBirthdate,         accessRecord => accessRecord.OwnerBirthdate),
                new Field<AccountB>(6,  "OwnerSex",          TypeFinancialSex,               accessRecord => accessRecord.OwnerSex),
                new Field<AccountB>(7,  "OwnerDistrictId",   TypeFinancialDistrictId,        accessRecord => accessRecord.OwnerDistrictId),
                new Field<AccountB>(8,  "OwnerCardType",     TypeFinancialOwnerCardType,     accessRecord => accessRecord.OwnerCardType),
                new Field<AccountB>(9,  "OwnerCardIssued",   TypeFinancialDate,              accessRecord => accessRecord.OwnerCardIssued),
                new Field<AccountB>(10, "UserBirthdate",     TypeFinancialBirthdate,         accessRecord => accessRecord.UserBirthdate),
                new Field<AccountB>(11, "UserSex",           TypeFinancialSex,               accessRecord => accessRecord.UserSex),
                new Field<AccountB>(12, "UserDistrictId",    TypeFinancialDistrictId,        accessRecord => accessRecord.UserDistrictId),
                new Field<AccountB>(13, "LoanDate",          TypeFinancialDate,              accessRecord => accessRecord.LoanDate),
                new Field<AccountB>(14, "LoanAmount",        TypeFinancialLoanAmount,        accessRecord => accessRecord.LoanAmount),
                new Field<AccountB>(15, "LoanDuration",      TypeFinancialLoanDuration,      accessRecord => accessRecord.LoanDuration),
                new Field<AccountB>(16, "LoanPayments",      TypeFinancialLoanPayments,      accessRecord => accessRecord.LoanPayments),
                new Field<AccountB>(17, "LoanStatus",        TypeFinancialLoanStatus,        accessRecord => accessRecord.LoanStatus),
                new Field<AccountB>(18, "Budget",            TypeFinancialBudget,            accessRecord => accessRecord.Budget)
            };
            return new Record<AccountB>(18, fields);
        }

        public static readonly Record<Account> Record = GenerateRecord();
        public static readonly Record<AccountB> RecordB = GenerateRecordB();

        public string ConnectionString { get; private set; }
        public BoundedDataAccess<AccountB> BoundedDataAccess { get; private set; }
        public IQueryable<AccountB> Accounts;

        public Metadata(string connectionString)
        {
#if DEBUG
            if (connectionString == null) throw new ArgumentNullException(nameof(connectionString));
#endif
            ConnectionString = connectionString;
            BoundedDataAccess = new BoundedDataAccess<AccountB>(RecordB, this, new ExpressionProvider<AccountB>());
            Accounts = new FinancialB(ConnectionString).Accounts;
        }

        public IQueryable<AccountB> Create()
        {
            return Accounts;
        }
    }
}
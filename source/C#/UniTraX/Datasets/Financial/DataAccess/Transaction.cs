﻿namespace UniTraX.Datasets.Financial.DataAccess
{
    using System.Data.Linq;
    using System.Data.Linq.Mapping;

    [Table(Name = "transaction")]
    public class Transaction
    {
        [Column(Name = "id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Id;
        [Column(Name = "account_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountId;
        [Column(Name = "date", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Date;
        [Column(Name = "type", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Type;
        [Column(Name = "operation", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Operation;
        [Column(Name = "amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Amount;
        [Column(Name = "balance", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Balance;
        [Column(Name = "k_symbol", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double KSymbol;
        [Column(Name = "bank", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double BankTo;
        [Column(Name = "account", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountTo;
        private EntityRef<Account> _account;
        [Association(Name = "FK_transaction_account_id", IsForeignKey = true, Storage = "_account", OtherKey = "AccountId", ThisKey = "AccountId")]
        public Account Account { get { return _account.Entity; } set { _account.Entity = value; } }
    }
}
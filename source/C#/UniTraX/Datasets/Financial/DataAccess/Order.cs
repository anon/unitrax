﻿namespace UniTraX.Datasets.Financial.DataAccess
{
    using System.Data.Linq;
    using System.Data.Linq.Mapping;

    [Table(Name = "order")]
    public class Order
    {
        [Column(Name = "id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never, IsPrimaryKey = true)]
        public double Id;
        [Column(Name = "account_id", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountId;
        [Column(Name = "bank_to", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double BankTo;
        [Column(Name = "account_to", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double AccountTo;
        [Column(Name = "amount", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double Amount;
        [Column(Name = "k_symbol", DbType = "Float NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public double KSymbol;
        private EntityRef<Account> _account;
        [Association(Name = "FK_order_account_id", IsForeignKey = true, Storage = "_account", OtherKey = "AccountId", ThisKey = "AccountId")]
        public Account Account { get { return _account.Entity; } set { _account.Entity = value; } }
    }
}
﻿namespace UniTraX.Datasets.Financial.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using UniTraX.Infrastructure.DataLoading;

    public class FinancialDisk
    {
        public static string[] AccountColumnNames = new string[] {
            "account_id",
            "account_district_id",
            "account_frequency",
            "account_date",
            "account_balance",
            "owner_birthdate",
            "owner_sex",
            "owner_district_id",
            "owner_card_type",
            "owner_card_issued",
            "user_birthdate",
            "user_sex",
            "user_district_id",
            "loan_date",
            "loan_amount",
            "loan_duration",
            "loan_payments",
            "loan_status",
            "budget"
        };
        public static string[] DistrictColumnNames = new string[] {
            "id",
            "name",
            "region",
            "no_inhabitants",
            "no_municipalities_inhabitants_0",
            "no_municipalities_inhabitants_500",
            "no_municipalities_inhabitants_2000",
            "no_municipalities_inhabitants_10000",
            "no_cities",
            "urban_ratio",
            "avg_salary",
            "unemployment_95",
            "unemployment_96",
            "entrepreneurs_per_1000",
            "crimes_95",
            "crimes_96"
        };
        public static string[] OrderColumnNames = new string[] {
            "id",
            "account_id",
            "bank_to",
            "account_to",
            "amount",
            "k_symbol"
        };
        public static string[] TransactionColumnNames = new string[] {
            "id",
            "account_id",
            "date",
            "type",
            "operation",
            "amount",
            "balance",
            "k_symbol",
            "bank",
            "account"
        };

        public static void CreateTable(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Table districtTable = TableDisk.CreateTable(database, false, "financial-disk", "district", DistrictColumnNames, "id", "budget", null);
            Table accountTable = TableDisk.CreateTable(database, withBudget, "financial-disk", "account", AccountColumnNames, "account_id", "budget", null);
            ForeignKey accountDistrictIdFK = new ForeignKey(accountTable, "FK_account_account_district_id");
            ForeignKeyColumn accountDistrictIdFKC = new ForeignKeyColumn(accountDistrictIdFK, "account_district_id", "id");
            accountDistrictIdFK.Columns.Add(accountDistrictIdFKC);
            accountDistrictIdFK.ReferencedTable = "district";
            accountDistrictIdFK.Create();
            ForeignKey ownerDistrictIdFK = new ForeignKey(accountTable, "FK_account_owner_district_id");
            ForeignKeyColumn ownerDistrictIdFKC = new ForeignKeyColumn(ownerDistrictIdFK, "owner_district_id", "id");
            ownerDistrictIdFK.Columns.Add(ownerDistrictIdFKC);
            ownerDistrictIdFK.ReferencedTable = "district";
            ownerDistrictIdFK.Create();
            ForeignKey userDistrictIdFK = new ForeignKey(accountTable, "FK_account_user_district_id");
            ForeignKeyColumn userDistrictIdFKC = new ForeignKeyColumn(userDistrictIdFK, "user_district_id", "id");
            userDistrictIdFK.Columns.Add(userDistrictIdFKC);
            userDistrictIdFK.ReferencedTable = "district";
            userDistrictIdFK.Create();
            accountTable.Alter();
            Table orderTable = TableDisk.CreateTable(database, false, "financial-disk", "order", OrderColumnNames, "id", "budget", null);
            ForeignKey orderAccountFK = new ForeignKey(orderTable, "FK_order_account_id");
            ForeignKeyColumn orderAccountFKC = new ForeignKeyColumn(orderAccountFK, "account_id", "account_id");
            orderAccountFK.Columns.Add(orderAccountFKC);
            orderAccountFK.ReferencedTable = "account";
            orderAccountFK.Create();
            orderTable.Alter();
            Table transactionTable = TableDisk.CreateTable(database, false, "financial-disk", "transaction", TransactionColumnNames, "id", "budget", null);
            ForeignKey transactionAccountFK = new ForeignKey(transactionTable, "FK_transaction_account_id");
            ForeignKeyColumn transactionAccountFKC = new ForeignKeyColumn(transactionAccountFK, "account_id", "account_id");
            transactionAccountFK.Columns.Add(transactionAccountFKC);
            transactionAccountFK.ReferencedTable = "account";
            transactionAccountFK.Create();
            transactionTable.Alter();
        }

        public static void CreateIndexes(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Console.WriteLine("FinancialDisk: Creating indexes on tables account, order, and transaction " + (withBudget ? "with" : "without") + " budget.");
            var accountTable = database.Tables["account"];
            var accountIndexOS = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_account_owner_sex");
            accountIndexOS.IndexedColumns.Add(new IndexedColumn(accountIndexOS, "owner_sex", false));
            if (withBudget) accountIndexOS.IndexedColumns.Add(new IndexedColumn(accountIndexOS, "budget", false));
            accountTable.Indexes.Add(accountIndexOS);
            accountIndexOS.Create();
            accountIndexOS.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            accountIndexOS.Alter();
            var accountIndexOSADI = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_account_owner_sex_account_district_id");
            accountIndexOSADI.IndexedColumns.Add(new IndexedColumn(accountIndexOSADI, "owner_sex", false));
            accountIndexOSADI.IndexedColumns.Add(new IndexedColumn(accountIndexOSADI, "account_district_id", false));
            if (withBudget) accountIndexOSADI.IndexedColumns.Add(new IndexedColumn(accountIndexOSADI, "budget", false));
            accountIndexOSADI.IndexedColumns.Add(new IndexedColumn(accountIndexOSADI, "owner_birthdate") { IsIncluded = true });
            accountIndexOSADI.IndexedColumns.Add(new IndexedColumn(accountIndexOSADI, "account_balance") { IsIncluded = true });
            accountTable.Indexes.Add(accountIndexOSADI);
            accountIndexOSADI.Create();
            accountIndexOSADI.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            accountIndexOSADI.Alter();
            var accountIndexOSAD = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_owner_sex_account_account_date");
            accountIndexOSAD.IndexedColumns.Add(new IndexedColumn(accountIndexOSAD, "owner_sex", false));
            accountIndexOSAD.IndexedColumns.Add(new IndexedColumn(accountIndexOSAD, "account_date", false));
            if (withBudget) accountIndexOSAD.IndexedColumns.Add(new IndexedColumn(accountIndexOSAD, "budget", false));
            accountTable.Indexes.Add(accountIndexOSAD);
            accountIndexOSAD.Create();
            accountIndexOSAD.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            accountIndexOSAD.Alter();
            var accountIndexOSAB = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_account_owner_sex_account_balance");
            accountIndexOSAB.IndexedColumns.Add(new IndexedColumn(accountIndexOSAB, "owner_sex", false));
            accountIndexOSAB.IndexedColumns.Add(new IndexedColumn(accountIndexOSAB, "account_balance", false));
            if (withBudget) accountIndexOSAB.IndexedColumns.Add(new IndexedColumn(accountIndexOSAB, "budget", false));
            accountTable.Indexes.Add(accountIndexOSAB);
            accountIndexOSAB.Create();
            accountIndexOSAB.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            accountIndexOSAB.Alter();
            var accountIndexOSOB = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_account_owner_sex_owner_birthdate");
            accountIndexOSOB.IndexedColumns.Add(new IndexedColumn(accountIndexOSOB, "owner_sex", false));
            accountIndexOSOB.IndexedColumns.Add(new IndexedColumn(accountIndexOSOB, "owner_birthdate", false));
            if (withBudget) accountIndexOSOB.IndexedColumns.Add(new IndexedColumn(accountIndexOSOB, "budget", false));
            accountTable.Indexes.Add(accountIndexOSOB);
            accountIndexOSOB.Create();
            accountIndexOSOB.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            accountIndexOSOB.Alter();
            var accountIndexOSLA = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_account_owner_sex_loan_amount");
            accountIndexOSLA.IndexedColumns.Add(new IndexedColumn(accountIndexOSLA, "owner_sex", false));
            accountIndexOSLA.IndexedColumns.Add(new IndexedColumn(accountIndexOSLA, "loan_amount", false));
            if (withBudget) accountIndexOSLA.IndexedColumns.Add(new IndexedColumn(accountIndexOSLA, "budget", false));
            accountTable.Indexes.Add(accountIndexOSLA);
            accountIndexOSLA.Create();
            accountIndexOSLA.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            accountIndexOSLA.Alter();
            if (withBudget)
            {
                var accountIndexB = TableDisk.CreateDefaultIndex(accountTable, "index_nc_financial-disk_account_budget");
                accountIndexB.IndexedColumns.Add(new IndexedColumn(accountIndexB, "budget", false));
                accountTable.Indexes.Add(accountIndexB);
                accountIndexB.Create();
                accountIndexB.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
                accountIndexB.Alter();
            }
            accountTable.Alter();
            var orderTable = database.Tables["order"];
            var orderIndexAI = TableDisk.CreateDefaultIndex(orderTable, "index_nc_financial-disk_order_account_id");
            orderIndexAI.IndexedColumns.Add(new IndexedColumn(orderIndexAI, "account_id", false));
            orderTable.Indexes.Add(orderIndexAI);
            orderIndexAI.Create();
            orderIndexAI.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            orderIndexAI.Alter();
            orderTable.Alter();
            var transactionTable = database.Tables["transaction"];
            var transactionIndexAI = TableDisk.CreateDefaultIndex(transactionTable, "index_nc_financial-disk_transaction_account_id");
            transactionIndexAI.IndexedColumns.Add(new IndexedColumn(transactionIndexAI, "account_id", false));
            transactionIndexAI.IndexedColumns.Add(new IndexedColumn(transactionIndexAI, "amount") { IsIncluded = true });
            transactionTable.Indexes.Add(transactionIndexAI);
            transactionIndexAI.Create();
            transactionIndexAI.PhysicalPartitions[0].DataCompression = DataCompressionType.Page;
            transactionIndexAI.Alter();
            transactionTable.Alter();
            Console.WriteLine("FinancialDisk: Created indexes on tables account, order, and transaction.");
        }
    }
}
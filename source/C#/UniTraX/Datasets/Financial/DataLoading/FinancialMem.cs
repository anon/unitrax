﻿namespace UniTraX.Datasets.Financial.DataLoading
{
    using Microsoft.SqlServer.Management.Smo;
    using System;
    using UniTraX.Infrastructure.DataLoading;

    public class FinancialMem
    {
        public static void CreateTable(Database database, bool withBudget)
        {
#if DEBUG
            if (database == null) throw new ArgumentNullException(nameof(database));
#endif
            Table districtTable = TableMem.CreateTableInit(database, withBudget, "financial-mem", "district", FinancialDisk.DistrictColumnNames, "id", "budget", null);
            TableMem.CreateTableFinish(districtTable, "district");
            Table accountTable = TableMem.CreateTableInit(database, withBudget, "financial-mem", "account", FinancialDisk.AccountColumnNames, "account_id", "budget", null);
            var accountIndexADI = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_account_district_id");
            accountIndexADI.IndexedColumns.Add(new IndexedColumn(accountIndexADI, "account_district_id", false));
            if (withBudget) accountIndexADI.IndexedColumns.Add(new IndexedColumn(accountIndexADI, "budget", false));
            accountTable.Indexes.Add(accountIndexADI);
            var accountIndexAD = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_account_date");
            accountIndexAD.IndexedColumns.Add(new IndexedColumn(accountIndexAD, "account_date", false));
            if (withBudget) accountIndexAD.IndexedColumns.Add(new IndexedColumn(accountIndexAD, "budget", false));
            accountTable.Indexes.Add(accountIndexAD);
            var accountIndexAB = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_account_balance");
            accountIndexAB.IndexedColumns.Add(new IndexedColumn(accountIndexAB, "account_balance", false));
            if (withBudget) accountIndexAB.IndexedColumns.Add(new IndexedColumn(accountIndexAB, "budget", false));
            accountTable.Indexes.Add(accountIndexAB);
            var accountIndexOB = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_owner_birthdate");
            accountIndexOB.IndexedColumns.Add(new IndexedColumn(accountIndexOB, "owner_birthdate", false));
            if (withBudget) accountIndexOB.IndexedColumns.Add(new IndexedColumn(accountIndexOB, "budget", false));
            accountTable.Indexes.Add(accountIndexOB);
            var accountIndexOS = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_owner_sex");
            accountIndexOS.IndexedColumns.Add(new IndexedColumn(accountIndexOS, "owner_sex", false));
            if (withBudget) accountIndexOS.IndexedColumns.Add(new IndexedColumn(accountIndexOS, "budget", false));
            accountTable.Indexes.Add(accountIndexOS);
            var accountIndexLA = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_loan_amount");
            accountIndexLA.IndexedColumns.Add(new IndexedColumn(accountIndexLA, "loan_amount", false));
            if (withBudget) accountIndexLA.IndexedColumns.Add(new IndexedColumn(accountIndexLA, "budget", false));
            accountTable.Indexes.Add(accountIndexLA);
            if (withBudget)
            {
                var accountIndexB = TableMem.CreateDefaultIndex(accountTable, "index_nc_financial-mem_account_budget");
                accountIndexB.IndexedColumns.Add(new IndexedColumn(accountIndexB, "budget", false));
                accountTable.Indexes.Add(accountIndexB);
            }
            TableMem.CreateTableFinish(accountTable, "account");
            ForeignKey accountDistrictIdFK = new ForeignKey(accountTable, "FK_account_account_district_id");
            ForeignKeyColumn accountDistrictIdFKC = new ForeignKeyColumn(accountDistrictIdFK, "account_district_id", "id");
            accountDistrictIdFK.Columns.Add(accountDistrictIdFKC);
            accountDistrictIdFK.ReferencedTable = "district";
            accountDistrictIdFK.Create();
            ForeignKey ownerDistrictIdFK = new ForeignKey(accountTable, "FK_account_owner_district_id");
            ForeignKeyColumn ownerDistrictIdFKC = new ForeignKeyColumn(ownerDistrictIdFK, "owner_district_id", "id");
            ownerDistrictIdFK.Columns.Add(ownerDistrictIdFKC);
            ownerDistrictIdFK.ReferencedTable = "district";
            ownerDistrictIdFK.Create();
            ForeignKey userDistrictIdFK = new ForeignKey(accountTable, "FK_account_user_district_id");
            ForeignKeyColumn userDistrictIdFKC = new ForeignKeyColumn(userDistrictIdFK, "user_district_id", "id");
            userDistrictIdFK.Columns.Add(userDistrictIdFKC);
            userDistrictIdFK.ReferencedTable = "district";
            userDistrictIdFK.Create();
            accountTable.Alter();
            Table orderTable = TableMem.CreateTableInit(database, withBudget, "financial-mem", "order", FinancialDisk.OrderColumnNames, "id", "budget", null);
            var orderIndexAI = TableMem.CreateDefaultIndex(orderTable, "index_nc_financial-mem_order_account_id");
            orderIndexAI.IndexedColumns.Add(new IndexedColumn(orderIndexAI, "account_id", false));
            orderTable.Indexes.Add(orderIndexAI);
            TableMem.CreateTableFinish(orderTable, "order");
            ForeignKey orderAccountFK = new ForeignKey(orderTable, "FK_order_account_id");
            ForeignKeyColumn orderAccountFKC = new ForeignKeyColumn(orderAccountFK, "account_id", "account_id");
            orderAccountFK.Columns.Add(orderAccountFKC);
            orderAccountFK.ReferencedTable = "account";
            orderAccountFK.Create();
            orderTable.Alter();
            Table transactionTable = TableMem.CreateTableInit(database, withBudget, "financial-mem", "transaction", FinancialDisk.TransactionColumnNames, "id", "budget", null);
            var transactionIndexAI = TableMem.CreateDefaultIndex(transactionTable, "index_nc_financial-mem_transaction_account_id");
            transactionIndexAI.IndexedColumns.Add(new IndexedColumn(transactionIndexAI, "account_id", false));
            transactionTable.Indexes.Add(transactionIndexAI);
            TableMem.CreateTableFinish(transactionTable, "transaction");
            ForeignKey transactionAccountFK = new ForeignKey(transactionTable, "FK_transaction_account_id");
            ForeignKeyColumn transactionAccountFKC = new ForeignKeyColumn(transactionAccountFK, "account_id", "account_id");
            transactionAccountFK.Columns.Add(transactionAccountFKC);
            transactionAccountFK.ReferencedTable = "account";
            transactionAccountFK.Create();
            transactionTable.Alter();
        }
    }
}
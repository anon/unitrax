﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System.Linq;

    public class PINQDummy<R> : IPINQCondition<R>
    {
        public IQueryable<R>[] ApplyTo(IQueryable<R> queryable)
        {
            return new IQueryable<R>[] { queryable };
        }

        public PINQueryable<R>[] ApplyTo(PINQueryable<R> queryable)
        {
            return new PINQueryable<R>[] { queryable };
        }

        public IPINQCondition<R> Clone()
        {
            return new PINQDummy<R>();
        }
    }
}
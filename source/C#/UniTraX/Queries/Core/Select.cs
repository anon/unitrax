﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Bookkeeper;

    public class Select<R, ST> : IQuery<R>
    {
        private readonly Expression<Func<R, ST>> SelectorExpression;
        private readonly IQuery<ST> Afterwards;

        public Select(Expression<Func<R, ST>> selectorExpression, IQuery<ST> afterwards)
        {
#if DEBUG
            if (selectorExpression == null) throw new ArgumentNullException(nameof(selectorExpression));
            if (afterwards == null) throw new ArgumentNullException(nameof(afterwards));
#endif
            SelectorExpression = selectorExpression;
            Afterwards = afterwards;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
            return Afterwards.ApplyTo(queryable.Select(SelectorExpression));
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
            return Afterwards.ApplyTo(queryable.Select(SelectorExpression));
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
            throw new InvalidOperationException("Bookkeeper does not support selects");
        }

        public IQuery<R> Clone()
        {
            return new Select<R, ST>(SelectorExpression, Afterwards);
        }
    }
}
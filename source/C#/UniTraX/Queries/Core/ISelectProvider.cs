﻿namespace UniTraX.Queries.Core
{
    using System;
    using System.Linq.Expressions;

    public interface ISelectProvider<R, ST>
    {
        Expression<Func<R, ST>> GetSelectorExpression(int[] selectIndexes);
    }
}
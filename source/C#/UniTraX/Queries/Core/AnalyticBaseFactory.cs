﻿namespace UniTraX.Queries.Core
{
    using System;

    public abstract class AnalyticBaseFactory<R, RB>
    {
        public long ID { get; private set; }
        public string QueryMessage { get; private set; }
        public bool UseEvenly { get; private set; }
        public bool BudgetOverRuntime { get; private set; }
        public int CleanThreshold { get; private set; }
        public long[] Epsilons { get; private set; }

        public AnalyticBaseFactory(long id, string queryMessage, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long[] epsilons)
        {
#if DEBUG
            if (epsilons == null) throw new ArgumentNullException(nameof(epsilons));
            foreach (long epsilon in epsilons) if (epsilon < 0L) throw new ArgumentException("epsilon below zero");
#endif
            ID = id;
            QueryMessage = queryMessage;
            UseEvenly = useEvenly;
            BudgetOverRuntime = budgetOverRuntime;
            CleanThreshold = cleanThreshold;
            Epsilons = epsilons;
        }

        public IQuery<R> GetAnalyticBaseDirect()
        {
            return new AnalyticBase<R>(ID, QueryMessage, GenerateQueryDirect());
        }

        public IQuery<R> GetAnalyticBasePINQ()
        {
            return new AnalyticBase<R>(ID, QueryMessage, GenerateQueryPINQ());
        }

        public IQuery<RB> GetAnalyticBaseBK()
        {
            return new AnalyticBase<RB>(ID, QueryMessage, GenerateQueryBK());
        }

        public abstract IQuery<R> GenerateQueryDirect();
        public abstract IQuery<R> GenerateQueryPINQ();
        public abstract IQuery<RB> GenerateQueryBK();
    }
}
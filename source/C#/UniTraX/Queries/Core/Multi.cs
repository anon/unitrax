﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
	using UniTraX.Core.Bookkeeper;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class Multi<R> : IQuery<R>
    {
        private readonly List<IQuery<R>> Queries;

        public Multi(List<IQuery<R>> queries)
        {
#if DEBUG
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (!queries.Any()) throw new ArgumentException("no queries provided");
#endif
            Queries = queries;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var results = new double[Queries.Count()][];
            for (int i = 0; i < Queries.Count(); i++) results[i] = Queries[i].ApplyTo(queryable);
            return results.SelectMany(item => item).ToArray();
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var results = new double[Queries.Count()][];
            for (int i = 0; i < Queries.Count(); i++) results[i] = Queries[i].ApplyTo(queryable);
            return results.SelectMany(item => item).ToArray();
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var results = new double[Queries.Count()][];
            for (int i = 0; i < Queries.Count(); i++) results[i] = Queries[i].ApplyTo(queryable);
            return results.SelectMany(item => item).ToArray();
        }

        public IQuery<R> Clone()
        {
            return new Multi<R>(Queries);
        }
    }
}
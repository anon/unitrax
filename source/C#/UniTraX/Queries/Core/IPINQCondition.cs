﻿namespace UniTraX.Queries.Core
{
    using PINQ;
    using System.Linq;

    public interface IPINQCondition<R>
    {
        IQueryable<R>[] ApplyTo(IQueryable<R> queryable);
        PINQueryable<R>[] ApplyTo(PINQueryable<R> queryable);

        IPINQCondition<R> Clone();
    }
}
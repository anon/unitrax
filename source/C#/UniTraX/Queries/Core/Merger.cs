﻿namespace UniTraX.Queries.Core
{
    using PINQ;
    using System;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;

    public class Merger<R> : IQuery<R>
    {
        private IQueryable<R> Queryable;
        private PINQueryable<R> PINQueryable;
        private BKQueryable<R> BKQueryable;

        private Merger<R> DerivedFrom;

        public Merger<R> Derive()
        {
            var ret = new Merger<R>
            {
                DerivedFrom = this
            };
            return ret;
        }

        public IQueryable<R> GetQueryable()
        {
#if DEBUG
            if (DerivedFrom != null && Queryable != null) throw new ArgumentException("Cannot be both set.");
#endif
            if (DerivedFrom != null) return DerivedFrom.GetQueryable();
            return Queryable;
        }

        public PINQueryable<R> GetPINQueryable()
        {
#if DEBUG
            if (DerivedFrom != null && PINQueryable != null) throw new ArgumentException("Cannot be both set.");
#endif
            if (DerivedFrom != null) return DerivedFrom.GetPINQueryable();
            return PINQueryable;
        }

        public BKQueryable<R> GetBKQueryable()
        {
#if DEBUG
            if (DerivedFrom != null && BKQueryable != null) throw new ArgumentException("Cannot be both set.");
#endif
            if (DerivedFrom != null) return DerivedFrom.GetBKQueryable();
            return BKQueryable;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Queryable = queryable;
            return null;
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            PINQueryable = queryable;
            return null;
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            BKQueryable = queryable;
            return null;
        }

        public IQuery<R> Clone()
        {
            var ret = new Merger<R>
            {
                Queryable = Queryable,
                PINQueryable = PINQueryable,
                BKQueryable = BKQueryable,
                DerivedFrom = DerivedFrom
            };
            return ret;
        }

        public Merger<R> Merge(Merger<R> other)
        {
            var ret = new Merger<R>();
            if (GetQueryable() == null) ret.Queryable = other.GetQueryable();
            else if (other.GetQueryable() != null) ret.Queryable = GetQueryable().Union(other.GetQueryable());
            else ret.Queryable = GetQueryable();
            if (GetPINQueryable() == null) ret.PINQueryable = other.GetPINQueryable();
            else if (other.GetPINQueryable() != null) ret.PINQueryable = GetPINQueryable().Union(other.GetPINQueryable());
            else ret.PINQueryable = GetPINQueryable();
            if (GetBKQueryable() == null) ret.BKQueryable = other.GetBKQueryable();
            else if (other.GetBKQueryable() != null) ret.BKQueryable = GetBKQueryable().Union(other.GetBKQueryable());
            else ret.BKQueryable = GetBKQueryable();
            return ret;
        }
    }
}
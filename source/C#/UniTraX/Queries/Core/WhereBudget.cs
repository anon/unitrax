﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;

    public class WhereBudget<R> : IQuery<R>
    {
        private readonly long BudgetAvailable;
        private readonly Field<R> FieldBudget;
        private readonly IQuery<R> Afterwards;

        public WhereBudget(long budgetAvailable, Field<R> fieldBudget, IQuery<R> afterwards)
        {
#if DEBUG
            if (budgetAvailable < 0) throw new ArgumentException("budgetAvailable is not valid");
            if (afterwards == null) throw new ArgumentNullException(nameof(afterwards));
#endif
            BudgetAvailable = budgetAvailable;
            FieldBudget = fieldBudget;
            Afterwards = afterwards;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
            return Afterwards.ApplyTo(queryable);
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
            return Afterwards.ApplyTo(queryable);
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (FieldBudget == null) throw new ArgumentNullException(nameof(FieldBudget));
#endif
            var maxUsage = queryable.MaxUsage();
            var budgetAvailable = BudgetAvailable;
            var budgetLow =  maxUsage + budgetAvailable;
            var range = new WhereRange<R>(FieldBudget, budgetLow, FieldBudget.FieldType.Bounds.High, Afterwards);
            return range.ApplyTo(queryable);
        }

        public IQuery<R> Clone()
        {
            return new WhereBudget<R>(BudgetAvailable, FieldBudget, Afterwards);
        }
    }
}
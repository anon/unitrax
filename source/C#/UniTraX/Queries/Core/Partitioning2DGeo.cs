﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;
    using UniTraX.Core.Utils;

    public class Partition2DGeo<R> : IQuery<R>
    {
        public static (int[], Expression<Func<R, int>>) KeysAndKeyExpression(Field<R> fieldLat, long startLat, long intervalLat, int numberIntervalsLat,
            Field<R> fieldLong, long startLong, long intervalLong, int numberIntervalsLong)
        {
#if DEBUG
            if (fieldLat == null) throw new ArgumentNullException(nameof(fieldLat));
            if (intervalLat < 0) throw new ArgumentException("intervalLat must be positive");
            if (numberIntervalsLat < 1) throw new ArgumentException("numberIntervalsLat must be above zero");
            if (fieldLong == null) throw new ArgumentNullException(nameof(fieldLong));
            if (intervalLong < 0) throw new ArgumentException("intervalLong must be positive");
            if (numberIntervalsLong < 1) throw new ArgumentException("numberIntervalsLong must be above zero");
#endif
            var maxNumber = numberIntervalsLat;
            if (numberIntervalsLong > maxNumber) maxNumber = numberIntervalsLong;
            var oom = Math.Pow(10.0, Math.Ceiling(Math.Log10(maxNumber) + Double.Epsilon));
            int offset = Convert.ToInt32(oom);

            var keys = new List<int> { -1 };
            for (int i = 0; i < numberIntervalsLat; i++) for (int j = 0; j < numberIntervalsLong; j++) keys.Add((offset + i) * offset * 10 + 2 * offset + j);
            var currentParameter = Expression.Parameter(typeof(R));
            var accessVisitorA = new ReplaceExpressionVisitor(fieldLat.Access.Parameters[0], currentParameter);
            var accessVisitorB = new ReplaceExpressionVisitor(fieldLong.Access.Parameters[0], currentParameter);
            var accessBodyA = accessVisitorA.Visit(fieldLat.Access.Body);
            var accessBodyB = accessVisitorB.Visit(fieldLong.Access.Body);
            var startDA = fieldLat.FieldType.Convert(startLat, false);
            var startDB = fieldLong.FieldType.Convert(startLong, false);
            var endDA = fieldLat.FieldType.Convert(startLat + (intervalLat + 1L) * numberIntervalsLat - 1L, true);
            var endDB = fieldLong.FieldType.Convert(startLong + (intervalLong + 1L) * numberIntervalsLong - 1L, true);
            var singleStepA = fieldLat.FieldType.Convert(1L, true);
            var singleStepB = fieldLong.FieldType.Convert(1L, true);
            var subtractA = startDA - 0.5 * singleStepA;
            var subtractB = startDB - 0.5 * singleStepB;
            double intervalDA = fieldLat.FieldType.ConvertUnchecked(intervalLat + 1L, true);
            double intervalDB = fieldLat.FieldType.ConvertUnchecked(intervalLong + 1L, true);
            Expression currentExpression = Expression.Add(
                Expression.Multiply(Expression.Constant(offset * 10), Expression.Add(Expression.Constant(offset), Expression.Convert(
                Expression.Divide(Expression.Subtract(accessBodyA, Expression.Constant(subtractA)), Expression.Constant(intervalDA)), typeof(int)))),
                Expression.Add(Expression.Constant(2 * offset), Expression.Convert(
                Expression.Divide(Expression.Subtract(accessBodyB, Expression.Constant(subtractB)), Expression.Constant(intervalDB)), typeof(int))));
            currentExpression = Expression.Condition(Expression.GreaterThan(accessBodyA, Expression.Constant(endDA)), Expression.Constant(-1), currentExpression);
            currentExpression = Expression.Condition(Expression.GreaterThan(accessBodyB, Expression.Constant(endDB)), Expression.Constant(-1), currentExpression);
            currentExpression = Expression.Condition(Expression.LessThan(accessBodyA, Expression.Constant(startDA)), Expression.Constant(-1), currentExpression);
            currentExpression = Expression.Condition(Expression.LessThan(accessBodyB, Expression.Constant(startDB)), Expression.Constant(-1), currentExpression);
            return (keys.ToArray(), Expression.Lambda<Func<R, int>>(currentExpression, currentParameter));
        }

        private readonly Field<R> FieldLat;
        private readonly long StartLat;
        private readonly long IntervalLat;
        private readonly int NumberIntervalsLat;
        private readonly Field<R> FieldLong;
        private readonly long StartLong;
        private readonly long IntervalLong;
        private readonly int NumberIntervalsLong;
        private readonly IQuery<R> ForEach;
        private readonly IQuery<R> ForOthers;
        private readonly Partitioning<R> Partitioning;
        private readonly bool BudgetOverRuntime;

        public Partition2DGeo(Field<R> fieldLat, long startLat, long intervalLat, int numberIntervalsLat,
            Field<R> fieldLong, long startLong, long intervalLong, int numberIntervalsLong,
            IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime)
        {
#if DEBUG
            if (fieldLat == null) throw new ArgumentNullException(nameof(fieldLat));
            if (intervalLat < 0) throw new ArgumentException("intervalLat below zero");
            if (numberIntervalsLat < 1) throw new ArgumentException("numberIntervalsLat below one");
            if (fieldLong == null) throw new ArgumentNullException(nameof(fieldLong));
            if (intervalLong < 0) throw new ArgumentException("intervalLong below zero");
            if (numberIntervalsLong < 1) throw new ArgumentException("numberIntervalsLong below one");
            if (forEach == null) throw new ArgumentNullException(nameof(forEach));
#endif
            FieldLat = fieldLat;
            StartLat = startLat;
            IntervalLat = intervalLat;
            NumberIntervalsLat = numberIntervalsLat;
            FieldLong = fieldLong;
            StartLong = startLong;
            IntervalLong = intervalLong;
            NumberIntervalsLong = numberIntervalsLong;
            ForEach = forEach;
            ForOthers = forOthers;
            BudgetOverRuntime = budgetOverRuntime;

            var maxNumber = NumberIntervalsLat;
            if (NumberIntervalsLong > maxNumber) maxNumber = NumberIntervalsLong;
            var oom = Math.Pow(10.0, Math.Ceiling(Math.Log10(maxNumber) + Double.Epsilon));
            int offset = Convert.ToInt32(oom);

            long startLatAmount = StartLat;
            long intervalLatAmount = IntervalLat + 1L;
            long startLongAmount = StartLong;
            long intervalLongAmount = IntervalLong + 1L;

            var partsLat = new List<Part>(numberIntervalsLat);
            for (int i = 0; i < numberIntervalsLat; i++) partsLat.Add(new Part(startLatAmount + intervalLatAmount * i, startLatAmount + intervalLatAmount * (i + 1) - 1L, (offset + i) * offset * 10));
            var partsLong = new List<Part>(numberIntervalsLong);
            for (int i = 0; i < numberIntervalsLong; i++) partsLong.Add(new Part(startLongAmount + intervalLongAmount * i, startLongAmount + intervalLongAmount * (i + 1) - 1L, 2 * offset + i));

            var dimensions = new Queue<Dimension<R>>();
            dimensions.Enqueue(new Dimension<R>(FieldLong, partsLong));
            dimensions.Enqueue(new Dimension<R>(FieldLat, partsLat));
            var keysAndKeyExpression = KeysAndKeyExpression(fieldLat, startLat, intervalLat, numberIntervalsLat, fieldLong, startLong, intervalLong, numberIntervalsLong);
            Partitioning = new Partitioning<R>(dimensions, ForEach, ForOthers, BudgetOverRuntime, keysAndKeyExpression.Item1, keysAndKeyExpression.Item2);
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (FieldLat == null) throw new ArgumentNullException(nameof(FieldLat));
            if (FieldLong == null) throw new ArgumentNullException(nameof(FieldLong));
            return Partitioning.ApplyTo(queryable);
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (FieldLat == null) throw new ArgumentNullException(nameof(FieldLat));
            if (FieldLong == null) throw new ArgumentNullException(nameof(FieldLong));
            return Partitioning.ApplyTo(queryable);
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (FieldLat == null) throw new ArgumentNullException(nameof(FieldLat));
            if (FieldLong == null) throw new ArgumentNullException(nameof(FieldLong));
            return Partitioning.ApplyTo(queryable);
        }

        public IQuery<R> Clone()
        {
            return new Partition2DGeo<R>(FieldLat, StartLat, IntervalLat, NumberIntervalsLat, FieldLong, StartLong, IntervalLong, NumberIntervalsLong, ForEach, ForOthers, BudgetOverRuntime);
        }
    }
}
﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class Conditional<R> : IQuery<R>
    {
        private readonly Func<double[], bool> EvalFunction;
        private readonly IQuery<R> QueryCondition;
        private readonly IQuery<R> QueryTrue;
        private readonly IQuery<R> QueryFalse;

        public Conditional(Func<double[], bool> evalFunction, IQuery<R> queryCondition, IQuery<R> queryTrue, IQuery<R> queryFalse)
        {
#if DEBUG
            if (queryCondition == null) throw new ArgumentNullException(nameof(queryCondition));
            if (queryTrue == null) throw new ArgumentNullException(nameof(queryTrue));
#endif
            EvalFunction = evalFunction;
            QueryCondition = queryCondition;
            QueryTrue = queryTrue;
            QueryFalse = queryFalse;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            double[][] results = new double[3][];
            results[0] = QueryCondition.ApplyTo(queryable);
            if (EvalFunction(results[0]))
            {
                results[1] = new double[1] { 1.0 };
                results[2] = QueryTrue.ApplyTo(queryable);
            }
            else if (QueryFalse != null)
            {
                results[1] = new double[1] { 0.0 };
                results[2] = QueryFalse.ApplyTo(queryable);
            }
            else
            {
                results[1] = new double[1] { 0.0 };
                results[2] = new double[0];
            }
            return results.SelectMany(item => item).ToArray();
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            double[][] results = new double[3][];
            results[0] = QueryCondition.ApplyTo(queryable);
            if (EvalFunction(results[0]))
            {
                results[1] = new double[1] { 1.0 };
                results[2] = QueryTrue.ApplyTo(queryable);
            }
            else if (QueryFalse != null)
            {
                results[1] = new double[1] { 0.0 };
                results[2] = QueryFalse.ApplyTo(queryable);
            }
            else
            {
                results[1] = new double[1] { 0.0 };
                results[2] = new double[0];
            }
            return results.SelectMany(item => item).ToArray();
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            double[][] results = new double[3][];
            results[0] = QueryCondition.ApplyTo(queryable);
            if (EvalFunction(results[0]))
            {
                results[1] = new double[1] { 1.0 };
                results[2] = QueryTrue.ApplyTo(queryable);
            }
            else if (QueryFalse != null)
            {
                results[1] = new double[1] { 0.0 };
                results[2] = QueryFalse.ApplyTo(queryable);
            }
            else
            {
                results[1] = new double[1] { 0.0 };
                results[2] = new double[0];
            }
            return results.SelectMany(item => item).ToArray();
        }

        public IQuery<R> Clone()
        {
            return new Conditional<R>(EvalFunction, QueryCondition, QueryTrue, QueryFalse);
        }
    }
}
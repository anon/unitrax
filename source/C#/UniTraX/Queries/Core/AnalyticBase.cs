﻿namespace UniTraX.Queries.Core
{
	using PINQ;
#if DEBUG
    using System;
#endif
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class AnalyticBase<R> : IQuery<R>
    {
        public long Id { get; private set; }
        public string Message { get; private set; }
        public IQuery<R> Query { get; private set; }

        public AnalyticBase(long id, string message, IQuery<R> query)
        {
#if DEBUG
            if (id < 0) throw new ArgumentException("id is not valid");
            if (message == null) throw new ArgumentNullException(nameof(message));
            if (query == null) throw new ArgumentNullException(nameof(query));
#endif
            Id = id;
            Message = message;
            Query = query;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
#if RECORD
            var timer = Recorder.StartTimer();
#endif
            var values = Query.ApplyTo(queryable);
#if RECORD
            var time = Recorder.StopTimer(timer);
            Log(queryable, time);
#endif
            return values;
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
#if RECORD
            var timer = Recorder.StartTimer();
#endif
            var values = Query.ApplyTo(queryable);
#if RECORD
            var time = Recorder.StopTimer(timer);
            Log(queryable, time);
#endif
            return values;
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
#if RECORD
            var timer = Recorder.StartTimer();
#endif
            var values = Query.ApplyTo(queryable);
#if RECORD
            var time = Recorder.StopTimer(timer);
            Log(queryable, time);
#endif
            return values;
        }

        public void Log(IQueryable<R> queryable, double time)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Recorder.Add(new EntryAnalytic(Id, time, Message));
        }

        public void Log(PINQueryable<R> queryable, double time)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Recorder.Add(new EntryAnalytic(Id, time, Message));
        }

        public void Log(BKQueryable<R> queryable, double time)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Recorder.Add(new EntryAnalytic(Id, time, Message));
        }

        public IQuery<R> Clone()
        {
            return new AnalyticBase<R>(Id, Message, Query);
        }
    }
}
﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;

    public interface IQuery<R>
    {
        double[] ApplyTo(IQueryable<R> queryable);
        double[] ApplyTo(PINQueryable<R> queryable);
        double[] ApplyTo(BKQueryable<R> queryable);
        IQuery<R> Clone();
    }
}
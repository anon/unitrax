﻿namespace UniTraX.Queries.Core
{
    using System;
    using System.Collections.Generic;

    public class AnalyticBaseProvider<R, RB>
    {
        private readonly List<AnalyticBaseFactory<R, RB>> Factories;

        public AnalyticBaseProvider(List<AnalyticBaseFactory<R, RB>> factories)
        {
#if DEBUG
            if (factories == null) throw new ArgumentNullException(nameof(factories));
            if (factories.Count < 1) throw new ArgumentException("need at least one factory");
#endif
            Factories = factories;
        }

        public List<IQuery<R>> GetQueriesDirect()
        {
            var queries = new List<IQuery<R>>();
            foreach (var factory in Factories) queries.Add(factory.GetAnalyticBaseDirect());
            return queries;
        }

        public List<IQuery<R>> GetQueriesPINQ()
        {
            var queries = new List<IQuery<R>>();
            foreach (var factory in Factories) queries.Add(factory.GetAnalyticBasePINQ());
            return queries;
        }

        public List<IQuery<RB>> GetQueriesBK()
        {
            var queries = new List<IQuery<RB>>();
            foreach (var factory in Factories) queries.Add(factory.GetAnalyticBaseBK());
            return queries;
        }
    }
}
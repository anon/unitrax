﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;
    using UniTraX.Core.Utils;

    public class Part
    {
        public long Low { get; private set; }
        public long High { get; private set; }
        public int MapTo { get; private set; }

        public Part(long low, long high, int mapTo)
        {
            Low = low;
            High = high;
            MapTo = mapTo;
        }
    }

    public class Dimension<R>
    {
        public Field<R> Field { get; private set; }
        public List<Part> Parts { get; private set; }

        public Dimension(Field<R> field, List<Part> parts)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (parts == null) throw new ArgumentNullException(nameof(parts));
            if (!parts.Any()) throw new ArgumentException("no parts provided");
#endif
            Field = field;
            Parts = parts;
        }

        public Dimension<R> Clone()
        {
            return new Dimension<R>(Field, Parts);
        }
    }

    public class Partitioning<R> : IQuery<R>
    {
        public static List<Tuple<WhereRange<R>, Merger<R>, int>> GeneratePartitionsStep(Queue<Dimension<R>> dimensions)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            Dimension<R> dimension = dimensions.Dequeue();
            List<Tuple<WhereRange<R>, Merger<R>, int>> partitions = null;
            if (dimensions.Any()) partitions = GeneratePartitions(dimensions);
            int resultCapacity = dimension.Parts.Count();
            if (partitions != null) resultCapacity *= partitions.Count();
            var result = new List<Tuple<WhereRange<R>, Merger<R>, int>>(resultCapacity);
            foreach (Part p in dimension.Parts)
            {
                if (partitions != null) foreach (Tuple<WhereRange<R>, Merger<R>, int> t in partitions) result.Add(new Tuple<WhereRange<R>, Merger<R>, int>(
                    new WhereRange<R>(dimension.Field, p.Low, p.High, t.Item1), t.Item2.Derive(), t.Item3 + p.MapTo));
                else
                {
                    var merger = new Merger<R>();
                    result.Add(new Tuple<WhereRange<R>, Merger<R>, int>(
                        new WhereRange<R>(dimension.Field, p.Low, p.High, merger), merger, p.MapTo));
                }
            }
            return result;
        }

        public static List<Tuple<WhereRange<R>, Merger<R>, int>> GeneratePartitions(Queue<Dimension<R>> dimensions)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            var dimensionsTmp = new Queue<Dimension<R>>();
            foreach (Dimension<R> d in dimensions) dimensionsTmp.Enqueue(d.Clone());
            return GeneratePartitionsStep(dimensionsTmp);
        }

        public static List<Tuple<WhereRange<R>, Merger<R>, int>> GenerateInversePartitionsStep(Queue<Dimension<R>> dimensions, int ignoreIndex)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            Dimension<R> dimension = dimensions.Dequeue();
            List<Tuple<WhereRange<R>, Merger<R>, int>> inversePartitions = null;
            if (dimensions.Any()) inversePartitions = GenerateInversePartitionsStep(dimensions, ignoreIndex);
            if (inversePartitions == null) inversePartitions = new List<Tuple<WhereRange<R>, Merger<R>, int>>();
            if (dimension.Field.Index == ignoreIndex) return inversePartitions;
            List<Part> inverseParts = InverseParts(dimension.Field, dimension.Parts);
            foreach (Part p in inverseParts)
            {
                var merger = new Merger<R>();
                inversePartitions.Add(new Tuple<WhereRange<R>, Merger<R>, int>(new WhereRange<R>(dimension.Field, p.Low, p.High, merger), merger, p.MapTo));
            }
            return inversePartitions;
        }

        public static List<Tuple<WhereRange<R>, Merger<R>, int>> GenerateInversePartitions(Queue<Dimension<R>> dimensions, int ignoreIndex)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            var dimensionsTmp = new Queue<Dimension<R>>();
            foreach (Dimension<R> d in dimensions) dimensionsTmp.Enqueue(d.Clone());
            return GenerateInversePartitionsStep(dimensionsTmp, ignoreIndex);
        }

        public static SortedDictionary<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> MergePartitions(List<Tuple<WhereRange<R>, Merger<R>, int>> partitions)
        {
#if DEBUG
            if (partitions == null) throw new ArgumentNullException(nameof(partitions));
            if (!partitions.Any()) throw new ArgumentException("partitions is empty");
#endif
            var result = new SortedDictionary<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>>();
            foreach (Tuple<WhereRange<R>, Merger<R>, int> p in partitions)
            {
                if (result.TryGetValue(p.Item3, out Tuple<List<WhereRange<R>>, List<Merger<R>>> t))
                {
                    t.Item1.Add(p.Item1);
                    t.Item2.Add(p.Item2);
                }
                else result.Add(p.Item3, new Tuple<List<WhereRange<R>>, List<Merger<R>>>(new List<WhereRange<R>>() { p.Item1 }, new List<Merger<R>>() { p.Item2 }));
            }
            return result;
        }

        public static List<Part> InverseParts(Field<R> field, List<Part> parts)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (parts == null) throw new ArgumentNullException(nameof(parts));
#endif
            var inverseParts = new List<Part>();
            long nextOfPreviousHigh = field.FieldType.Bounds.Low;
            bool nophIsNaN = false;
            foreach (Part p in parts)
            {
                if (nextOfPreviousHigh < p.Low) inverseParts.Add(new Part(nextOfPreviousHigh, p.Low - 1L, -1));
                if (p.High < field.FieldType.Bounds.High) nextOfPreviousHigh = p.High + 1L;
                else
                {
                    nextOfPreviousHigh = 0L;
                    nophIsNaN = true;
                    break;
                }
            }
            if (!nophIsNaN) inverseParts.Add(new Part(nextOfPreviousHigh, field.FieldType.Bounds.High, -1));
            return inverseParts;
        }

        public static List<Part> BothParts(Field<R> field, List<Part> parts)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (parts == null) throw new ArgumentNullException(nameof(parts));
#endif
            var bothParts = new List<Part>();
            long nextOfPreviousHigh = field.FieldType.Bounds.Low;
            bool nophIsNaN = false;
            foreach (Part p in parts)
            {
                if (nextOfPreviousHigh < p.Low) bothParts.Add(new Part(nextOfPreviousHigh, p.Low - 1, -1));
                if (p.High < field.FieldType.Bounds.High) nextOfPreviousHigh = p.High + 1;
                else
                {
                    nextOfPreviousHigh = 0L;
                    nophIsNaN = true;
                }
                bothParts.Add(p);
                if (nophIsNaN) break;
            }
            if (!nophIsNaN) bothParts.Add(new Part(nextOfPreviousHigh, field.FieldType.Bounds.High, -1));
            return bothParts;
        }

        public static Expression GenerateKeyExpression(List<Part> parts, Expression accessBody, Func<long, bool, double> convert)
        {
#if DEBUG
            if (parts == null) throw new ArgumentNullException(nameof(parts));
            if (!parts.Any()) throw new ArgumentException("parts is empty");
            if (accessBody == null) throw new ArgumentNullException(nameof(accessBody));
            if (convert == null) throw new ArgumentNullException(nameof(convert));
#endif
            if (parts.Count == 1) return Expression.Constant(parts.First().MapTo);
            var left = new List<Part>();
            var right = new List<Part>();
            var i = 0;
            for (; i < parts.Count / 2; i++) left.Add(parts[i]);
            for (; i < parts.Count; i++) right.Add(parts[i]);
            return Expression.Condition(Expression.LessThan(accessBody, Expression.Constant(convert(right.First().Low, false))), GenerateKeyExpression(left, accessBody, convert), GenerateKeyExpression(right, accessBody, convert));
        }

        public static Expression<Func<R, int>> GenerateKeyExpression(Queue<Dimension<R>> dimensions, ParameterExpression parameter)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            Dimension<R> dimension = dimensions.Dequeue();
            var currentParameter = parameter;
            if (currentParameter == null) currentParameter = Expression.Parameter(typeof(R));
            var accessVisitor = new ReplaceExpressionVisitor(dimension.Field.Access.Parameters[0], currentParameter);
            var accessBody = accessVisitor.Visit(dimension.Field.Access.Body);
            var dimensionExpression = GenerateKeyExpression(BothParts(dimension.Field, dimension.Parts), accessBody, dimension.Field.FieldType.Convert);
            Expression result = dimensionExpression;
            if (dimensions.Any())
            {
                Expression<Func<R, int>> lowerKeyExpression = GenerateKeyExpression(dimensions, currentParameter);
                result = Expression.Condition(Expression.LessThan(dimensionExpression, Expression.Constant(0)),
                    dimensionExpression, 
                    Expression.Condition(Expression.LessThan(lowerKeyExpression.Body, Expression.Constant(0)),
                        lowerKeyExpression.Body,
                        Expression.Add(dimensionExpression, lowerKeyExpression.Body)));
            }
            return Expression.Lambda<Func<R, int>>(result, currentParameter);
        }

        public static Expression<Func<R, int>> GenerateKeyExpression(Queue<Dimension<R>> dimensions)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            var dimensionsTmp = new Queue<Dimension<R>>();
            foreach (Dimension<R> d in dimensions) dimensionsTmp.Enqueue(d.Clone());
            return GenerateKeyExpression(dimensionsTmp, null);
        }

        public static int[] GenerateKeys(Queue<Dimension<R>> dimensions)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            var keySet = new SortedSet<int>() { -1 };
            var keyList = new List<int>();
            foreach (Dimension<R> d in dimensions)
            {
                var tmpList = keyList;
                if (tmpList.Count > 0)
                {
                    keyList = new List<int>(d.Parts.Count() * tmpList.Count());
                    foreach (Part p in d.Parts) foreach (int i in tmpList) keyList.Add(p.MapTo + i);
                }
                else
                {
                    keyList = new List<int>(d.Parts.Count());
                    foreach (Part p in d.Parts) keyList.Add(p.MapTo);
                }
            }
            foreach (int i in keyList) keySet.Add(i);
            return keySet.ToArray();
        }

        public static SortedDictionary<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> GenerateInversePartitionMap(Queue<Dimension<R>> dimensions)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            var dimensionsTmp = new Queue<Dimension<R>>();
            foreach (Dimension<R> d in dimensions) dimensionsTmp.Enqueue(d.Clone());
            var inversePartitions = GenerateInversePartitions(dimensionsTmp, -1);
            if (!inversePartitions.Any()) return null;
            return MergePartitions(inversePartitions);
        }

        public static SortedDictionary<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> GeneratePartitionMap(Queue<Dimension<R>> dimensions)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
#endif
            var dimensionsTmp = new Queue<Dimension<R>>();
            foreach (Dimension<R> d in dimensions) dimensionsTmp.Enqueue(d.Clone());
            return MergePartitions(GeneratePartitions(dimensionsTmp));
        }

        private readonly int[] Keys;
        private readonly Expression<Func<R, int>> KeyExpression;
        private readonly SortedDictionary<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> PartitionMap;
        private readonly SortedDictionary<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> InversePartitionMap;
        private readonly Queue<Dimension<R>> Dimensions;
        private readonly IQuery<R> ForEach;
        private readonly IQuery<R> ForOthers;
        private readonly bool BudgetOverRuntime;

        public Partitioning(Queue<Dimension<R>> dimensions, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime)
        {
#if DEBUG
            if (dimensions == null) throw new ArgumentNullException(nameof(dimensions));
            if (!dimensions.Any()) throw new ArgumentException("dimensions is empty");
            if (forEach == null) throw new ArgumentNullException(nameof(forEach));
#endif
            Keys = GenerateKeys(dimensions);
            KeyExpression = GenerateKeyExpression(dimensions);
            PartitionMap = GeneratePartitionMap(dimensions);
            if (ForOthers != null) InversePartitionMap = GenerateInversePartitionMap(dimensions);
            Dimensions = dimensions;
            ForEach = forEach;
            ForOthers = forOthers;
            BudgetOverRuntime = budgetOverRuntime;
        }

        public Partitioning(Queue<Dimension<R>> dimensions, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime, int[] keys, Expression<Func<R, int>> keyExpression) :
            this(dimensions, forEach, forOthers, budgetOverRuntime)
        {
#if DEBUG
            if (keys == null) throw new ArgumentNullException(nameof(keys));
            if (!keys.Any()) throw new ArgumentException();
            if (keyExpression == null) throw new ArgumentNullException(nameof(keyExpression));
#endif
            Keys = keys;
            KeyExpression = keyExpression;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var queryableMap = new SortedDictionary<int, IQueryable<R>>();
            foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in PartitionMap)
            {
                foreach (WhereRange<R> wr in kv.Value.Item1) wr.ApplyTo(queryable);
                var merger = new Merger<R>();
                foreach (Merger<R> m in kv.Value.Item2) merger = merger.Merge(m);
                var q = merger.GetQueryable();
                if (BudgetOverRuntime) q = q.ToArray().AsQueryable();
                queryableMap.Add(kv.Key, q);
            }

            var results = new double[PartitionMap.Count()][];
            var count = 0;
            foreach (KeyValuePair<int, IQueryable<R>> kv in queryableMap)
            {
                var r = ForEach.Clone().ApplyTo(kv.Value);
                results[count] = new double[r.Length + 1];
                results[count][0] = kv.Key;
                for (int i = 1; i < r.Length + 1; i++) results[count][i] = r[i - 1];
                count++;
            }
            if (ForOthers != null && InversePartitionMap != null) foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in InversePartitionMap)
                    ForOthers.Clone().ApplyTo(Enumerable.Empty<R>().AsQueryable());
            return results.SelectMany(item => item).ToArray();
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (BudgetOverRuntime)
            {
                var queryables = queryable.Partition(Keys, KeyExpression);
                var results = new List<double[]>();
                for (int i = 0; i < Keys.Count(); i++)
                {
                    if (Keys[i] < 0) continue;
                    var r = ForEach.Clone().ApplyTo(queryables[Keys[i]]);
                    var res = new double[r.Count() + 1];
                    res[0] = Keys[i];
                    for (int j = 0; j < r.Count(); j++) res[j + 1] = r[j];
                    results.Add(res);
                }
                if (ForOthers != null && InversePartitionMap != null) foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in InversePartitionMap)
                        ForOthers.Clone().ApplyTo(Enumerable.Empty<R>().AsQueryable());
                return results.SelectMany(item => item).ToArray();
            }
            else
            {
                var results = new double[PartitionMap.Count()][];
                var count = 0;
                foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in PartitionMap)
                {
                    foreach (WhereRange<R> wr in kv.Value.Item1) wr.ApplyTo(queryable);
                    var merger = new Merger<R>();
                    foreach (Merger<R> m in kv.Value.Item2) merger = merger.Merge(m);
                    var r = ForEach.Clone().ApplyTo(merger.GetPINQueryable());
                    results[count] = new double[r.Length + 1];
                    results[count][0] = kv.Key;
                    for (int i = 0; i < r.Length; i++) results[count][i + 1] = r[i];
                    count++;
                }
                if (ForOthers != null && InversePartitionMap != null) foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in InversePartitionMap)
                        ForOthers.Clone().ApplyTo(Enumerable.Empty<R>().AsQueryable());
                return results.SelectMany(item => item).ToArray();
            }
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var queryableMap = new SortedDictionary<int, BKQueryable<R>>();
            foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in PartitionMap)
            {
                foreach (WhereRange<R> wr in kv.Value.Item1) wr.ApplyTo(queryable);
                var merger = new Merger<R>();
                foreach (Merger<R> m in kv.Value.Item2) merger = merger.Merge(m);
                var q = merger.GetBKQueryable();
                if (BudgetOverRuntime) q = q.Materialize();
                queryableMap.Add(kv.Key, q);
            }

            var results = new double[PartitionMap.Count()][];
            var count = 0;
            foreach (KeyValuePair<int, BKQueryable<R>> kv in queryableMap)
            {
                var r = ForEach.Clone().ApplyTo(kv.Value);
                results[count] = new double[r.Length + 1];
                results[count][0] = kv.Key;
                for (int i = 1; i < r.Length + 1; i++) results[count][i] = r[i - 1];
                count++;
            }
            if (ForOthers != null && InversePartitionMap != null)
            {
                foreach (KeyValuePair<int, Tuple<List<WhereRange<R>>, List<Merger<R>>>> kv in InversePartitionMap)
                {
                    foreach (WhereRange<R> r in kv.Value.Item1) r.ApplyTo(queryable);
                    var merger = new Merger<R>();
                    foreach (Merger<R> m in kv.Value.Item2) merger = merger.Merge(m);
                    ForOthers.Clone().ApplyTo(merger.GetBKQueryable());
                }
            }
            return results.SelectMany(item => item).ToArray();
        }

        public IQuery<R> Clone()
        {
            return new Partitioning<R>(Dimensions, ForEach, ForOthers, BudgetOverRuntime, Keys, KeyExpression);
        }
    }
}
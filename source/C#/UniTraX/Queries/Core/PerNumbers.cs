﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;
    using UniTraX.Core.Utils;

    public class PerNumbers<R> : IQuery<R>
    {
        public static (int[], Expression<Func<R, int>>) KeysAndKeyExpression(Field<R> field, long start, long interval, int numberIntervals, int mapOffset)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (interval < 0) throw new ArgumentException("interval must be positive");
            if (numberIntervals < 1) throw new ArgumentException("numberIntervals must be above zero");
#endif
            var keys = new List<int> { -1 };
            for (int i = 0; i < numberIntervals; i++) keys.Add(i + mapOffset);
            var currentParameter = Expression.Parameter(typeof(R));
            var accessVisitor = new ReplaceExpressionVisitor(field.Access.Parameters[0], currentParameter);
            var accessBody = accessVisitor.Visit(field.Access.Body);
            var startD = field.FieldType.Convert(start, false);
            var endD = field.FieldType.Convert(start + (interval + 1L) * numberIntervals - 1L, true);
            var singleStep = field.FieldType.Convert(1L, true);
            var subtract = startD - 0.5 * singleStep;
            double intervalD = field.FieldType.ConvertUnchecked(interval + 1L, true);
            Expression currentExpression = null;
            currentExpression = Expression.Convert(Expression.Divide(Expression.Subtract(accessBody, Expression.Constant(subtract)), Expression.Constant(intervalD)), typeof(int));
            if (mapOffset != 0) currentExpression = Expression.Add(currentExpression, Expression.Constant(mapOffset));
            currentExpression = Expression.Condition(Expression.GreaterThan(accessBody, Expression.Constant(endD)), Expression.Constant(-1), currentExpression);
            currentExpression = Expression.Condition(Expression.LessThan(accessBody, Expression.Constant(startD)), Expression.Constant(-1), currentExpression);
            return (keys.ToArray(), Expression.Lambda<Func<R, int>>(currentExpression, currentParameter));
        }

        private readonly Field<R> Field;
        private readonly List<Part> Parts;
        private readonly IQuery<R> ForEach;
        private readonly IQuery<R> ForOthers;
        private readonly Partitioning<R> Partitioning;
        private readonly bool BudgetOverRuntime;

        private PerNumbers(Field<R> field, IQuery<R> forEach, IQuery<R> forOthers)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (forEach == null) throw new ArgumentNullException(nameof(forEach));
#endif
            Field = field;
            ForEach = forEach;
            ForOthers = forOthers;
        }

        public PerNumbers(Field<R> field, List<Part> parts, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) : this(field, forEach, forOthers)
        {
#if DEBUG
            if (parts == null) throw new ArgumentNullException(nameof(parts));
            foreach (Part part in parts)
            {
                if (field != null && !field.FieldType.Bounds.Encloses(part.Low)) throw new ArgumentException("part low not valid in field bounds");
                if (field != null && !field.FieldType.Bounds.Encloses(part.High)) throw new ArgumentException("part high not valid in field bounds");
                if (part.MapTo < 0) throw new ArgumentException("mapTo below zero");
            }
#endif
            Parts = parts;
            var dimensions = new Queue<Dimension<R>>();
            dimensions.Enqueue(new Dimension<R>(Field, parts));
            BudgetOverRuntime = budgetOverRuntime;
            Partitioning = new Partitioning<R>(dimensions, ForEach, ForOthers, BudgetOverRuntime);
        }

        public PerNumbers(Field<R> field, long start, long interval, int numberIntervals, int mapOffset, IQuery<R> forEach, IQuery<R> forOthers, bool budgetOverRuntime) :
            this(field, forEach, forOthers)
        {
#if DEBUG
            if (interval < 0) throw new ArgumentException("interval below zero");
            if (numberIntervals < 1) throw new ArgumentException("numberIntervals below one");
            if (mapOffset < 0) throw new ArgumentException("mapOffset below zero");
            if (!field.FieldType.Bounds.Encloses(start)) throw new ArgumentException("start not valied in field step");
            var value = start;
            for (int i = 0; i < numberIntervals; i++)
            {
                value += interval;
                if (!field.FieldType.Bounds.Encloses(value)) throw new ArgumentException("value is not valid in fieldB step at interval " + i);
                if (i < numberIntervals - 1)
                {
                    if (!field.FieldType.Bounds.Encloses(value + 1)) throw new ArgumentException("value has no next at interval " + i);
                    value = value + 1;
                }
            }
#endif
            var parts = new List<Part>(numberIntervals);
            var begin = start;
            for (int i = 0; i < numberIntervals; i++)
            {
                var end = begin + interval;
                parts.Add(new Part(begin, end, mapOffset + i));
                if (i < numberIntervals - 1) begin = end + 1;
            }
            Parts = parts;
            var dimensions = new Queue<Dimension<R>>();
            dimensions.Enqueue(new Dimension<R>(Field, parts));
            BudgetOverRuntime = budgetOverRuntime;

            var keysAndKeyExpression = KeysAndKeyExpression(field, start, interval, numberIntervals, mapOffset);
            Partitioning = new Partitioning<R>(dimensions, ForEach, ForOthers, BudgetOverRuntime, keysAndKeyExpression.Item1, keysAndKeyExpression.Item2);
//            More expensive
//            Partitioning = new Partitioning<R>(dimensions, ForEach, ForOthers, BudgetOverRuntime);
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Field == null) throw new ArgumentNullException(nameof(Field));
            return Partitioning.ApplyTo(queryable);
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Field == null) throw new ArgumentNullException(nameof(Field));
            return Partitioning.ApplyTo(queryable);
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Field == null) throw new ArgumentNullException(nameof(Field));
            return Partitioning.ApplyTo(queryable);
        }

        public IQuery<R> Clone()
        {
            return new PerNumbers<R>(Field, Parts, ForEach, ForOthers, BudgetOverRuntime);
        }
    }
}
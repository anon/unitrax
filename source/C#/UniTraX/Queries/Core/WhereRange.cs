﻿namespace UniTraX.Queries.Core
{
	using PINQ;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class WhereRange<R> : IQuery<R>
    {
        public static Expression<Func<R, bool>> GenerateExpression(Field<R> field, long low, long high)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
#endif
            if (low <= field.FieldType.Bounds.Low && high >= field.FieldType.Bounds.High) return null;
            Expression<Func<R, bool>> expHigh = Expression.Lambda<Func<R, bool>>(Expression.LessThanOrEqual(field.Access.Body, Expression.Constant(field.FieldType.Convert(high, true))), field.Access.Parameters);
            if (low <= field.FieldType.Bounds.Low) return expHigh;
            Expression<Func<R, bool>> expLow = Expression.Lambda<Func<R, bool>>(Expression.GreaterThanOrEqual(field.Access.Body, Expression.Constant(field.FieldType.Convert(low, false))), field.Access.Parameters);
            if (high >= field.FieldType.Bounds.High) return expLow;
            return Expression.Lambda<Func<R, bool>>(Expression.And(expLow.Body, expHigh.Body), field.Access.Parameters);
        }

        private readonly Field<R> Field;
        private readonly long Low;
        private readonly long High;
        private readonly IQuery<R> Afterwards;

        public WhereRange(Field<R> field, long low, long high, IQuery<R> afterwards)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (high < low) throw new ArgumentException("high below low");
            if (afterwards == null) throw new ArgumentNullException(nameof(afterwards));
#endif
            Field = field;
            Low = low;
            High = high;
            Afterwards = afterwards;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Field == null) throw new ArgumentNullException(nameof(Field));
            var newQueryable = queryable.Where(GenerateExpression(Field, Low, High));
#if RECORD
            Recorder.Add(new EntryRange(Field.Name, Low, High));
#endif
            return Afterwards.ApplyTo(newQueryable);
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Field == null) throw new ArgumentNullException(nameof(Field));
            var newQueryable = queryable.Where(GenerateExpression(Field, Low, High));
#if RECORD
            Recorder.Add(new EntryRange(Field.Name, Low, High));
#endif
            return Afterwards.ApplyTo(newQueryable);
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Field == null) throw new ArgumentNullException(nameof(Field));
            var newQueryable = queryable.Apply(x => Partition.LessEqual(Partition.GreaterEqual(x, new IndexedValue(Field.Index, Low), Field.FieldType.Bounds), new IndexedValue(Field.Index, High)));
#if RECORD
            Recorder.Add(new EntryRange(Field.Name, Low, High));
#endif
            return Afterwards.ApplyTo(newQueryable);
        }

        public IQuery<R> Clone()
        {
            return new WhereRange<R>(Field, Low, High, Afterwards);
        }
    }
}
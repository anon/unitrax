﻿namespace UniTraX.Queries.Recording
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    public class Recorder
    {
#if RECORD
        public static List<IEntry> Entries = new List<IEntry>();
        private static Stopwatch GlobalTimer = null;

        public static void Add(IEntry entry)
        {
            Entries.Add(entry);
        }

        public static void PrintStatus()
        {
            Console.WriteLine("Recording is ACTIVE");
        }

        public static void StartGlobalTimer()
        {
#if DEBUG
            if (GlobalTimer != null) throw new InvalidOperationException("Global timer already running");
#endif
            GlobalTimer = StartTimer();
        }

        public static double StopGlobalTimer()
        {
#if DEBUG
            if (GlobalTimer == null) throw new InvalidOperationException("Global timer not running");
#endif
            var time = StopTimer(GlobalTimer);
            GlobalTimer = null;
            return time;
        }

        public static Stopwatch StartTimer()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            return timer;
        }

        public static double StopTimer(Stopwatch timer)
        {
            timer.Stop();
            return timer.Elapsed.TotalMilliseconds;
        }

        public static void Reset()
        {
            Entries = new List<IEntry>();
        }

        public static void ToFile(string filename)
        {
            ToFile(filename, X => true);
        }

        public static void ToFile(string filename, Func<IEntry, bool> filter)
        {
            Console.Write("Reporting to file ... ");
            using (var file = new System.IO.StreamWriter(filename))
            {
                for (var i = 0; i < Entries.Count; i++)
                {
                    if (filter(Entries[i])) file.WriteLine(Entries[i].ToString());
                }
            }
            Console.WriteLine("Done!");
        }
#else
        public static List<IEntry> Entries;

        public static void Add(IEntry entry) {}

        public static void PrintStatus()
        {
            Console.WriteLine("Recording is INACTIVE");
        }

        public static Stopwatch StartTimer()
        {
            return null;
        }

        public static double StopTimer(Stopwatch timer)
        {
            return -1.0;
        }

        public static void Reset() {}

        public static void ToFile(string filename) {}

        public static void ToFile(string filename, Func<IEntry, bool> filter) {}
#endif
    }
}
﻿namespace UniTraX.Queries.Recording
{
    using System;

    public class EntryNumberPartitions : IEntry
    {
        private readonly int SetupNumberPartitions;
        private readonly int ExecuteNumberPartitions;
        private readonly int TeardownNumberPartitions;

        public EntryNumberPartitions(int setupNumberPartitions, int executeNumberPartitions, int teardownNumberPartitions)
        {
#if DEBUG
            if (setupNumberPartitions < 0) throw new ArgumentException("setupNumberPartitions is not valid");
            if (executeNumberPartitions < 0) throw new ArgumentException("executeNumberPartitions is not valid");
            if (teardownNumberPartitions < 0) throw new ArgumentException("teardownNumberPartitions is not valid");
#endif
            SetupNumberPartitions = setupNumberPartitions;
            ExecuteNumberPartitions = executeNumberPartitions;
            TeardownNumberPartitions = teardownNumberPartitions;
        }

        public bool SameAs(object comparand)
        {
            if (comparand == null) return false;
            if (!GetType().Equals(comparand.GetType())) return false;
            EntryNumberPartitions c = (EntryNumberPartitions)comparand;
            if (!SetupNumberPartitions.Equals(c.SetupNumberPartitions)) return false;
            if (!ExecuteNumberPartitions.Equals(c.ExecuteNumberPartitions)) return false;
            if (!TeardownNumberPartitions.Equals(c.TeardownNumberPartitions)) return false;
            return true;
        }

        public override string ToString()
        {
            return "NumberPartitions;" + SetupNumberPartitions + ";" + ExecuteNumberPartitions + ";" + TeardownNumberPartitions;
        }
    }
}
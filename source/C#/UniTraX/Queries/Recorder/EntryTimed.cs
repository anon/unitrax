﻿namespace UniTraX.Queries.Recording
{
    using System;

    public class EntryTimed : IEntry
    {
        public double SetupTime { get; private set; }
        public double ExecuteTime { get; private set; }
        public double BKExecuteTime { get; private set; }
        public double TeardownTime { get; private set; }

        public EntryTimed(double setupTime, double executeTime, double bkExecuteTime, double teardownTime)
        {
#if DEBUG
            if (setupTime < 0) throw new ArgumentException("setupTime is not valid");
            if (executeTime < 0) throw new ArgumentException("executeTime is not valid");
            if (bkExecuteTime < 0) throw new ArgumentException("bkExecuteTime is not valid");
            if (teardownTime < 0) throw new ArgumentException("teardownTime is not valid");
#endif
            SetupTime = setupTime;
            ExecuteTime = executeTime;
            BKExecuteTime = bkExecuteTime;
            TeardownTime = teardownTime;
        }

        public virtual bool SameAs(object comparand)
        {
            if (comparand == null) return false;
            if (!GetType().Equals(comparand.GetType())) return false;
            return true;
        }

        public override string ToString()
        {
            return SetupTime + ";" + ExecuteTime + ";" + BKExecuteTime + ";" + TeardownTime;
        }
    }
}
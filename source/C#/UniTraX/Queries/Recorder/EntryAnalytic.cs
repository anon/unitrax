﻿namespace UniTraX.Queries.Recording
{
    using System;

    public class EntryAnalytic : IEntry
    {
        public long Id { get; private set; }
        public double Time { get; private set; }
        public string Message { get; private set; }

        public EntryAnalytic(long id, double time, string message)
        {
#if DEBUG
            if (time < 0) throw new ArgumentException("time is not valid");
            if (message == null) throw new ArgumentNullException(nameof(message));
#endif
            Id = id;
            Time = time;
            Message = message;
        }

        public bool SameAs(object comparand)
        {
            if (comparand == null) return false;
            if (!GetType().Equals(comparand.GetType())) return false;
            return true;
        }

        public override string ToString()
        {
            return "Analytic;" + Id + ";" + Time + ";" + Message;
        }
    }
}
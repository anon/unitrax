﻿namespace UniTraX.Queries.Recording
{
    using System;

    public class EntryRange : IEntry
    {
        public string ColumnName { get; private set; }
        public long Low { get; private set; }
        public long High { get; private set; }

        public EntryRange(string columnName, long low, long high)
        {
#if DEBUG
            if (columnName == null) throw new ArgumentNullException(nameof(columnName));
            if (high < low) throw new ArgumentException("high below low");
#endif
            ColumnName = columnName;
            Low = low;
            High = high;
        }

        public bool SameAs(object comparand)
        {
            if (comparand == null) return false;
            if (!GetType().Equals(comparand.GetType())) return false;
            EntryRange c = (EntryRange)comparand;
            if (!ColumnName.Equals(c.ColumnName)) return false;
            if (!Low.Equals(c.Low)) return false;
            if (!High.Equals(c.High)) return false;
            return true;
        }

        public override string ToString()
        {
            return "Range;" + ColumnName + ";" + Low + ";" + High;
        }
    }
}
﻿namespace UniTraX.Queries.Recording
{
    using System;

    public class EntryAggregate : EntryTimed
    {
        public string Name { get; private set; }
        public string Message { get; private set; }
        public long Epsilon { get; private set; }
        public double Aggregate { get; private set; }
        public int CleanThreshold { get; private set; }

        public EntryAggregate(string name, string message, long epsilon, double aggregate, double setupTime, double executeTime, double bkExecuteTime, double teardownTime, int cleanThreshold) : base(setupTime, executeTime, bkExecuteTime, teardownTime)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (message == null) throw new ArgumentNullException(nameof(message));
#endif
            Name = name;
            Message = message;
            Epsilon = epsilon;
            Aggregate = aggregate;
            CleanThreshold = cleanThreshold;
        }

        public override bool SameAs(object comparand)
        {
            if (comparand == null) return false;
            if (!GetType().Equals(comparand.GetType())) return false;
            EntryAggregate c = (EntryAggregate)comparand;
            if (!Name.Equals(c.Name)) return false;
            if (!Message.Equals(c.Message)) return false;
            if (CleanThreshold != c.CleanThreshold) return false;
            return true;
        }

        public override string ToString()
        {
            return "Aggregate;" + Name + ";" + Message + ";" + Epsilon + ";" + Aggregate + ";" + base.ToString() + ";" + CleanThreshold;
        }
    }
}
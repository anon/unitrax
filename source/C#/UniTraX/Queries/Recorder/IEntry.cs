﻿namespace UniTraX.Queries.Recording
{
    public interface IEntry
    {
        bool SameAs(object comparand);
    }
}
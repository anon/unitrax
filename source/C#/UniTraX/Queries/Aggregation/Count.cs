﻿namespace UniTraX.Queries.Aggregation
{
	using PINQ;
    using System;
    using System.Linq;
    using UniTraX.Queries.Core;

    public class Count<R> : Aggregate<R>
    {
        private class PINQCount<T>
        {
            public static double Calculate(PINQueryable<T> queryable, long epsilon, Func<long, bool, double> convertBudget)
            {
#if DEBUG
                if (queryable == null) throw new ArgumentNullException(nameof(queryable));
                if (epsilon < 0) throw new ArgumentException("epsilon is not valid");
                if (convertBudget == null) throw new ArgumentNullException(nameof(convertBudget));
#endif
                return queryable.NoisyCount(convertBudget(epsilon, false));
            }
        }

        public Count(long epsilon, Func<long, bool, double> convertBudget, IPINQCondition<R> pinqCondition, int cleanThreshold) :
            base("Count", epsilon, convertBudget, pinqCondition, cleanThreshold)
        {

        }

        public override double SpecificApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return queryable.Count();
        }

        public override double SpecificApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return PINQCount<R>.Calculate(queryable, Epsilon, ConvertBudget);
        }

        public override IQuery<R> Clone()
        {
            return new Count<R>(Epsilon, ConvertBudget, PINQCondition, CleanThreshold);
        }
    }
}
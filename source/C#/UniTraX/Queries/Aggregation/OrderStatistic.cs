﻿namespace UniTraX.Queries.Aggregation
{
	using PINQ;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
	using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;
    using UniTraX.Queries.Core;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class OrderStatistic<R> : Aggregate<R>
    {
        private class PINQOrderStatistic<T>
        {
            public static double Calculate(PINQueryable<T> queryable, long epsilon, Func<long, bool, double> convertBudget,
                Field<T> field, double shiftValue, double scaleValue, double fraction)
            {
#if DEBUG
                if (queryable == null) throw new ArgumentNullException(nameof(queryable));
                if (epsilon < 0L) throw new ArgumentException("epsilon is not valid");
                if (convertBudget == null) throw new ArgumentNullException(nameof(convertBudget));
                if (field == null) throw new ArgumentNullException(nameof(field));
                if (scaleValue == 0) throw new ArgumentException("scaleValue is not valid");
                if (fraction < 0 || 1 < fraction) throw new ArgumentException("fraction is not valid");
#endif
                Expression<Func<T, double>> exp = Expression.Lambda<Func<T, double>>(Expression.Divide(Expression.Add(field.Access.Body, Expression.Constant(shiftValue)), Expression.Constant(scaleValue)), field.Access.Parameters);
                return queryable.NoisyOrderStatistic(convertBudget(epsilon, false), fraction, exp);
            }
        }

        private readonly Field<R> Field;
        private readonly double ShiftValue;
        private readonly double ScaleValue;
        private readonly double Fraction;

        public OrderStatistic(long epsilon, Func<long, bool, double> convertBudget, IPINQCondition<R> pinqCondition, Field<R> field, double shiftValue, double scaleValue, double fraction, int cleanThreshold) :
            base("OrderStatistics", epsilon, convertBudget, pinqCondition, cleanThreshold)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (scaleValue == 0) throw new ArgumentException("scaleValue is not valid");
            if (fraction < 0.0 || 1.0 < fraction) throw new ArgumentException("fraction is not valid");
#endif
            Field = field;
            ShiftValue = shiftValue;
            ScaleValue = scaleValue;
            Fraction = fraction;
        }

        public override double SpecificApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            var projectedQueryable = queryable.Select(Field.Access);
            var orderedQueryable = projectedQueryable.OrderBy(x => x);
            var count = orderedQueryable.Count();
            return (orderedQueryable.Skip(Convert.ToInt32(Fraction * count)).First() + ShiftValue) / ScaleValue;
        }

        public override double SpecificApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return PINQOrderStatistic<R>.Calculate(queryable, Epsilon, ConvertBudget, Field, ShiftValue, ScaleValue, Fraction);
        }

        public override void Log(IQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            Recorder.Add(new EntryAggregate(Name, Field.Name, -1L, aggregate, setupTime, executeTime, 0.0, teardownTime, cleanThreshold));
        }

        public override void Log(PINQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            Recorder.Add(new EntryAggregate(Name, Field.Name, -1L, aggregate, setupTime, executeTime, 0.0, teardownTime, cleanThreshold));
        }

        public override void Log(BKQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double bkExecuteTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            Recorder.Add(new EntryAggregate(Name, Field.Name, Epsilon, aggregate, setupTime, executeTime, bkExecuteTime, teardownTime, cleanThreshold));
        }

        public override IQuery<R> Clone()
        {
            return new OrderStatistic<R>(Epsilon, ConvertBudget, PINQCondition, Field, ShiftValue, ScaleValue, Fraction, CleanThreshold);
        }
    }
}
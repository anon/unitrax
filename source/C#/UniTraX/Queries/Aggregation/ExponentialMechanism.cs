﻿namespace UniTraX.Queries.Aggregation
{
    using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Specification;
    using UniTraX.Queries.Core;

    public class ExponentialMechanism<R> : Aggregate<R>
    {
        private class PINQExponentialMechanism<T>
        {
            public static double Calculate(PINQueryable<T> queryable, long epsilon, Func<long, bool, double> convertBudget,
                IQueryable<T> values, Expression<Func<T, T, double>> scoreFunction)
            {
#if DEBUG
                if (queryable == null) throw new ArgumentNullException(nameof(queryable));
                if (epsilon < 0) throw new ArgumentException("epsilon is not valid");
                if (convertBudget == null) throw new ArgumentNullException(nameof(convertBudget));
                if (values == null) throw new ArgumentNullException(nameof(values));
                if (scoreFunction == null) throw new ArgumentNullException(nameof(scoreFunction));
#endif
                var target = queryable.ExponentialMechanism(convertBudget(epsilon, false), values, scoreFunction);
                return values.Select((x, i) => new { Value = x, Index = i }).Where(t => t.Value.Equals(target)).First().Index;
            }
        }

        private readonly IQueryable<R> Values;
        private readonly Expression<Func<R, R, double>> ScoreFunction;

        public ExponentialMechanism(long epsilon, Func<long, bool, double> convertBudget, IPINQCondition<R> pinqCondition, IQueryable<R> values, Expression<Func<R, R, double>> scoreFunction, int cleanThreshold) :
            base("ExponentialMechanism", epsilon, convertBudget, pinqCondition, cleanThreshold)
        {
#if DEBUG
            if (values == null) throw new ArgumentNullException(nameof(values));
            if (scoreFunction == null) throw new ArgumentNullException(nameof(scoreFunction));
#endif
            Values = values;
            ScoreFunction = scoreFunction;
        }

        public override double SpecificApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Values == null) throw new ArgumentNullException(nameof(Values));
            if (ScoreFunction == null) throw new ArgumentNullException(nameof(ScoreFunction));
#endif
            var compiled = ScoreFunction.Compile();
            var scores = Values.Select(r => new KeyValuePair<R, double>(r, queryable.Select(x => compiled.Invoke(x, r)).Sum())).ToArray();
            var target = scores.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
            return Values.Select((x, i) => new { Value = x, Index = i }).Where(t => t.Value.Equals(target)).First().Index;
        }

        public override double SpecificApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Values == null) throw new ArgumentNullException(nameof(Values));
            if (ScoreFunction == null) throw new ArgumentNullException(nameof(ScoreFunction));
#endif
            return PINQExponentialMechanism<R>.Calculate(queryable, Epsilon, ConvertBudget, Values, ScoreFunction);
        }

        public override IQuery<R> Clone()
        {
            return new ExponentialMechanism<R>(Epsilon, ConvertBudget, PINQCondition, Values, ScoreFunction, CleanThreshold);
        }
    }
}
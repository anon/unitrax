﻿namespace UniTraX.Queries.Aggregation
{
	using PINQ;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;
    using UniTraX.Queries.Core;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class Median<R> : Aggregate<R>
    {
        private class PINQMedian<T>
        {
            public static double Calculate(PINQueryable<T> queryable, long epsilon, Func<long, bool, double> convertBudget,
                Field<T> field, double shiftValue, double scaleValue)
            {
#if DEBUG
                if (queryable == null) throw new ArgumentNullException(nameof(queryable));
                if (epsilon < 0) throw new ArgumentException("epsilon is not valid");
                if (convertBudget == null) throw new ArgumentNullException(nameof(convertBudget));
                if (field == null) throw new ArgumentNullException(nameof(field));
                if (scaleValue == 0) throw new ArgumentException("scaleValue is not valid");
#endif
                Expression<Func<T, double>> exp = Expression.Lambda<Func<T, double>>(Expression.Divide(Expression.Add(
                    field.Access.Body, Expression.Constant(shiftValue)), Expression.Constant(scaleValue)),field.Access.Parameters);
                return queryable.NoisyMedian(convertBudget(epsilon, false), exp);
            }
        }

        private readonly Field<R> Field;
        private readonly double ShiftValue;
        private readonly double ScaleValue;

        public Median(long epsilon, Func<long, bool, double> convertBudget, IPINQCondition<R> pinqCondition, Field<R> field, double shiftValue, double scaleValue, int cleanThreshold) :
            base("Median", epsilon, convertBudget, pinqCondition, cleanThreshold)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (scaleValue == 0) throw new ArgumentException("scaleValue is not valid");
#endif
            Field = field;
            ShiftValue = shiftValue;
            ScaleValue = scaleValue;
        }

        public override double SpecificApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            var projectedQueryable = queryable.Select(Field.Access);
            var orderedQueryable = projectedQueryable.OrderBy(x => x);
            var list = orderedQueryable.ToList();
            return (list.Skip(list.Count / 2).First() + ShiftValue) / ScaleValue;
        }

        public override double SpecificApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            return PINQMedian<R>.Calculate(queryable, Epsilon, ConvertBudget, Field, ShiftValue, ScaleValue);
        }

        public override void Log(IQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            Recorder.Add(new EntryAggregate(Name, Field.Name, -1L, aggregate, setupTime, executeTime, 0.0, teardownTime, cleanThreshold));
        }

        public override void Log(PINQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            Recorder.Add(new EntryAggregate(Name, Field.Name, -1L, aggregate, setupTime, executeTime, 0.0, teardownTime, cleanThreshold));
        }

        public override void Log(BKQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double bkExecuteTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (Field == null) throw new ArgumentNullException(nameof(Field));
#endif
            Recorder.Add(new EntryAggregate(Name, Field.Name, Epsilon, aggregate, setupTime, executeTime, bkExecuteTime, teardownTime, cleanThreshold));
        }

        public override IQuery<R> Clone()
        {
            return new Median<R>(Epsilon, ConvertBudget, PINQCondition, Field, ShiftValue, ScaleValue, CleanThreshold);
        }
    }
}
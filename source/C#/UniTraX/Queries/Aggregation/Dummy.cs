﻿namespace UniTraX.Queries.Aggregation
{
	using PINQ;
    using System;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Queries.Core;

    public class Dummy<R> : Aggregate<R>
    {
        private readonly bool Run;

        public Dummy(long epsilon, Func<long, bool, double> convertBudget, IPINQCondition<R> pinqCondition, bool run, int cleanThreshold) :
            base("Dummy", run ? epsilon : 0L, convertBudget, pinqCondition, cleanThreshold)
        {
            Run = run;
        }

        public override IQuery<R> Clone()
        {
            return new Dummy<R>(Epsilon, ConvertBudget, PINQCondition, Run, CleanThreshold);
        }

        public override double SpecificApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return 0.0;
        }

        public override double SpecificApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            return 0.0;
        }

        public override void Teardown(BKQueryable<R> queryable, int executeNumberPartitions, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (Run && executeNumberPartitions - queryable.Bookkeeper.NumberPartitionsAfterClean() > cleanThreshold) queryable.CleanHistory();
        }
    }
}
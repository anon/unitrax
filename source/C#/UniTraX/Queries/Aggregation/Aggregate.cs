﻿namespace UniTraX.Queries.Aggregation
{
	using PINQ;
    using System;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Queries.Core;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public abstract class Aggregate<R> : IQuery<R>
    {
        public string Name { get; private set; }
        public long Epsilon { get; private set; }
        public Func<long, bool, double> ConvertBudget { get; private set; }
        public IPINQCondition<R> PINQCondition { get; private set; }
        public int CleanThreshold { get; private set; }

        public Aggregate(string name, long epsilon, Func<long, bool, double> convertBudget, IPINQCondition<R> pinqCondition, int cleanThreshold)
        {
#if DEBUG
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (epsilon < 0) throw new ArgumentException("epsilon is not valid");
            if (convertBudget == null) throw new ArgumentNullException(nameof(convertBudget));
            if (pinqCondition == null) throw new ArgumentNullException(nameof(pinqCondition));
#endif
            Name = name;
            Epsilon = epsilon;
            ConvertBudget = convertBudget;
            PINQCondition = pinqCondition;
            CleanThreshold = cleanThreshold;
        }

        public double[] ApplyTo(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
#if RECORD
            var setupTime = Recorder.StopGlobalTimer();
            Recorder.StartGlobalTimer();
#endif
            var queryables = PINQCondition.ApplyTo(queryable);
            var aggregates = new double[queryables.Length];
            for (int i = 0; i < queryables.Length; i++)
            {
                aggregates[i] = SpecificApplyTo(queryables[i]);
#if RECORD
                var executeTime = Recorder.StopGlobalTimer();
                Log(queryable, aggregates[i], setupTime, executeTime, 0.0, CleanThreshold);
                setupTime = 0.0;
                Recorder.StartGlobalTimer();
#endif
            }
            return aggregates;
        }

        public double[] ApplyTo(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
#if RECORD
            var setupTime = Recorder.StopGlobalTimer();
            Recorder.StartGlobalTimer();
#endif
            var queryables = PINQCondition.ApplyTo(queryable);
            var aggregates = new double[queryables.Length];
            for (int i = 0; i < queryables.Length; i++)
            {
                aggregates[i] = SpecificApplyTo(queryables[i]);
#if RECORD
                var executeTime = Recorder.StopGlobalTimer();
                Log(queryable, aggregates[i], setupTime, executeTime, 0.0, CleanThreshold);
                setupTime = 0.0;
                Recorder.StartGlobalTimer();
#endif
            }
            return aggregates;
        }

        public double[] ApplyTo(BKQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
#if RECORD
            var setupTime = Recorder.StopGlobalTimer();
            var setupNumberPartitions = queryable.Bookkeeper.NumberPartitions();
            Recorder.StartGlobalTimer();
#endif
            PINQueryable<R>[] queryables = null;
            var bkTime = 0.0;
            if (Epsilon > 0L)
            {
                var pinqQueryable = queryable.AsPinq(Epsilon);
#if RECORD
                bkTime = Recorder.StopGlobalTimer();
                Recorder.StartGlobalTimer();
#endif
                queryables = PINQCondition.ApplyTo(pinqQueryable);
            }
            else queryables = new PINQueryable<R>[] { new PINQueryable<R>(Enumerable.Empty<R>().AsQueryable(), new PINQAgent()) };
            var aggregates = new double[queryables.Length];
#if RECORD
            var executeTime = 0.0;
#endif
            for (int i = 0; i < queryables.Length; i++)
            {
                aggregates[i] = SpecificApplyTo(queryables[i]);
#if RECORD
                executeTime = Recorder.StopGlobalTimer();
                if (i < queryables.Length - 1)
                {
                    Log(queryable, aggregates[i], setupTime, executeTime + bkTime, bkTime, 0.0, CleanThreshold);
                    setupTime = 0.0;
                    bkTime = 0.0;
                }
                Recorder.StartGlobalTimer();
#endif
            }
            var executeNumberPartitions = queryable.Bookkeeper.NumberPartitions();
            Teardown(queryable, executeNumberPartitions, CleanThreshold);
#if RECORD
            var teardownTime = Recorder.StopGlobalTimer();
            var teardownNumberPartitions = queryable.Bookkeeper.NumberPartitions();
            Recorder.Add(new EntryNumberPartitions(setupNumberPartitions, executeNumberPartitions, teardownNumberPartitions));
            Log(queryable, aggregates[aggregates.Length - 1], setupTime, executeTime + bkTime, bkTime, teardownTime, CleanThreshold);
            Recorder.StartGlobalTimer();
#endif
            return aggregates;
        }

        public virtual void Teardown(BKQueryable<R> queryable, int executeNumberPartitions, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (executeNumberPartitions - queryable.Bookkeeper.NumberPartitionsAfterClean() > cleanThreshold) queryable.CleanHistory();
        }

        public virtual void Log(IQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Recorder.Add(new EntryAggregate(Name, "", -1L, aggregate, setupTime, executeTime, 0.0, teardownTime, cleanThreshold));
        }

        public virtual void Log(PINQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Recorder.Add(new EntryAggregate(Name, "", -1L, aggregate, setupTime, executeTime, 0.0, teardownTime, cleanThreshold));
        }

        public virtual void Log(BKQueryable<R> queryable, double aggregate, double setupTime, double executeTime, double bkExecuteTime, double teardownTime, int cleanThreshold)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            Recorder.Add(new EntryAggregate(Name, "", Epsilon, aggregate, setupTime, executeTime, bkExecuteTime, teardownTime, cleanThreshold));
        }

        public abstract double SpecificApplyTo(IQueryable<R> queryable);

        public abstract double SpecificApplyTo(PINQueryable<R> queryable);

        public abstract IQuery<R> Clone();
    }
}
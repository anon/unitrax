﻿namespace UniTraX.Experiments.MobilityStream
{
	using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.History;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;
    using UniTraX.Core.Utils;
    using UniTraX.Datasets.Mobility.DataAccess;
    using UniTraX.Datasets.Mobility.DataLoading;
    using UniTraX.Datasets.Mobility.Queries;
    using UniTraX.Infrastructure.BudgetProvider;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Infrastructure.DataLoading;
    using UniTraX.Infrastructure.ExperimentUtils;
    using UniTraX.Infrastructure.Provisioning;
    using UniTraX.Queries.Recording;

    public class MobilityStreamQueries
    {
        public List<ITimedAction<Ride, RideB>> List { get; private set; }

        public MobilityStreamQueries(long baseId, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long defaultEpsilon, Field<Ride> field, Field<RideB> fieldB, ContiguousPartition originalFocus, int timedColumnIndex)
        {
#if DEBUG
            if (defaultEpsilon < 0L) throw new ArgumentException("defaultEpsilon is below zero");
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (fieldB == null) throw new ArgumentNullException(nameof(fieldB));
            if (originalFocus == null) throw new ArgumentNullException(nameof(originalFocus));
#endif
            List = new List<ITimedAction<Ride, RideB>>(2)
            {
                new TimedQueryExecution<Ride, RideB>(0, field, fieldB, new StreamInvestigationHourly(baseId + 1L, useEvenly, budgetOverRuntime, cleanThreshold,
                new long[8] { defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon }), 1356994800L, 1359673200L, 3600L, originalFocus, timedColumnIndex),
                new TimedQueryExecution<Ride, RideB>(1, field, fieldB, new StreamInvestigationMonthly(baseId + 2L, useEvenly, budgetOverRuntime, cleanThreshold,
                new long[2] { defaultEpsilon, defaultEpsilon }), 1356994800L, 1359673200L, 2678400L, originalFocus, timedColumnIndex)
            };
        }
    }

    public class MobilityStreamExperiment
    {
        public static bool LoadingSimulated = true;
        public static int Repeats = 8;
        public static int CleanThreshold = 500;
        public static long BaseId = 4400L;
        public static long DefaultEpsilon = DoubleTypeConverter.TwoDecimal.ToSystem(10000.0);
        public static string BaseDirName = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax", "experiments", "mobility", "experimentStream");
        public static string InstanceName = BaseId + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff", CultureInfo.InvariantCulture);
        public static string InstanceDirName = Path.Combine(BaseDirName, InstanceName);
        public static string DataFileName = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "mobility", "transformed", "rides.csv");
        public static double DefaultTotalBudget = 10000000000L;
        public static IBudgetProvider BudgetProvider = new ConstantBudgetProvider(DefaultTotalBudget);

        public static TimedRecordLoader<Ride, RideB> SetupDatabaseMem(IExperimentServer server, DoubleTypeConverter typeConverter, int loaderId, DataTable dataTable, bool withBudget, bool loadingSimulated, bool repeat)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (typeConverter == null) throw new ArgumentNullException(nameof(typeConverter));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
#endif
            if ((!loadingSimulated) || (loadingSimulated && !repeat))
            {
                server.Setup();
                server.CreateDatabaseInMem("mobility-mem");
                MobilityMem.CreateTable(server.GetDatabase("mobility-mem"), withBudget);
            }
            if (loadingSimulated && !repeat)
            {
                var connection = server.GetSqlConnection("mobility-mem");
                connection.Open();
                Console.WriteLine("Filling table rides");
                TableMem.FillTable(dataTable, connection, "rides");
                connection.Close();
            }
            return new TimedRecordLoader<Ride, RideB>(loaderId, server.GetSqlConnection("mobility-mem"), dataTable, true, "rides", MobilityDisk.ColumnNames[2], 2, typeConverter, loadingSimulated);
        }

        public static TimedRecordLoader<Ride, RideB> SetupDatabaseDisk(IExperimentServer server, DoubleTypeConverter typeConverter, int loaderId, DataTable dataTable, bool withBudget, bool loadingSimulated, bool repeat)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (typeConverter == null) throw new ArgumentNullException(nameof(typeConverter));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
#endif
            if ((!loadingSimulated) || (loadingSimulated && !repeat))
            {
                server.Setup();
                server.CreateDatabaseOnDisk("mobility-disk");
                MobilityDisk.CreateTable(server.GetDatabase("mobility-disk"), withBudget);
            }
            if (loadingSimulated && !repeat)
            {
                var connection = server.GetSqlConnection("mobility-disk");
                connection.Open();
                TableDisk.FillTable(dataTable, connection, "rides");
                connection.Close();
            }
            if ((!loadingSimulated) || (loadingSimulated && !repeat)) MobilityDisk.CreateIndexes(server.GetDatabase("mobility-disk"), withBudget);
            return new TimedRecordLoader<Ride, RideB>(loaderId, server.GetSqlConnection("mobility-disk"), dataTable, false, "rides", MobilityDisk.ColumnNames[2], 2, typeConverter, loadingSimulated);
        }

        public static TimedExecution<Ride, RideB> PrepareTimedExecution(List<ITimedAction<Ride, RideB>> queries, bool inMem, IExperimentServer server, DoubleTypeConverter typeConverter, IFieldType fieldType, DataTable dataTable, bool withBudget, bool loadingSimulated, bool repeat)
        {
#if DEBUG
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (queries.Count == 0) throw new ArgumentException("queries is empty");
            foreach (ITimedAction<Ride, RideB> q in queries) if (q == null) throw new ArgumentNullException(nameof(q));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (typeConverter == null) throw new ArgumentNullException(nameof(typeConverter));
            if (fieldType == null) throw new ArgumentNullException(nameof(fieldType));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
#endif
            if ((!loadingSimulated) || (loadingSimulated && !repeat)) server.Wipe();
            foreach (ITimedAction<Ride, RideB> q in queries) q.Reset(null);
            TimedRecordLoader<Ride, RideB> recordLoader;
            if (inMem) recordLoader = SetupDatabaseMem(server, typeConverter, queries.Count, dataTable, withBudget, loadingSimulated, repeat);
            else recordLoader = SetupDatabaseDisk(server, typeConverter, queries.Count, dataTable, withBudget, loadingSimulated, repeat);
            var actions = new List<ITimedAction<Ride, RideB>>(queries.Count + 1);
            actions.AddRange(queries);
            actions.Add(recordLoader);
            return new TimedExecution<Ride, RideB>(fieldType, actions);
        }

        public static void RepeatDirect(string experimentName, List<ITimedAction<Ride, RideB>> queries, bool inMem, IExperimentServer server, DoubleTypeConverter typeConverter, IFieldType fieldType, DataTable dataTable, bool withBudget, bool loadingSimulated)
        {
#if DEBUG
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (queries.Count == 0) throw new ArgumentException("queries is empty");
            foreach (ITimedAction<Ride, RideB> q in queries) if (q == null) throw new ArgumentNullException(nameof(q));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (typeConverter == null) throw new ArgumentNullException(nameof(typeConverter));
            if (fieldType == null) throw new ArgumentNullException(nameof(fieldType));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
#endif
            Console.WriteLine("Setup SubExperiment Mobility " + experimentName + ": STARTED");
            ExperimentUtils<Ride>.SetupExperimentDirect(InstanceDirName, experimentName);
            Console.WriteLine("Setup SubExperiment Mobility " + experimentName + ": FINISHED");
            Console.WriteLine("SubExperiment Mobility " + experimentName + ": STARTED");
            for (int i = 0; i < Repeats; i++)
            {
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": STARTED");
                var experimentDirName = Path.Combine(InstanceDirName, experimentName);
                TimedExecution<Ride, RideB> timedExecution = PrepareTimedExecution(queries, inMem, server, typeConverter, fieldType, dataTable, withBudget, loadingSimulated, i != 0);
                Console.WriteLine("Execution prepared");
                IQueryable<Ride> queryable;
                if (inMem) queryable = new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides;
                else queryable = new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides;
                Console.WriteLine("Queryable prepared");
                Recorder.Reset();
                Recorder.StartGlobalTimer();
                timedExecution.Execute(queryable);
                Recorder.StopGlobalTimer();
                Recorder.ToFile(Path.Combine(experimentDirName, "run" + i + "_results.csv"));
                FileUtils.WriteLineToFile("0," + queryable.Count(), Path.Combine(experimentDirName, "run" + i + "_budgetUsed.csv"));
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": FINISHED");
            }
            Console.WriteLine("SubExperiment Mobility " + experimentName + ": FINISHED");
        }

        public static void RepeatPINQ(string experimentName, List<ITimedAction<Ride, RideB>> queries, bool inMem, IExperimentServer server, DoubleTypeConverter typeConverter, IFieldType fieldType, DataTable dataTable, bool withBudget, bool loadingSimulated)
        {
#if DEBUG
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (queries.Count == 0) throw new ArgumentException("queries is empty");
            foreach (ITimedAction<Ride, RideB> q in queries) if (q == null) throw new ArgumentNullException(nameof(q));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (typeConverter == null) throw new ArgumentNullException(nameof(typeConverter));
            if (fieldType == null) throw new ArgumentNullException(nameof(fieldType));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
#endif
            Console.WriteLine("Setup SubExperiment Mobility " + experimentName + ": STARTED");
            ExperimentUtils<Ride>.SetupExperimentPINQ(InstanceDirName, experimentName);
            Console.WriteLine("Setup SubExperiment Mobility " + experimentName + ": FINISHED");
            Console.WriteLine("SubExperiment Mobility " + experimentName + ": STARTED");
            for (int i = 0; i < Repeats; i++)
            {
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": STARTED");
                var experimentDirName = Path.Combine(InstanceDirName, experimentName);
                TimedExecution<Ride, RideB> timedExecution = PrepareTimedExecution(queries, inMem, server, typeConverter, fieldType, dataTable, withBudget, loadingSimulated, i != 0);
                IQueryable<Ride> queryable;
                if (inMem) queryable = new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides;
                else queryable = new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides;
                PINQueryable<Ride> queryablePINQ;
                if (inMem) queryablePINQ = new PINQueryable<Ride>(new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides, new PINQAgentBudget(DefaultTotalBudget));
                else queryablePINQ = new PINQueryable<Ride>(new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides, new PINQAgentBudget(DefaultTotalBudget));
                Recorder.Reset();
                Recorder.StartGlobalTimer();
                timedExecution.Execute(queryablePINQ);
                Recorder.StopGlobalTimer();
                Recorder.ToFile(Path.Combine(experimentDirName, "run" + i + "_results.csv"));
                FileUtils.WriteLineToFile((DefaultTotalBudget - queryablePINQ.remaining()) + "," + queryable.Count(), Path.Combine(experimentDirName, "run" + i + "_budgetUsed.csv"));
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": FINISHED");
            }
            Console.WriteLine("SubExperiment Mobility " + experimentName + ": FINISHED");
        }

        public static void RepeatBK(string experimentName, List<ITimedAction<Ride, RideB>> queries, bool inMem, IExperimentServer server, DoubleTypeConverter typeConverter, IFieldType fieldType, DataTable dataTable, bool withBudget, bool loadingSimulated)
        {
#if DEBUG
            if (experimentName == null) throw new ArgumentNullException(nameof(experimentName));
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (queries.Count == 0) throw new ArgumentException("queries is empty");
            foreach (ITimedAction<Ride, RideB> q in queries) if (q == null) throw new ArgumentNullException(nameof(q));
            if (server == null) throw new ArgumentNullException(nameof(server));
            if (typeConverter == null) throw new ArgumentNullException(nameof(typeConverter));
            if (fieldType == null) throw new ArgumentNullException(nameof(fieldType));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
#endif
            Console.WriteLine("Setup SubExperiment Mobility " + experimentName + ": STARTED");
            ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, experimentName);
            Console.WriteLine("Setup SubExperiment Mobility " + experimentName + ": FINISHED");
            Console.WriteLine("SubExperiment Mobility " + experimentName + ": STARTED");
            for (int i = 0; i < Repeats; i++)
            {
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": STARTED");
                var experimentDirName = Path.Combine(InstanceDirName, experimentName);
                Metadata metadata;
                if (inMem) metadata = new Metadata(server.GetDatabaseConnectionString("mobility-mem"));
                else metadata = new Metadata(server.GetDatabaseConnectionString("mobility-disk"));
                var originalFocus = metadata.BoundedDataAccess.Record.Boundary;
                var startTime = originalFocus.Intervals[2].Low;
                originalFocus = ResetFocus(originalFocus, 2, startTime);
                TimedExecution<Ride, RideB> timedExecution = PrepareTimedExecution(queries, inMem, server, typeConverter, fieldType, dataTable, withBudget, loadingSimulated, i != 0);
                BKQueryable<RideB> queryableBK = new BKQueryable<RideB>(metadata.BoundedDataAccess.GetQueryable(), new Bookkeeper<RideB>(originalFocus, metadata.BoundedDataAccess, new InMemoryHistory(metadata.BoundedDataAccess.Record.GetBudgetIndex())), metadata.BoundedDataAccess.Record.Boundary);
                Recorder.Reset();
                Recorder.StartGlobalTimer();
                timedExecution.Execute(queryableBK);
                Recorder.StopGlobalTimer();
                Recorder.ToFile(Path.Combine(experimentDirName, "run" + i + "_results.csv"));
                queryableBK.CleanHistory();
                queryableBK.WriteDetailedBudgetUseToFile(Path.Combine(experimentDirName, "run" + i + "_budgetUsed.csv"));
                Console.WriteLine("SubExperiment " + experimentName + " Run " + i + ": FINISHED");
            }
            Console.WriteLine("SubExperiment Mobility " + experimentName + ": FINISHED");
        }

        public static ContiguousPartition ResetFocus(ContiguousPartition focus, int index, long high)
        {
#if DEBUG
            if (focus == null) throw new ArgumentNullException(nameof(focus));
            if (index < 0) throw new ArgumentException("index below zero");
            if (index > focus.Intervals.Length - 1) throw new ArgumentException("index above number of columns");
            if (!focus.Intervals[index].Encloses(high)) throw new ArgumentException("high not valid in column");
#endif
            var newIntervals = new Interval[focus.Intervals.Length];
            for (int i = 0; i < focus.Intervals.Length; i++)
            {
                if (i == index) newIntervals[i] = new Interval(focus.Intervals[i].Low, high);
                else newIntervals[i] = focus.Intervals[i];
            }
            return new ContiguousPartition(newIntervals);
        }

        public static void Main(string[] args)
        {
#if RECORD
            var timerT = Recorder.StartTimer();
#endif
            Console.WriteLine("Experiment Mobility Stream: STARTED");
            var server = new ExampleDBServer();

            ContiguousPartition originalFocus = Metadata.Record.Boundary;
            var startTime = originalFocus.Intervals[2].Low;
            originalFocus = ResetFocus(originalFocus, 2, startTime);

            var queriesAlwaysCleanDirectPINQ = new MobilityStreamQueries(BaseId, false, false, -1, DefaultEpsilon, Metadata.Record.Fields[2], Metadata.RecordB.Fields[2], originalFocus, 2);

            var TypeMobilityDropoff = new DoubleTypeConverter(new DoubleType(1356994800.0, 1359716399.0, 1.0));

            Directory.CreateDirectory(InstanceDirName);

            var dataTable = new DataTableProvider(DataFileName, null, MobilityDisk.ColumnNames, "budget", null).GetDataTable();

            RepeatDirect("Disk-Direct-Runtime", queriesAlwaysCleanDirectPINQ.List, false, server, TypeMobilityDropoff, Metadata.Record.Fields[2].FieldType, dataTable, false, LoadingSimulated);
            RepeatPINQ("Disk-PINQ-Runtime", queriesAlwaysCleanDirectPINQ.List, false, server, TypeMobilityDropoff, Metadata.Record.Fields[2].FieldType, dataTable, false, LoadingSimulated);
           
            ContiguousPartition originalFocusB = Metadata.RecordB.Boundary;
            var startTimeB = originalFocusB.Intervals[2].Low;
            originalFocusB = ResetFocus(originalFocusB, 2, startTimeB);

            var queriesAlwaysCleanB = new MobilityStreamQueries(BaseId, false, false, -1, DefaultEpsilon, Metadata.Record.Fields[2], Metadata.RecordB.Fields[2], originalFocusB, 2);
            var dataTableB = new DataTableProvider(DataFileName, BudgetProvider, MobilityDisk.ColumnNames, "budget", null).GetDataTable();

            RepeatBK("Disk-Always-BK-Runtime", queriesAlwaysCleanB.List, false, server, TypeMobilityDropoff, Metadata.Record.Fields[2].FieldType, dataTableB, true, LoadingSimulated);
#if RECORD
            var timeT = Recorder.StopTimer(timerT);
            Console.WriteLine("Took time ms: " + timeT);
#endif
            Console.WriteLine("Experiment Mobility Stream: FINISHED");
            Console.Read();
        }
    }
}
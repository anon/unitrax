﻿namespace UniTraX.Experiments.MobilityStream
{
    using PINQ;
    using System.Data.SqlClient;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;

    public interface ITimedAction<TR, TRB>
    {
        int GetId();
        bool HasNextTime();
        long GetNextTime();
        void Execute(IQueryable<TR> queryable, long endOn);
        void Execute(PINQueryable<TR> queryable, long endOn);
        void Execute(BKQueryable<TRB> queryable, long endOn);
        void Reset(SqlConnection sqlConnection);
        void Close();
    }
}
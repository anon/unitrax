﻿namespace UniTraX.Experiments.MobilityStream
{
    using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;
    using UniTraX.Datasets.Mobility.Queries;

    public class TimedQueryExecution<R, RB> : ITimedAction<R, RB>
    {
        private readonly int Id;
        private readonly Field<R> Field;
        private readonly Field<RB> FieldB;
        private readonly ITimedAnalyticBaseFactory<R, RB> AnalyticBaseFactory;
        private readonly long StartTime;
        private readonly long EndTime;
        private long NextTime;
        private readonly long Interval;
        private readonly ContiguousPartition OriginalFocus;
        private readonly int TimedColumnIndex;

        public TimedQueryExecution(int id, Field<R> field, Field<RB> fieldB, ITimedAnalyticBaseFactory<R, RB> analyticBaseFactory, long startTime, long endTime, long interval, ContiguousPartition originalFocus, int timedColumnIndex)
        {
#if DEBUG
            if (field == null) throw new ArgumentNullException(nameof(field));
            if (fieldB == null) throw new ArgumentNullException(nameof(fieldB));
            if (analyticBaseFactory == null) throw new ArgumentNullException(nameof(analyticBaseFactory));
            if (startTime < endTime && interval < 0) throw new ArgumentException("interval has wrong direction");
            if (startTime > endTime && interval > 0) throw new ArgumentException("interval has wrong direction");
            if (startTime != endTime && interval == 0) throw new ArgumentException("interval is zero");
            if (originalFocus == null) throw new ArgumentNullException(nameof(originalFocus));
            if (startTime < originalFocus.Intervals[timedColumnIndex].Low) throw new ArgumentException("startTime cannot be before timedcolumn low");
#endif
            Id = id;
            Field = field;
            FieldB = fieldB;
            AnalyticBaseFactory = analyticBaseFactory;
            StartTime = startTime;
            EndTime = endTime;
            Interval = interval;
            OriginalFocus = originalFocus;
            TimedColumnIndex = timedColumnIndex;
            Reset(null);
        }

        public int GetId()
        {
            return Id;
        }

        public void Execute(IQueryable<R> queryable, long endOn)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            AnalyticBaseFactory.GenerateQuery(NextTime, Field, FieldB).GetAnalyticBaseDirect().ApplyTo(queryable);
            NextTime += Interval;
        }

        public void Execute(PINQueryable<R> queryable, long endOn)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            AnalyticBaseFactory.GenerateQuery(NextTime, Field, FieldB).GetAnalyticBasePINQ().ApplyTo(queryable);
            NextTime += Interval;
        }

        public void Execute(BKQueryable<RB> queryable, long endOn)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (!HasNextTime()) throw new Exception("there is no NextTime");

            var newIntervals = new Interval[OriginalFocus.Intervals.Length];
            for (int i = 0; i < OriginalFocus.Intervals.Length; i++)
            {
                if (i == TimedColumnIndex) newIntervals[i] = new Interval(OriginalFocus.Intervals[i].Low, NextTime);
                else newIntervals[i] = OriginalFocus.Intervals[i];
            }
            queryable.Bookkeeper.ExtendFocus(new Partition(new HashSet<ContiguousPartition>() { new ContiguousPartition(newIntervals) }));

            AnalyticBaseFactory.GenerateQuery(NextTime, Field, FieldB).GetAnalyticBaseBK().ApplyTo(queryable);
            NextTime += Interval;
        }

        public bool HasNextTime()
        {
            return NextTime <= EndTime;
        }

        public long GetNextTime()
        {
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            return NextTime;
        }

        public void Close() { }

        public void Reset(SqlConnection sqlConnection)
        {
            NextTime = StartTime + Interval - 1L;
        }
    }
}
﻿namespace UniTraX.Experiments.MobilityStream
{
    using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Core.Specification;

    public class TimedExecution<R, RB>
    {
        private class TimedActionComparer : Comparer<ITimedAction<R, RB>>
        {
            public override int Compare(ITimedAction<R, RB> x, ITimedAction<R, RB> y)
            {
#if DEBUG
                if (x == null) throw new ArgumentNullException(nameof(x));
                if (y == null) throw new ArgumentNullException(nameof(y));
#endif
                if (!x.HasNextTime() && !y.HasNextTime()) return x.GetId().CompareTo(y.GetId());
                if (!x.HasNextTime()) return 1;
                if (!y.HasNextTime()) return -1;
                var c = x.GetNextTime().CompareTo(y.GetNextTime());
                if (c == 0) return x.GetId().CompareTo(y.GetId());
                return c;
            }
        }

        private readonly IFieldType FieldType;
        private readonly List<ITimedAction<R, RB>> Actions;
        private readonly TimedActionComparer Comparer;

        public TimedExecution(IFieldType fieldType, List<ITimedAction<R, RB>> actions)
        {
#if DEBUG
            if (fieldType == null) throw new ArgumentNullException(nameof(fieldType));
            if (actions == null) throw new ArgumentNullException(nameof(actions));
            if (!actions.Any()) throw new ArgumentException("actions is empty");
            foreach (ITimedAction<R, RB> a in actions)
            {
                if (a == null) throw new ArgumentNullException(nameof(a));
                if (!a.HasNextTime()) throw new ArgumentException("action can never be executed");
            }
#endif
            FieldType = fieldType;
            Actions = actions;
            Comparer = new TimedActionComparer();
            Actions.Sort(Comparer);
        }

        public void Execute(IQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            while (Actions.Count > 0)
            {
                if (Actions.Count > 1) Actions.First().Execute(queryable, Actions[1].GetNextTime());
                else Actions.First().Execute(queryable, FieldType.Bounds.High);
                Actions.Sort(Comparer);
                while (Actions.Count > 0 && !Actions.Last().HasNextTime())
                {
                    Actions.Last().Close();
                    Actions.RemoveAt(Actions.Count - 1);
                }
            }
        }

        public void Execute(PINQueryable<R> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            while (Actions.Count > 0)
            {
                if (Actions.Count > 1) Actions.First().Execute(queryable, Actions[1].GetNextTime());
                else Actions.First().Execute(queryable, FieldType.Bounds.High);
                Actions.Sort(Comparer);
                while (Actions.Count > 0 && !Actions.Last().HasNextTime())
                {
                    Actions.Last().Close();
                    Actions.RemoveAt(Actions.Count - 1);
                }
            }
        }

        public void Execute(BKQueryable<RB> queryable)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            while (Actions.Count > 0)
            {
                if (Actions.Count > 1) Actions.First().Execute(queryable, Actions[1].GetNextTime());
                else Actions.First().Execute(queryable, FieldType.Bounds.High);
                Actions.Sort(Comparer);
                while (Actions.Count > 0 && !Actions.Last().HasNextTime())
                {
                    Actions.Last().Close();
                    Actions.RemoveAt(Actions.Count - 1);
                }
            }
        }
    }
}
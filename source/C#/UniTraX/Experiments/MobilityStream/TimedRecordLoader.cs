﻿namespace UniTraX.Experiments.MobilityStream
{
    using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using UniTraX.Core.Bookkeeper;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Infrastructure.DataLoading;

    public class TimedRecordLoader<R, RB> : ITimedAction<R, RB>
    {
        private readonly int Id;
        private readonly DataTable DataTable;
        private readonly bool InMem;
        private readonly string TableName;
        private readonly string TimedColumnName;
        private readonly DoubleTypeConverter TimedFieldConverter;
        private SqlConnection SqlConnection;
        private IEnumerator<DataRow> RowEnumerator;
        private bool HasCurrent;
        private bool Closed;
        private int ConsoleCursorTop;
        private int ConsoleMessageLength;
        private int CurrentCount;
        private readonly bool LoadingSimulated;

        public TimedRecordLoader(int id, SqlConnection sqlConnection, DataTable dataTable, bool inMem, string tableName, string timedColumnName, int timedColumnIndex, DoubleTypeConverter timedFieldConverter, bool loadingSimulated)
        {
#if DEBUG
            if (sqlConnection == null) throw new ArgumentNullException(nameof(sqlConnection));
            if (dataTable == null) throw new ArgumentNullException(nameof(dataTable));
            if (tableName == null) throw new ArgumentNullException(nameof(tableName));
            if (timedColumnName == null) throw new ArgumentNullException(nameof(timedColumnName));
            if (timedColumnIndex < 0 || timedColumnIndex > dataTable.Columns.Count - 1) throw new IndexOutOfRangeException("timedColumnIndex is out of range");
            if (timedFieldConverter == null) throw new ArgumentNullException(nameof(timedFieldConverter));
            if (!dataTable.Columns[timedColumnIndex].ColumnName.Equals(timedColumnName)) throw new ArgumentException("name and index do not match");
#endif
            Id = id;
            DataTable = dataTable;
            InMem = inMem;
            TableName = tableName;
            TimedColumnName = timedColumnName;
            TimedFieldConverter = timedFieldConverter;
            LoadingSimulated = loadingSimulated;
            Closed = true;
            Reset(sqlConnection);
        }

        public int GetId()
        {
            return Id;
        }

        private bool Execute(long endOn)
        {
            if (Closed)
            {
                SqlConnection.Open();
                Closed = false;
            }
            var currentTime = GetNextTime();
            DataTable dataTable = null;
            if (!LoadingSimulated) dataTable = DataTable.Clone();
            int count = 0;
            while(HasCurrent && TimedFieldConverter.ToSystem(RowEnumerator.Current.Field<double>(TimedColumnName)) <= endOn)
            {
                if (!LoadingSimulated) dataTable.Rows.Add(RowEnumerator.Current.ItemArray);
                count++;
                HasCurrent = RowEnumerator.MoveNext();
            }
            if (count == 0) return false;
            if (!LoadingSimulated)
            {
                if (InMem) TableMem.FillTable(dataTable, SqlConnection, TableName);
                else TableDisk.FillTable(dataTable, SqlConnection, TableName);
                CurrentCount += dataTable.Rows.Count;
                var message = "Loaded " + dataTable.Rows.Count + " records now counting " + CurrentCount + "/" + DataTable.Rows.Count + " at time " + currentTime;
                var padding = "";
                if (Console.CursorTop == ConsoleCursorTop)
                {
                    Console.SetCursorPosition(0, ConsoleCursorTop - 1);
                    if (message.Length < ConsoleMessageLength) padding = new string(' ', ConsoleMessageLength - message.Length);
                }
                Console.WriteLine(message + padding);
                ConsoleCursorTop = Console.CursorTop;
                ConsoleMessageLength = message.Length;
                dataTable.Dispose();
            }
            return true;
        }

        public void Execute(IQueryable<R> queryable, long endOn)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            Execute(endOn);
        }

        public void Execute(PINQueryable<R> queryable, long endOn)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            Execute(endOn);
        }

        public void Execute(BKQueryable<RB> queryable, long endOn)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
#endif
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            Execute(endOn);
        }

        public bool HasNextTime()
        {
            return HasCurrent;
        }

        public long GetNextTime()
        {
            if (!HasNextTime()) throw new Exception("there is no NextTime");
            return TimedFieldConverter.ToSystem(RowEnumerator.Current.Field<double>(TimedColumnName));
        }

        public void Close()
        {
            if (!Closed)
            {
                SqlConnection.Close();
                Closed = true;
            }
        }

        public void Reset(SqlConnection sqlConnection)
        {
#if DEBUG
            if (sqlConnection == null) throw new ArgumentNullException(nameof(sqlConnection));
#endif
            Close();
            SqlConnection = sqlConnection;
            RowEnumerator = DataTable.AsEnumerable().OrderBy(x => x.Field<double>(TimedColumnName)).GetEnumerator();
            HasCurrent = RowEnumerator.MoveNext();
            Closed = true;
            ConsoleCursorTop = -1;
            ConsoleMessageLength = -1;
            CurrentCount = 0;
        }
    }
}
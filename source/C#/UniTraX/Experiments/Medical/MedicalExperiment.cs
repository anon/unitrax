﻿namespace UniTraX.Experiments.Medical
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using UniTraX.Datasets.Medical.DataAccess;
    using UniTraX.Datasets.Medical.DataLoading;
    using UniTraX.Datasets.Medical.Queries;
    using UniTraX.Infrastructure.BudgetProvider;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Infrastructure.DataLoading;
    using UniTraX.Infrastructure.ExperimentUtils;
    using UniTraX.Infrastructure.Provisioning;
    using UniTraX.Queries.Core;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class MedicalQueries
    {
        public AnalyticBaseProvider<Patient, PatientB> Provider { get; private set; }

        public MedicalQueries(long baseId, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long defaultEpsilon)
        {
#if DEBUG
            if (defaultEpsilon < 0L) throw new ArgumentException("defaultEpsilon is below zero");
#endif
            var factories = new List<AnalyticBaseFactory<Patient, PatientB>>(16)
            {

                new PatientsPerThrombosis(baseId + 1L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerSex(baseId + 2L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerDateOfBirth(baseId + 3L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerDateOfInitialDataCollection(baseId + 4L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerDateOfFirstVisit(baseId + 5L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerDateOfExamination(baseId + 6L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerAdmission(baseId + 7L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerAclIga(baseId + 8L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerAclIgg(baseId + 9L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerAclIgm(baseId + 10L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerAna(baseId + 11L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerKct(baseId + 12L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerRvvt(baseId + 13L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerLac(baseId + 14L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerSusp(baseId + 15L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new SevereThrombosisPatientsPerLaboratories(baseId + 16L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon)
            };
            Provider = new AnalyticBaseProvider<Patient, PatientB>(factories);
        }
    }

    public class MedicalExperiment
    {
        public static bool DiskOnly = false;
        public static bool RuntimeOnly = false;
        public static int Repeats = 8;
        public static int CleanThreshold = 500;
        public static long BaseId = 3300L;
        public static long DefaultEpsilon = DoubleTypeConverter.TwoDecimal.ToSystem(10000.0);
        public static string BaseDirName = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax", "experiments", "medical", "experiment");
        public static string InstanceName = BaseId + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff", CultureInfo.InvariantCulture);
        public static string InstanceDirName = Path.Combine(BaseDirName, InstanceName);
        public static string DataFileNamePatient = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "medical", "transformed", "patient.csv");
        public static string DataFileNameLaboratory = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "medical", "transformed", "laboratory.csv");
        public static double DefaultTotalBudget = 100000000.0;
        public static IBudgetProvider BudgetProvider = new ConstantBudgetProvider(DefaultTotalBudget);

        public static void SetupDatabaseMem(IExperimentServer server, IBudgetProvider budgetProvider)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
#endif
            server.Setup();
            server.CreateDatabaseInMem("medical-mem");
            MedicalMem.CreateTable(server.GetDatabase("medical-mem"), budgetProvider != null);
            var dataTablePatient = new DataTableProvider(DataFileNamePatient, budgetProvider, MedicalDisk.PatientColumnNames, "budget", new string[] { "diagnosis_general", "diagnosis_special", "symptoms" }).GetDataTable();
            var dataTableLaboratory = new DataTableProvider(DataFileNameLaboratory, budgetProvider, MedicalDisk.LaboratoryColumnNames, "budget", null).GetDataTable();
            var connection = server.GetSqlConnection("medical-mem");
            connection.Open();
            TableMem.FillTable(dataTablePatient, connection, "patient");
            TableMem.FillTable(dataTableLaboratory, connection, "laboratory");
            connection.Close();
            dataTablePatient.Dispose();
            dataTableLaboratory.Dispose();
        }

        public static void SetupDatabaseDisk(IExperimentServer server, IBudgetProvider budgetProvider)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
#endif
            server.Setup();
            server.CreateDatabaseOnDisk("medical-disk");
            MedicalDisk.CreateTable(server.GetDatabase("medical-disk"), budgetProvider != null);
            var dataTablePatient = new DataTableProvider(DataFileNamePatient, budgetProvider, MedicalDisk.PatientColumnNames, "budget", new string[] { "diagnosis_general", "diagnosis_special", "symptoms" }).GetDataTable();
            var dataTableLaboratory = new DataTableProvider(DataFileNameLaboratory, budgetProvider, MedicalDisk.LaboratoryColumnNames, "budget", null).GetDataTable();
            var connection = server.GetSqlConnection("medical-disk");
            connection.Open();
            TableDisk.FillTable(dataTablePatient, connection, "patient");
            TableDisk.FillTable(dataTableLaboratory, connection, "laboratory");
            connection.Close();
            dataTablePatient.Dispose();
            dataTableLaboratory.Dispose();
            MedicalDisk.CreateIndexes(server.GetDatabase("medical-disk"), budgetProvider != null);
            server.MakeDatabaseReadonly("medical-disk");
        }

        public static void Main(string[] args)
        {
#if RECORD
            var timerT = Recorder.StartTimer();
#endif
            Console.WriteLine("Experiment Medical: STARTED");
            var server = new ExampleDBServer();

            var queriesAlwaysClean = new MedicalQueries(BaseId, false, false, -1, DefaultEpsilon);
            var queriesAlwaysCleanEven = new MedicalQueries(BaseId, true, false, -1, DefaultEpsilon);
            var queriesThresholdClean = new MedicalQueries(BaseId, false, false, CleanThreshold, DefaultEpsilon);
            var queriesThresholdCleanEven = new MedicalQueries(BaseId, true, false, CleanThreshold, DefaultEpsilon);

            var queriesAlwaysCleanBudget = new MedicalQueries(BaseId, false, true, -1, DefaultEpsilon);
            var queriesAlwaysCleanEvenBudget = new MedicalQueries(BaseId, true, true, -1, DefaultEpsilon);
            var queriesThresholdCleanBudget = new MedicalQueries(BaseId, false, true, CleanThreshold, DefaultEpsilon);
            var queriesThresholdCleanEvenBudget = new MedicalQueries(BaseId, true, true, CleanThreshold, DefaultEpsilon);

            Metadata metadataMem = null;
            if (!DiskOnly) metadataMem = new Metadata(server.GetDatabaseConnectionString("medical-mem"));
            var metadataDisk = new Metadata(server.GetDatabaseConnectionString("medical-disk"));

            Directory.CreateDirectory(InstanceDirName);

            Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Runtime");
            Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Medical Disk-Always-BK-Runtime: STARTED");
            ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Runtime", server,
                new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                queriesAlwaysClean.Provider.GetQueriesBK(), "medical-disk", Repeats);
            Console.WriteLine("SubExperiment Medical Disk-Always-BK-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Even-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Runtime");
            Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Even-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Medical Disk-Always-BK-Even-Runtime: STARTED");
            ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Runtime", server,
                new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                queriesAlwaysCleanEven.Provider.GetQueriesBK(), "medical-disk", Repeats);
            Console.WriteLine("SubExperiment Medical Disk-Always-BK-Even-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Medical Disk-Direct-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, null);
            ExperimentUtils<Patient>.SetupExperimentDirect(InstanceDirName, "Disk-Direct-Runtime");
            Console.WriteLine("Setup SubExperiment Medical Disk-Direct-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Medical Disk-Direct-Runtime: STARTED");
            ExperimentUtils<Patient>.RepeatExperimentDirect(InstanceDirName, "Disk-Direct-Runtime", server,
                new Medical(server.GetDatabaseConnectionString("medical-disk")).Patients,
                queriesAlwaysClean.Provider.GetQueriesDirect(), "medical-disk", Repeats);
            Console.WriteLine("SubExperiment Medical Disk-Direct-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Medical Disk-PINQ-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, null);
            ExperimentUtils<Patient>.SetupExperimentPINQ(InstanceDirName, "Disk-PINQ-Runtime");
            Console.WriteLine("Setup SubExperiment Medical Disk-PINQ-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Medical Disk-PINQ-Runtime: STARTED");
            ExperimentUtils<Patient>.RepeatExperimentPINQ(InstanceDirName, "Disk-PINQ-Runtime", server,
                new PINQueryableFactory<Patient>(new Medical(server.GetDatabaseConnectionString("medical-disk")).Patients, DefaultTotalBudget),
                queriesAlwaysClean.Provider.GetQueriesPINQ(), DefaultTotalBudget, "medical-disk", Repeats);
            Console.WriteLine("SubExperiment Medical Disk-PINQ-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Runtime");
            Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Runtime: STARTED");
            ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Runtime", server,
                new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                queriesThresholdClean.Provider.GetQueriesBK(), "medical-disk", Repeats);
            Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Even-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Runtime");
            Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Even-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Even-Runtime: STARTED");
            ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Runtime", server,
                new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                queriesThresholdCleanEven.Provider.GetQueriesBK(), "medical-disk", Repeats);
            Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Even-Runtime: FINISHED");

            if (!RuntimeOnly)
            {
                Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Budget");
                Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Budget: FINISHED");
                Console.WriteLine("SubExperiment Medical Disk-Always-BK-Budget: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Budget", server,
                    new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                    queriesAlwaysCleanBudget.Provider.GetQueriesBK(), "medical-disk", Repeats);
                Console.WriteLine("SubExperiment Medical Disk-Always-BK-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Even-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Budget");
                Console.WriteLine("Setup SubExperiment Medical Disk-Always-BK-Even-Budget: FINISHED");
                Console.WriteLine("SubExperiment Medical Disk-Always-BK-Even-Budget: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Budget", server,
                    new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                    queriesAlwaysCleanEvenBudget.Provider.GetQueriesBK(), "medical-disk", Repeats);
                Console.WriteLine("SubExperiment Medical Disk-Always-BK-Even-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Disk-Direct-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, null);
                ExperimentUtils<Patient>.SetupExperimentDirect(InstanceDirName, "Disk-Direct-Budget");
                Console.WriteLine("Setup SubExperiment Medical Disk-Direct-Budget: FINISHED");
                Console.WriteLine("SubExperiment Medical Disk-Direct-Budget: STARTED");
                ExperimentUtils<Patient>.RepeatExperimentDirect(InstanceDirName, "Disk-Direct-Budget", server,
                    new Medical(server.GetDatabaseConnectionString("medical-disk")).Patients,
                    queriesAlwaysCleanBudget.Provider.GetQueriesDirect(), "medical-disk", Repeats);
                Console.WriteLine("SubExperiment Medical Disk-Direct-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Disk-PINQ-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, null);
                ExperimentUtils<Patient>.SetupExperimentPINQ(InstanceDirName, "Disk-PINQ-Budget");
                Console.WriteLine("Setup SubExperiment Medical Disk-PINQ-Budget: FINISHED");
                Console.WriteLine("SubExperiment Medical Disk-PINQ-Budget: STARTED");
                ExperimentUtils<Patient>.RepeatExperimentPINQ(InstanceDirName, "Disk-PINQ-Budget", server,
                    new PINQueryableFactory<Patient>(new Medical(server.GetDatabaseConnectionString("medical-disk")).Patients, DefaultTotalBudget),
                    queriesAlwaysCleanBudget.Provider.GetQueriesPINQ(), DefaultTotalBudget, "medical-disk", Repeats);
                Console.WriteLine("SubExperiment Medical Disk-PINQ-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Budget");
                Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Budget: FINISHED");
                Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Budget: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Budget", server,
                    new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                    queriesThresholdCleanBudget.Provider.GetQueriesBK(), "medical-disk", Repeats);
                Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Even-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Budget");
                Console.WriteLine("Setup SubExperiment Medical Disk-Threshold-BK-Even-Budget: FINISHED");
                Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Even-Budget: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Budget", server,
                    new BKQueryableFactory<PatientB>(metadataDisk.BoundedDataAccess),
                    queriesThresholdCleanEvenBudget.Provider.GetQueriesBK(), "medical-disk", Repeats);
                Console.WriteLine("SubExperiment Medical Disk-Threshold-BK-Even-Budget: FINISHED");
            }

            if (!DiskOnly)
            {
                Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Runtime");
                Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Medical Mem-Always-BK-Runtime: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Runtime", server,
                    new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                    queriesAlwaysClean.Provider.GetQueriesBK(), "medical-mem", Repeats);
                Console.WriteLine("SubExperiment Medical Mem-Always-BK-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Even-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Runtime");
                Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Even-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Medical Mem-Always-BK-Even-Runtime: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Runtime", server,
                    new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                    queriesAlwaysCleanEven.Provider.GetQueriesBK(), "medical-mem", Repeats);
                Console.WriteLine("SubExperiment Medical Mem-Always-BK-Even-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Mem-Direct-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, null);
                ExperimentUtils<Patient>.SetupExperimentDirect(InstanceDirName, "Mem-Direct-Runtime");
                Console.WriteLine("Setup SubExperiment Medical Mem-Direct-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Medical Mem-Direct-Runtime: STARTED");
                ExperimentUtils<Patient>.RepeatExperimentDirect(InstanceDirName, "Mem-Direct-Runtime", server,
                    new Medical(server.GetDatabaseConnectionString("medical-mem")).Patients,
                    queriesAlwaysClean.Provider.GetQueriesDirect(), "medical-mem", Repeats);
                Console.WriteLine("SubExperiment Medical Mem-Direct-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Mem-PINQ-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, null);
                ExperimentUtils<Patient>.SetupExperimentPINQ(InstanceDirName, "Mem-PINQ-Runtime");
                Console.WriteLine("Setup SubExperiment Medical Mem-PINQ-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Medical Mem-PINQ-Runtime: STARTED");
                ExperimentUtils<Patient>.RepeatExperimentPINQ(InstanceDirName, "Mem-PINQ-Runtime", server,
                    new PINQueryableFactory<Patient>(new Medical(server.GetDatabaseConnectionString("medical-mem")).Patients, DefaultTotalBudget),
                    queriesAlwaysClean.Provider.GetQueriesPINQ(), DefaultTotalBudget, "medical-mem", Repeats);
                Console.WriteLine("SubExperiment Medical Mem-PINQ-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Runtime");
                Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Runtime: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Runtime", server,
                    new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                    queriesThresholdClean.Provider.GetQueriesBK(), "medical-mem", Repeats);
                Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Even-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Runtime");
                Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Even-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Even-Runtime: STARTED");
                ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Runtime", server,
                    new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                    queriesThresholdCleanEven.Provider.GetQueriesBK(), "medical-mem", Repeats);
                Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Even-Runtime: FINISHED");

                if (!RuntimeOnly)
                {
                    Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Budget");
                    Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Medical Mem-Always-BK-Budget: STARTED");
                    ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Budget", server,
                        new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                        queriesAlwaysCleanBudget.Provider.GetQueriesBK(), "medical-mem", Repeats);
                    Console.WriteLine("SubExperiment Medical Mem-Always-BK-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Even-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Budget");
                    Console.WriteLine("Setup SubExperiment Medical Mem-Always-BK-Even-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Medical Mem-Always-BK-Even-Budget: STARTED");
                    ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Budget", server,
                        new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                        queriesAlwaysCleanEvenBudget.Provider.GetQueriesBK(), "medical-mem", Repeats);
                    Console.WriteLine("SubExperiment Medical Mem-Always-BK-Even-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Medical Mem-Direct-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, null);
                    ExperimentUtils<Patient>.SetupExperimentDirect(InstanceDirName, "Mem-Direct-Budget");
                    Console.WriteLine("Setup SubExperiment Medical Mem-Direct-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Medical Mem-Direct-Budget: STARTED");
                    ExperimentUtils<Patient>.RepeatExperimentDirect(InstanceDirName, "Mem-Direct-Budget", server,
                        new Medical(server.GetDatabaseConnectionString("medical-mem")).Patients,
                        queriesAlwaysCleanBudget.Provider.GetQueriesDirect(), "medical-mem", Repeats);
                    Console.WriteLine("SubExperiment Medical Mem-Direct-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Medical Mem-PINQ-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, null);
                    ExperimentUtils<Patient>.SetupExperimentPINQ(InstanceDirName, "Mem-PINQ-Budget");
                    Console.WriteLine("Setup SubExperiment Medical Mem-PINQ-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Medical Mem-PINQ-Budget: STARTED");
                    ExperimentUtils<Patient>.RepeatExperimentPINQ(InstanceDirName, "Mem-PINQ-Budget", server,
                        new PINQueryableFactory<Patient>(new Medical(server.GetDatabaseConnectionString("medical-mem")).Patients, DefaultTotalBudget),
                        queriesAlwaysCleanBudget.Provider.GetQueriesPINQ(), DefaultTotalBudget, "medical-mem", Repeats);
                    Console.WriteLine("SubExperiment Medical Mem-PINQ-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Budget");
                    Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Budget: STARTED");
                    ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Budget", server,
                        new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                        queriesThresholdCleanBudget.Provider.GetQueriesBK(), "medical-mem", Repeats);
                    Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Even-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<PatientB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Budget");
                    Console.WriteLine("Setup SubExperiment Medical Mem-Threshold-BK-Even-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Even-Budget: STARTED");
                    ExperimentUtils<PatientB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Budget", server,
                        new BKQueryableFactory<PatientB>(metadataMem.BoundedDataAccess),
                        queriesThresholdCleanEvenBudget.Provider.GetQueriesBK(), "medical-mem", Repeats);
                    Console.WriteLine("SubExperiment Medical Mem-Threshold-BK-Even-Budget: FINISHED");
                }
            }

#if RECORD
            var timeT = Recorder.StopTimer(timerT);
            Console.WriteLine("Took time ms: " + timeT);
#endif
            Console.WriteLine("Experiment Medical: FINISHED");
            Console.Read();
        }
    }
}
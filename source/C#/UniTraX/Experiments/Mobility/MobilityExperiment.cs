﻿namespace UniTraX.Experiments.Mobility
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using UniTraX.Datasets.Mobility.DataAccess;
    using UniTraX.Datasets.Mobility.DataLoading;
    using UniTraX.Datasets.Mobility.Queries;
    using UniTraX.Infrastructure.BudgetProvider;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Infrastructure.DataLoading;
    using UniTraX.Infrastructure.ExperimentUtils;
    using UniTraX.Infrastructure.Provisioning;
    using UniTraX.Queries.Core;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class MobilityQueries
    {
        public AnalyticBaseProvider<Ride, RideB> Provider { get; private set; }

        public MobilityQueries(long baseId, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long defaultEpsilon)
        {
#if DEBUG
            if (defaultEpsilon < 0L) throw new ArgumentException("defaultEpsilon is below zero");
#endif
            var factories = new List<AnalyticBaseFactory<Ride, RideB>>(8)
            {
                new RidesPerPickupDateTime(baseId + 1L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new RidesPerTotalAmount(baseId + 2L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new RidesPerTipAmount(baseId + 3L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new RidesPerPassenegerCount(baseId + 4L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new RidesPerTripTimeInSecs(baseId + 5L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new RidesPerTripDistance(baseId + 6L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new LocalInvestigation1(baseId + 7L, useEvenly, budgetOverRuntime, cleanThreshold, new long[3] { defaultEpsilon, defaultEpsilon, defaultEpsilon }),
                new LocalInvestigation2(baseId + 8L, useEvenly, budgetOverRuntime, cleanThreshold, new long[2] { defaultEpsilon, defaultEpsilon })
            };
            Provider = new AnalyticBaseProvider<Ride, RideB>(factories);
        }
    }

    public class MobilityExperiment
    {
        public static bool DiskOnly = false;
        public static bool RuntimeOnly = false;
        public static int Repeats = 8;
        public static int CleanThreshold = 500;
        public static long BaseId = 1100L;
        public static long DefaultEpsilon = DoubleTypeConverter.TwoDecimal.ToSystem(10000.0);
        public static string BaseDirName = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax", "experiments", "mobility", "experiment");
        public static string InstanceName = BaseId + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff", CultureInfo.InvariantCulture);
        public static string InstanceDirName = Path.Combine(BaseDirName, InstanceName);
        public static string DataFileName = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "mobility", "transformed", "rides.csv");
        public static double DefaultTotalBudget = 100000000.0;
        public static IBudgetProvider BudgetProvider = new ConstantBudgetProvider(DefaultTotalBudget);

        public static void SetupDatabaseMem(IExperimentServer server, IBudgetProvider budgetProvider)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
#endif
            server.Setup();
            server.CreateDatabaseInMem("mobility-mem");
            MobilityMem.CreateTable(server.GetDatabase("mobility-mem"), budgetProvider != null);
            var dataTable = new DataTableProvider(DataFileName, budgetProvider, MobilityDisk.ColumnNames, "budget", null).GetDataTable();
            var connection = server.GetSqlConnection("mobility-mem");
            connection.Open();
            TableMem.FillTable(dataTable, connection, "rides");
            connection.Close();
            dataTable.Dispose();
        }

        public static void SetupDatabaseDisk(IExperimentServer server, IBudgetProvider budgetProvider)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
#endif
            server.Setup();
            server.CreateDatabaseOnDisk("mobility-disk");
            MobilityDisk.CreateTable(server.GetDatabase("mobility-disk"), budgetProvider != null);
            var dataTable = new DataTableProvider(DataFileName, budgetProvider, MobilityDisk.ColumnNames, "budget", null).GetDataTable();
            var connection = server.GetSqlConnection("mobility-disk");
            connection.Open();
            TableDisk.FillTable(dataTable, connection, "rides");
            connection.Close();
            dataTable.Dispose();
            MobilityDisk.CreateIndexes(server.GetDatabase("mobility-disk"), budgetProvider != null);
            server.MakeDatabaseReadonly("mobility-disk");
        }

        public static void Main(string[] args)
        {
#if RECORD
            var timerT = Recorder.StartTimer();
#endif
            Console.WriteLine("Experiment Mobility Original: STARTED");
            var server = new ExampleDBServer();

            var queriesAlwaysClean = new MobilityQueries(BaseId, false, false, -1, DefaultEpsilon);
            var queriesAlwaysCleanEven = new MobilityQueries(BaseId, true, false, -1, DefaultEpsilon);
            var queriesThresholdClean = new MobilityQueries(BaseId, false, false, CleanThreshold, DefaultEpsilon);
            var queriesThresholdCleanEven = new MobilityQueries(BaseId, true, false, CleanThreshold, DefaultEpsilon);

            var queriesAlwaysCleanBudget = new MobilityQueries(BaseId, false, true, -1, DefaultEpsilon);
            var queriesAlwaysCleanEvenBudget = new MobilityQueries(BaseId, true, true, -1, DefaultEpsilon);
            var queriesThresholdCleanBudget = new MobilityQueries(BaseId, false, true, CleanThreshold, DefaultEpsilon);
            var queriesThresholdCleanEvenBudget = new MobilityQueries(BaseId, true, true, CleanThreshold, DefaultEpsilon);

            Metadata metadataMem = null;
            if (!DiskOnly) metadataMem = new Metadata(server.GetDatabaseConnectionString("mobility-mem"));
            var metadataDisk = new Metadata(server.GetDatabaseConnectionString("mobility-disk"));

            Directory.CreateDirectory(InstanceDirName);

            if (!RuntimeOnly)
            {
                Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Budget");
                Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Budget: FINISHED");
                Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Budget: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Budget", server,
                    new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                    queriesAlwaysCleanBudget.Provider.GetQueriesBK(), "mobility-disk", Repeats);
                Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Even-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Budget");
                Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Even-Budget: FINISHED");
                Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Even-Budget: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Budget", server,
                    new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                    queriesAlwaysCleanEvenBudget.Provider.GetQueriesBK(), "mobility-disk", Repeats);
                Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Even-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Disk-Direct-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, null);
                ExperimentUtils<Ride>.SetupExperimentDirect(InstanceDirName, "Disk-Direct-Budget");
                Console.WriteLine("Setup SubExperiment Mobility Disk-Direct-Budget: FINISHED");
                Console.WriteLine("SubExperiment Mobility Disk-Direc-Budget: STARTED");
                ExperimentUtils<Ride>.RepeatExperimentDirect(InstanceDirName, "Disk-Direct-Budget", server,
                    new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides,
                    queriesAlwaysCleanBudget.Provider.GetQueriesDirect(), "mobility-disk", Repeats);
                Console.WriteLine("SubExperiment Mobility Disk-Direct-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Disk-PINQ-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, null);
                ExperimentUtils<Ride>.SetupExperimentPINQ(InstanceDirName, "Disk-PINQ-Budget");
                Console.WriteLine("Setup SubExperiment Mobility Disk-PINQ-Budget: FINISHED");
                Console.WriteLine("SubExperiment Mobility Disk-PINQ-Budget: STARTED");
                ExperimentUtils<Ride>.RepeatExperimentPINQ(InstanceDirName, "Disk-PINQ-Budget", server,
                    new PINQueryableFactory<Ride>(new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides, DefaultTotalBudget),
                    queriesAlwaysCleanBudget.Provider.GetQueriesPINQ(), DefaultTotalBudget, "mobility-disk", Repeats);
                Console.WriteLine("SubExperiment Mobility Disk-PINQ-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Budget");
                Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Budget: FINISHED");
                Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Budget: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Budget", server,
                    new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                    queriesThresholdCleanBudget.Provider.GetQueriesBK(), "mobility-disk", Repeats);
                Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Even-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Budget");
                Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Even-Budget: FINISHED");
                Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Even-Budget: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Budget", server,
                    new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                    queriesThresholdCleanEvenBudget.Provider.GetQueriesBK(), "mobility-disk", Repeats);
                Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Even-Budget: FINISHED");
            }

            Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Runtime");
            Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Runtime: STARTED");
            ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Runtime", server,
                new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                queriesAlwaysClean.Provider.GetQueriesBK(), "mobility-disk", Repeats);
            Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Even-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Runtime");
            Console.WriteLine("Setup SubExperiment Mobility Disk-Always-BK-Even-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Even-Runtime: STARTED");
            ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Runtime", server,
                new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                queriesAlwaysCleanEven.Provider.GetQueriesBK(), "mobility-disk", Repeats);
            Console.WriteLine("SubExperiment Mobility Disk-Always-BK-Even-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Mobility Disk-Direct-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, null);
            ExperimentUtils<Ride>.SetupExperimentDirect(InstanceDirName, "Disk-Direct-Runtime");
            Console.WriteLine("Setup SubExperiment Mobility Disk-Direct-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Mobility Disk-Direc-Runtime: STARTED");
            ExperimentUtils<Ride>.RepeatExperimentDirect(InstanceDirName, "Disk-Direct-Runtime", server,
                new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides,
                queriesAlwaysClean.Provider.GetQueriesDirect(), "mobility-disk", Repeats);
            Console.WriteLine("SubExperiment Mobility Disk-Direct-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Mobility Disk-PINQ-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, null);
            ExperimentUtils<Ride>.SetupExperimentPINQ(InstanceDirName, "Disk-PINQ-Runtime");
            Console.WriteLine("Setup SubExperiment Mobility Disk-PINQ-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Mobility Disk-PINQ-Runtime: STARTED");
            ExperimentUtils<Ride>.RepeatExperimentPINQ(InstanceDirName, "Disk-PINQ-Runtime", server,
                new PINQueryableFactory<Ride>(new Mobility(server.GetDatabaseConnectionString("mobility-disk")).Rides, DefaultTotalBudget),
                queriesAlwaysClean.Provider.GetQueriesPINQ(), DefaultTotalBudget, "mobility-disk", Repeats);
            Console.WriteLine("SubExperiment Mobility Disk-PINQ-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Runtime");
            Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Runtime: STARTED");
            ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Runtime", server,
                new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                queriesThresholdClean.Provider.GetQueriesBK(), "mobility-disk", Repeats);
            Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Even-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Runtime");
            Console.WriteLine("Setup SubExperiment Mobility Disk-Threshold-BK-Even-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Even-Runtime: STARTED");
            ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Runtime", server,
                new BKQueryableFactory<RideB>(metadataDisk.BoundedDataAccess),
                queriesThresholdCleanEven.Provider.GetQueriesBK(), "mobility-disk", Repeats);
            Console.WriteLine("SubExperiment Mobility Disk-Threshold-BK-Even-Runtime: FINISHED");


            if (!DiskOnly)
            {
                Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Runtime");
                Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Runtime: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Runtime", server,
                    new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                    queriesAlwaysClean.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Even-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Runtime");
                Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Even-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Even-Runtime: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Runtime", server,
                    new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                    queriesAlwaysCleanEven.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Even-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Mem-Direct-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, null);
                ExperimentUtils<Ride>.SetupExperimentDirect(InstanceDirName, "Mem-Direct-Runtime");
                Console.WriteLine("Setup SubExperiment Mobility Mem-Direct-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Mobility Mem-Direct-Runtime: STARTED");
                ExperimentUtils<Ride>.RepeatExperimentDirect(InstanceDirName, "Mem-Direct-Runtime", server,
                    new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides,
                    queriesAlwaysClean.Provider.GetQueriesDirect(), "mobility-mem", Repeats);
                Console.WriteLine("SubExperiment Mobility Mem-Direct-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Mem-PINQ-Runtime-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, null);
                ExperimentUtils<Ride>.SetupExperimentPINQ(InstanceDirName, "Mem-PINQ-Runtime");
                Console.WriteLine("Setup SubExperiment Mobility Mem-PINQ-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Mobility Mem-PINQ-Runtime: STARTED");
                ExperimentUtils<Ride>.RepeatExperimentPINQ(InstanceDirName, "Mem-PINQ-Runtime", server,
                    new PINQueryableFactory<Ride>(new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides, DefaultTotalBudget),
                    queriesAlwaysClean.Provider.GetQueriesPINQ(), DefaultTotalBudget, "mobility-mem", Repeats);
                Console.WriteLine("SubExperiment Mobility Mem-PINQ-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Runtime");
                Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Runtime: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Runtime", server,
                    new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                    queriesThresholdClean.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Even-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Runtime");
                Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Even-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Even-Runtime: STARTED");
                ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Runtime", server,
                    new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                    queriesThresholdCleanEven.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Even-Runtime: FINISHED");

                if (!RuntimeOnly)
                {
                    Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Budget");
                    Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Budget: STARTED");
                    ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Budget", server,
                        new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                        queriesAlwaysCleanBudget.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                    Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Even-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Budget");
                    Console.WriteLine("Setup SubExperiment Mobility Mem-Always-BK-Even-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Even-Budget: STARTED");
                    ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Budget", server,
                        new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                        queriesAlwaysCleanEvenBudget.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                    Console.WriteLine("SubExperiment Mobility Mem-Always-BK-Even-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Mobility Mem-Direct-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, null);
                    ExperimentUtils<Ride>.SetupExperimentDirect(InstanceDirName, "Mem-Direct-Budget");
                    Console.WriteLine("Setup SubExperiment Mobility Mem-Direct-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Mobility Mem-Direct-Budget: STARTED");
                    ExperimentUtils<Ride>.RepeatExperimentDirect(InstanceDirName, "Mem-Direct-Budget", server,
                        new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides,
                        queriesAlwaysCleanBudget.Provider.GetQueriesDirect(), "mobility-mem", Repeats);
                    Console.WriteLine("SubExperiment Mobility Mem-Direct-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Mobility Mem-PINQ-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, null);
                    ExperimentUtils<Ride>.SetupExperimentPINQ(InstanceDirName, "Mem-PINQ-Budget");
                    Console.WriteLine("Setup SubExperiment Mobility Mem-PINQ-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Mobility Mem-PINQ-Budget: STARTED");
                    ExperimentUtils<Ride>.RepeatExperimentPINQ(InstanceDirName, "Mem-PINQ-Budget", server,
                        new PINQueryableFactory<Ride>(new Mobility(server.GetDatabaseConnectionString("mobility-mem")).Rides, DefaultTotalBudget),
                        queriesAlwaysCleanBudget.Provider.GetQueriesPINQ(), DefaultTotalBudget, "mobility-mem", Repeats);
                    Console.WriteLine("SubExperiment Mobility Mem-PINQ-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Budget");
                    Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Budget: STARTED");
                    ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Budget", server,
                        new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                        queriesThresholdCleanBudget.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                    Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Even-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<RideB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Budget");
                    Console.WriteLine("Setup SubExperiment Mobility Mem-Threshold-BK-Even-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Even-Budget: STARTED");
                    ExperimentUtils<RideB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Budget", server,
                        new BKQueryableFactory<RideB>(metadataMem.BoundedDataAccess),
                        queriesThresholdCleanEvenBudget.Provider.GetQueriesBK(), "mobility-mem", Repeats);
                    Console.WriteLine("SubExperiment Mobility Mem-Threshold-BK-Even-Budget: FINISHED");
                }
            }

#if RECORD
            var timeT = Recorder.StopTimer(timerT);
            Console.WriteLine("Took time ms: " + timeT);
#endif
            Console.WriteLine("Experiment Mobility Original: FINISHED");
            Console.Read();
        }
    }
}
﻿namespace UniTraX.Experiments.Financial
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using UniTraX.Datasets.Financial.DataAccess;
    using UniTraX.Datasets.Financial.DataLoading;
    using UniTraX.Datasets.Financial.Queries;
    using UniTraX.Infrastructure.BudgetProvider;
    using UniTraX.Infrastructure.DataConversion;
    using UniTraX.Infrastructure.DataLoading;
    using UniTraX.Infrastructure.ExperimentUtils;
    using UniTraX.Infrastructure.Provisioning;
    using UniTraX.Queries.Core;
#if RECORD
    using UniTraX.Queries.Recording;
#endif

    public class FinancialQueries
    {
        public AnalyticBaseProvider<Account, AccountB> Provider { get; private set; }

        public FinancialQueries(long baseId, bool useEvenly, bool budgetOverRuntime, int cleanThreshold, long defaultEpsilon)
        {
#if DEBUG
            if (defaultEpsilon < 0L) throw new ArgumentException("defaultEpsilon is below zero");
#endif
            var factories = new List<AnalyticBaseFactory<Account, AccountB>>(8)
            {
                new AccountsPerOwnerSex(baseId + 1L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new FemaleAccountsPerAccountDistrict(baseId + 2L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new FemaleAccountsPerAccountDate(baseId + 3L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new FemaleAccountsPerAccountBalance(baseId + 4L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new FemaleAccountsPerOwnerBirthdate(baseId + 5L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new FemaleAccountsPerLoanAmount(baseId + 6L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon),
                new LocalInvestigation(baseId + 7L, useEvenly, budgetOverRuntime, cleanThreshold, new long[] { defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon, defaultEpsilon }),
                new FemaleAccountsPerTransactionsAmount(baseId + 8L, useEvenly, budgetOverRuntime, cleanThreshold, defaultEpsilon)
            };
            Provider = new AnalyticBaseProvider<Account, AccountB>(factories);
        }
    }

    public class FinancialExperiment
    {
        public static bool DiskOnly = false;
        public static bool RuntimeOnly = false;
        public static int Repeats = 8;
        public static int CleanThreshold = 500;
        public static long BaseId = 2200L;
        public static long DefaultEpsilon = DoubleTypeConverter.TwoDecimal.ToSystem(10000.0);
        public static string BaseDirName = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax", "experiments", "financial", "experiment");
        public static string InstanceName = BaseId + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff", CultureInfo.InvariantCulture);
        public static string InstanceDirName = Path.Combine(BaseDirName, InstanceName);
        public static string DataFileNameDistrict = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "financial", "transformed", "district.csv");
        public static string DataFileNameAccount = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "financial", "transformed", "account.csv");
        public static string DataFileNameOrder = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "financial", "transformed", "order.csv");
        public static string DataFileNameTransaction = Path.Combine("C:" + Path.DirectorySeparatorChar, "Users", "User", "Documents", "unitrax-data", "financial", "transformed", "transaction.csv");
        public static double DefaultTotalBudget = 100000000.0;
        public static IBudgetProvider BudgetProvider = new ConstantBudgetProvider(DefaultTotalBudget);

        public static void SetupDatabaseMem(IExperimentServer server, IBudgetProvider budgetProvider)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
#endif
            server.Setup();
            server.CreateDatabaseInMem("financial-mem");
            FinancialMem.CreateTable(server.GetDatabase("financial-mem"), budgetProvider != null);
            var dataTableDistrict = new DataTableProvider(DataFileNameDistrict, budgetProvider, FinancialDisk.DistrictColumnNames, "budget", null).GetDataTable();
            var dataTableAccount = new DataTableProvider(DataFileNameAccount, budgetProvider, FinancialDisk.AccountColumnNames, "budget", null).GetDataTable();
            var dataTableOrder = new DataTableProvider(DataFileNameOrder, budgetProvider, FinancialDisk.OrderColumnNames, "budget", null).GetDataTable();
            var dataTableTransaction = new DataTableProvider(DataFileNameTransaction, budgetProvider, FinancialDisk.TransactionColumnNames, "budget", null).GetDataTable();
            var connection = server.GetSqlConnection("financial-mem");
            connection.Open();
            TableMem.FillTable(dataTableDistrict, connection, "district");
            TableMem.FillTable(dataTableAccount, connection, "account");
            TableMem.FillTable(dataTableOrder, connection, "order");
            TableMem.FillTable(dataTableTransaction, connection, "transaction");
            connection.Close();
            dataTableDistrict.Dispose();
            dataTableAccount.Dispose();
            dataTableOrder.Dispose();
            dataTableTransaction.Dispose();
        }

        public static void SetupDatabaseDisk(IExperimentServer server, IBudgetProvider budgetProvider)
        {
#if DEBUG
            if (server == null) throw new ArgumentNullException(nameof(server));
#endif
            server.Setup();
            server.CreateDatabaseOnDisk("financial-disk");
            FinancialDisk.CreateTable(server.GetDatabase("financial-disk"), budgetProvider != null);
            var dataTableDistrict = new DataTableProvider(DataFileNameDistrict, budgetProvider, FinancialDisk.DistrictColumnNames, "budget", null).GetDataTable();
            var dataTableAccount = new DataTableProvider(DataFileNameAccount, budgetProvider, FinancialDisk.AccountColumnNames, "budget", null).GetDataTable();
            var dataTableOrder = new DataTableProvider(DataFileNameOrder, budgetProvider, FinancialDisk.OrderColumnNames, "budget", null).GetDataTable();
            var dataTableTransaction = new DataTableProvider(DataFileNameTransaction, budgetProvider, FinancialDisk.TransactionColumnNames, "budget", null).GetDataTable();
            var connection = server.GetSqlConnection("financial-disk");
            connection.Open();
            TableDisk.FillTable(dataTableDistrict, connection, "district");
            TableDisk.FillTable(dataTableAccount, connection, "account");
            TableDisk.FillTable(dataTableOrder, connection, "order");
            TableDisk.FillTable(dataTableTransaction, connection, "transaction");
            connection.Close();
            dataTableDistrict.Dispose();
            dataTableAccount.Dispose();
            dataTableOrder.Dispose();
            dataTableTransaction.Dispose();
            FinancialDisk.CreateIndexes(server.GetDatabase("financial-disk"), budgetProvider != null);
            server.MakeDatabaseReadonly("financial-disk");
        }

        public static void Main(string[] args)
        {
#if RECORD
            var timerT = Recorder.StartTimer();
#endif
            Console.WriteLine("Experiment Financial: STARTED");
            var server = new ExampleDBServer();

            var queriesAlwaysClean = new FinancialQueries(BaseId, false, false, -1, DefaultEpsilon);
            var queriesAlwaysCleanEven = new FinancialQueries(BaseId, true, false, -1, DefaultEpsilon);
            var queriesThresholdClean = new FinancialQueries(BaseId, false, false, CleanThreshold, DefaultEpsilon);
            var queriesThresholdCleanEven = new FinancialQueries(BaseId, true, false, CleanThreshold, DefaultEpsilon);

            var queriesAlwaysCleanBudget = new FinancialQueries(BaseId, false, true, -1, DefaultEpsilon);
            var queriesAlwaysCleanEvenBudget = new FinancialQueries(BaseId, true, true, -1, DefaultEpsilon);
            var queriesThresholdCleanBudget = new FinancialQueries(BaseId, false, true, CleanThreshold, DefaultEpsilon);
            var queriesThresholdCleanEvenBudget = new FinancialQueries(BaseId, true, true, CleanThreshold, DefaultEpsilon);

            Metadata metadataMem = null;
            if (!DiskOnly) metadataMem = new Metadata(server.GetDatabaseConnectionString("financial-mem"));
            var metadataDisk = new Metadata(server.GetDatabaseConnectionString("financial-disk"));

            Directory.CreateDirectory(InstanceDirName);

            Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Runtime");
            Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Financial Disk-Always-BK-Runtime: STARTED");
            ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Runtime", server,
                new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                queriesAlwaysClean.Provider.GetQueriesBK(), "financial-disk", Repeats);
            Console.WriteLine("SubExperiment Financial Disk-Always-BK-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Even-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Runtime");
            Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Even-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Financial Disk-Always-BK-Even-Runtime: STARTED");
            ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Runtime", server,
                new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                queriesAlwaysCleanEven.Provider.GetQueriesBK(), "financial-disk", Repeats);
            Console.WriteLine("SubExperiment Financial Disk-Always-BK-Even-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Financial Disk-Direct-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, null);
            ExperimentUtils<Account>.SetupExperimentDirect(InstanceDirName, "Disk-Direct-Runtime");
            Console.WriteLine("Setup SubExperiment Financial Disk-Direct-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Financial Disk-Direct-Runtime: STARTED");
            ExperimentUtils<Account>.RepeatExperimentDirect(InstanceDirName, "Disk-Direct-Runtime", server,
                new Financial(server.GetDatabaseConnectionString("financial-disk")).Accounts,
                queriesAlwaysClean.Provider.GetQueriesDirect(), "financial-disk", Repeats);
            Console.WriteLine("SubExperiment Financial Disk-Direct-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Financial Disk-PINQ-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, null);
            ExperimentUtils<Account>.SetupExperimentPINQ(InstanceDirName, "Disk-PINQ-Runtime");
            Console.WriteLine("Setup SubExperiment Financial Disk-PINQ-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Financial Disk-PINQ-Runtime: STARTED");
            ExperimentUtils<Account>.RepeatExperimentPINQ(InstanceDirName, "Disk-PINQ-Runtime", server,
                new PINQueryableFactory<Account>(new Financial(server.GetDatabaseConnectionString("financial-disk")).Accounts, DefaultTotalBudget),
                queriesAlwaysClean.Provider.GetQueriesPINQ(), DefaultTotalBudget, "financial-disk", Repeats);
            Console.WriteLine("SubExperiment Financial Disk-PINQ-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Runtime");
            Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Runtime: STARTED");
            ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Runtime", server,
                new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                queriesThresholdClean.Provider.GetQueriesBK(), "financial-disk", Repeats);
            Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Runtime: FINISHED");

            Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Even-Runtime: STARTED");
            server.Wipe();
            SetupDatabaseDisk(server, BudgetProvider);
            ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Runtime");
            Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Even-Runtime: FINISHED");
            Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Even-Runtime: STARTED");
            ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Runtime", server,
                new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                queriesThresholdCleanEven.Provider.GetQueriesBK(), "financial-disk", Repeats);
            Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Even-Runtime: FINISHED");

            if (!RuntimeOnly)
            {
                Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Budget");
                Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Budget: FINISHED");
                Console.WriteLine("SubExperiment Financial Disk-Always-BK-Budget: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Budget", server,
                    new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                    queriesAlwaysCleanBudget.Provider.GetQueriesBK(), "financial-disk", Repeats);
                Console.WriteLine("SubExperiment Financial Disk-Always-BK-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Even-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Budget");
                Console.WriteLine("Setup SubExperiment Financial Disk-Always-BK-Even-Budget: FINISHED");
                Console.WriteLine("SubExperiment Financial Disk-Always-BK-Even-Budget: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Always-BK-Even-Budget", server,
                    new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                    queriesAlwaysCleanEvenBudget.Provider.GetQueriesBK(), "financial-disk", Repeats);
                Console.WriteLine("SubExperiment Financial Disk-Always-BK-Even-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Disk-Direct-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, null);
                ExperimentUtils<Account>.SetupExperimentDirect(InstanceDirName, "Disk-Direct-Budget");
                Console.WriteLine("Setup SubExperiment Financial Disk-Direct-Budget: FINISHED");
                Console.WriteLine("SubExperiment Financial Disk-Direct-Budget: STARTED");
                ExperimentUtils<Account>.RepeatExperimentDirect(InstanceDirName, "Disk-Direct-Budget", server,
                    new Financial(server.GetDatabaseConnectionString("financial-disk")).Accounts,
                    queriesAlwaysCleanBudget.Provider.GetQueriesDirect(), "financial-disk", Repeats);
                Console.WriteLine("SubExperiment Financial Disk-Direct-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Disk-PINQ-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, null);
                ExperimentUtils<Account>.SetupExperimentPINQ(InstanceDirName, "Disk-PINQ-Budget");
                Console.WriteLine("Setup SubExperiment Financial Disk-PINQ-Budget: FINISHED");
                Console.WriteLine("SubExperiment Financial Disk-PINQ-Budget: STARTED");
                ExperimentUtils<Account>.RepeatExperimentPINQ(InstanceDirName, "Disk-PINQ-Budget", server,
                    new PINQueryableFactory<Account>(new Financial(server.GetDatabaseConnectionString("financial-disk")).Accounts, DefaultTotalBudget),
                    queriesAlwaysCleanBudget.Provider.GetQueriesPINQ(), DefaultTotalBudget, "financial-disk", Repeats);
                Console.WriteLine("SubExperiment Financial Disk-PINQ-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Budget");
                Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Budget: FINISHED");
                Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Budget: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Budget", server,
                    new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                    queriesThresholdCleanBudget.Provider.GetQueriesBK(), "financial-disk", Repeats);
                Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Budget: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Even-Budget: STARTED");
                server.Wipe();
                SetupDatabaseDisk(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Budget");
                Console.WriteLine("Setup SubExperiment Financial Disk-Threshold-BK-Even-Budget: FINISHED");
                Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Even-Budget: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Disk-Threshold-BK-Even-Budget", server,
                    new BKQueryableFactory<AccountB>(metadataDisk.BoundedDataAccess),
                    queriesThresholdCleanEvenBudget.Provider.GetQueriesBK(), "financial-disk", Repeats);
                Console.WriteLine("SubExperiment Financial Disk-Threshold-BK-Even-Budget: FINISHED");
            }

            if (!DiskOnly)
            {
                Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Runtime");
                Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Financial Mem-Always-BK-Runtime: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Runtime", server,
                    new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                    queriesAlwaysClean.Provider.GetQueriesBK(), "financial-mem", Repeats);
                Console.WriteLine("SubExperiment Financial Mem-Always-BK-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Even-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Runtime");
                Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Even-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Financial Mem-Always-BK-Even-Runtime: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Runtime", server,
                    new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                    queriesAlwaysCleanEven.Provider.GetQueriesBK(), "financial-mem", Repeats);
                Console.WriteLine("SubExperiment Financial Mem-Always-BK-Even-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Mem-Direct-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, null);
                ExperimentUtils<Account>.SetupExperimentDirect(InstanceDirName, "Mem-Direct-Runtime");
                Console.WriteLine("Setup SubExperiment Financial Mem-Direct-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Financial Mem-Direct-Runtime: STARTED");
                ExperimentUtils<Account>.RepeatExperimentDirect(InstanceDirName, "Mem-Direct-Runtime", server,
                    new Financial(server.GetDatabaseConnectionString("financial-mem")).Accounts,
                    queriesAlwaysClean.Provider.GetQueriesDirect(), "financial-mem", Repeats);
                Console.WriteLine("SubExperiment Financial Mem-Direct-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Mem-PINQ-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, null);
                ExperimentUtils<Account>.SetupExperimentPINQ(InstanceDirName, "Mem-PINQ-Runtime");
                Console.WriteLine("Setup SubExperiment Financial Mem-PINQ-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Financial Mem-PINQ-Runtime: STARTED");
                ExperimentUtils<Account>.RepeatExperimentPINQ(InstanceDirName, "Mem-PINQ-Runtime", server,
                    new PINQueryableFactory<Account>(new Financial(server.GetDatabaseConnectionString("financial-mem")).Accounts, DefaultTotalBudget),
                    queriesAlwaysClean.Provider.GetQueriesPINQ(), DefaultTotalBudget, "financial-mem", Repeats);
                Console.WriteLine("SubExperiment Financial Mem-PINQ-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Runtime");
                Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Runtime: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Runtime", server,
                    new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                    queriesThresholdClean.Provider.GetQueriesBK(), "financial-mem", Repeats);
                Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Runtime: FINISHED");

                Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Even-Runtime: STARTED");
                server.Wipe();
                SetupDatabaseMem(server, BudgetProvider);
                ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Runtime");
                Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Even-Runtime: FINISHED");
                Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Even-Runtime: STARTED");
                ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Runtime", server,
                    new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                    queriesThresholdCleanEven.Provider.GetQueriesBK(), "financial-mem", Repeats);
                Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Even-Runtime: FINISHED");

                if (!RuntimeOnly)
                {
                    Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Budget");
                    Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Financial Mem-Always-BK-Budget: STARTED");
                    ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Budget", server,
                        new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                        queriesAlwaysCleanBudget.Provider.GetQueriesBK(), "financial-mem", Repeats);
                    Console.WriteLine("SubExperiment Financial Mem-Always-BK-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Even-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Budget");
                    Console.WriteLine("Setup SubExperiment Financial Mem-Always-BK-Even-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Financial Mem-Always-BK-Even-Budget: STARTED");
                    ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Always-BK-Even-Budget", server,
                        new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                        queriesAlwaysCleanEvenBudget.Provider.GetQueriesBK(), "financial-mem", Repeats);
                    Console.WriteLine("SubExperiment Financial Mem-Always-BK-Even-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Financial Mem-Direct-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, null);
                    ExperimentUtils<Account>.SetupExperimentDirect(InstanceDirName, "Mem-Direct-Budget");
                    Console.WriteLine("Setup SubExperiment Financial Mem-Direct-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Financial Mem-Direct-Budget: STARTED");
                    ExperimentUtils<Account>.RepeatExperimentDirect(InstanceDirName, "Mem-Direct-Budget", server,
                        new Financial(server.GetDatabaseConnectionString("financial-mem")).Accounts,
                        queriesAlwaysCleanBudget.Provider.GetQueriesDirect(), "financial-mem", Repeats);
                    Console.WriteLine("SubExperiment Financial Mem-Direct-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Financial Mem-PINQ-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, null);
                    ExperimentUtils<Account>.SetupExperimentPINQ(InstanceDirName, "Mem-PINQ-Budget");
                    Console.WriteLine("Setup SubExperiment Financial Mem-PINQ-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Financial Mem-PINQ-Budget: STARTED");
                    ExperimentUtils<Account>.RepeatExperimentPINQ(InstanceDirName, "Mem-PINQ-Budget", server,
                        new PINQueryableFactory<Account>(new Financial(server.GetDatabaseConnectionString("financial-mem")).Accounts, DefaultTotalBudget),
                        queriesAlwaysCleanBudget.Provider.GetQueriesPINQ(), DefaultTotalBudget, "financial-mem", Repeats);
                    Console.WriteLine("SubExperiment Financial Mem-PINQ-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Budget");
                    Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Budget: STARTED");
                    ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Budget", server,
                        new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                        queriesThresholdCleanBudget.Provider.GetQueriesBK(), "financial-mem", Repeats);
                    Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Budget: FINISHED");

                    Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Even-Budget: STARTED");
                    server.Wipe();
                    SetupDatabaseMem(server, BudgetProvider);
                    ExperimentUtils<AccountB>.SetupExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Budget");
                    Console.WriteLine("Setup SubExperiment Financial Mem-Threshold-BK-Even-Budget: FINISHED");
                    Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Even-Budget: STARTED");
                    ExperimentUtils<AccountB>.RepeatExperimentBK(InstanceDirName, "Mem-Threshold-BK-Even-Budget", server,
                        new BKQueryableFactory<AccountB>(metadataMem.BoundedDataAccess),
                        queriesThresholdCleanEvenBudget.Provider.GetQueriesBK(), "financial-mem", Repeats);
                    Console.WriteLine("SubExperiment Financial Mem-Threshold-BK-Even-Budget: FINISHED");
                }
            }

#if RECORD
            var timeT = Recorder.StopTimer(timerT);
            Console.WriteLine("Took time ms: " + timeT);
#endif
            Console.WriteLine("Experiment Financial: FINISHED");
            Console.Read();
        }
    }
}
﻿namespace UniTraX.Core.BoundedData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.History;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;

    public class BoundedDataAccess<R> : ICounting
    {
        public Record<R> Record { get; private set; }
        public IQueryableFactory<R> QueryableFactory { get; private set; }
        private ExpressionProvider<R> ExpressionProvider { get; set; }

        public BoundedDataAccess(Record<R> record, IQueryableFactory<R> queryableFactory, ExpressionProvider<R> expressionProvider)
        {
#if DEBUG
            if (record == null) throw new ArgumentNullException(nameof(record));
            if (queryableFactory == null) throw new ArgumentNullException(nameof(queryableFactory));
            if (expressionProvider == null) throw new ArgumentNullException(nameof(expressionProvider));
#endif
            Record = record;
            QueryableFactory = queryableFactory;
            ExpressionProvider = expressionProvider;
        }

        public IQueryable<R> GetQueryable()
        {
            return QueryableFactory.Create();
        }

        public IQueryable<R> BoundQueryable(IQueryable<R> queryable, Partition partition)
        {
#if DEBUG
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (partition.Parts.Count == 0) throw new ArgumentException("partition contains no parts");
#endif
            return queryable.Where(ExpressionProvider.EnclosedIn(partition, Record, null));
        }

        public override string ToString()
        {
            return "[BDA: ...]";
        }

        public int CountWithin(ContiguousPartition contiguousPartition)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
#endif
            return BoundQueryable(GetQueryable(), new Partition(new HashSet<ContiguousPartition>() { contiguousPartition })).Count();
        }

        public int CountAll()
        {
            return CountWithin(Record.Boundary);
        }
    }
}
﻿namespace UniTraX.Core.BoundedData
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;
    using UniTraX.Core.Utils;

    public class ExpressionProvider<R>
    {
        public Expression<Func<R, bool>> EnclosedIn(Partition partition, Record<R> record, ParameterExpression parameter)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (record == null) throw new ArgumentNullException(nameof(record));
#endif
            ParameterExpression currentParameter = parameter;
            if (currentParameter == null) currentParameter = Expression.Parameter(typeof(R));
            return partition.Parts.Aggregate<ContiguousPartition, Expression<Func<R, bool>>>(null, (e, p) =>
            {
                if (e == null) return EnclosedIn(p, currentParameter, record);
                else return Expression.Lambda<Func<R, bool>>(Expression.OrElse(e.Body, EnclosedIn(p, currentParameter, record).Body), currentParameter);
            });
        }

        public Expression<Func<R, bool>> EnclosedIn(ContiguousPartition contiguousPartition, ParameterExpression parameter, Record<R> record)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (record == null) throw new ArgumentNullException(nameof(record));
            if (contiguousPartition.Intervals.Length != record.Fields.Length) throw new ArgumentException("intervals do not match");
#endif
            Expression expression = null;
            ParameterExpression currentParameter = parameter;
            if (currentParameter == null) currentParameter = Expression.Parameter(typeof(R));
            for (var i = 0; i < contiguousPartition.Intervals.Length; i++)
            {
                var field = record.Fields[i];
                var range = contiguousPartition.Intervals[i];
                var accessVisitor = new ReplaceExpressionVisitor(field.Access.Parameters[0], currentParameter);
                var accessBody = accessVisitor.Visit(field.Access.Body);

                if (field.FieldType.Bounds.Low < range.Low)
                {
                    var exp = Expression.GreaterThanOrEqual(accessBody, Expression.Constant(field.FieldType.Convert(range.Low, false)));
                    if (expression == null) expression = exp;
                    else expression = Expression.AndAlso(expression, exp);
                }
                if (range.High < field.FieldType.Bounds.High)
                {
                    var exp = Expression.LessThanOrEqual(accessBody, Expression.Constant(field.FieldType.Convert(range.High, true)));
                    if (expression == null) expression = exp;
                    else expression = Expression.AndAlso(expression, exp);
                }
            }
            if (expression == null) expression = Expression.Constant(true);
            return Expression.Lambda<Func<R, bool>>(expression, currentParameter);
        }
    }
}
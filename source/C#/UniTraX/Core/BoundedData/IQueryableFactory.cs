﻿namespace UniTraX.Core.BoundedData
{
    using System.Linq;

    public interface IQueryableFactory<R>
    {
        IQueryable<R> Create();
    }
}
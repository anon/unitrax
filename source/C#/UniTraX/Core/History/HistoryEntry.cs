﻿namespace UniTraX.Core.History
{
    using System;
    using UniTraX.Core.Partition;

    public class HistoryEntry
    {
        public Partition Partition { get; private set; }
        public long Epsilon { get; private set; }

        public HistoryEntry(Partition partition, long epsilon)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
#endif
            Partition = partition;
            Epsilon = epsilon;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is HistoryEntry o)) return false;
            return Partition.Equals(o.Partition) && Epsilon == o.Epsilon;
        }

        public override int GetHashCode()
        {
            throw new NotSupportedException("cannot get perfect hash of partition");
        }
    }
}
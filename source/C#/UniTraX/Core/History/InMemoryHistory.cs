﻿namespace UniTraX.Core.History
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;

    public class InMemoryHistory : IHistory
    {
        public int BudgetIndex { get; private set; }
        public int Count { get; private set; }
        private SortedDictionary<long, Partition> History;
        private SortedDictionary<long, Partition> WasExpanded;
        private SortedDictionary<long, Partition> NeedsCleaning;

        public InMemoryHistory(int budgetIndex)
        {
            BudgetIndex = budgetIndex;
            Count = 0;
            History = new SortedDictionary<long, Partition>();
            WasExpanded = new SortedDictionary<long, Partition>();
            NeedsCleaning = new SortedDictionary<long, Partition>();
        }

        public void Add(HistoryEntry historyEntry)
        {
#if DEBUG
            if (historyEntry == null) throw new ArgumentNullException(nameof(historyEntry));
#endif
            var contains = History.TryGetValue(historyEntry.Epsilon, out Partition partition);
            var previousPartitionSize = contains ? partition.Count() : 0;
            var newPartition = contains ? partition.UnionWith(historyEntry.Partition) : historyEntry.Partition;
            var newPartitionSize = newPartition.Count();
            History[historyEntry.Epsilon] = newPartition;
            WasExpanded[historyEntry.Epsilon] = newPartition;
            Count = Count + newPartitionSize - previousPartitionSize;
        }

        public void Clean()
        {
            foreach (var kv in WasExpanded)
            {
                var preCount = kv.Value.Count();
                kv.Value.Clean();
                var postCount = kv.Value.Count();
                Count += postCount - preCount;
            }
            var wasExpanded = WasExpanded.ToArray();
            WasExpanded.Clear();
            foreach (var kv in wasExpanded)
            {
                var epsilon = kv.Key;
                var partition = kv.Value;
                var keysSmaller = History.Keys.ToList().Where(k => k < epsilon);
                foreach (var k in keysSmaller)
                {
                    var other = History[k];
                    var preCount = other.Count();
                    if (partition.Encloses(other))
                    {
                        History.Remove(k);
                        if (NeedsCleaning.ContainsKey(k)) NeedsCleaning.Remove(k);
                        Count -= preCount;
                        continue;
                    }
                    if (!partition.Intersects(other)) continue;
                    other = other.RemainderWithout(partition);
                    var postCount = other.Count();
                    if (postCount < 1)
                    {
                        History.Remove(k);
                        if (NeedsCleaning.ContainsKey(k)) NeedsCleaning.Remove(k);
                        Count -= preCount;
                        continue;
                    }
                    History[k] = other;
                    NeedsCleaning[k] = other;
                    Count += postCount - preCount;
                }
                NeedsCleaning[epsilon] = partition;
            }
            var needsCleaning = NeedsCleaning.ToArray();
            NeedsCleaning.Clear();
            foreach (var kv in needsCleaning)
            {
                var preCount = kv.Value.Count();
                kv.Value.Clean();
                var postCount = kv.Value.Count();
                Count += postCount - preCount;
            }
        }

        public OverlapInformation Overlapping(Partition partition)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
#endif
            var overlappings = new List<HistoryEntry>();
            var intersections = new List<Partition>();
            var maxEpsilon = -1L;
            Partition maxPartition = null;
            foreach (var kv in History)
            {
                if (!kv.Value.Intersects(partition)) continue;
                overlappings.Add(new HistoryEntry(kv.Value, kv.Key));
                if (kv.Key == maxEpsilon) throw new Exception("history has same epsilon key twice. should never happen.");
                var intersection = kv.Value.IntersectionWith(partition);
                intersections.Add(intersection);
                if (kv.Key < maxEpsilon) continue;
                maxEpsilon = kv.Key;
                maxPartition = intersection;
            }
            if (maxEpsilon < 0L) maxEpsilon = 0L;
            return new OverlapInformation(overlappings, intersections, maxEpsilon, maxPartition);
        }

        public void WriteDetailedBudgetUseToFile(string fileName, ICounting counting, IFieldType budgetType)
        {
#if DEBUG
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));
            if (counting == null) throw new ArgumentNullException(nameof(counting));
            if (budgetType == null) throw new ArgumentNullException(nameof(budgetType));
#endif
            Clean();
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName))
            {
                var totalCount = 0;
                foreach(KeyValuePair<long, Partition> kvp in History)
                {
                    var count = 0;
                    foreach (ContiguousPartition cp in kvp.Value.Parts) count += counting.CountWithin(cp);
                    file.WriteLine(budgetType.ConvertUnchecked(kvp.Key, true) + "," + count);
                    totalCount += count;
                }
                var dataCount = counting.CountAll();
                if (dataCount > totalCount) file.WriteLine(budgetType.ConvertUnchecked(0L, true) + "," + (dataCount - totalCount));
            }
        }

        public override string ToString()
        {
            var ret = History.Aggregate("[IMH: ...\n",
                (current, kv) => current + ("\t[Budget:" + kv.Key + "; #ContP:" + kv.Value.Count() + "]\n"));
            // + _recordSpec;
            ret += "]\n";
            return ret;
        }
    }
}
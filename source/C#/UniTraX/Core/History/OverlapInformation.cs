﻿namespace UniTraX.Core.History
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.Partition;

    public class OverlapInformation
    {
        public List<HistoryEntry> Overlappings { get; private set; }
        public List<Partition> Intersections { get; private set; }
        public long MaxEpsilon { get; private set; }
        public Partition MaxPartition { get; private set; }

        public OverlapInformation(List<HistoryEntry> overlappings, List<Partition> intersections, long maxEpsilon, Partition maxPartition)
        {
#if DEBUG
            if (overlappings == null) throw new ArgumentNullException(nameof(overlappings));
            if (intersections == null) throw new ArgumentNullException(nameof(intersections));
            if (overlappings.Count != intersections.Count) throw new ArgumentException("must be an intersection for every overlap and vice versa");
            if (maxPartition != null && maxPartition.Parts.Any())
            {
                foreach(HistoryEntry h in overlappings)
                {
                    if (h.Partition != null && h.Partition.Parts.Any())
                    {
                        if (h.Partition.Parts.First().Intervals.Length != maxPartition.Parts.First().Intervals.Length)
                            throw new ArgumentException("intervals do not match");
                    }
                }
            }
#endif
            Overlappings = overlappings;
            Intersections = intersections;
            MaxEpsilon = maxEpsilon;
            MaxPartition = maxPartition;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is OverlapInformation o)) return false;
            if (Overlappings.Any(overlap => !o.Overlappings.Contains(overlap))) return false;
            if (Intersections.Any(intersection => !o.Intersections.Contains(intersection))) return false;
            return MaxEpsilon == o.MaxEpsilon && MaxPartition.Equals(o.MaxPartition);
        }

        public override int GetHashCode()
        {
            throw new NotSupportedException("cannot get perfect hash of partition");
        }
    }
}
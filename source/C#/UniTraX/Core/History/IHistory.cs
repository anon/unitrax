﻿namespace UniTraX.Core.History
{
    using UniTraX.Core.Partition;
    using UniTraX.Core.Specification;

    public interface IHistory
    {
        int Count { get; }
        void Add(HistoryEntry historyEntry);
        void Clean();
        OverlapInformation Overlapping(Partition partition);
        void WriteDetailedBudgetUseToFile(string fileName, ICounting counting, IFieldType budgetType);
        string ToString();
    }
}
﻿namespace UniTraX.Core.History
{
    using UniTraX.Core.Partition;

    public interface ICounting
    {
        int CountWithin(ContiguousPartition contiguousPartition);
        int CountAll();
    }
}
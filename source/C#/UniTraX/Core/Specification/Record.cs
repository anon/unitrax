﻿namespace UniTraX.Core.Specification
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using UniTraX.Core.Partition;

    public class Record<R>
    {
        public static ContiguousPartition GenerateBoundary(Field<R>[] fields)
        {
#if DEBUG
            if (fields == null) throw new ArgumentNullException(nameof(fields));
            if (!fields.Any()) throw new ArgumentException("no fields specified");
#endif
            var intervals = new Interval[fields.Length];
            for (int i = 0; i < fields.Length; i++) intervals[i] = fields[i].FieldType.Bounds;
            return new ContiguousPartition(intervals);
        }

        private readonly int BudgetIndex;
        public Field<R>[] Fields { get; private set; }
        public ContiguousPartition Boundary { get; private set; }

        public Record(int budgetIndex, Field<R>[] fields)
        {
#if DEBUG
            if (fields == null) throw new ArgumentNullException(nameof(fields));
            if (!fields.Any()) throw new ArgumentException("no fields specified");
            if (budgetIndex < 0 || fields.Length - 1 < budgetIndex) throw new ArgumentException("budgetIndex is out of bounds");
#endif
            BudgetIndex = budgetIndex;
            Fields = fields;
            Boundary = GenerateBoundary(Fields);
        }

        public Record(Field<R>[] fields)
        {
#if DEBUG
            if (fields == null) throw new ArgumentNullException(nameof(fields));
            if (!fields.Any()) throw new ArgumentException("no fields specified");
#endif
            BudgetIndex = -1;
            Fields = fields;
            Boundary = GenerateBoundary(Fields);
        }

        public int GetBudgetIndex()
        {
            if (BudgetIndex < 0) throw new InvalidOperationException("no budget specified on this record");
            return BudgetIndex;
        }

        public Record<PT> Derive<PT>()
        {
            var newFields = new Field<PT>[Fields.Length];
            var ptParam = Expression.Parameter(typeof(PT), "x");
            for (int i = 0; i < Fields.Length; i++) newFields[i] = Fields[i].Derive<PT>(Expression.Lambda<Func<PT, double>>(Expression.Field(ptParam, Fields[i].Name), new ParameterExpression[] { ptParam }));
            if (BudgetIndex < 0) return new Record<PT>(newFields);
            return new Record<PT>(BudgetIndex, newFields);
        }

        public override string ToString()
        {
            var ret = "[R:";
            var first = true;
            foreach (var field in Fields)
            {
                if (!first) ret = ret + ";";
                ret = ret + field;
                first = false;
            }
            ret = ret + "]";
            return ret;
        }
    }
}
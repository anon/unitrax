﻿namespace UniTraX.Core.Specification
{
    using System;
    using UniTraX.Core.Partition;

    public interface IFieldType
    {
        Interval Bounds { get; }
        Func<long, bool, double> Convert { get; }
        Func<long, bool, double> ConvertUnchecked { get; }
    }
}
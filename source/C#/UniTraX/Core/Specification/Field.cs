﻿namespace UniTraX.Core.Specification
{
    using System;
    using System.Linq.Expressions;

    public class Field<R>
    {
        public int Index { get; private set; }
        public string Name { get; private set; }
        public IFieldType FieldType { get; private set; }
        public Expression<Func<R, double>> Access { get; private set; }

        public Field(int index, string name, IFieldType fieldType, Expression<Func<R, double>> access)
        {
#if DEBUG
            if (index < 0) throw new ArgumentException("index below 0");
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (fieldType == null) throw new ArgumentNullException(nameof(fieldType));
#endif
            Index = index;
            Name = name;
            FieldType = fieldType;
            Access = access;
        }

        public Field<PT> Derive<PT>(Expression<Func<PT, double>> access)
        {
            return new Field<PT>(Index, Name, FieldType, access);
        }

        public override string ToString()
        {
            return "[F:" + Index + ":" + Name + ";" + FieldType.Bounds.ToString() + "]";
        }
    }
}
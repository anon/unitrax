﻿namespace UniTraX.Core.Utils
{
    using System;
    using System.Linq;

    public class Hash
    {
        public static int GenerateHash()
        {
            return GenerateHash(null);
        }

        public static int GenerateHash(int[] hashes)
        {
            if (hashes == null || !hashes.Any()) return 17;
            return hashes.Aggregate(17, MergeHashes);
        }

        public static int MergeHashes(int hashA, int hashB)
        {
            unchecked
            {
                return hashA * 23 + hashB;
            }
        }
    }
}
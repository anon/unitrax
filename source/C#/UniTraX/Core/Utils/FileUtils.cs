﻿namespace UniTraX.Core.Utils
{
    using System;

    public class FileUtils
    {
        public static void WriteLineToFile(string line, string fileName)
        {
#if DEBUG
            if (line == null) throw new ArgumentNullException(nameof(line));
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));
#endif
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName))
            {
                file.WriteLine(line);
            }
        }
    }
}
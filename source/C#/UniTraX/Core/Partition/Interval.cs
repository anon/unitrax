﻿namespace UniTraX.Core.Partition
{
    using System;
    using UniTraX.Core.Utils;

    public class Interval
    {
        public long Low { get; private set; }
        public long High { get; private set; }
        public int HashCode { get; private set; }

#if DEBUG
        public int LowHash { get; private set; }
        public int HighHash { get; private set; }
#endif

        public Interval(long low, long high)
        {
#if DEBUG
            if (low > high) throw new ArgumentException("low is greater than high");
            LowHash = low.GetHashCode();
            HighHash = high.GetHashCode();
#endif
            Low = low;
            High = high;
            HashCode = GenerateHashCode();
        }

        public long Length()
        {
            return High - Low;
        }

        public long SmallerMiddle()
        {
            return Low + Length() / 2;
        }

        #region ContainsData
        public bool ContainsDataLessThan(long value)
        {
            return Low < value;
        }

        public bool ContainsDataLessEqual(long value)
        {
            return Low <= value;
        }

        public bool ContainsDataGreaterThan(long value)
        {
            return value < High;
        }

        public bool ContainsDataGreaterEqual(long value)
        {
            return value <= High;
        }
        #endregion

        #region Enclosure
        public bool Encloses(Interval other)
        {
            return Low <= other.Low && other.High <= High;
        }

        public bool Encloses(long value)
        {
            return Low <= value && value <= High;
        }
        #endregion

        #region Intersection
        public bool Intersects(Interval other)
        {
            return !(other.High < Low || High < other.Low);
        }

        public Interval IntersectionWith(Interval other)
        {
#if DEBUG
            if (!Intersects(other)) throw new ArgumentException("intervals do not intersect");
#endif
            var newLow = Low;
            var newHigh = High;
            if (newLow < other.Low) newLow = other.Low;
            if (other.High < newHigh) newHigh = other.High;
            return new Interval(newLow, newHigh);
        }
        #endregion

        #region Split & Remainder
        public Interval[] SplitAt(long value)
        {
#if DEBUG
            if (!Encloses(value)) throw new ArgumentException("value is outside the interval");
            if (value == High) throw new ArgumentException("value must be below high");
#endif
            return new[] { new Interval(Low, value), new Interval(value + 1, High) };
        }

        public bool HasSingleRemainderWithout(Interval other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (!Intersects(other)) throw new ArgumentException("intervals do not intersect");
            if (other.Encloses(this)) throw new ArgumentException("other encloses this; no remainder possible");
#endif
            return !Encloses(other) || Low == other.Low || other.High == High;
        }

        public long SplitPointForLargestRemainderWithout(Interval other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (!Intersects(other)) throw new ArgumentException("intervals do not intersect");
            if (other.Encloses(this)) throw new ArgumentException("other encloses this; no remainder possible");
#endif
            if (Low < other.Low)
            {
                if (other.High < High) return other.Low - 1 - Low > High - other.High - 1 ? other.Low - 1 : other.High;
                return other.Low - 1;
            }
            return other.High;
        }

        public double LengthForLargestRemainderWithout(Interval other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (!Intersects(other)) throw new ArgumentException("intervals do not intersect");
            if (other.Encloses(this)) throw new ArgumentException("other encloses this; no remainder possible");
#endif
            if (Low < other.Low)
            {
                if (other.High < High) return other.Low - 1 - Low > High - other.High - 1 ? other.Low - 1 - Low : High - other.High - 1;
                return other.Low - 1 - Low;
            }
            return High - other.High - 1;
        }
        #endregion

        #region Merge
        public bool IsMergeableWith(Interval other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
#endif
            if (Intersects(other)) return true;
            return other.High == Low - 1 || High + 1 == other.Low;
        }

        public Interval MergeWith(Interval other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (!IsMergeableWith(other)) throw new ArgumentException("intervals are not mergeable");
#endif
            var newLow = Low;
            var newHigh = High;
            if (other.Low < Low) newLow = other.Low;
            if (High < other.High) newHigh = other.High;
            return new Interval(newLow, newHigh);
        }
        #endregion

        public override bool Equals(object obj)
        {
            return obj is Interval o && Equals(o);
        }

        public bool Equals(Interval other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
#endif
            if (HashCode != other.HashCode) return false;
            return Low == other.Low && High == other.High;
        }

        private int GenerateHashCode()
        {
            return Hash.GenerateHash(new int[] { (Low + 4611686018427387776L).GetHashCode(), (High + 4611686018427387776L).GetHashCode() });
        }

        public override int GetHashCode()
        {
            return HashCode;
        }

        public override string ToString()
        {
            return "[I:" + Low + ";" + High + "]";
        }
    }
}
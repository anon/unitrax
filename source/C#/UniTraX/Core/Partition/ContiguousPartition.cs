﻿namespace UniTraX.Core.Partition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.Utils;

    public struct IndexedValue
    {
        public int Index { get; private set; }
        public long Value { get; private set; }

        public IndexedValue(int index, long value)
        {
            Index = index;
            Value = value;
        }
    }

    public class ContiguousPartitionComparer : Comparer<ContiguousPartition>
    {
        public override int Compare(ContiguousPartition x, ContiguousPartition y)
        {
#if DEBUG
            if (x == null) throw new ArgumentNullException(nameof(x));
            if (y == null) throw new ArgumentNullException(nameof(y));
            if (x.Intervals == null) throw new ArgumentNullException(nameof(x.Intervals));
            if (y.Intervals == null) throw new ArgumentNullException(nameof(y.Intervals));
            if (x.Intervals.Length != y.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            for (int i = 0; i < x.Intervals.Length; i++)
            {
                int c = x.Intervals[i].Low.CompareTo(y.Intervals[i].Low);
                if (c != 0) return c;
                c = x.Intervals[i].High.CompareTo(y.Intervals[i].High);
                if (c != 0) return c;
            }
            return 0;
        }
    }

    public class ContiguousPartition
    {
        public Interval[] Intervals { get; private set; }
        public int HashCode { get; private set; }
#if DEBUG
        public int[] HashCodes { get; private set; }
#endif


        public ContiguousPartition(Interval[] intervals)
        {
#if DEBUG
            if (intervals == null) throw new ArgumentNullException(nameof(intervals));
            if (!intervals.Any()) throw new ArgumentException("no intervals defined");
            HashCodes = new int[intervals.Length];
            for (int i = 0; i < intervals.Length; i++) HashCodes[i] = intervals[i].GetHashCode();
#endif
            Intervals = intervals;
            HashCode = GenerateHashCode();
        }

        private bool IsZeroSize()
        {
            return Intervals.All(f => !(f.Length() > 0));
        }

        #region ContainsData
        public bool ContainsDataLessThan(IndexedValue iVal)
        {
#if DEBUG
            if (iVal.Index < 0 || iVal.Index > Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            return Intervals[iVal.Index].ContainsDataLessThan(iVal.Value);
        }

        public bool ContainsDataLessEqual(IndexedValue iVal)
        {
#if DEBUG
            if (iVal.Index < 0 || iVal.Index > Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            return Intervals[iVal.Index].ContainsDataLessEqual(iVal.Value);
        }

        public bool ContainsDataGreaterThan(IndexedValue iVal)
        {
#if DEBUG
            if (iVal.Index < 0 || iVal.Index > Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            return Intervals[iVal.Index].ContainsDataGreaterThan(iVal.Value);
        }

        public bool ContainsDataGreaterEqual(IndexedValue iVal)
        {
#if DEBUG
            if (iVal.Index < 0 || iVal.Index > Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            return Intervals[iVal.Index].ContainsDataGreaterEqual(iVal.Value);
        }
        #endregion

        #region Enclosure
        public bool Encloses(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            return !Intervals.Where((t, i) => !t.Encloses(other.Intervals[i])).Any();
        }

        public bool Encloses(IndexedValue iVal)
        {
#if DEBUG
            if (iVal.Index < 0 || iVal.Index > Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            return Intervals[iVal.Index].Encloses(iVal.Value);
        }
        #endregion

        #region Intersection
        public bool Intersects(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            return !Intervals.Where((t, i) => !t.Intersects(other.Intervals[i])).Any();
        }

        public ContiguousPartition IntersectionWith(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(other)) throw new ArgumentException("partitions do not intersect");
#endif
            var newIntervals = new Interval[Intervals.Length];
            for (int i = 0; i < Intervals.Length; i++) newIntervals[i] = Intervals[i].IntersectionWith(other.Intervals[i]);
            return new ContiguousPartition(newIntervals);
        }
        #endregion

        #region Split & Remainder
        public ContiguousPartition[] Halve()
        {
            Interval longest = null;
            var index = -1;
            for (var i = 0; i < Intervals.Length; i++)
                if (longest == null || Intervals[i].Length() > longest.Length())
                {
                    longest = Intervals[i];
                    index = i;
                }
            if (longest != null) return SplitAt(new IndexedValue(index, longest.SmallerMiddle()));
            throw new InvalidOperationException("should never happen");
        }

        public ContiguousPartition[] Halve(ICollection<int> ignore)
        {
            Interval longest = null;
            var index = -1;
            for (var i = 0; i < Intervals.Length; i++)
                if (!ignore.Contains(i) && (longest == null || Intervals[i].Length() > longest.Length()))
                {
                    longest = Intervals[i];
                    index = i;
                }
            if (longest != null) return SplitAt(new IndexedValue(index, longest.SmallerMiddle()));
            throw new InvalidOperationException("should never happen");
        }

        public ContiguousPartition[] SplitAt(IndexedValue iVal)
        {
#if DEBUG
            if (iVal.Index < 0 || iVal.Index > Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
            if (!Encloses(iVal)) throw new ArgumentException("value not inside partition");
            if (IsZeroSize()) throw new InvalidOperationException("cannot split zero size partition");
#endif
            var intervalsLeft = new Interval[Intervals.Length];
            var intervalsRight = new Interval[Intervals.Length];
            Array.Copy(Intervals, intervalsLeft, Intervals.Length);
            Array.Copy(Intervals, intervalsRight, Intervals.Length);

            var intervalsSplit = Intervals[iVal.Index].SplitAt(iVal.Value);
            intervalsLeft[iVal.Index] = intervalsSplit[0];
            intervalsRight[iVal.Index] = intervalsSplit[1];

            return new[]
            {
                new ContiguousPartition(intervalsLeft),
                new ContiguousPartition(intervalsRight)
            };
        }

        public bool HasSingleRemainderWithout(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(other)) throw new ArgumentException("partitions do not intersect");
            if (other.Encloses(this)) throw new ArgumentException("other encloses this; no remainder possible");
#endif
            var countSingle = 0;
            for (int i = 0; i < Intervals.Length; i++)
            {
                if (Intervals[i].HasSingleRemainderWithout(other.Intervals[i]))
                {
                    countSingle++;
                    if (countSingle > 1) return false;
                } else if (!Intervals[i].Encloses(other.Intervals[i]) && other.Intervals[i].Encloses(Intervals[i])) return false;
            }
            return true;
        }

        public IndexedValue SplitPointForLargestRemainderWithout(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(other)) throw new ArgumentException("partitions do not intersect");
            if (other.Encloses(this)) throw new ArgumentException("other encloses this; no remainder possible");
#endif
            var maxLength = 0.0;
            var maxIndex = -1;
            for (var i = 0; i < Intervals.Length; i++)
            {
                if (other.Intervals[i].Encloses(Intervals[i]) || !(Intervals[i].LengthForLargestRemainderWithout(other.Intervals[i]) >= maxLength)) continue;
                maxLength = Intervals[i].LengthForLargestRemainderWithout(other.Intervals[i]);
                maxIndex = i;
            }

            return new IndexedValue(maxIndex, Intervals[maxIndex].SplitPointForLargestRemainderWithout(other.Intervals[maxIndex]));
        }
        #endregion

        #region Merge
        public bool IsMergeableWith(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            var countMergeable = 0;
            for (int i = 0; i < Intervals.Length; i++)
            {
                if (!Intervals[i].Equals(other.Intervals[i]))
                {
                    if (!Intervals[i].IsMergeableWith(other.Intervals[i])) return false;
                    countMergeable++;
                    if (countMergeable > 1) return false;
                }
            }
            return true;
        }

        public ContiguousPartition MergeWith(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (Intervals.Length != other.Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!IsMergeableWith(other)) throw new ArgumentException("partitions are not mergeable");
#endif
            if (Encloses(other)) return this;
            if (other.Encloses(this)) return other;
            var intervals = new Interval[Intervals.Length];
            Array.Copy(Intervals, intervals, Intervals.Length);
            for (var i = 0; i < Intervals.Length; i++)
            {
                if (Intervals[i].Equals(other.Intervals[i])) continue;
                intervals[i] = Intervals[i].MergeWith(other.Intervals[i]);
                break;
            }
            return new ContiguousPartition(intervals);
        }
        #endregion

        #region Standard functions
        public override bool Equals(object obj)
        {
            return obj is ContiguousPartition o && Equals(o);
        }

        public bool Equals(ContiguousPartition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
#endif
            if (HashCode != other.HashCode) return false;
            if (Intervals.Length != other.Intervals.Length) return false;
            for (int i = 0; i < Intervals.Length; i++) if (!Intervals[i].Equals(other.Intervals[i])) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return HashCode;
        }

        private int GenerateHashCode()
        {
            return Intervals.Aggregate(Hash.GenerateHash(), (current, i) => Hash.MergeHashes(current, i.GetHashCode()));
        }

        public override string ToString()
        {
            var ret = "[CP:";
            var first = true;
            foreach (var interval in Intervals)
            {
                if (!first) ret = ret + ";";
                ret = ret + "\n" + interval;
                first = false;
            }
            ret = ret + "\n]";
            return ret;
        }
        #endregion

        #region k-means

        public static IDictionary<Point, ISet<ContiguousPartition>> Kmeans(
            ContiguousPartition contiguousPartition, ISet<Point> centroids,
            IDictionary<int, Interval> ignoreAndReplace)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (centroids == null) throw new ArgumentNullException(nameof(centroids));
            if (!centroids.Any()) throw new ArgumentException("need at least one centroid");
#endif
            var todo = new List<ContiguousPartition> { contiguousPartition };
            return Kmeans(todo, centroids, null, ignoreAndReplace);
        }

        public static IDictionary<Point, ISet<ContiguousPartition>> Kmeans(
            IList<ContiguousPartition> todo, ISet<Point> centroids,
            IDictionary<Point, ISet<ContiguousPartition>> done,
            IDictionary<int, Interval> ignoreAndReplace)
        {
            while (true)
            {
                if (todo == null || !todo.Any())
                    return done;
#if DEBUG
                if (centroids == null) throw new ArgumentNullException(nameof(centroids));
                if (!centroids.Any()) throw new ArgumentException("need at least one centroid");
#endif
                if (done == null) done = new Dictionary<Point, ISet<ContiguousPartition>>();
                var next = todo.First();
                todo.RemoveAt(0);
                var closest = ClosestTo(next, centroids, ignoreAndReplace);
                if (closest != null)
                {
                    if (!done.ContainsKey(closest)) done.Add(closest, new HashSet<ContiguousPartition>());
                    done[closest].Add(next);
                }
                else
                {
                    var halves = next.Halve(ignoreAndReplace.Keys);
                    todo.Add(halves[0]);
                    todo.Add(halves[1]);
                }
            }
        }

        private static Point ClosestTo(ContiguousPartition contiguousPartition, ISet<Point> centroids, IDictionary<int, Interval> ignoreAndReplace)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (centroids == null) throw new ArgumentNullException(nameof(centroids));
            if (!centroids.Any()) throw new ArgumentException("need at least one centroid");
#endif
            var corners = CornersOf(contiguousPartition, ignoreAndReplace);
            Point closest = null;
            foreach (var c in corners)
            {
                var cl = Point.ClosestTo(c, centroids);
                if (closest == null) closest = cl;
                if (!closest.Equals(cl)) return null;
            }
            return closest;
        }

        private static ISet<Point> CornersOf(ContiguousPartition contiguousPartition, IDictionary<int, Interval> ignoreAndReplace)
        {
            var unconfigured = new List<Interval>();
            for (var i = 0; i < contiguousPartition.Intervals.Length; i++)
            {
                unconfigured.Add(ignoreAndReplace.ContainsKey(i)
                    ? ignoreAndReplace[i]
                    : contiguousPartition.Intervals[i]);
            }
            return CornersOf(null, unconfigured);
        }

        private static ISet<Point> CornersOf(IList<Interval> configured, IList<Interval> unconfigured)
        {
#if DEBUG
            if ((configured == null || !configured.Any()) && (unconfigured == null || !unconfigured.Any()))
                throw new ArgumentException("neither configured nor unconfigured present");
#endif
            if (unconfigured == null || !unconfigured.Any())
                return new HashSet<Point>() { new Point(configured.ToArray()) };
            var next = unconfigured.First();
            unconfigured.RemoveAt(0);
            var a = new List<Interval>();
            if (configured != null) a.AddRange(configured);
            a.Add(new Interval(next.Low, next.Low));
            var au = new List<Interval>();
            au.AddRange(unconfigured);
            var s = CornersOf(a, au);
            if (next.Length() == 0) return s;
            var b = new List<Interval>();
            if (configured != null) b.AddRange(configured);
            b.Add(new Interval(next.High, next.High));
            var bu = new List<Interval>();
            bu.AddRange(unconfigured);
            var s2 = CornersOf(b, bu);
            var ret = new HashSet<Point>();
            ret.UnionWith(s);
            ret.UnionWith(s2);
            return ret;
        }
        #endregion
    }
}
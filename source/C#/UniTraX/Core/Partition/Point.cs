﻿namespace UniTraX.Core.Partition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Point : ContiguousPartition
    {
        public Point(Interval[] intervals) : base(intervals)
        {
            if (intervals.Any(f => f.Length() > 0)) throw new ArgumentException("intervals must be of zero step size");
        }

        public static Point ClosestTo(Point a, ISet<Point> others)
        {
            if (a == null) throw new ArgumentNullException(nameof(a));
            if (others == null) throw new ArgumentNullException(nameof(others));
            if (!others.Any()) throw new ArgumentException("need at least one other point");
            Point closest = null;
            foreach (var o in others) if (closest == null || DistanceBetween(a, o) < DistanceBetween(a, closest)) closest = o;
            return closest;
        }

        public static double DistanceBetween(Point a, Point b)
        {
            if (a == null) throw new ArgumentNullException(nameof(a));
            if (b == null) throw new ArgumentNullException(nameof(b));
#if DEBUG
            if (a.Intervals.Length != b.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            var sum = 0.0;
            for (var i = 0; i < a.Intervals.Length; i++) sum += Math.Pow((double)a.Intervals[i].Low - b.Intervals[i].Low, 2.0);
            return Math.Sqrt(sum);
        }
    }
}
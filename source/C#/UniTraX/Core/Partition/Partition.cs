﻿namespace UniTraX.Core.Partition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.Utils;

    public class Partition
    {
        public ISet<ContiguousPartition> Parts { get; private set; }
        private ISet<ContiguousPartition> Changed { get; set; }

        public Partition(ISet<ContiguousPartition> parts)
        {
#if DEBUG
            if (parts == null) throw new ArgumentNullException(nameof(parts));
            if (parts.Any(part => part.Intervals.Length != parts.First().Intervals.Length))
                throw new ArgumentException("intervals of parts do not match");
#endif
            Parts = parts;
            Changed = new HashSet<ContiguousPartition>();
            Changed.UnionWith(Parts);
        }

        public int Count()
        {
            return Parts.Count;
        }

        #region Enclosure
        public bool Encloses(Partition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Parts.Any() && Parts.First().Intervals.Length != other.Parts.First().Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            return other.Parts.All(Encloses);
        }

        private bool Encloses(ContiguousPartition contiguousPartition)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (Parts.First().Intervals.Length != contiguousPartition.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            return Encloses(new List<ContiguousPartition> { contiguousPartition });
        }

        private bool Encloses(IList<ContiguousPartition> parts)
        {
#if DEBUG
            if (parts == null) throw new ArgumentNullException(nameof(parts));
#endif
            if (!parts.Any()) return true;
            while (true)
            {
                var last = parts.Last();
                parts.RemoveAt(parts.Count - 1);
                var intersects = new List<ContiguousPartition>();
                foreach (var p in Parts)
                {
                    if (p.Encloses(last)) return Encloses(parts);
                    if (p.Intersects(last)) intersects.Add(p);
                }
                if (intersects.Count < 2) return false;
                var splitPoint = last.SplitPointForLargestRemainderWithout(intersects[0]);
                var splits = last.SplitAt(splitPoint);
                parts.Add(splits[0]);
                parts.Add(splits[1]);
            }
        }
        #endregion

        #region Intersection
        public bool Intersects(Partition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Parts.Any() && Parts.First().Intervals.Length != other.Parts.First().Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            return other.Parts.Any(Intersects);
        }

        public bool Intersects(ContiguousPartition contiguousPartition)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (Parts.First().Intervals.Length != contiguousPartition.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            return Parts.Any(p => p.Intersects(contiguousPartition));
        }

        public Partition IntersectionWith(Partition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Parts.Any() && Parts.First().Intervals.Length != other.Parts.First().Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(other)) throw new ArgumentException("partitions do not intersect");
#endif
            var newParts = new HashSet<ContiguousPartition>();
            foreach (var p in Parts)
            {
                foreach (var op in other.Parts)
                {
                    if (p.Intersects(op)) newParts.Add(p.IntersectionWith(op));
                }
            }
            var newPartition = new Partition(newParts);
            return newPartition;
        }

        private Partition IntersectionWith(ContiguousPartition contiguousPartition)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (Parts.First().Intervals.Length != contiguousPartition.Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(contiguousPartition)) throw new ArgumentException("partitions do not intersect");
#endif
            var newParts = new HashSet<ContiguousPartition>();
            foreach (var p in Parts)
            {
                if (p.Intersects(contiguousPartition)) newParts.Add(p.IntersectionWith(contiguousPartition));
            }
            var newPartition = new Partition(newParts);
            return newPartition;
        }
        #endregion

        #region Union
        public Partition UnionWith(Partition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Parts.Any() && Parts.First().Intervals.Length != other.Parts.First().Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            var newParts = new HashSet<ContiguousPartition>(Parts);
            newParts.UnionWith(other.Parts);
            return new Partition(newParts);
        }

        private void Add(ContiguousPartition contiguousPartition)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (Parts.First().Intervals.Length != contiguousPartition.Intervals.Length) throw new ArgumentException("intervals do not match");
#endif
            if (Parts.Contains(contiguousPartition)) return;

            /** 
             * too expensive
            if (Encloses(contiguousPartition)) return;

            Partition toAdd = new Partition(new HashSet<ContiguousPartition>() { contiguousPartition });
            if (Intersects(toAdd)) toAdd = toAdd.RemainderWithout(this);
            if (toAdd.Count() == 0) return;
            foreach (ContiguousPartition part in toAdd.Parts)
            {
                Parts.Add(part);
                Changed.Add(part);
            }
            **/
            Parts.Add(contiguousPartition);
            Changed.Add(contiguousPartition);
        }

        public void Clean()
        {
            while (true)
            {
                if (Parts.Count < 2)
                {
                    Changed.Clear();
                    return;
                }
                var merged = false;
                var intersected = false;
                ContiguousPartition part = null;
                ContiguousPartition changed = null;
                HashSet<ContiguousPartition> toAdd = null;
                while (!merged && !intersected && Changed.Count > 0)
                {
                    changed = Changed.First();
                    if (!Changed.Remove(changed)) throw new Exception("shouldn't happen");
                    foreach (var p in Parts)
                    {
                        if (p.Equals(changed)) continue;
                        if (p.IsMergeableWith(changed))
                        {
                            part = p;
                            toAdd = new HashSet<ContiguousPartition>() { p.MergeWith(changed) };
                            merged = true;
                            break;
                        }
                        else if (p.Intersects(changed))
                        {
                            part = p;
                            var newParts = new HashSet<ContiguousPartition>();
                            var oldParts = new List<ContiguousPartition>();
                            oldParts.Add(changed);
                            RemainderOf(oldParts, newParts, p);
                            toAdd = newParts;
                            intersected = true;
                            break;
                        }
                    }
                }
                if (!merged && !intersected)
                {
                    if (Changed.Count > 0) throw new Exception("shouldn't happen");
                    return;
                }
                if (merged)
                {
                    Parts.Remove(part);
                    Changed.Remove(part);
                }
                Parts.Remove(changed);
                Parts.UnionWith(toAdd);
                Changed.UnionWith(toAdd);
            }
        }
        #endregion

        #region Split & Remainder
        public Partition RemainderWithout(Partition other)
        {
#if DEBUG
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Parts.Any() && Parts.First().Intervals.Length != other.Parts.First().Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(other)) throw new ArgumentException("partitions do not intersect");
            if (other.Encloses(this)) throw new ArgumentException("other encloses this partition; no remainder possible");
#endif
            var ret = this;
            foreach (var p in other.Parts) if (ret.Intersects(p)) ret = ret.RemainderWithout(p);
            return ret;
        }

        private Partition RemainderWithout(ContiguousPartition contiguousPartition)
        {
#if DEBUG
            if (contiguousPartition == null) throw new ArgumentNullException(nameof(contiguousPartition));
            if (Parts.First().Intervals.Length != contiguousPartition.Intervals.Length) throw new ArgumentException("intervals do not match");
            if (!Intersects(contiguousPartition)) throw new ArgumentException("partitions do not intersect");
            var enc = true;
            foreach (var p in Parts) if (!contiguousPartition.Encloses(p)) enc = false;
            if (enc) throw new ArgumentException("contiguousPartition encloses this partition; no remainder possible");
#endif
            var newParts = new HashSet<ContiguousPartition>();
            var oldParts = new List<ContiguousPartition>();
            oldParts.AddRange(Parts);
            RemainderOf(oldParts, newParts, contiguousPartition);
            return new Partition(newParts);
        }

        private void RemainderOf(List<ContiguousPartition> inParts, ISet<ContiguousPartition> outParts, ContiguousPartition withoutPartition)
        {
#if DEBUG
            if (inParts == null) throw new ArgumentNullException(nameof(inParts));
            if (outParts == null) throw new ArgumentNullException(nameof(outParts));
            if (withoutPartition == null) throw new ArgumentNullException(nameof(withoutPartition));
#endif
            while (true)
            {
                if (!inParts.Any()) return;
                var last = inParts.Last();
                inParts.RemoveAt(inParts.Count - 1);
                if (withoutPartition.Encloses(last)) continue;
                if (!last.Intersects(withoutPartition))
                {
                    outParts.Add(last);
                    continue;
                }
                var splitPoint = last.SplitPointForLargestRemainderWithout(withoutPartition);
                var splits = last.SplitAt(splitPoint);
                if (!splits[0].Intersects(withoutPartition))
                {
                    outParts.Add(splits[0]);
                    inParts.Add(splits[1]);
                    continue;
                }
                if (splits[1].Intersects(withoutPartition))
                    throw new InvalidOperationException("this should never happen");
                outParts.Add(splits[1]);
                inParts.Add(splits[0]);
            }
        }
        #endregion

        #region Standard functions
        public override bool Equals(object obj)
        {
            if (!(obj is Partition o)) return false;
            return Encloses(o) && o.Encloses(this);
        }

        // WARNING: One cannot get a perfect hash for a partition. Some partitions with different hashes can still be equal.
        public override int GetHashCode()
        {
            var hash = Hash.GenerateHash();
            return Parts.Aggregate(hash, (current, p) => Hash.MergeHashes(current, p.GetHashCode()));
        }

        public override string ToString()
        {
            var ret = "[P: ...\n"; // + Record;
            ret = Parts.Aggregate(ret, (current, p) => current + ";\n" + p);
            ret = ret + "\n]";
            return ret;
        }
        #endregion

        #region Projection
        public static Partition Equals(Partition partition, IndexedValue iVal, Interval bounds)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (bounds == null) throw new ArgumentNullException(nameof(bounds));
            if (!partition.Parts.Any()) throw new ArgumentException("partition is empty");
            if (iVal.Index > partition.Parts.First().Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            return GreaterEqual(LessEqual(partition, iVal), iVal, bounds);
        }

        public static Partition LessThan(Partition partition, IndexedValue iVal, Interval bounds)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (bounds == null) throw new ArgumentNullException(nameof(bounds));
            if (!partition.Parts.Any()) throw new ArgumentException("partition is empty");
            if (iVal.Index > partition.Parts.First().Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            var newParts = new HashSet<ContiguousPartition>();
            foreach (var p in partition.Parts)
            {
                if (!p.ContainsDataLessThan(iVal)) continue;
                var bound = bounds.Low < iVal.Value ? iVal.Value - 1 : iVal.Value;
                var newIVal = new IndexedValue(iVal.Index, bound);
                newParts.Add(!p.ContainsDataGreaterEqual(iVal) ? p : p.SplitAt(newIVal)[0]);
            }
            return new Partition(newParts);
        }

        public static Partition LessEqual(Partition partition, IndexedValue iVal)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (!partition.Parts.Any()) throw new ArgumentException("partition is empty");
            if (iVal.Index > partition.Parts.First().Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            var newParts = new HashSet<ContiguousPartition>();
            foreach (var p in partition.Parts)
            {
                if (!p.ContainsDataLessEqual(iVal)) continue;
                newParts.Add(!p.ContainsDataGreaterThan(iVal) ? p : p.SplitAt(iVal)[0]);
            }
            return new Partition(newParts);
        }

        public static Partition GreaterThan(Partition partition, IndexedValue iVal)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (!partition.Parts.Any()) throw new ArgumentException("partition is empty");
            if (iVal.Index > partition.Parts.First().Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            var newParts = new HashSet<ContiguousPartition>();
            foreach (var p in partition.Parts)
            {
                if (!p.ContainsDataGreaterThan(iVal)) continue;
                newParts.Add(!p.ContainsDataLessEqual(iVal) ? p : p.SplitAt(iVal)[1]);
            }
            return new Partition(newParts);
        }

        public static Partition GreaterEqual(Partition partition, IndexedValue iVal, Interval bounds)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (bounds == null) throw new ArgumentNullException(nameof(bounds));
            if (!partition.Parts.Any()) throw new ArgumentException("partition is empty");
            if (iVal.Index > partition.Parts.First().Intervals.Length - 1) throw new IndexOutOfRangeException("index out of range");
#endif
            var newParts = new SortedSet<ContiguousPartition>(new ContiguousPartitionComparer());
            foreach (var p in partition.Parts)
            {
                if (!p.ContainsDataGreaterEqual(iVal)) continue;
                var bound = bounds.Low < iVal.Value ? iVal.Value - 1 : iVal.Value;
                var newIVal = new IndexedValue(iVal.Index, bound);
                newParts.Add(!p.ContainsDataLessThan(iVal) ? p : p.SplitAt(newIVal)[1]);
            }
            return new Partition(newParts);
        }
        #endregion

        #region k-means

        public static IDictionary<Point, Partition> Kmeans(Partition partition, ISet<Point> centroids, IDictionary<int, Interval> ignoreAndReplace)
        {
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (centroids == null) throw new ArgumentNullException(nameof(centroids));
            if (!centroids.Any()) throw new ArgumentException("need at least one centroid");
            var todo = new List<ContiguousPartition>();
            todo.AddRange(partition.Parts);
            var split = ContiguousPartition.Kmeans(todo, centroids, null, ignoreAndReplace);
            var d = split.ToDictionary(s => s.Key, s => new Partition(s.Value));
            return d;
        }
        #endregion
    }
}
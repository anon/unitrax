﻿namespace UniTraX.Core.Bookkeeper
{
    using UniTraX.Core.History;
    using UniTraX.Core.Partition;

    public class BookkeeperState
    {
        public Partition Focus;
        public Partition Forbidden;
        public IHistory History;
        public int NumberPartitionsAfterCleanCount;
    }
}
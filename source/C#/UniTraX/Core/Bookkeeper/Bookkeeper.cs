﻿namespace UniTraX.Core.Bookkeeper
{
    using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.BoundedData;
    using UniTraX.Core.History;
    using UniTraX.Core.Partition;

    public class Bookkeeper<R>
    {
        private BookkeeperState State;
        private BoundedDataAccess<R> BoundedDataAccess;

        public Bookkeeper(BookkeeperState state, BoundedDataAccess<R> boundedDataAccess)
        {
#if DEBUG
            if (state == null) throw new ArgumentNullException(nameof(state));
            if (boundedDataAccess == null) throw new ArgumentNullException(nameof(boundedDataAccess));
#endif
            State = state;
            BoundedDataAccess = boundedDataAccess;
        }

        public Bookkeeper(Partition focus, BoundedDataAccess<R> boundedDataAccess, IHistory history)
        {
#if DEBUG
            if (focus == null) throw new ArgumentNullException(nameof(focus));
            if (boundedDataAccess == null) throw new ArgumentNullException(nameof(boundedDataAccess));
            if (history == null) throw new ArgumentNullException(nameof(history));
#endif
            State = new BookkeeperState
            {
                Forbidden = new Partition(new HashSet<ContiguousPartition>()),
                Focus = focus ?? throw new ArgumentNullException(nameof(focus)),
                History = history ?? throw new ArgumentNullException(nameof(history)),
                NumberPartitionsAfterCleanCount = 0
            };
            BoundedDataAccess = boundedDataAccess ?? throw new ArgumentNullException(nameof(boundedDataAccess));
        }

        public Bookkeeper(ContiguousPartition focus, BoundedDataAccess<R> boundedDataAccess, IHistory history) :
            this(new Partition(new HashSet<ContiguousPartition>() { focus }), boundedDataAccess, history) { }

        private Partition GetFocus()
        {
            return State.Focus;
        }

        private void SetFocus(Partition focus)
        {
            if (focus == null) throw new ArgumentNullException(nameof(focus));
#if DEBUG
            if (focus.Parts.Any() && BoundedDataAccess.Record.Fields.Length != focus.Parts.First().Intervals.Length)
                throw new ArgumentException("intervals do not match");
#endif
            if (focus.Intersects(State.Forbidden)) throw new ArgumentException("focus cannot be expanded into previous focus space");
            var boundary = new Partition(new HashSet<ContiguousPartition>() { BoundedDataAccess.Record.Boundary });
            if (!boundary.Encloses(focus)) throw new ArgumentException("focus cannot be expanded into space that does not exist in the database");
            if (State.Focus != null && !focus.Encloses(State.Focus)) State.Forbidden = State.Forbidden.UnionWith(focus.Intersects(State.Focus) ? State.Focus.RemainderWithout(focus) : State.Focus);
            State.Focus = focus;

        }

        public int NumberPartitions()
        {
            return State.History.Count;
        }

        public int NumberPartitionsAfterClean()
        {
            return State.NumberPartitionsAfterCleanCount;
        }

        public int BudgetIndex() => BoundedDataAccess.Record.GetBudgetIndex();

        private bool BudgetGreaterEquals(Partition partition, long minBudget)
        {
#if DEBUG
            if (partition == null) throw new ArgumentNullException(nameof(partition));
#endif
            if (minBudget < 0) throw new ArgumentException("minBudget is negative");
            if (minBudget == 0) return true;
            var b = minBudget - 1;
            var newIntervals = new Interval[BoundedDataAccess.Record.Fields.Length];
            for (var i = 0; i < newIntervals.Length; i++) newIntervals[i] = i != BoundedDataAccess.Record.GetBudgetIndex() ?
                    new Interval(long.MinValue,long.MaxValue) : new Interval(0L, b);
            return !partition.Intersects(new ContiguousPartition(newIntervals));
        }

        public PINQueryable<R> CreatePinQueryable(IQueryable<R> queryable, Partition partition, long epsilon)
        {
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (partition.Count() == 0) throw new ArgumentException("partition contains no parts");
            if (!GetFocus().Encloses(partition)) throw new ArgumentException("partition is not fully in focus");
            if (!BudgetGreaterEquals(partition, epsilon)) throw new ArgumentException("not enough budget");
            var intersectionToBudget = new Dictionary<Partition, long>();
            var overlapInformation = State.History.Overlapping(partition);
            for (int i = 0; i < overlapInformation.Overlappings.Count; i++)
            {
                var intersection = overlapInformation.Intersections[i];
                var budget = overlapInformation.Overlappings[i].Epsilon + epsilon;
                if (!BudgetGreaterEquals(intersection, budget)) throw new ArgumentException("not enough budget");
                if (intersectionToBudget.ContainsKey(intersection) && intersectionToBudget[intersection] > budget) budget = intersectionToBudget[intersection];
                intersectionToBudget[intersection] = budget;
            }
            State.History.Add(new HistoryEntry(partition, epsilon));
            foreach (var kv in intersectionToBudget) State.History.Add(new HistoryEntry(kv.Key, kv.Value));
            return new PINQueryable<R>(BoundedDataAccess.BoundQueryable(queryable, partition),
                new PINQAgentBudget(BoundedDataAccess.Record.Fields[BoundedDataAccess.Record.GetBudgetIndex()].FieldType.Convert(epsilon, true)));
        }

        public EpsilonInformation MaxUsage(Partition partition)
        {
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            var o = State.History.Overlapping(partition);
            return !o.Overlappings.Any() ? new EpsilonInformation(null, 0L) : new EpsilonInformation(o.MaxPartition, o.MaxEpsilon);
        }

        public void CleanHistory()
        {
            State.History.Clean();
            State.NumberPartitionsAfterCleanCount = State.History.Count;
        }

        public IQueryable<R> Materialize(IQueryable<R> queryable, Partition partition)
        {
            if (queryable == null) throw new ArgumentNullException(nameof(queryable));
            if (partition == null) throw new ArgumentNullException(nameof(partition));
            if (partition.Count() == 0) throw new ArgumentException("partition contains no parts");
            return BoundedDataAccess.BoundQueryable(queryable, partition).ToArray().AsQueryable();
        }

        public void WriteDetailedBudgetUseToFile(string fileName)
        {
#if DEBUG
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));
#endif
            State.History.WriteDetailedBudgetUseToFile(fileName, BoundedDataAccess, BoundedDataAccess.Record.Fields[BoundedDataAccess.Record.GetBudgetIndex()].FieldType);
        }

        public void ExtendFocus(Partition extension)
        {
            if (extension == null) throw new ArgumentNullException(nameof(extension));
            SetFocus(GetFocus().UnionWith(extension));
            GetFocus().Clean();
        }

        public override string ToString()
        {
            var ret = "[BK: ...\n";
            ret += State.History.ToString();
            ret += "]\n";
            return ret;
        }
    }
}
﻿namespace UniTraX.Core.Bookkeeper
{
    using System;
    using UniTraX.Core.Partition;

    public class EpsilonInformation
    {
        public Partition Partition { get; private set; }
        public long Epsilon { get; private set; }

        public EpsilonInformation(Partition partition, long epsiplon)
        {
            // WARNING: Partition can be null if there is no partition
            Partition = partition;
            Epsilon = epsiplon;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EpsilonInformation o)) return false;
            if (!Partition.Equals(o.Partition)) return false;
            return Epsilon == o.Epsilon;
        }

        public override int GetHashCode()
        {
            throw new NotSupportedException("cannot get perfect hash of partition");
        }
    }
}
﻿namespace UniTraX.Core.Bookkeeper
{
    using PINQ;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UniTraX.Core.Partition;

    public class BKQueryable<R>
    {
        public IQueryable<R> Queryable { get; private set; }
        public Bookkeeper<R> Bookkeeper { get; private set; }
        public Partition Current { get; private set; }

        public BKQueryable(IQueryable<R> queryable, Bookkeeper<R> bookkeeper, Partition current)
        {
            Queryable = queryable ?? throw new ArgumentNullException(nameof(queryable));
            Bookkeeper = bookkeeper ?? throw new ArgumentNullException(nameof(bookkeeper));
            Current = current ?? throw new ArgumentNullException(nameof(current));
        }

        public BKQueryable(IQueryable<R> queryable, Bookkeeper<R> bookkeeper, ContiguousPartition current) :
            this(queryable, bookkeeper, new Partition(new HashSet<ContiguousPartition>() { current })) { }

        public long MaxUsage()
        {
            return Bookkeeper.MaxUsage(Current).Epsilon;
        }

        public void WriteDetailedBudgetUseToFile(string fileName)
        {
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));
            Bookkeeper.WriteDetailedBudgetUseToFile(fileName);
        }

        public int BudgetIndex() => Bookkeeper.BudgetIndex();

        public Boolean CoversDataspace()
        {
            return Current.Parts.Count > 0;
        }

        public PINQueryable<R> AsPinq(long epsilon)
        {
            return Bookkeeper.CreatePinQueryable(Queryable, Current, epsilon);
        }

        public void CleanHistory() => Bookkeeper.CleanHistory();

        public BKQueryable<R> Materialize()
        {
            return new BKQueryable<R>(Bookkeeper.Materialize(Queryable, Current), Bookkeeper, Current);
        }

        public BKQueryable<R> Apply(Func<Partition, Partition> function)
        {
            if (function == null) throw new ArgumentNullException(nameof(function));
            return new BKQueryable<R>(Queryable, Bookkeeper, function(Current));
        }

        public BKQueryable<R> Except(BKQueryable<R> other)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Bookkeeper != Bookkeeper) throw new ArgumentException("bookkeepers do not match");
            return new BKQueryable<R>(Queryable, Bookkeeper, Current.RemainderWithout(other.Current));
        }

        public BKQueryable<R> Intersect(BKQueryable<R> other)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Bookkeeper != Bookkeeper) throw new ArgumentException("bookkeepers do not match");
            return new BKQueryable<R>(Queryable, Bookkeeper, Current.IntersectionWith(other.Current));
        }

        public BKQueryable<R> Union(BKQueryable<R> other)
        {
            if (other == null) throw new ArgumentNullException(nameof(other));
            if (other.Bookkeeper != Bookkeeper) throw new ArgumentException("bookkeepers do not match");
            return new BKQueryable<R>(Queryable, Bookkeeper, Current.UnionWith(other.Current));
        }

        public override string ToString()
        {
            return "BKQ:\n" + Bookkeeper + "\n" + Current;
        }
    }
}